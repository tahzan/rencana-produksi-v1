$(document).ready(function() {
    var bloodhound = new Bloodhound({
       datumTokenizer: Bloodhound.tokenizers.whitespace,
       queryTokenizer: Bloodhound.tokenizers.whitespace,
       remote: {
             url: '/mesins/search?q=%QUERY%',
             wildcard: '%QUERY%'
       },
    });
    var input = $('#search_mesin');
    var result = '';

    input.typeahead({
       hint: true,
       highlight: true,
       minLength: 0
    }, {
       name: 'users',
       source: bloodhound,
       display: function(data3) {
             result = data3;
             return data3.name  //Input value to be set when you select a suggestion. 
       },
       templates: {
             empty: [
                '<div class="list-group search-results-dropdown"><div class="list-group-item">Nothing found.</div></div>'
             ],
             header: [
                '<div class="list-group search-results-dropdown">'
             ],
             suggestion: function(data3) {
                return '<div style="font-weight:normal; margin-top:-10px ! important;" class="list-group-item">' + data3.name + '</div></div>'
             }
       }
    });
    input.on('typeahead:autocompleted typeahead:selected', function($e, data3) {
       $('input[class=programid]').val(data3.user_wp_id);
     });
 });
 
 
 
//  tinymce.init({
//        selector: "textarea",
//        theme: "modern",
//        paste_data_images: true,
//        forced_root_block : false,
//        plugins: [
//          "advlist autolink lists link image charmap print preview hr anchor pagebreak",
//          "searchreplace wordcount visualblocks visualchars code fullscreen",
//          "insertdatetime media nonbreaking save table contextmenu directionality",
//          "emoticons template paste textcolor colorpicker textpattern",
//          "image code",        
//        ],
//        toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
//        toolbar2: "print preview media | forecolor backcolor emoticons | sizeselect | bold italic |   fontsizeselect",
//        image_title: true, 
//        automatic_uploads: true,
//        file_picker_types: 'image', 
//        file_picker_callback: function(cb, value, meta) {
//        var input = document.createElement('input');
//        input.setAttribute('type', 'file');
//        input.setAttribute('accept', 'image/*');
 
//        input.onchange = function() {
//              var file = this.files[0];
             
//              var reader = new FileReader();
//              reader.onload = function () {
//              var id = 'blobid' + (new Date()).getTime();
//              var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
//              var base64 = reader.result.split(',')[1];
//              var blobInfo = blobCache.create(id, file, base64);
//              blobCache.add(blobInfo);
 
//              cb(blobInfo.blobUri(), { title: file.name });
//              };
//              reader.readAsDataURL(file);
//        };
       
//        input.click();
//        }
//  });
 
 // $('ul[class=alert-danger]').ready(function() {
 //       $('input[id=search_user_participants]').val('');
 //       $('input[id=search_program]').val('');
 // })
//  $(document).ready(function() {
//        $('.js-example-basic-multiple').select2();
//   });