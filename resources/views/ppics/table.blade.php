<div class="table-responsive">
    <table class="table" id="ppics-table">
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>Type Id</th>
                <th>Outstanding Stok</th>
                <th>Status</th>
                <th>Selesai Produksi</th>
                <th>Keterangan</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($ppics as $ppic)
            <tr>
                <td>{{ $ppic->tanggal }}</td>
                <td>{{ $ppic->type_id }}</td>
                <td>{{ $ppic->outstanding_stok }}</td>
                <td>{{ $ppic->status }}</td>
                <td>{{ $ppic->tanggal_produksi }}</td>
                <td>{{ $ppic->keterangan }}</td>
                <td>
                    {!! Form::open(['route' => ['ppics.destroy', $ppic->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('ppics.show', [$ppic->id]) }}" class='btn btn-info btn-xs table-button'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('ppics.edit', [$ppic->id]) }}" class='btn btn-info btn-xs table-button'><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        {!! Form::button('<i class="fa fa-times" aria-hidden="true"></i>', ['type' => 'submit', 'class' => 'btn btn-warning btn-xs table-button', 'onclick' => "return confirm('Apakah anda yakin?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
