@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Ppic
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ppic, ['route' => ['ppics.update', $ppic->id], 'method' => 'patch']) !!}

                        @include('ppics.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection