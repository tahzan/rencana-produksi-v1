<div class="form-group">
    {!! Form::label('tanggal', 'No Transaksi:') !!}
    <p>{{ $ppic->no_transaksi }}</p>
</div>


<!-- Tanggal Field -->
<div class="form-group">
    {!! Form::label('tanggal', 'Tanggal:') !!}
    <p>{{ $ppic->tanggal->format('d/m/Y') }}</p>
</div>

<!-- Type Id Field -->
<div class="form-group">
    {!! Form::label('type_id', 'Type:') !!}
    <p>{{ $ppic->type->nama }}</p>
</div>

<!-- Outstanding Stok Field -->
<div class="form-group">
    {!! Form::label('outstanding_stok', 'Outstanding Stok:') !!}
    <p>{{ $ppic->outstanding_stok }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $ppic->status }}</p>
</div>

<!-- Tanggal Produksi Field -->
<div class="form-group">
    {!! Form::label('tanggal_produksi', 'Selesai Produksi:') !!}
    <p>{{ (isset($ppic->tanggal_produksi)) ? $ppic->tanggal_produksi->format('d/m/Y') : '-' }} </p>
</div>

<!-- Keterangan Field -->
<div class="form-group">
    {!! Form::label('keterangan', 'Keterangan:') !!}
    <p>{{ $ppic->keterangan }}</p>
</div>


