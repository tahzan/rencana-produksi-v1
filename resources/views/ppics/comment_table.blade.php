<div class="table-responsive">
    <table class="table" id="ppicComments-table">
        <thead>
            <tr>
                <th style="width: 15%">Nama</th>
                <th>Keterangan</th>
                <!-- <th colspan="3">Action</th> -->
            </tr>
        </thead>
        <tbody>
        @foreach($ppicComments as $ppicComment)
            <tr>
            <td>{{ $ppicComment->user ? $ppicComment->user->name : $ppicComment->user_id }}</td>
            
            <td>{{ $ppicComment->keterangan }}</td>
                <!-- <td>
                    {!! Form::open(['route' => ['ppicComments.destroy', $ppicComment->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('ppicComments.show', [$ppicComment->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('ppicComments.edit', [$ppicComment->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td> -->
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
