@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Ppic
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('ppics.show_fields')
                    <a href="{{ route('report-daily-stock.index') }}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>

        <div class="box box-primary">
            <h4 class="title-box">LOG DATA</h4>
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('ppics.logs_table')
                </div>
            </div>
        </div>

        <div class="box box-primary">
            <h4 class="title-box">DISKUSI</h4>
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('ppics.comment_table')
                </div>
            </div>
            <div class="form-group"  >
                <div class="modal-footer">
                    <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" 
                        href="{{ route('ppicComments.create') }}?ppic_id={{$ppic->id}}&ppic_status={{$ppic->status}}&ppic_sum={{$ppic->outstanding_stok}}&&ppic_typename={{ $ppic->type->nama }}">Buat Komentar</a>
                </div>
            </div>
        </div>
    </div>
@endsection
