
<div class="form-group col-sm-6">
    {!! Form::label('report_id', 'Report No:') !!}
    {!! Form::text('', (isset($ppic)) ? $ppic->no_transaksi : null, ['class' => 'form-control', 'id'=>'search_type', 'readonly' => 'true']) !!}
    {!! Form::hidden('report_id', $report_id, ['class' => 'form-control', 'id'=>'search_type', 'readonly' => 'true']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('type_id', 'Type Name:') !!}
    {!! Form::text('', $type_name, ['class' => 'form-control', 'id'=>'search_type', 'readonly' => 'true']) !!}

    {!! Form::hidden('type_id', $type_id, ['class' => 'form-control', 'id'=>'search_type', 'readonly' => 'true']) !!}
</div>

<!-- Outstanding Stok Field -->
<div class="form-group col-sm-6">
    {!! Form::label('outstanding_stok', 'Outstanding Stok:') !!}
    {!! Form::number('outstanding_stok', $outstanding, ['class' => 'form-control', 'readonly' => 'true']) !!}
</div>


<!-- Tanggal Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tanggal', 'Tanggal:') !!}
    {!! Form::date('tanggal', (isset($ppic)) ? $ppic->tanggal : null, ['class' => 'form-control','id'=>'tanggal']) !!}
</div>

<!-- Type Id Field -->
<!-- <div class="form-group col-sm-6">
{!! Form::label('type_id', 'ID Tipe & Nama Tipe:') !!}
    {!! Form::text('', $type_id, ['class' => 'form-control', 'id'=>'search_type',
            'readonly' => (isset($ppic) and $ppic->status == 'aktual') ? true : false]) !!}
    {!! Form::text('type_id', (isset($ppic)) ? $ppic->type_id : null, ['class' => 'form-control','id'=>'type_id', 'readonly' => 'true', 'placeholder' => 'ID']) !!}
    {!! Form::text('nama', (isset($ppic)) ? $ppic->nama : null, ['class' => 'form-control','id'=>'type_name', 'readonly' => 'true', 'placeholder' => 'Tipe']) !!}
</div> -->

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::select('status', ['Waiting Response' => 'Waiting Response', 
        'On Schedule' => 'On Schedule', 
        'delay' => 'Delay', 
        'failed' => 'Failed', 'closed' => 'Closed'], null, ['class' => 'form-control', 'id'=>'requiredSource']) !!}
</div>

<!-- Tanggal Produksi Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tanggal_produksi', 'Selesai Produksi:', ['class' => 'labeltanggal']) !!}
    {!! Form::date('tanggal_produksi',  (isset($ppic->tanggal_produksi)) ? $ppic->tanggal_produksi : null, ['class' => 'form-control ','id'=>'tanggal_produksi']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('notify_purchasing', 'Notify Purchasing:') !!}
    {!! Form::select('notify_purchasing', [
        'y' => 'Yes', 
        'n' => 'No', 
    ], null, ['class' => 'form-control']) !!}
</div>


<!-- Keterangan Field -->
<div class="form-group col-sm-12">
    {!! Form::label('keterangan', 'Keterangan / Alasan:',['class' => 'labelketerangan'] )  !!}
    {!! Form::textarea('keterangan', null, ['class' => 'form-control','id'=>'keterangan']) !!}
</div>




<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('report-daily-stock.index') }}" class="btn btn-default">Cancel</a>
</div>

@section('scripts')
    <script type="text/javascript">
        $('#requiredSource').change(function() {
            if($(this).val() == 'On Schedule')
            {
              $('#tanggal_produksi').attr('required', 'true');
              $('.labeltanggal').html("Tanggal Produksi: **");
              $('.labeltanggal').css('color', 'red');
            }
            else if($(this).val() == 'failed')
            {
              $('#keterangan').attr('required', 'true');
              $('.labelketerangan').html("Keterangan: **");
              $('.labelketerangan').css('color', 'red');
            }
            else
            {
              $('#tanggal_produksi').removeAttr('required');
              $('.labeltanggal').html("Tanggal Produksi:");
              $('.labeltanggal').css('color', 'black');
            }
        });
        
    </script>
@endsection


