@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Jadwal Mesin
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'jadwal-mesin.store']) !!}

                        @include('jadwal_mesins.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
