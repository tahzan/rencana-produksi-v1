<!-- Mulai Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mulai', 'Mulai:') !!}
    {!! Form::datetimelocal('mulai', (isset($jadwalMesin)) ? $jadwalMesin->mulai : null, ['class' => 'form-control','id'=>'mulai']) !!}
</div>

<!-- Selesai Field -->
<div class="form-group col-sm-6">
    {!! Form::label('selesai', 'Selesai:') !!}
    {!! Form::datetimelocal('selesai', (isset($jadwalMesin)) ? $jadwalMesin->selesai : null, ['class' => 'form-control','id'=>'selesai']) !!}
</div>

<!-- Mesin Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mesin_id', 'Mesin:') !!}
    {!! Form::text('null', (isset($jadwalMesin))  ? $jadwalMesin->mesin->nama : null, ['class' => 'form-control', 'id'=>'search_mesin']) !!}
    {!! Form::hidden('mesin_id', (isset($jadwalMesin)) ? $jadwalMesin->mesin_id : null, ['class' => 'form-control','id'=>'mesin_id']) !!}
</div>

<!-- Tipe Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tipe', 'Tipe:') !!}
    {!! Form::select('tipe', ['istirahat' => 'istirahat'], null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('jadwal-mesin.index') }}" class="btn btn-info">Cancel</a>
</div>

@section('scripts')
    <script type="text/javascript">

        $('#mulai, #selesai').datetimepicker({
            format: 'YYYY-MM-DDTHH:mm',
            useCurrent: true,
            locale: 'id'
        })

        var path = "{{ url('/mesin/cari') }}";
        $input = $('#search_mesin');
        $input.typeahead({
            minLength: 0,
            items: 10,
            source:  function (query, process) {
                return $.get(path, { q: query }, function (data) {
                    return process(data);
                });
            },
            autoSelect: true,
            displayText: function(item){ return `${item.kode} - ${item.nama} `;}
        });
        $input.change(function() {
            var current = $input.typeahead("getActive");
            $('input[id=mesin_id]').val(current.id);
        });

    </script>
@endsection