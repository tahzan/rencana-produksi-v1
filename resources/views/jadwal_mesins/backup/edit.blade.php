@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Jadwal Mesin
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($jadwalMesin, ['route' => ['jadwal-mesin.update', $jadwalMesin->id], 'method' => 'patch']) !!}

                        @include('jadwal_mesins.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection