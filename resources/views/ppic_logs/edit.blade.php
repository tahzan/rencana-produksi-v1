@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Ppic Log
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ppicLog, ['route' => ['ppicLogs.update', $ppicLog->id], 'method' => 'patch']) !!}

                        @include('ppic_logs.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection