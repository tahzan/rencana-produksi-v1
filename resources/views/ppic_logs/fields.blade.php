<!-- Ppic Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ppic_id', 'Ppic Id:') !!}
    {!! Form::number('ppic_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Tanggal Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tanggal', 'Tanggal:') !!}
    {!! Form::date('tanggal', null, ['class' => 'form-control','id'=>'tanggal']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#tanggal').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Type Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type_id', 'Type Id:') !!}
    {!! Form::number('type_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Outstanding Stok Field -->
<div class="form-group col-sm-6">
    {!! Form::label('outstanding_stok', 'Outstanding Stok:') !!}
    {!! Form::number('outstanding_stok', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::select('status', ['Waiting Response' => 'Waiting Response', 'On Schedule' => 'On Schedule', 'failed' => 'failed', 'closed' => 'closed'], null, ['class' => 'form-control']) !!}
</div>

<!-- Tanggal Produksi Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tanggal_produksi', 'Tanggal Produksi:') !!}
    {!! Form::date('tanggal_produksi', null, ['class' => 'form-control','id'=>'tanggal_produksi']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#tanggal_produksi').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Keterangan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('keterangan', 'Keterangan:') !!}
    {!! Form::text('keterangan', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('ppicLogs.index') }}" class="btn btn-default">Cancel</a>
</div>
