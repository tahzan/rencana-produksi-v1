<!-- Ppic Id Field -->
<div class="form-group">
    {!! Form::label('ppic_id', 'Ppic Id:') !!}
    <p>{{ $ppicLog->ppic_id }}</p>
</div>

<!-- Tanggal Field -->
<div class="form-group">
    {!! Form::label('tanggal', 'Tanggal:') !!}
    <p>{{ $ppicLog->tanggal }}</p>
</div>

<!-- Type Id Field -->
<div class="form-group">
    {!! Form::label('type_id', 'Type Id:') !!}
    <p>{{ $ppicLog->type_id }}</p>
</div>

<!-- Outstanding Stok Field -->
<div class="form-group">
    {!! Form::label('outstanding_stok', 'Outstanding Stok:') !!}
    <p>{{ $ppicLog->outstanding_stok }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $ppicLog->status }}</p>
</div>

<!-- Tanggal Produksi Field -->
<div class="form-group">
    {!! Form::label('tanggal_produksi', 'Tanggal Produksi:') !!}
    <p>{{ $ppicLog->tanggal_produksi }}</p>
</div>

<!-- Keterangan Field -->
<div class="form-group">
    {!! Form::label('keterangan', 'Keterangan:') !!}
    <p>{{ $ppicLog->keterangan }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $ppicLog->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $ppicLog->updated_at }}</p>
</div>

