<div class="table-responsive">
    <table class="table" id="ppicLogs-table">
        <thead>
            <tr>
                <th>Ppic Id</th>
                <th>Tanggal</th>
                <th>Type Id</th>
                <th>Outstanding Stok</th>
                <th>Status</th>
                <th>Tanggal Produksi</th>
                <th>Keterangan</th>
                <!-- <th colspan="3">Action</th> -->
        </tr>
        </thead>
        <tbody>
        @foreach($ppicLogs as $ppicLog)
            <tr>
                <td>{{ $ppicLog->ppic_id }}</td>
            <td>{{ $ppicLog->tanggal }}</td>
            <td>{{ $ppicLog->type_id }}</td>
            <td>{{ $ppicLog->outstanding_stok }}</td>
            <td>{{ $ppicLog->status }}</td>
            <td>{{ $ppicLog->tanggal_produksi }}</td>
            <td>{{ $ppicLog->keterangan }}</td>
                <!-- <td>
                    {!! Form::open(['route' => ['ppicLogs.destroy', $ppicLog->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('ppicLogs.show', [$ppicLog->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('ppicLogs.edit', [$ppicLog->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td> -->
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
