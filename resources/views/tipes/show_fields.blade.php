<!-- Nama Field -->
<div class="form-group">
    {!! Form::label('nama', 'Nama:') !!}
    <p>{{ $tipe->nama }}</p>
</div>

<!-- Jumlah Operator Field -->
<div class="form-group">
    {!! Form::label('jumlah_operator', 'Jumlah Operator:') !!}
    <p>{{ $tipe->jumlah_operator }}</p>
</div>

<!-- Cycle Time Field -->
<div class="form-group">
    {!! Form::label('cycle_time', 'Cycle Time:') !!}
    <p>{{ $tipe->cycle_time }}</p>
</div>

<!-- Durasi Dandon Field -->
<div class="form-group">
    {!! Form::label('durasi_dandon', 'Durasi Dandon:') !!}
    <p>{{ $tipe->durasi_dandon }}</p>
</div>

<!-- Toleransi Field -->
<div class="form-group">
    {!! Form::label('toleransi', 'Toleransi:') !!}
    <p>{{ $tipe->toleransi }}</p>
</div>

<!-- Durasi Produksi Field -->
<div class="form-group">
    {!! Form::label('durasi_produksi', 'Durasi Produksi:') !!}
    <p>{{ $tipe->durasi_produksi }}</p>
</div>

<!-- Kombinasi Field -->
<div class="form-group">
    {!! Form::label('kombinasi', 'Kombinasi:') !!}
    <p>{{ $tipe->kombinasi }}</p>
</div>

<!-- Barang Lain Field -->
<div class="form-group">
    {!! Form::label('barang_lain', 'Barang Lain:') !!}
    <p>{{ $tipe->barang_lain }}</p>
</div>

<!-- Barang Lain Field -->
<div class="form-group">
    {!! Form::label('berat', 'Berat(gr):') !!}
    <p>{{ $tipe->berat }}</p>
</div>

<!-- Type Pasangan Field -->
<div class="form-group">
    {!! Form::label('type_pasangan', 'Tipe Pasangan:') !!}
    <p>{{ $tipe->type_pasangan }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $tipe->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $tipe->updated_at }}</p>
</div>

