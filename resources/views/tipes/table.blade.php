<div class="table-responsive">
    <table class="table data" id="tipes-table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Tipe</th>
                <th>Jml Operator</th>
                <th>Cycle Time</th>
                <th>Durasi Dandon</th>
                <th>Toleransi %</th>
                <th>Durasi Produksi</th>
                <th>Barang Lain</th>
                <th ></th>
            </tr>
        </thead>
        <tbody>
        @foreach($tipes as $tipe)
            <tr>
            <td>{{ $tipe->id }}</td>
            <td>{{ $tipe->nama }}</td>
            <td>{{ $tipe->jumlah_operator }}</td>
            <td>{{ $tipe->cycle_time }}</td>
            <td>{{ $tipe->durasi_dandon }}</td>
            <td>{{ $tipe->toleransi }} %</td>
            <td>{{ $tipe->durasi_produksi }}</td>
            <td>{{ $tipe->barang_lain }}</td>
            <td>
                {!! Form::open(['route' => ['tipe.destroy', $tipe->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <!-- <a href="{{ route('tipe.show', [$tipe->id]) }}" class='btn btn-info btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a> -->
                    <a href="{{ route('tipe.edit', [$tipe->id]) }}" class='btn btn-info btn-xs table-button'><i class="fa fa-pencil" aria-hidden="true"></i></a>
                    {!! Form::button('<i class="fa fa-times" aria-hidden="true"></i>', ['type' => 'submit', 'class' => 'btn btn-warning btn-xs table-button', 'onclick' => "return confirm('Apakah anda yakin?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
