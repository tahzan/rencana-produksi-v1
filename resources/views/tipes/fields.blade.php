<!-- Nama Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama', 'Nama:') !!}
    {!! Form::text('nama', null, ['class' => 'form-control']) !!}
</div>

<!-- Cycle Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cycle_time', 'Cycle Time / Menit: ') !!}
    {!! Form::text('cycle_time', null, ['class' => 'form-control', 'id'=>'cycle_time', 'onchange' => 'hitungDurasiProduksi()']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('berat', 'Berat (gr): ') !!}
    {!! Form::text('berat', null, ['class' => 'form-control', 'id'=>'berat']) !!}
</div>


<!-- Jumlah Operator Field -->
<div class="form-group col-sm-6">
    {!! Form::label('jumlah_operator', 'Jumlah Operator:') !!}
    {!! Form::text('jumlah_operator', null, ['class' => 'form-control']) !!}
</div>

<!-- Toleransi Field -->
<div class="form-group col-sm-6">
    {!! Form::label('toleransi', 'Toleransi (%):') !!}
    {!! Form::number('toleransi', null, ['class' => 'form-control', 'id'=>'toleransi', 'onchange' => 'hitungDurasiProduksi()']) !!}
</div>

<!-- Kombinasi Field -->
<div class="form-group col-sm-6">
    {!! Form::label('kombinasi', 'Kombinasi Barang Lain:') !!}
    {!! Form::select('kombinasi', ['Ya' => 'Ya', 'Tidak' => 'Tidak'], null, ['class' => 'form-control']) !!}
</div>


<!-- Durasi Produksi Field -->
<div class="form-group col-sm-6">
    {!! Form::label('durasi_produksi', 'Durasi Produksi (detik):') !!}
    {!! Form::text('durasi_produksi', null, ['class' => 'form-control', 'readonly' => 'true', 'id'=>'durasi_produksi']) !!}
</div>

<!-- Type Pasangan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('barang_lain', 'Barang Lain:') !!}
    {!! Form::text('barang_lain', (isset($tipe)) ? $tipe->barang_lain : null, ['class' => 'form-control', 'id'=>'barang_lain']) !!}
    {!! Form::hidden('barang_lain_id', (isset($tipe)) ? $tipe->barang_lain_id : null, ['class' => 'form-control','id'=>'barang_lain_id']) !!}
</div>

<!-- Durasi Dandon Field -->
<div class="form-group col-sm-6">
    {!! Form::label('durasi_dandon', 'Durasi Dandon  (menit): ') !!}
    {!! Form::number('durasi_dandon', null, ['class' => 'form-control', 'id'=>'durasi_dandon']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('type_pasangan', 'Tipe Pasangan:') !!}
    {!! Form::text('type_pasangan', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('count', 'Count: ') !!}
    {!! Form::text('count', null, ['class' => 'form-control', 'id'=>'count']) !!}
</div>



<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('tipe.index') }}" class="btn btn-info">Cancel</a>
</div>

@section('scripts')
    <script type="text/javascript">
        hitungDurasiProduksi = () => {
            let cycleTime = parseInt($("#cycle_time").val());
            let toleransi = parseInt($("#toleransi").val());
            durasiProduksi = 60 + 60 * toleransi / 100;
            $("#durasi_produksi").val(durasiProduksi);
        }

        $("#cycle_time").inputmask('Regex', {regex: "^[0-9]{1,6}(\\.\\d{1,2})?$"});

        var path2 = "{{ url('/tipe/cari') }}";
        $input2 = $('#barang_lain');
        $input2.typeahead({
            minLength: 0,
            items: 10,
            source:  function (query, process) {
                return $.get(path2, { q: query }, function (data) {
                    return process(data);
                });
            },
            autoSelect: true,
            displayText: function(item){ return `${item.nama} `;}
        });

        $input2.change(function() {
            var current = $input2.typeahead("getActive");
            $('input[id=barang_lain_id]').val(current.id);
        });

    </script>
@endsection