@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Tipe</h1>
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{ route('tipe.create') }}">Tambah Data</a>
        </h1>
        <div class="pull-right">
            <button type="button" class="btn btn-primary mr-5" style="margin-top: -10px; margin-right: 10px" 
                data-toggle="modal" data-target="#importExcel">Import Excel</button>
        </div>
        {!! Form::open(['route' => ['tipe.hapusSemua'], 'method' => 'post']) !!}
        <div class='pull-right'>
            {!! Form::button('Hapus Semua', ['type' => 'submit', 'style'=>'margin-top: -10px; margin-right: 10px', 'class' => 'btn btn-warning mr-5', 'onclick' => "return confirm('Apakah anda yakin?')"]) !!}
        </div>
        {!! Form::close() !!}
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')
        @include('adminlte-templates::common.errors')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"></h3>
                <div class="box-tools">
                    <form action="" method="GET" class="search-apsc">
                        <div class="input-group input-group-sm hidden-xs" style="width: 300px;">
                            <input type="text" name="query" class="form-control pull-right" placeholder="Search">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-info"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
            <div class="box-body">
                @include('tipes.table')
            </div>
            <div class="box-footer clearfix">
                {{ $tipes->links() }}
            </div>
        </div>
        <div class="text-center">
        
        </div>
 
		<!-- Import Excel -->
		<div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
                {!! Form::open(['route' => 'tipe.import_excel', "enctype"=>"multipart/form-data"]) !!}
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
						</div>
						<div class="modal-body">
 
							{{ csrf_field() }}
 
							<label>Pilih file excel</label>
							<div class="form-group">
								<input type="file" name="file" required="required">
							</div>
 
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
							<button type="submit" class="btn btn-primary">Import</button>
						</div>
					</div>
                {!! Form::close() !!}
			</div>
		</div>

    </div>
@endsection

