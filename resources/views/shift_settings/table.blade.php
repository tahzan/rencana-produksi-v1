<div class="table-responsive">
    <table class="table" id="shiftSettings-table">
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>Kategori Shift</th>
                <th>Shift 1</th>
                <th>Shift 2</th>
                <th>Shift 3</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($shiftSettings as $shiftSetting)
            <tr>
                <td>{{ $shiftSetting->tanggal->format('d M Y') }}</td>
                <td>{{ $shiftSetting->shift_category }}</td>
                <td>{{ $shiftSetting->shift_1 }}</td>
                <td>{{ $shiftSetting->shift_2 }}</td>
                <td>{{ $shiftSetting->shift_3 }}</td>
                <td>
                    {!! Form::open(['route' => ['shiftSettings.destroy', $shiftSetting->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <!-- <a href="{{ route('jadwal-mesin.show', [$shiftSetting->id]) }}" class='btn btn-info btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a> -->
                        <a href="{{ route('shiftSettings.edit', [$shiftSetting->id]) }}" class='btn btn-info btn-xs table-button'><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        {!! Form::button('<i class="fa fa-times" aria-hidden="true"></i>', ['type' => 'submit', 'class' => 'btn btn-warning btn-xs table-button', 'onclick' => "return confirm('Apakah anda yakin?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
