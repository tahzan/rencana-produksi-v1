<!-- Tanggal Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tanggal', 'Tanggal:') !!}
    {!! Form::text('tanggal', null, ['class' => 'form-control', 'id'=>'date']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('shift_category', 'Kategori Shift:') !!}
    {!! Form::select('shift_category', ['1' => '1', '2' => '2', '3' => '3'], null, ['class' => 'form-control']) !!}
</div>

<!-- Shift 1 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('shift_1', 'Shift 1:') !!}
    {!! Form::text('shift_1', null, ['class' => 'form-control']) !!}
</div>

<!-- Shift 2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('shift_2', 'Shift 2:') !!}
    {!! Form::text('shift_2', null, ['class' => 'form-control']) !!}
</div>

<!-- Shift 3 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('shift_3', 'Shift 3:') !!}
    {!! Form::text('shift_3', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('shiftSettings.index') }}" class="btn btn-default">Cancel</a>
</div>

@section('scripts')
    <script type="text/javascript">
        $('#date').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: true,
            locale: 'id'
        })
    </script>
@endsection