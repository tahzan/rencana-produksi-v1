@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Shift Setting
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($shiftSetting, ['route' => ['shiftSettings.update', $shiftSetting->id], 'method' => 'patch']) !!}

                        @include('shift_settings.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection