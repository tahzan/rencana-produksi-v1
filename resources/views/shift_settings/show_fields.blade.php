<!-- Tanggal Field -->
<div class="form-group">
    {!! Form::label('tanggal', 'Tanggal:') !!}
    <p>{{ $shiftSetting->tanggal }}</p>
</div>

<!-- Shift 1 Field -->
<div class="form-group">
    {!! Form::label('shift_1', 'Shift 1:') !!}
    <p>{{ $shiftSetting->shift_1 }}</p>
</div>

<!-- Shift 2 Field -->
<div class="form-group">
    {!! Form::label('shift_2', 'Shift 2:') !!}
    <p>{{ $shiftSetting->shift_2 }}</p>
</div>

<!-- Shift 3 Field -->
<div class="form-group">
    {!! Form::label('shift_3', 'Shift 3:') !!}
    <p>{{ $shiftSetting->shift_3 }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $shiftSetting->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $shiftSetting->updated_at }}</p>
</div>

