<!-- Rencana Produksi Id Field -->
<div class="form-group">
    {!! Form::label('rencana_produksi_id', 'Rencana Produksi Id:') !!}
    <p>{{ $attachments->rencana_produksi_id }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $attachments->name }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $attachments->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $attachments->updated_at }}</p>
</div>

