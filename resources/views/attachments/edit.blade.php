@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Attachments
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($attachments, ['route' => ['attachments.update', $attachments->id], 'method' => 'patch']) !!}

                        @include('attachments.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection