<div class="table-responsive">
    <table class="table" id="attachments-table">
        <thead>
            <tr>
                <th>Rencana Produksi Id</th>
        <th>Name</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($attachments as $attachments)
            <tr>
                <td>{{ $attachments->rencana_produksi_id }}</td>
            <td>{{ $attachments->name }}</td>
                <td>
                    {!! Form::open(['route' => ['attachments.destroy', $attachments->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('attachments.show', [$attachments->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('attachments.edit', [$attachments->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
