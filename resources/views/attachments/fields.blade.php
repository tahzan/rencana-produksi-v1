<!-- Rencana Produksi Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rencana_produksi_id', 'Rencana Produksi Id:') !!}
    {!! Form::number('rencana_produksi_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('attachments.index') }}" class="btn btn-default">Cancel</a>
</div>
