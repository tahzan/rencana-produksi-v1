<!-- Mulai Field -->
<div class="form-group">
    {!! Form::label('mulai', 'Mulai:') !!}
    <p>{{ $jadwalMesin->mulai }}</p>
</div>

<!-- Selesai Field -->
<div class="form-group">
    {!! Form::label('selesai', 'Selesai:') !!}
    <p>{{ $jadwalMesin->selesai }}</p>
</div>

<!-- Perencanaan Id Field -->
<div class="form-group">
    {!! Form::label('perencanaan_id', 'Perencanaan Id:') !!}
    <p>{{ $jadwalMesin->perencanaan_id }}</p>
</div>

<!-- Mesin Id Field -->
<div class="form-group">
    {!! Form::label('mesin_id', 'Mesin Id:') !!}
    <p>{{ $jadwalMesin->mesin_id }}</p>
</div>

<!-- Tipe Field -->
<div class="form-group">
    {!! Form::label('tipe', 'Tipe:') !!}
    <p>{{ $jadwalMesin->tipe }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $jadwalMesin->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $jadwalMesin->updated_at }}</p>
</div>

