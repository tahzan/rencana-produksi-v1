<div class="table-responsive">
    <table class="table data" id="jadwalMesins-table">
        <thead>
            <tr>
                <th>Mulai</th>
                <th>Selesai</th>
                <th>Durasi</th>
                <th>Mesin</th>
                <th>Tipe</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        @foreach($jadwalMesins as $jadwalMesin)
            <tr>
            <td>{{ $jadwalMesin->mulai->format('d/m/y H:i') }}</td>
            <td>{{ $jadwalMesin->selesai->format('d/m/y H:i') }}</td>
            <td>{{ $jadwalMesin->durasi > 0 ? convert_to_time($jadwalMesin->durasi) : 0 }}</td>
            <td>{{ $jadwalMesin->mesin->nama }}</td>
            <td>{{ $jadwalMesin->tipe }}</td>
            <td>
                {!! Form::open(['route' => ['jadwal-istirahat.destroy', $jadwalMesin->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <!-- <a href="{{ route('jadwal-mesin.show', [$jadwalMesin->id]) }}" class='btn btn-info btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a> -->
                    <a href="{{ route('jadwal-istirahat.edit', [$jadwalMesin->id]) }}" class='btn btn-info btn-xs table-button'><i class="fa fa-pencil" aria-hidden="true"></i></a>
                    {!! Form::button('<i class="fa fa-times" aria-hidden="true"></i>', ['type' => 'submit', 'class' => 'btn btn-warning btn-xs table-button', 'onclick' => "return confirm('Apakah anda yakin?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
