@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Jadwal Istirahat</h1>
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{ route('jadwal-istirahat.create') }}">Tambah Data</a>
        </h1>
        <div class="pull-right">
            <button type="button" class="btn btn-primary mr-5" style="margin-top: -10px; margin-right: 10px" 
                data-toggle="modal" data-target="#importExcel">Import Excel</button>
        </div>
        <!-- <div class="pull-right">
            <button type="button" class="btn btn-primary mr-5" style="margin-top: -10px; margin-right: 10px" 
                data-toggle="modal" data-target="#generate">Generate</button>
        </div> -->
        {!! Form::open(['route' => ['jadwal-istirahat.hapusSemua'], 'method' => 'post']) !!}
        <div class='pull-right'>
            {!! Form::button('Hapus Semua', ['type' => 'submit', 'style'=>'margin-top: -10px; margin-right: 10px', 'class' => 'btn btn-warning mr-5', 'onclick' => "return confirm('Apakah anda yakin?')"]) !!}
        </div>
        {!! Form::close() !!}
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')
        @include('adminlte-templates::common.errors')

        <div class="clearfix"></div>

        <div class="box box-primary">            
            <div class="box-header">
                <div class="box-tools">
                    <form action="" method="GET" class="search-apsc">
                        {!! Form::select('mesin_id_bottom', 
                            $mesins, 
                            $aktif_mesin_bottom, 
                            ['class' => 'form-control filter-input']) 
                        !!}
                        <div class="input-group-btn search-button-container">
                            <button type="submit" class="btn btn-info search-button"><i class="fa fa-search"></i></button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="box-body">
                @include('jadwal_istirahat.table')
            </div>
            <div class="box-footer clearfix">
                {{ $jadwalMesins->links() }}
            </div>
        </div>
        <div class="text-center">
        
        </div>

        <!-- Import Excel -->
		<div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
                {!! Form::open(['route' => 'jadwal-istirahat.import_excel', "enctype"=>"multipart/form-data"]) !!}
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
						</div>
						<div class="modal-body">
 
							{{ csrf_field() }}
 
							<label>Pilih file excel</label>
							<div class="form-group">
								<input type="file" name="file" required="required">
							</div>
 
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
							<button type="submit" class="btn btn-primary">Import</button>
						</div>
					</div>
                {!! Form::close() !!}
			</div>
		</div>

        <!-- Generate Istirahat -->
		<div class="modal fade" id="generate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
                {!! Form::open(['route' => 'jadwal-istirahat.generate', "enctype"=>"multipart/form-data"]) !!}
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Generate Jadwal Istirhatat</h5>
						</div>
						<div class="modal-body">
 
							{{ csrf_field() }}
 
							<label>Tentukan Bulan & Tahun</label>
							<div class="form-group">
                                {!! Form::text('tanggal', null, ['class' => 'form-control', 'id'=>'tanggal']) !!}
							</div>
 
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
							<button type="submit" class="btn btn-primary">Generate</button>
						</div>
					</div>
                {!! Form::close() !!}
			</div>
		</div>

    </div>
    
@endsection

@section('scripts')
    <script type="text/javascript">

        $('#tanggal').datetimepicker({
            format: 'MM-YYYY',
            useCurrent: true,
            locale: 'id'
        })

        var path = "{{ url('/mesin/cari') }}";
        $input = $('#mesin');
        $input.typeahead({
            minLength: 0,
            items: 10,
            source:  function (query, process) {
                return $.get(path, { q: query }, function (data) {
                    return process(data);
                });
            },
            autoSelect: true,
            displayText: function(item){ return `${item.nama} `;}
        });
        $input.change(function() {
            var current = $input.typeahead("getActive");
            $('input[id=mesin_id]').val(current.id);
        });

    </script>
@endsection
