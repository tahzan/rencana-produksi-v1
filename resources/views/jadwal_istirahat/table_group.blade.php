<div class="table-responsive">
    <table class="table" id="jadwalMesins-table">
        <thead>
            <tr>
                <th width="33%">Tanggal</th>
                <th width="33%">Mesin</th>
                <th width="33%">Total Jam Kerja</th>
            </tr>
        </thead>
        <tbody>
        @foreach($jadwalMesinsGroup as $jadwalMesin)
            <tr>
            <td>{{ date('d-m-Y', strtotime($jadwalMesin->mulai)) }}</td>
            <td>{{ $jadwalMesin->nama }}</td>
            <td>{{ $jadwalMesin->total_durasi > 0 ? convert_to_time($jadwalMesin->total_durasi) : 0 }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
