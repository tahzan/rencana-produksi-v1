<!-- No Transaksi Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('no_transaksi', 'No Transaksi:') !!}
    {!! Form::text('no_transaksi', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Tanggal Field -->
<div class="form-group col-sm-12">
    {!! Form::label('tanggal', 'Tanggal:') !!}
    {!! Form::date('tanggal', (isset($dailyStock)) ? $dailyStock->tanggal : null, 
        ['class' => 'form-control','readonly'=>'True']) !!}
</div>


<div class="form-group col-sm-12">
        <label>Pilih file excel</label>
        <div class="form-group">
            <input type="file" name="file" required="required">
        </div>
</div>


<div class="form-group col-sm-12">
    <div class="table-responsive" id="excel_data"> </div>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('daily-stocks.index') }}" class="btn btn-default">Cancel</a>
</div>
