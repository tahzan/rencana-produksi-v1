<div class="table-responsive">
    <table class="table" id="dailyStockItems-table">
        <thead>
            <tr>
                <th>Type</th>
                <th>Stock</th>
                <th>Error</th>
                <!-- <th colspan="3">Action</th> -->
            </tr>
        </thead>
        <tbody>
         @foreach($dailyStockItems as $dailyStockItem)
            <tr>
                <td>{{ $dailyStockItem->type ? $dailyStockItem->type->nama : "-"}}</td>
                <td>{{ $dailyStockItem->stock }}</td>
                <td>{{ $dailyStockItem->error }}</td>
               {{-- <td>
                    @if($dailyStock->status !== 'processed')
                        {!! Form::open(['route' => ['dailyStockItems.destroy', $dailyStockItem->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <!-- <a href="{{ route('dailyStockItems.show', [$dailyStockItem->id]) }}" class='btn btn-info btn-xs table-button'><i class="fa fa-eye"></i></a> -->
                            <a href="{{ route('dailyStockItems.edit', [$dailyStockItem->id]) }}" class='btn btn-info btn-xs table-button'><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            {!! Form::button('<i class="fa fa-times" aria-hidden="true"></i>', ['type' => 'submit', 'class' => 'btn btn-warning btn-xs table-button', 'onclick' => "return confirm('Apakah anda yakin?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    @else
                        <a href="{{ route('dailyStockItems.edit', [$dailyStockItem->id]) }}" class='btn btn-info btn-xs table-button'><i class="fa fa-pencil" aria-hidden="true"></i></a>
                    @endif
                </td>
                --}}
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

<!-- @if($dailyStock->status !== 'processed')
<div class="form-group"  >
    <div class="modal-footer">
        {!! Form::open(['route' => ['daily-stocks.update_status',  $dailyStock->id]]) !!}        
            {!! Form::hidden('status', 'processed') !!}
            {!! Form::button('CONFIRM ', ['type' => 'submit', 'class' => 'btn btn-primary', 'onclick' => "return confirm('Apakah anda yakin?')"]) !!}
        {!! Form::close() !!}
    </div>
</div>
@endif -->