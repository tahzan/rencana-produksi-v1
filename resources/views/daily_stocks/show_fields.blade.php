<!-- No Transaksi Field -->

<div class="form-group">
    {!! Form::label('no_transaksi', 'ID:') !!}
    <p>{{ $dailyStock->id }}</p>
</div>

<div class="form-group">
    {!! Form::label('no_transaksi', 'No Transaksi:') !!}
    <p>{{ $dailyStock->no_transaksi }}</p>
</div>

<!-- Tanggal Field -->
<div class="form-group">
    {!! Form::label('tanggal', 'Tanggal:') !!}
    <p>{{ $dailyStock->tanggal->format('d/m/Y') }}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User:') !!}
    <p>{{ $dailyStock->user->name }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $dailyStock->status }}</p>
</div>

<!-- Total Type Field -->
<div class="form-group">
    {!! Form::label('total_type', 'Total Type:') !!}
    <p>{{ count($dailyStockItems) }}</p>
</div>

<!-- Total Type Field -->
<div class="form-group">
    {!! Form::label('total_item', 'Total Item:') !!}
    <p>{{ $dailyStock->total_qty }}</p>
</div>
