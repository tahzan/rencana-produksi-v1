<div class="table-responsive">
    <table class="table" id="dailyStocks-table">
        <thead>
            <tr>
                <th>No Transaksi</th>
                <th>Tanggal</th>
                <th>User Id</th>
                <th>Status</th>
                <th>Total Type</th>
                <th>Total Item</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($dailyStocks as $dailyStock)
            <tr>
                <td>{{ $dailyStock->no_transaksi }}</td>
                <td>{{ $dailyStock->tanggal->format('d/m/Y') }}</td>
                <td>
                    {{ $dailyStock->user ? $dailyStock->user->name : $dailyStock->user_id }}
                    {{ $dailyStock->user_id }}
                </td>
                <td>{{ $dailyStock->status }}</td>
                <td>{{ $dailyStock->total_type }}</td>
                <td>{{ $dailyStock->total_qty }}</td>
                <td>
                    {!! Form::open(['route' => ['daily-stocks.destroy', $dailyStock->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                         <input type="hidden" name='start' class="form-control filter-input" 
                             value="{{$start}}" >
                            <input type="hidden" name='end' class="form-control filter-input" 
                             value="{{$end}}" >
                        <a href="{{ route('daily-stocks.show', [$dailyStock->id]) }}" class='btn btn-info btn-xs table-button'><i class="fa fa-eye"></i></a>
                        @if($dailyStock->status !== 'processed' && $dailyStock->status !== 'cancel')
                        <a href="{{ route('daily-stocks.edit', [$dailyStock->id]) }}" class='btn btn-info btn-xs table-button'><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        @endif
                        {!! Form::button('<i class="fa fa-times" aria-hidden="true"></i>', ['type' => 'submit', 'class' => 'btn btn-warning btn-xs table-button', 'onclick' => "return confirm('Apakah anda yakin?')"]) !!}
                    </div>
                    {!! Form::close() !!}

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
