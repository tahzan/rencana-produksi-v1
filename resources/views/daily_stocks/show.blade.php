@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Daily Stock
        </h1>
    </section>
    <div class="content">
        @include('flash::message')
        @include('adminlte-templates::common.errors')
        <div class="clearfix"></div>

        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('daily_stocks.show_fields')
                </div>
            </div>
        </div>

        <!-- @if($dailyStock->status !== 'processed')
        <div class="box box-primary">
            <h4 class="title-box">IMPORT DATA BARU</h4>
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('daily_stocks.import_form')
                </div>
            </div>
        </div>
        @endif -->

        <div class="box box-primary">
            <h4 class="title-box">DATA TERIMPORT</h4>
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('daily_stocks.current_data')

                    @if($dailyStock->status !== 'processed')
                    <div class="form-group"  >
                        <div class="">
                            {!! Form::open(['route' => ['daily-stocks.update_status',  $dailyStock->id]]) !!}  
                                <a href="{{ route('daily-stocks.index') }}" class="btn btn-default">Back</a>   
                                @if($dailyStock->status !== 'processed' && $dailyStock->status !== 'cancel')
                                <a href="{{ route('daily-stocks.edit', [$dailyStock->id]) }}" class="btn btn-primary">Edit</a>  
                                {!! Form::hidden('status', 'processed') !!}
                                {!! Form::button('CONFIRM ', ['type' => 'submit', 'class' => 'btn btn-primary', 'onclick' => "return confirm('Apakah anda yakin?')"]) !!}
                                @endif
                            {!! Form::close() !!}
                        </div>
                    </div>
                    @else
                    <div class="form-group"  >
                        <div class="">
                            <a href="{{ route('daily-stocks.index') }}" class="btn btn-default">Back</a>   
                        </div>
                    </div>
                    @endif
                    
                </div>
            </div>
        </div>


        
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="https://unpkg.com/xlsx@0.15.1/dist/xlsx.full.min.js"></script>
    <script type="text/javascript">
        const excel_file = document.getElementById('excel_file');
        excel_file.addEventListener('change', (event) => {
            if(!['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.ms-excel'].includes(event.target.files[0].type))
            {
                document.getElementById('excel_data').innerHTML = '<div class="alert alert-danger">Only .xlsx or .xls file format are allowed</div>';

                excel_file.value = '';

                return false;
            }

            var reader = new FileReader();

            reader.readAsArrayBuffer(event.target.files[0]);

            reader.onload = function(event){

                var data = new Uint8Array(reader.result);

                var work_book = XLSX.read(data, {type:'array'});

                var sheet_name = work_book.SheetNames;

                var sheet_data = XLSX.utils.sheet_to_json(work_book.Sheets[sheet_name[0]], {header:1});

                if(sheet_data.length > 0)
                {
                    var table_output = '<table class="table table-striped table-bordered">';

                    for(var row = 0; row < sheet_data.length; row++)
                    {

                        table_output += '<tr>';

                        for(var cell = 0; cell < sheet_data[row].length; cell++)
                        {

                            if(row == 0)
                            {

                                table_output += '<th>'+sheet_data[row][cell]+'</th>';

                            }
                            else
                            {

                                table_output += '<td>'+sheet_data[row][cell]+'</td>';

                            }

                        }

                        table_output += '</tr>';

                    }

                    table_output += '</table>';

                    document.getElementById('excel_data').innerHTML = table_output;
                }

                excel_file.value = '';

            }

        });

    </script>
@endsection
