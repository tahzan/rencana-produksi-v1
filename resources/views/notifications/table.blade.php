<div class="table-responsive">
    <table class="table" id="notifications-table">
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>Department</th>
                <th>Notifikasi</th>

                <!-- <th>PPIC ID</th> -->
                <!-- <th>Status</th> -->
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($notifications as $notification)
            <tr>
                <td @if($notification->is_read == 'F')style="background: #D4F1F4" @endif>
                    {{ $notification->tanggal->format('d/m/Y') }}</td>
                <td @if($notification->is_read == 'F')style="background: #D4F1F4" @endif>
                    {{ $notification->departement }}</td>
                <td @if($notification->is_read == 'F')style="background: #D4F1F4" @endif>
                    {{ $notification->message }}</td>
                <!-- <td @if($notification->is_read == 'F')style="background: #D4F1F4" @endif>
                    {{ $notification->ppic_id }}</td> -->
                <!-- <td @if($notification->is_read == 'F')style="background: #D4F1F4" @endif>
                    {{ $notification->status }}</td> -->
                <td @if($notification->is_read == 'F')style="background: #D4F1F4" @endif>
                    {!! Form::open(['route' => ['notifications.destroy', $notification->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <button 
                            type="button"
                            url="{{ route('ppics.show', [$notification->ppic_id]) }}" 
                            updateStatusURL="{{ route('notifications.update_status', [$notification->id]) }}"
                            class='btn btn-info btn-xs table-button' 
                            onclick="display(this)"
                        ><i class="fa fa-eye"></i></button>

                        @if(Auth::user()->jabatan == 'ppic')
                        <button 
                            type="button"
                            url="{{ route('ppics.edit', [$notification->ppic_id]) }}" 
                            updateStatusURL="{{ route('notifications.update_status', [$notification->id]) }}"
                            class='btn btn-info btn-xs table-button' 
                            onclick="display(this)"
                        ><i class="fa fa-edit"></i></button>
                        @endif

                        {!! Form::button('<i class="fa fa-times" aria-hidden="true"></i>', ['type' => 'submit', 'class' => 'btn btn-warning btn-xs table-button', 'onclick' => "return confirm('Apakah anda yakin?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

@section('scripts')
    <script type="text/javascript">
       function display(elm) {
            console.log(elm, "sdfsdfsd")
            console.log(elm.updateStatusURL)
            let updateStatusURL = elm.getAttribute('updateStatusURL')
            $.get(updateStatusURL, ()=>{});
            let url = elm.getAttribute('url')
            window.location.href =url;
        };
    </script>
@endsection