<!-- Departement Field -->
<div class="form-group">
    {!! Form::label('departement', 'Departement:') !!}
    <p>{{ $notification->departement }}</p>
</div>

<!-- Tanggal Field -->
<div class="form-group">
    {!! Form::label('tanggal', 'Tanggal:') !!}
    <p>{{ $notification->tanggal }}</p>
</div>

<!-- No Transc Field -->
<div class="form-group">
    {!! Form::label('no_transc', 'No Transc:') !!}
    <p>{{ $notification->no_transc }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $notification->status }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $notification->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $notification->updated_at }}</p>
</div>

