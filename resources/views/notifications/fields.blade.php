<!-- Departement Field -->
<div class="form-group col-sm-6">
    {!! Form::label('departement', 'Departement:') !!}
    {!! Form::text('departement', null, ['class' => 'form-control']) !!}
</div>

<!-- Tanggal Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tanggal', 'Tanggal:') !!}
    {!! Form::date('tanggal', null, ['class' => 'form-control','id'=>'tanggal']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#tanggal').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- No Transc Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_transc', 'No Transc:') !!}
    {!! Form::text('no_transc', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::select('status', ['Waiting Response' => 'Waiting Response', 'On Schedule' => 'On Schedule', 'failed' => 'failed', 'closed' => 'closed'], null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('notifications.index') }}" class="btn btn-default">Cancel</a>
</div>
