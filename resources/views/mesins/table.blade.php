<div class="table-responsive">
    <table class="table" id="mesins-table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Kode Mesin</th>
                <th>Nama Mesin</th>
                <th>Tipe</th>
                <th>Rusak</th>
                <th>Tombol ID</th>
                <th>Kategori Shift</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        @foreach($mesins as $mesin)
            <tr>
                <td>{{ $mesin->id }}</td>
                <td>{{ $mesin->kode }}</td>
                <td>{{ $mesin->nama }}</td>
                <td>{{ $mesin->tipe }}</td>
                <td>{{ $mesin->rusak }}</td>
                <td>{{ $mesin->button_id }}</td>
                <td>{{ $mesin->shift_category }}</td>
                
                <td>
                    {!! Form::open(['route' => ['mesin.destroy', $mesin->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <!-- <a href="{{ route('mesin.show', [$mesin->id]) }}" class='btn btn-info btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a> -->
                        <a href="{{ route('mesin.edit', [$mesin->id]) }}" class='btn btn-info btn-xs table-button'>
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                        {!! Form::button('<i class="fa fa-times" aria-hidden="true"></i>', ['type' => 'submit', 'class' => 'btn btn-warning btn-xs table-button', 'onclick' => "return confirm('Apakah anda yakin?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
