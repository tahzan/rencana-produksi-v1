
<!-- Keterangan Field -->
<div class="form-group col-sm-12">
    {!! Form::label('keterangan', 'Keterangan:') !!}
    {!! Form::text('keterangan', null, ['class' => 'form-control']) !!}
    {!! Form::hidden('ppic_id', $ppic_id, ['class' => 'form-control']) !!}
    {!! Form::hidden('ppic_sum', $ppic_sum, ['class' => 'form-control']) !!}
    {!! Form::hidden('ppic_status', $ppic_status, ['class' => 'form-control']) !!}
    {!! Form::hidden('ppic_typename', $ppic_typename, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('ppicComments.index') }}" class="btn btn-default">Cancel</a>
</div>
