@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Ppic Comment
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ppicComment, ['route' => ['ppicComments.update', $ppicComment->id], 'method' => 'patch']) !!}

                        @include('ppic_comments.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection