@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Ppic Comment
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'ppicComments.store']) !!}

                        @include('ppic_comments.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
