@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Rencana Produksi
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($rencProd, 
                    ['route' => ['draft-rencana-produksi.update', $rencProd->id], 'method' => 'patch']) !!}
                        @include('rencana_proudksis_draft.fields')
                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection