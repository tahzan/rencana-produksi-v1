<div class="table-responsive">
    <table class="table data" id="rencanaProduksis-table">
        <thead>
            <tr>
                <th>Urutan Booking</th>
                <!-- <th>Mulai</th> -->
                <!-- <th>Selesai</th> -->
                <!-- <th>Urutan</th> -->
                <th>Mesin</th>
                <th>Tipe</th>
                <th>Status</th>
                <!-- <th>Kapasitas Produksi</th>  -->
                <th>Est. Total Durasi</th>
                <th>Jumlah Produksi</th>
                <th>Action</th>
                <!-- <th>Jumlah Aktual</th> -->
                <!-- <th>% Aktual</th> -->
                <!-- <th>% Aktual Kap. Mesin</th> -->
                <!-- <th></th> -->
            </tr>
        </thead>
        <tbody>
        @foreach($rencanaProduksisDraft as $rencanaProduksi)
            <tr>
                <td><a href="{{ route('draft-rencana-produksi.show', [$rencanaProduksi->id]) }}" class='table-link'>{{ $rencanaProduksi->urutan_booking }}</a></td>
                <!-- <td>{{ isset($rencanaProduksi->mulai) ? $rencanaProduksi->mulai->format('d/m/y H:i') : '-'}}</td> -->
                <!-- <td>{{ isset($rencanaProduksi->selesai) ? $rencanaProduksi->selesai->format('d/m/y H:i') : '-' }}</td> -->
                <!-- <td>{{ $rencanaProduksi->urutan ?? '-' }}</td> -->
                <td>{{ $rencanaProduksi->mesin ? $rencanaProduksi->mesin->nama : '-' }}</td>
                <td>{{ $rencanaProduksi->nama }}</td>  <!-- TIPE -->
                <td> {{ strtoupper($rencanaProduksi->status) }} </td>
                <!-- <td> ? </td> -->
                <td>{{ $rencanaProduksi->estimasi_durasi ?? '-' }}</td>
                <td>{{ $rencanaProduksi->jumlah_rencana_produksi ?? '-' }}</td>
                <!-- <td>{{ $rencanaProduksi->jumlah_aktual_produksi ?? '-' }}</td> -->
                <!-- <td>{{ $rencanaProduksi->persentasi_aktual ?? '-' }}</td> -->
                <!-- <td> ? </td> -->
                <td>
                    {!! Form::open(['route' => ['draft-rencana-produksi.destroy', $rencanaProduksi->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('draft-rencana-produksi.edit', [$rencanaProduksi->id]) }}" class='btn btn-info btn-xs table-button'>
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                        {!! Form::button('<i class="fa fa-times" aria-hidden="true"></i>', 
                            ['type' => 'submit', 
                            'class' => 'btn btn-warning btn-xs table-button', 
                            'onclick' => "return confirm('Apakah anda yakin?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
