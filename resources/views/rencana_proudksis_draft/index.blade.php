@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Draft Rencana Produksi</h1>
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right" 
            style="margin-top: -10px;margin-bottom: 5px" 
            href="{{ route('draft-rencana-produksi.create') }}">Tambah Data</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
       
        <div class="box box-primary">
            <div class="box-header">
            <h3 class="box-title"></h3>
                <div class="box-tools">
                    <form action="" method="GET" class="search-apsc">
                        <!-- {!! Form::select('status', ['aktif' => 'Aktif', 'pending'=>'Pending', 'aktual'=>'Aktual', 'cancel' => 'Cancel', 'semua' => 'Semua'], null, ['class' => 'form-control filter-input', 'id'=>'status']) !!} -->
                        <input type="text" class="form-control filter-input" placeholder="Mesin" id='mesin'>
                        <input type="hidden" name="mesin_id" class="form-control filter-input" placeholder="Mesin" id='mesin_id'>
                        <!-- <input type="date" name='tanggal' class="form-control filter-input" placeholder="Tanggal" id='tanggalfilter' > -->
                        <!-- <input type="text" name='selesai' class="form-control filter-input" placeholder="Selesai" id='tanggalfilter2' > -->
                        <div class="input-group-btn search-button-container">
                            <button type="submit" class="btn btn-info search-button" ><i class="fa fa-search"></i></button>
                            <!-- <button type="button" data-href="{{ route('cetak-laporan') }}"  class="btn btn-info search-button" id="searchButton">PRINT</i></button> -->
                            <a class="btn btn-info search-button" href="{{ route('rencana-produksi.index', ['status'=>'aktif']) }}" 
                                style="z-index: 10">Lihat Semua RP</a>
                        </div>
                    </form>
                </div>
            </div>

            <div class="box-body">
                @include('rencana_proudksis_draft.table')
            </div>

            <div class="box-footer clearfix">
                {{ $rencanaProduksisDraft->links() }}
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">

        // $('#tanggalfilter, #tanggalfilter2').datetimepicker({
        //     format: 'DD-MM-YYYY',
        //     useCurrent: true,
        //     locale: 'id'
        // })

        var path = "{{ url('/mesin/cari') }}";
        $input = $('#mesin');
        $input.typeahead({
            minLength: 0,
            items: 10,
            source:  function (query, process) {
                return $.get(path, { q: query }, function (data) {
                    return process(data);
                });
            },
            autoSelect: true,
            displayText: function(item){ return `${item.nama} `;}
        });

        $input.change(function() {
            var current = $input.typeahead("getActive");
            $('input[id=mesin_id]').val(current.id);
        });

        $("#searchButton").click(function() {
            let status = document.getElementById("status").value
            let mesin_id = document.getElementById("mesin").value
            let date = document.getElementById("tanggalfilter").value
            let href = $(this).data("href");
            let url = href + "?tanggal=" + date + '&status=' + status + '&mesin_id=' + mesin_id 
            window.open(
                url,
                '_blank' // <- This is what makes it open in a new window.
            );
        });

        $("#searchButton2").click(function() {
            let status = document.getElementById("status").value
            let mesin_id = document.getElementById("mesin").value
            let date = document.getElementById("tanggalfilter").value
            // let selesai = document.getElementById("tanggalfilter2").value
            let href = $(this).data("href");
            let url = href + "?tanggal=" + date + '&status=' + status + '&mesin_id=' + mesin_id 
            window.open(
                url,
                '_blank' // <- This is what makes it open in a new window.
            );
        });

    </script>
@endsection
