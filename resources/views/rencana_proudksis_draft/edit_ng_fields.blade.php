<div class="col-sm-12" style="margin-bottom:30px">
    <div class="form-group col-sm-12 border-box-middle"> OPERATOR & NG SETTINGS</div>
    
    <div class="form-group col-sm-6">
        {!! Form::label('operator', 'Operator:') !!}
        {!! Form::text('operator', null, ['class' => 'form-control',  'id'=>'operator', 
            'readonly' => (isset($rencProd) and $rencProd->status == 'aktual') ? false : true ]) !!}
    </div>

    <div class="form-group col-sm-6">
        {!! Form::label('berat_ng_produksi', 'NG proses: Berat NG produksi (Gr):') !!}
        {!! Form::text('berat_ng_produksi', null, ['class' => 'form-control',  'id'=>'berat_ng_produksi',
            'readonly' => (isset($rencProd) and $rencProd->status == 'aktual') ? false : true]) !!}
    </div>

    <div class="form-group col-sm-6">
        {!! Form::label('berat_ng_produksi', 'NG setting: Berat Set Awal (Gr):') !!}
        {!! Form::text('berat_set_awal', null, ['class' => 'form-control',  'id'=>'berat_set_awal',
            'readonly' => (isset($rencProd) and $rencProd->status == 'aktual') ? false : true]) !!}
    </div>

    <div class="form-group col-sm-6">
        {!! Form::label('target_settings', 'Target Setting:') !!}
        {!! Form::text('target_settings', null, ['class' => 'form-control',  'id'=>'target_settings',
            'readonly' => (isset($rencProd) and $rencProd->status == 'aktual') ? false : true]) !!}
    </div>
</div>

<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('rencana-produksi.index') }}" class="btn btn-info">Cancel</a>
</div>

