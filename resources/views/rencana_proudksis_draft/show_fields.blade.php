<!-- Nomor Transaksi Field -->
<div class="form-group col-sm-12 border-box-middle"> RENCANA PRODUKSI </div>

<div class="form-group col-sm-3">
    {!! Form::label('urutan', 'Nama:') !!}
    <p>{{ $rencanaProduksi->nama ?:"-" }} </p>
</div>

<div class="form-group col-sm-3">
    {!! Form::label('urutan', 'Urutan Booking:') !!}
    <p>{{ $rencanaProduksi->urutan_booking ?:"-" }} </p>
</div>

<!-- Tanggal Field -->

<!-- Mesin Id Field -->
<div class="form-group col-sm-3">
    {!! Form::label('mesin_id', 'Mesin:') !!}
    <p>{{ $rencanaProduksi->mesin->nama ?:"-" }} </p>
</div>

<!-- Estimasi Durasi Field -->
<div class="form-group col-sm-3">
    {!! Form::label('estimasi_durasi', 'Durasi Kerja:') !!}
    <!-- {{ $rencanaProduksi->estimasi_durasi ?:"-" }} / -->
    <p>{{ convert_to_time($rencanaProduksi->estimasi_durasi) }}</p>
</div>

<!-- Estimasi Total Shift Field -->
<div class="form-group col-sm-3">
    {!! Form::label('estimasi_total_shift', 'Estimasi shift kerja:') !!}
    <p>{{ $rencanaProduksi->estimasi_total_shift ?:"-" }} </p>
</div>

<div class="form-group col-sm-3">
    {!! Form::label('estimasi_total_shift', 'Kategori Shift:') !!}
    <p>{{ $rencanaProduksi->shift_category ?:"-" }} </p>
</div>


<!-- Jumlah Rencana Produksi Field -->
<div class="form-group col-sm-3">
    {!! Form::label('jumlah_rencana_produksi', 'Jumlah Rencana Produksi:') !!}
    <p>{{ $rencanaProduksi->jumlah_rencana_produksi ?:"-" }} </p>
</div>


<div class="form-group col-sm-12 border-box-middle"> DETAIL TIPE </div>
<div class="form-group col-sm-3">
    {!! Form::label('nama', 'Nama Tipe:') !!}
    <p>{{ $rencanaProduksi->nama ?:"-" }} </p>
</div>

<!-- Jumlah Operator Field -->
<div class="form-group col-sm-3">
    {!! Form::label('jumlah_operator', 'Jumlah Operator:') !!}
    <p>{{ $rencanaProduksi->jumlah_operator ?:"-" }} </p>
</div>

<!-- Cycle Time Field -->
<div class="form-group col-sm-3">
    {!! Form::label('cycle_time', 'Cycle Time:') !!}
    <p>{{ $rencanaProduksi->cycle_time ?:"-" }} </p>
</div>

<!-- Durasi Dandon Field -->
<div class="form-group col-sm-3">
    {!! Form::label('durasi_dandon', 'Durasi Dandon:') !!}
    <p>{{ $rencanaProduksi->durasi_dandon ?:"-" }} </p>
</div>

<!-- Toleransi Field -->
<div class="form-group col-sm-3">
    {!! Form::label('toleransi', 'Toleransi:') !!}
    <p>{{ $rencanaProduksi->toleransi ?:"-" }} </p>
</div>

<!-- Durasi Produksi Field -->
<div class="form-group col-sm-3">
    {!! Form::label('durasi_produksi', 'Durasi Produksi:') !!}
    <p>{{ $rencanaProduksi->durasi_produksi ?:"-" }} </p>
</div>

<!-- Type Pasangan:nullable Field -->
<div class="form-group col-sm-3">
    {!! Form::label('type_pasangan', 'Tipe Pasangan:') !!}
    <p>{{ $rencanaProduksi->type_pasangan ?:"-" }} </p>
</div>


<!-- Created At Field -->
<div class="form-group col-sm-3">
    {!! Form::label('created_at', 'Dibuat:') !!}
    <p>{{ $rencanaProduksi->created_at->format('d/m/Y') ?:"-"}} </p>
</div>

<!-- Updated At Field -->
<div class="form-group col-sm-3">
    {!! Form::label('updated_at', 'Diubah:') !!}
    <p>{{ $rencanaProduksi->updated_at->format('d/m/Y') ?:"-" }} </p>
</div>

<!-- Updated At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    <p><b>{{ strtoupper($rencanaProduksi->status) ?:"-" }} </b></p>

    <div style="margin-top: 16px" id="divselesai" data-selesai="{{ $rencanaProduksi->selesai }}">
        @if($selfActivation)
            {!! Form::label('', 'Jadwalkan sebagai rencana produksi pertama:') !!}
            <form action="{{ route('rencana-produksi.generateNext', [$rencanaProduksi->id]) }}" method="get" >
                @csrf
                <p>Tanggal: 
                    <input type="datetime-local" id="time" name="nextMulai"></input> 
                    <input type="hidden" value="true" name="selftActivation"></input> 
                </p>
                <button type="submit" class='btn btn-danger btn-xs table-button'>CONFIRM</a>
            </form>
        @else
            {!! Form::label('', 'Aktifkan dari plan sebelumnya') !!}
        @endif
    </div>
   
</div>
