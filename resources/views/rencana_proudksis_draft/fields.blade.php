<div class="col-sm-12" style="margin-bottom:30px">
    <div class="form-group col-sm-6">
        {!! Form::label('type_id', 'ID Tipe & Nama Tipe:') !!}
        {!! Form::text('', null, ['class' => 'form-control', 'id'=>'search_type',
            'readonly' => (isset($rencProd) and $rencProd->status == 'aktual') ? true : false]) !!}
        <div style="display: flex; flex-direction: row">
        {!! Form::text('type_id', (isset($rencProd)) ? $rencProd->type_id : null, ['class' => 'form-control','id'=>'type_id', 'readonly' => 'true', 'placeholder' => 'ID']) !!}
        {!! Form::text('nama', (isset($rencProd)) ? $rencProd->nama : null, ['class' => 'form-control','id'=>'type_name', 'readonly' => 'true', 'placeholder' => 'Tipe']) !!}
        </div>
    </div>

    <div class="form-group col-sm-4">
        {!! Form::label('jumlah_rencana_produksi', 'Jumlah Rencana Produksi:') !!}
        {!! Form::number('jumlah_rencana_produksi', null, [
            'class' => 'form-control', 'id'=>'rencana_produksi', 'onchange' => 'calculateEstimasi()',
            'readonly' => (isset($rencProd) and $rencProd->status == 'aktual') ? true : false]) !!}
    </div>

    <div class="form-group col-sm-2">
        {!! Form::label('stauan', 'Satuan:') !!}
        {!! Form::select('satuan', [
            'pcs' => 'PCS', 
            'set' => 'SET', 
        ], null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-sm-8">
        {!! Form::label('mesin_id', 'Mesin:') !!}
        {!! Form::text('null', (isset($rencProd)) ? $rencProd->mesin->nama : null, [
            'class' => 'form-control', 
            'id'=>'search_mesin',
            'readonly' => (isset($rencProd) and $rencProd->status == 'aktual') ? true : false]) !!}
        {!! Form::hidden('mesin_id', (isset($rencProd)) ? $rencProd->mesin_id : null, ['class' => 'form-control','id'=>'mesin_id']) !!}
        {!! Form::hidden('shift_category', (isset($rencProd)) ? $rencProd->shift_category : null, ['class' => 'form-control','id'=>'shift_category']) !!}
    </div>
    <div class="form-group col-sm-4">
        {!! Form::label('mesin_id', 'Urutan Booking:') !!}
        {!! Form::text('urutan_booking',  null, 
            [
                'class' => 'form-control', 
                'id'=>'urutan_booking',
                'readonly' => !isset($rencProd) ? true : false
            ] )
        !!}
    </div>

    <!-- @if( !empty($rencProd)) -->
    <!-- <div class="form-group col-sm-6">
        {!! Form::label('jumlah_aktual_produksi', 'Jumlah Aktual Produksi:') !!}
        {!! Form::number('jumlah_aktual_produksi', null, ['class' => 'form-control', 'readonly' => 'true']) !!}
    </div> -->
    <!-- @endif -->
    


</div>


<div class="col-sm-12" style="margin-bottom:30px">
    <div class="form-group col-sm-12 border-box-middle"> ESTIMASI </div>
    <div class="form-group col-sm-6">
        {!! Form::label('estimasi_durasi', 'Estimasi Durasi (Hari Jam:Menit):') !!}
        {!! Form::text('estimasi_durasi_display', null, ['class' => 'form-control', 'readonly'=>'true', 'id'=>'estimasi_durasi_display']) !!}
        {!! Form::hidden('estimasi_durasi', (isset($rencProd)) ? $rencProd->estimasi_durasi : null, ['class' => 'form-control', 'readonly'=>'true', 'id'=>'estimasi_durasi']) !!}
    </div>

    <div class="form-group col-sm-6">
        {!! Form::label('estimasi_total_shift', 'Estimasi Total Shift:') !!}
        {!! Form::text('estimasi_total_shift', null, ['class' => 'form-control', 'readonly'=>'true', 'id'=>'estimasi_total_shift']) !!}
    </div>


    <!-- Estimasi Total Shift Field -->
    <!-- <div class="form-group col-sm-6">
        {!! Form::label('persentasi_cycle_time', 'Persentasi Cycle Time:') !!}
        {!! Form::text('persentasi_cycle_time', null, ['class' => 'form-control', 'readonly'=>'true']) !!}
    </div> -->

    <div class="form-group col-sm-12 border-box-middle"> DETAIL TIPE </div>
    <div class="form-group col-sm-6">
        {!! Form::label('cycle_time', 'Cycle Time / Menit: ') !!}
        {!! Form::text('cycle_time', null, ['class' => 'form-control', 'id'=>'cycle_time', 'readonly'=>'true']) !!}
    </div>

    <!-- Jumlah Operator Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('jumlah_operator', 'Jumlah Operator:') !!}
        {!! Form::text('jumlah_operator', null, ['class' => 'form-control', 'readonly'=>'true', 'id'=>'jumlah_operator']) !!}
    </div>

    <!-- Toleransi Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('toleransi', 'Toleransi (%):') !!}
        {!! Form::number('toleransi', null, ['class' => 'form-control', 'id'=>'toleransi', 'readonly'=>'true']) !!}
    </div>
    
    <!-- Durasi Dandon Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('durasi_dandon', 'Durasi Dandon  (menit): ') !!}
        {!! Form::number('durasi_dandon', null, ['class' => 'form-control', 'id'=>'durasi_dandon', 'readonly'=>'true']) !!}
    </div>

    <!-- Durasi Produksi Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('durasi_produksi', 'Durasi Produksi:') !!}
        {!! Form::text('durasi_produksi', null, ['class' => 'form-control', 'readonly' => 'true', 'id'=>'durasi_produksi', 'readonly'=>'true']) !!}
    </div>

    <!-- Type Pasangan Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('type_pasangan', 'Tipe Pasangan:') !!}
        {!! Form::text('type_pasangan', null, ['class' => 'form-control', 'readonly'=>'true', 'id'=>'type_pasangan']) !!}
    </div>

    <div class="form-group col-sm-6">
        {!! Form::label('berat_per_pcs', 'Berat / Pcs:') !!}
        {!! Form::text('berat_per_pcs', null, ['class' => 'form-control', 'readonly'=>'true', 'id'=>'berat_per_pcs']) !!}
    </div>

    

    <!-- Durasi Produksi Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('barang_lain', 'Barang Lain:') !!}
        {!! Form::text('barang_lain', null, ['class' => 'form-control', 'readonly' => 'true', 'id'=>'barang_lain', 'readonly'=>'true']) !!}
        <p id="messageBR" style="color: red"></p>
    </div>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('rencana-produksi.index') }}" class="btn btn-info">Cancel</a>
</div>

@section('scripts')
    <script type="text/javascript">

        $('#mulai, #selesai').datetimepicker({
            format: 'YYYY-MM-DDTHH:mm',
            useCurrent: true,
            locale: 'id'
        })

        var path = "{{ url('/mesin/cari') }}";
        $input = $('#search_mesin');
        $input.typeahead({
            minLength: 0,
            items: 10,
            source:  function (query, process) {
                return $.get(path, { q: query }, function (data) {
                    return process(data);
                });
            },
            autoSelect: true,
            displayText: function(item){ return `kode:${item.kode} - ${item.nama} `;}
        });
        $input.change(function() {
            var current = $input.typeahead("getActive");
            $('input[id=mesin_id]').val(current.id);
            $('input[id=shift_category]').val(current.shift_category);
            $('input[id=kode_mesin]').val(current.kode);
            calculateTanggal()
        });

        var path2 = "{{ url('/tipe/cari') }}";
        $input2 = $('#search_type');
        $input2.typeahead({
            minLength: 0,
            items: 10,
            source:  function (query, process) {
                return $.get(path2, { q: query }, function (data) {
                    return process(data);
                });
            },
            autoSelect: true,
            displayText: function(item){ return `${item.nama} `;}
        });
    
        $input2.change(function() {
            $('input[id=type_name]').val('');
            var current = $input2.typeahead("getActive");
            $('input[class=search_type]').val(current.nama);
            $('input[id=type_name]').val(current.nama);
            $('input[id=type_id]').val(current.id);
            $('input[id=cycle_time]').val(current.cycle_time);
            $('input[id=jumlah_operator]').val(current.jumlah_operator);
            $('input[id=toleransi]').val(current.toleransi);
            $('input[id=durasi_produksi]').val(current.durasi_produksi);
            $('input[id=durasi_dandon]').val(current.durasi_dandon);
            $('input[id=type_pasangan]').val(current.type_pasangan);
            $('input[id=berat_per_pcs]').val(current.berat);
            $('input[id=barang_lain]').val(current.barang_lain);
            if (current.barang_lain) {
                document.querySelector("#messageBR").innerHTML = "Jangan lupa untuk membuat rencana produksi barang ini!!";
            } else {
                document.querySelector("#messageBR").innerHTML = "";
            }
            calculateEstimasi()
        });

        function calculateEstimasi() {
            let rencanaProduksi = $('input[id=rencana_produksi]').val();
            let cycleTime =  $('input[id=cycle_time]').val(); //60 per 60 second
            let durasiProduksi = $('input[id=durasi_produksi]').val(); // 90 second
            let durasiDandon = $('input[id=durasi_dandon]').val() * 60; // durasi dandon convert to be second
            let estimasiDurasi = rencanaProduksi / cycleTime * durasiProduksi // hasil dalam second
            estimasiDurasi = durasiDandon + estimasiDurasi
            var formatted = secondsToDhms(estimasiDurasi);
            $('input[id=estimasi_durasi_display]').val(formatted);
            $('input[id=estimasi_durasi]').val(estimasiDurasi);
            // calculate estimasi total shift
            const aShiftInSecond = 28800 //8 jam * 60 menit * 60 second
            let shiftEstimation = estimasiDurasi / aShiftInSecond;
            $('input[id=estimasi_total_shift]').val(shiftEstimation);
        }

        function secondsToDhms(seconds) {
            seconds = Number(seconds);
            var d = Math.floor(seconds / (3600*24));
            var h = Math.floor(seconds % (3600*24) / 3600);
            var m = Math.floor(seconds % 3600 / 60);
            var s = Math.floor(seconds % 60);
            return `${d} ${h}:${m}`
        }

        async function calculateTanggal() {
            let path = "{{ url('/jadwal-mesin/cari-tanggal') }}";
            let mesin_id = $('input[id=mesin_id]').val();
            let data = await $.get(path, { mesin_id: mesin_id });
            $('input[id=mulai]').val(data.mulai);
            $('input[id=shift]').val(data.shift);
        }

    </script>
@endsection