<!DOCTYPE html>
<html>
<head>
	<title>Cetak Laporan</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">
        table {
            width: 100%;    
        }
        
		table tr th{
            padding: 1px 2px !important;
            border: 0.5px solid gray !important;
            text-align: center;
            padding: 6px 6px;
            font-weight: bold;
            font-size: 10px;
        }

        table tr {
            border: 0.5px solid gray !important;
        }

        table {
            margin: 5px !important;
        }

        table td{
            padding: 1px 2px !important;
            border: 0.5px solid gray !important;
            text-align: center;
            padding: 5px 0; 
            font-size: 11px
        }

        @page {
            margin: 40px !important;
            padding: 40px !important;
        }

	</style>
</head>
<body>
	<center>
		<p style="font-size: 14px; font-weight: bold">
            LAPORAN RENCANA PRODUKSI 
        </p> 
        <p style="font-size: 14px; font-weight: bold; line-height:0"> TANGGAL {{ $date }}</p>
	</center>
    
    <table class="table" cellspacing="0" style="margin-top: 50px">
        <tr>
            <th width=10>NO</th>
            <th width=60>MESIN</th>
            <th width=100>TIPE</th>
            <th width=50>JUMLAH</th>
            <th width=70 colspan=2 style="text-align: center">MULAI</th>
            <th width=70 colspan=2 style="text-align: center">SELESAI</th>
            <th width=50 style="text-align: center">% AKTUAL</th>
        </tr>

        @foreach($RPS as $idx => $rp)
        <tr>
            @if($idx == 0) 
                {{ $id = 1 }}
                <td>{{ $id }}</td>
            @else
                @if($mesin_id == $rp->mesin_id) 
                    <td> </td>
                @else 
                    {{ $id = $id + 1 }}
                    <td>{{ $id  }}</td>
                @endif
            @endif

            
            @if($idx == 0) 
                {{ $mesin_id = $rp->mesin_id }}
                <td>{{ $rp->mesin->nama  }}</td>
            @else
                @if($mesin_id == $rp->mesin_id) 
                    <td> </td>
                @else 
                    {{ $mesin_id = $rp->mesin_id }}
                    <td>{{ $rp->mesin->nama  }}</td>
                @endif
            @endif

            <td>{{ $rp->nama }}</td>
            <td>{{ $estimates[$idx] . ' / ' . $aktuals[$idx] }}</td>
            <td>{{ $rp->mulai->format('d/m/Y') ?:"-" }}</td>
            <td>{{ $rp->mulai->format('H:i') ?:"-" }}</td>
            <td>{{ $rp->selesai->format('d/m/Y') ?:"-" }}</td>
            <td>{{ $rp->selesai->format('H:i') ?:"-" }}</td>
            <td>{{ $rp->persentasi_aktual ?:"-" }}</td>
        </tr>
        @endforeach
	</table>



</body>
</html>