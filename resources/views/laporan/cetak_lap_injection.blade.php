<!DOCTYPE html>
<html>
<head>
	<title>Cetak Laporan</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">
        body {
            padding: 16px;
        }

        table {
            width: 100%;    
        }
        
		table tr th{
            padding: 1px 2px !important;
            border: 0.5px solid gray !important;
            text-align: center;
            padding: 4px;
            font-weight: normal;
            font-size: 9px;
        }

        table tr {
            border: 0.5px solid gray !important;
        }

        table {
            margin: 5px !important;
        }

        table td{
            padding: 1px 2px !important;
            border: 0.5px solid gray !important;
            text-align: center;
            padding: 5px 0; 
            font-size: 9px
        }

        @page {
            margin: 30px !important;
            padding: 30px !important;
        }

	</style>
</head>
<body>
	<center>
		<p style="font-size: 14px; font-weight: bold">
            REKAPITULASI HASIL INJECTION
            <br/>
            MULAI: {{$tanggal}} - SELESAI: {{$selesai}}
        </p> 
	</center>
    
    <table class="table" cellspacing="0" style="margin-top: 50px">
        <tr>
            <th width=10 rowspan=2>Shift</th>
            <th rowspan=2 >MC</th>
            <th rowspan=2 >Tipe</th>
            <th rowspan=2 >Dandon (Mint)</th>
            <th rowspan=2 >Operator</th>
            <th  colspan=6>Productivity %</th>
            <th  colspan=2>NG Proses %</th>
            <th  colspan=2>NG Settings %</th>
            <th rowspan=2>Diff %</th>
            <th rowspan=2>Target Setting</th>
            <th rowspan=2>Durasi</th>
            <th colspan=4>Gangguan (Menit)</th>
        </tr>
        <tr>
            <!-- <th ></th>
            <th ></th>
            <th ></th>
            <th ></th>
            <th ></th> -->
            <th>Cycle (pcs/jam)</th>
            <th>Berat / Pcs</th>
            <th>Qty Plan (Pcs)</th>
            <th>Qty Akctual (Pcs)</th>
            <th>Total Berat Produksi (gr)</th>
            <th>% Productivity</th>
            
            <th >Berat Yg Produksi (gr)</th>
            <th >% NG Proses </th>

            <th >Berat Set Awal (gr) %</th>
            <th >% NG Settings %</th>

            <th>?</th>
            <th>SETT</th>
            <th>MLD</th>
            <th>BHN</th>
        </tr>

        @foreach($rps as $idx => $rp)
        <tr>
            <td>{{$rp->shift_number}}</td>
            <td>{{$rp->nama_mesin}}</td>
            <td>{{$rp->nama}}</td>
            <td>{{$rp->durasi_dandon}}</td>
            <td>{{$rp->operator}}</td>
            <td>{{$rp->cycle_time * 60}}</td>
            <td>{{$rp->berat_per_pcs}}</td>
            <td>{{$rp->estimasi_hasil}}</td>
            <td>{{$rp->aktual_hasil}}</td>
            <td>{{$rp->berat_per_pcs * $rp->estimasi_hasil}}</td>
            <td>
                {{$rp->estimasi_hasil > 0 ? sprintf('%0.2f', $rp->aktual_hasil / $rp->estimasi_hasil * 100) : "" }}
            </td>
            
            <td @if($rp->type_pasangan) rowspan=2 @endif >
                {{$rp->berat_ng_produksi}}
            </td>
                
            @php
            $value = 0;
            if ($rp->aktual_hasil > 0 && (float)$rp->berat_per_pcs > 0) {
                if($rp->type_pasangan)
                    $value = sprintf('%0.2f', $rp->berat_ng_produksi / (((float)$rp->berat_per_pcs * 2) * ($rp->aktual_hasil*2)) * 100);
                else
                    $value = sprintf('%0.2f', $rp->berat_ng_produksi / ((float)$rp->berat_per_pcs * $rp->aktual_hasil) * 100);
            }
            @endphp
            
            <td @if($rp->type_pasangan) rowspan=2 @endif>
                {{ $value }}
            </td>

            <td @if($rp->type_pasangan) rowspan=2 @endif>
                {{ $rp->berat_set_awal }}
            </td>
            
            <td @if($rp->type_pasangan) rowspan=2 @endif > 
                {{ 
                    $rp->berat_set_awal > 0 && (float)$rp->berat_per_pcs > 0 ? sprintf('%0.2f', $rp->berat_set_awal / (10 * (float)$rp->berat_per_pcs)) : 0
                }}
            </td>

            <td >
                {{ $rp->estimasi_hasil - $rp->aktual_hasil}}
            </td>
            <td >
                {{ $rp->target_settings }}
            </td>
            <td @if($rp->type_pasangan) rowspan=2 @endif >
                {{ convert_to_time($rp->durasi) }} 
            </td>
            <td>{{ $rp->gangguan__ }}</td>
            <td>{{ $rp->gangguan_sett }}</td>
            <td>{{ $rp->gangguan_mld }}</td>
            <td>{{ $rp->gangguan_bhn }}</td>
  
        </tr>

        @if($rp->type_pasangan) 
        <tr>
            <td>{{$rp->shift_number}}</td>
            <td>{{$rp->nama_mesin}}</td>
            <td>{{$rp->type_pasangan}}</td>
            <td>{{$rp->durasi_dandon}}</td>
            <td>{{$rp->operator}}</td>
            <td>{{$rp->cycle_time}}</td>
            <td>{{$rp->berat_per_pcs}}</td>
            <td>{{$rp->estimasi_hasil}}</td>
            <td>{{$rp->aktual_hasil}}</td>
            <td>{{$rp->berat_per_pcs * $rp->estimasi_hasil}}</td>
            <td>
                {{ $rp->estimasi_hasil > 0 ? sprintf('%0.2f', $rp->aktual_hasil / $rp->estimasi_hasil * 100) : "" }}
            </td>

            <td >{{ $rp->estimasi_hasil - $rp->aktual_hasil}}</td>
            <td >{{ $rp->target_settings }}</td>

            <td>{{ $rp->gangguan__ }}</td>
            <td>{{ $rp->gangguan_sett }}</td>
            <td>{{ $rp->gangguan_mld }}</td>
            <td>{{ $rp->gangguan_bhn }}</td>
        </tr>
        @endif
        @endforeach
	</table>



</body>
</html>