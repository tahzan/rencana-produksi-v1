@extends('layouts.app')

@section('css')
    <link rel="stylesheet" href="{{ asset('/css/laporan.css') }}">
@endsection

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Tabel Estimasi Produksi</h1>
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{ route('rencana-produksi.create') }}">Tambah Data</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"></h3>
                <div class="box-tools">
                    <form action="" method="GET" class="search-apsc">
                        <input type="text" name='mulai' class="form-control filter-input" placeholder="Mulai" id='mulai' >
                        <input type="text" name='selesai' class="form-control filter-input" placeholder="Selesai" id='selesai' >
                        <div class="input-group-btn search-button-container">
                            <button type="submit" class="btn btn-info search-button"><i class="fa fa-search"></i></button>
                            <button type="button" data-href="{{ route('cetaklaporaninjection') }}"  
                                class="btn btn-info search-button" id="print">PRINT LAP INJC</i></button>
                        </div>
                    </form>
                </div>
            </div>


            <div class="box-body">
                @include('laporan.table')
            </div>
            
            <div class="box-footer clearfix">
           
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection


@section('scripts')
    <script type="text/javascript">

        $('#mulai, #selesai').datetimepicker({
            format: 'DD-MM-YYYY',
            useCurrent: true,
            locale: 'id'
        })

        $("#print").click(function() {
            let date = document.getElementById("mulai").value
            let endDate = document.getElementById("selesai").value
            if (!date) {
                alert("Tanggal Mulai harus diisi");
                rreturn;
            }
            if (!endDate) {
                alert("Tanggal Selsai harus diisi");
                rreturn;
            }
            let href = $(this).data("href");
            let url = href + "?tanggal=" + date;
            if (endDate) {
                url = url + "&selesai=" + endDate;
            }
            window.open(
                url,
                '_blank' // <- This is what makes it open in a new window.
            );
        });
    </script>
@endsection
