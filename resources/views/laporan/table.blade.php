<div class="table-responsive tableFixHead">
    <!-- <table class="table table-bordered datashiftLeft " id="">
        <thead>
            <tr>
                @foreach($listDates as $key => $day)
                    @if($key < 2) 
                        <th colspan=3>{{ $day }}</th>
                    @endif
                @endforeach
            </tr>
            <tr>
                @foreach($listShifts as $key => $shift)
                    @if($key == 0) 
                        <td colspan=3 class="main-data row2">
                            Jml Operator
                            <br/>
                            <b style='color: green'> Shift </b>
                        </td>
                    @elseif ($key == 1) 
                        <td colspan=3 class="main-data row2"></td>
                    @endif
                @endforeach
            </tr>
        </thead>
        <tbody>
            @foreach($rowDatas  as $key => $row)
                <tr>
                    @foreach($row as $key2 => $data)
                        @if($key2 < 2) 
                            <td colspan="{{ $data['colspan'] }}" class="{{ $data['class'] }}">
                                @if (isset($data['link']))
                                    <a href="{{ route('rencana-produksi.show', [$data['link']]) }}" class='table-link'>
                                        {!! $data['data'] !!}
                                    </a>
                                @else
                                    {!! $data['data'] !!}
                                @endif
                            </td>
                        @endif
                    @endforeach
                <tr>
            @endforeach
        </tbody>
    </table>

    <table class="table table-bordered datashift tableFixHead" id="mesins-table">
        <thead id="header2">
            <tr>
                @foreach($listDates as $key => $day)
                    @if($key >= 2) 
                        <th colspan=3 style="min-width: 150px !important">{{ $day }}</th>
                    @endif
                @endforeach
            </tr>
            <tr>
                @foreach($listShifts as $key => $shift)
                    @if($key >= 2) 
                        @foreach($shift as $shiftNumber)
                            <td style="min-width: 50 !important; text-align: center"> 
                                {{ $shiftNumber['operator'] }} 
                                <br/>
                                <b style='color: green'> {{ $shiftNumber['shift'] }} </b>
                            </td>
                        @endforeach
                    @endif
                @endforeach
            </tr>
        </thead>
        <tbody>
            @foreach($rowDatas  as $key => $row)
                <tr>
                    @foreach($row as $key2 => $data)
                        @if($key2 >= 2) 
                            <td colspan="{{ $data['colspan'] }}" class="{{ $data['class'] }}">
                                @if (isset($data['link']))
                                    <a href="{{ route('rencana-produksi.show', [$data['link']]) }}" class='table-link'>
                                        {!! $data['data'] !!}
                                    </a>
                                @else
                                    {!! $data['data'] !!}
                                @endif
                            </td>
                        @endif
                    @endforeach
                <tr>
            @endforeach
        </tbody>
    </table>  -->

    <table class="table tableFixHead" >
        <thead >
            <tr>
                @foreach($listDates as $key => $day)
                    <th colspan=3 style="min-width: 80px !important; text-align: center">{{ $day }}</th>
                @endforeach
            </tr>
            <tr>
                @foreach($listShifts as $key => $shift)
                    @if($key == 0) 
                        <td colspan=3 >
                            <b>Jml Operator </b>
                            <br/>
                            <b style='color: green'> Shift </b>
                        </td>
                    @elseif ($key == 1) 
                        <td colspan=3></td>
                    @else
                        @foreach($shift as $shiftNumber)
                            <td style="text-align: center"> 
                                {{ $shiftNumber['operator'] }} 
                                <br/>
                                <b style='color: green'> {{ $shiftNumber['shift'] }} </b>
                            </td>
                        @endforeach
                    @endif
                @endforeach
            </tr>
        </thead>
        <tbody>
            @foreach($rowDatas as $key => $row)
                <tr>
                    @foreach($row as $key2 => $data)
                        @if($key2 < 2) 
                            <td colspan="{{ $data['colspan'] }}" class="{{ $data['class'] }}" style="vertical-align: middle !important">
                                @if (isset($data['link']))
                                    <a href="{{ route('rencana-produksi.show', [$data['link']]) }}" >
                                        {!! $data['data'] !!}
                                    </a>
                                @else
                                    {!! $data['data'] !!}
                                @endif
                            </td>
                        @else
                            <td colspan="{{ $data['colspan'] }}" class="{{ $data['class'] }}">
                                @if (isset($data['link']))
                                    <a href="{{ route('rencana-produksi.show', [$data['link']]) }}" >
                                        {!! $data['data'] !!}
                                    </a>
                                @else
                                    {!! $data['data'] !!}
                                @endif
                            </td>
                        @endif
                    @endforeach
                <tr>
            @endforeach
        </tbody>
        
    </table>

</div>
