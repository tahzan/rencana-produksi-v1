@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Daily Stock Item
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($dailyStockItem, ['route' => ['dailyStockItems.update', $dailyStockItem->id], 'method' => 'patch']) !!}

                        @include('daily_stock_items.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection