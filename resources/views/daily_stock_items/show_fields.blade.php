<!-- Daily Stock Id Field -->
<div class="form-group">
    {!! Form::label('daily_stock_id', 'Daily Stock Id:') !!}
    <p>{{ $dailyStockItem->daily_stock_id }}</p>
</div>

<!-- Type Id Field -->
<div class="form-group">
    {!! Form::label('type_id', 'Type Id:') !!}
    <p>{{ $dailyStockItem->type_id }}</p>
</div>

<!-- Stock Field -->
<div class="form-group">
    {!! Form::label('stock', 'Stock:') !!}
    <p>{{ $dailyStockItem->stock }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $dailyStockItem->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $dailyStockItem->updated_at }}</p>
</div>

