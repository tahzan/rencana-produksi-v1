<div class="table-responsive">
    <table class="table" id="dailyStockItems-table">
        <thead>
            <tr>
                <th>Daily Stock Id</th>
        <th>Type Id</th>
        <th>Stock</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($dailyStockItems as $dailyStockItem)
            <tr>
                <td>{{ $dailyStockItem->daily_stock_id }}</td>
            <td>{{ $dailyStockItem->type_id }}</td>
            <td>{{ $dailyStockItem->stock }}</td>
                <td>
                    {!! Form::open(['route' => ['dailyStockItems.destroy', $dailyStockItem->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <!-- <a href="{{ route('dailyStockItems.show', [$dailyStockItem->id]) }}" class='btn btn-info btn-xs table-button'><i class="fa fa-eye"></i></a> -->
                        <a href="{{ route('dailyStockItems.edit', [$dailyStockItem->id]) }}" class='btn btn-info btn-xs table-button'><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        {!! Form::button('<i class="fa fa-times" aria-hidden="true"></i>', ['type' => 'submit', 'class' => 'btn btn-warning btn-xs table-button', 'onclick' => "return confirm('Apakah anda yakin?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
