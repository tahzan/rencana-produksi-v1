@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Outstandings</h1>
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{ route('outstandings.create') }}">Add New</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-tools">
                    <form action="" method="GET" class="search-apsc">
                    {!! Form::select('status', [''=>'Status', 'draft' => 'draft', 'processed' => 'processed', 'cancel' => 'cancel', ], 
                            $status, ['class' => 'form-control filter-input']) !!}
                        <input type="date" name='start' class="form-control filter-input" placeholder="Start"  value="{{$start}}" >
                        <input type="date" name='end' class="form-control filter-input" placeholder="End"  value="{{$end}}" >
                        <div class="input-group-btn search-button-container">
                            <button type="submit" class="btn btn-info search-button" ><i class="fa fa-search"></i></button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="box-body">
                    @include('outstandings.table')
            </div>
            <div class="box-footer clearfix">
                {{ $outstandings->links() }}
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

