<!-- <div class="form-group col-sm-6">
    {!! Form::label('no_transaksi', 'No Transaksi:') !!}
    {!! Form::text('no_transaksi', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Tanggal Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tanggal', 'Tanggal:') !!}
    {!! Form::date('tanggal',  (isset($outstanding)) ? $outstanding->tanggal : null, ['class' => 'form-control','id'=>'tanggal', 'readonly'=>'True']) !!}
</div>


<!-- Type Id Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('type_id', 'ID Tipe & Nama Tipe:') !!}
    {!! Form::text('', null, ['class' => 'form-control', 'id'=>'search_type',
            'readonly' => (isset($rencProd) and $rencProd->status == 'aktual') ? true : false]) !!}
    {!! Form::text('type_id', (isset($rencProd)) ? $rencProd->type_id : null, ['class' => 'form-control','id'=>'type_id', 'readonly' => 'true', 'placeholder' => 'ID']) !!}
    {!! Form::text('nama', (isset($rencProd)) ? $rencProd->nama : null, ['class' => 'form-control','id'=>'type_name', 'readonly' => 'true', 'placeholder' => 'Tipe']) !!}
</div> -->

<!-- Keterangan Field -->
<div class="form-group col-sm-12">
    {!! Form::label('keterangan', 'Keterangan:') !!}
    {!! Form::textarea('keterangan', null, ['class' => 'form-control', "rows"=>2]) !!}
</div>

<!-- Stok Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('stok', 'Stok:') !!}
    {!! Form::number('stok', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Deliv Date Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('deliv_date', 'Deliv Date:') !!}
    {!! Form::date('deliv_date', null, ['class' => 'form-control','id'=>'deliv_date']) !!}
</div> -->


<!-- Status Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::select('status', ['draft' => 'draft', 'processed' => 'processed'], null, ['class' => 'form-control']) !!}
</div> -->

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('outstandings.index') }}" class="btn btn-default">Cancel</a>
</div>


