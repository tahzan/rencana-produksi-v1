
<div class="table-responsive">
    <table class="table" id="outstandingItems-table">
        <thead>
            <th>Qty</th>
            <th>Customer</th>
            <th>Type Name</th>
            <th>Status </th>
            <th>Error </th>
            <!-- <th colspan="3">Action</th> -->
        </thead>
        <tbody>
        @foreach($outstandingItems as $outstandingItem)
            <tr>
                <td>{{ $outstandingItem->qty }}</td>
                <td>{{ $outstandingItem->customer }}</td>
                <td>{{ optional($outstandingItem->type)->nama }}</td>
                <td>{{ $outstandingItem->status }}</td>
                <td>{{ $outstandingItem->error }}</td>
                {{-- <td>
                    @if($outstanding->status !== 'processed')
                        {!! Form::open(['route' => ['outstandingItems.destroy', $outstandingItem->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{{ route('outstandingItems.show', [$outstandingItem->id]) }}" class='btn btn-info btn-xs table-button'><i class="fa fa-eye"></i></a> -->
                            <a href="{{ route('outstandingItems.edit', [$outstandingItem->id]) }}" class='btn btn-info btn-xs table-button'><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            {!! Form::button('<i class="fa fa-times" aria-hidden="true"></i>', ['type' => 'submit', 'class' => 'btn btn-warning btn-xs table-button', 'onclick' => "return confirm('Apakah anda yakin?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    @else 
                    <a href="{{ route('outstandingItems.edit', [$outstandingItem->id]) }}" class='btn btn-info btn-xs table-button'><i class="fa fa-pencil" aria-hidden="true"></i></a>
                    @endif
                </td>
                --}}

            </tr>
        @endforeach
        </tbody>
    </table>
</div>
