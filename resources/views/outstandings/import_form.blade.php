<div class="form-group" id="importExcel" >
        {!! Form::open(['route' => 'outstandingItems.import_excel', "enctype"=>"multipart/form-data", "id"=>"excel_file"]) !!}

        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
        </div>
        <div class="modal-body">

            {{ csrf_field() }}

            <label>Pilih file excel</label>
            <div class="form-group">
                <input type="file" name="file" required="required">
            </div>
            <div class="form-group">
                {!! Form::label('outstanding_id', 'Outstanding ID:') !!}
                {!! Form::number('outstanding_id', $outstanding->id, ['class' => 'form-control', "readonly"]) !!}
            </div>
        </div>
        <div class="modal-footer">
            <a href="{{ route('daily-stocks.index') }}" class="btn btn-default">Back</a>
            <button type="submit" class="btn btn-primary">Import</button>
        </div>
        {!! Form::close() !!}
</div>

<div class="table-responsive" id="excel_data">
    
</div>

