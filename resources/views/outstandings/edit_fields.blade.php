<!-- No Transaksi Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_transaksi', 'No Transaksi:') !!}
    {!! Form::text('no_transaksi', (isset($outstanding)) ? $outstanding->no_transaksi : '-', ['class' => 'form-control', 'readonly'=>'True']) !!}
</div>

<!-- Tanggal Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tanggal', 'Tanggal:') !!}
    {!! Form::date('tanggal',  (isset($outstanding)) ? $outstanding->tanggal : null, ['class' => 'form-control','id'=>'tanggal', 'readonly'=>'True']) !!}
</div>

<!-- Keterangan Field -->
<div class="form-group col-sm-12">
    {!! Form::label('keterangan', 'Keterangan:') !!}
    {!! Form::textarea('keterangan', null, ['class' => 'form-control', "rows"=>2]) !!}
</div>

<!-- <div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::select('status', ['draft' => 'draft', 'processed' => 'processed'], null, ['class' => 'form-control']) !!}
</div>
 -->

<!-- No Transaksi Field -->
<div class="form-group col-sm-12">
        <label>Pilih file excel</label>
        <div class="form-group">
            <input type="file" name="file" required="required">
        </div>
</div>


<!-- Stok Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('stok', 'Stok:') !!}
    {!! Form::number('stok', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Deliv Date Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('deliv_date', 'Deliv Date:') !!}
    {!! Form::date('deliv_date', null, ['class' => 'form-control','id'=>'deliv_date']) !!}
</div> -->


<!-- Status Field -->
<div class="form-group col-sm-12">
    <div class="table-responsive" id="excel_data"> </div>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>


