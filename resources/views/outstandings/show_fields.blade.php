<!-- No Transaksi Field -->
<div class="form-group">
    {!! Form::label('no_transaksi', 'No Transaksi:') !!}
    <p>{{ $outstanding->no_transaksi }}</p>
</div>

<!-- Tanggal Field -->
<div class="form-group">
    {!! Form::label('tanggal', 'Tanggal:') !!}
    <p>{{ $outstanding->tanggal->format('d/m/Y') }}</p>
</div>

<!-- Type Id Field -->

<!-- Keterangan Field -->
<div class="form-group">
    {!! Form::label('keterangan', 'Keterangan:') !!}
    <p>{{ $outstanding->keterangan }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $outstanding->status }}</p>
</div>

<div class="form-group">
    {!! Form::label('total_type', 'Total Type:') !!}
    <p>{{ $outstanding->total_type }}</p>
</div>

<div class="form-group">
    {!! Form::label('total_qty', 'Total Qty:') !!}
    <p>{{ $outstanding->total_qty }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Dibuat Oleh:') !!}
    <p>{{ $outstanding->user->name }}</p>
</div>
