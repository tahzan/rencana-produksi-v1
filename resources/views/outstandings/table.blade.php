<div class="table-responsive">
    <table class="table" id="outstandings-table">
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>No Transaksi</th>
                <th>Keterangan</th>
                <th>Status</th>
                <th>Total Type</th>
                <th>Total Qty</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($outstandings as $outstanding)
            <tr>
                <td>{{ $outstanding->tanggal->format('d/m/Y') }}</td>
                <td>{{ $outstanding->no_transaksi }}</td>
                <td>{{ $outstanding->keterangan }}</td>
                <td>{{ $outstanding->status }}</td>
                <td>{{ $outstanding->total_type }}</td>
                <td>{{ $outstanding->total_qty }}</td>
                <td>
                    {!! Form::open(['route' => ['outstandings.destroy', $outstanding->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                    <input type="hidden" name='start' class="form-control filter-input" 
                             value="{{$start}}" >
                            <input type="hidden" name='end' class="form-control filter-input" 
                             value="{{$end}}" >
                        <a href="{{ route('outstandings.show', [$outstanding->id]) }}" class='btn btn-info btn-xs table-button'><i class="fa fa-eye"></i></a>
                        @if($outstanding->status !== 'processed' && $outstanding->status !== 'cancel')
                            <a href="{{ route('outstandings.edit', [$outstanding->id]) }}" class='btn btn-info btn-xs table-button'><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        @endif
                        {!! Form::button('<i class="fa fa-times" aria-hidden="true"></i>', ['type' => 'submit', 'class' => 'btn btn-warning btn-xs table-button', 'onclick' => "return confirm('Apakah anda yakin?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
