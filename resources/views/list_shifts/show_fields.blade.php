<!-- Shift Field -->
<div class="form-group">
    {!! Form::label('shift', 'Shift:') !!}
    <p>{{ $listShift->shift }}</p>
</div>

<!-- Mulai Field -->
<div class="form-group">
    {!! Form::label('mulai', 'Mulai:') !!}
    <p>{{ $listShift->mulai }}</p>
</div>

<!-- Selesai Field -->
<div class="form-group">
    {!! Form::label('selesai', 'Selesai:') !!}
    <p>{{ $listShift->selesai }}</p>
</div>

<!-- Cross Day Field -->
<div class="form-group">
    {!! Form::label('cross_day', 'Cross Day:') !!}
    <p>{{ $listShift->cross_day }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $listShift->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $listShift->updated_at }}</p>
</div>

