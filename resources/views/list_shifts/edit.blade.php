@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            List Shift
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($listShift, ['route' => ['listShifts.update', $listShift->id], 'method' => 'patch']) !!}

                        @include('list_shifts.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection