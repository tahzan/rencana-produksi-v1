<div class="table-responsive">
    <table class="table" id="listShifts-table">
        <thead>
            <tr>
                <th>Shift</th>
                <th>Kategori Shfit</th>
        <th>Mulai</th>
        <th>Selesai</th>
        <th>Cross Day</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($listShifts as $listShift)
            <tr>
                <td>{{ $listShift->shift }}</td>
                <td>{{ $listShift->shift_category }}</td>
            <td>{{ $listShift->mulai }}</td>
            <td>{{ $listShift->selesai }}</td>
            <td>{{ $listShift->cross_day }}</td>
                <td>
                    {!! Form::open(['route' => ['listShifts.destroy', $listShift->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('listShifts.show', [$listShift->id]) }}" class='btn btn-info btn-xs table-button'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('listShifts.edit', [$listShift->id]) }}" class='btn btn-info btn-xs table-button'><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        {!! Form::button('<i class="fa fa-times" aria-hidden="true"></i>', ['type' => 'submit', 'class' => 'btn btn-warning btn-xs table-button', 'onclick' => "return confirm('Apakah anda yakin?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
