<!-- Shift Field -->
<div class="form-group col-sm-6">
    {!! Form::label('shift', 'Shift:') !!}
    {!! Form::number('shift', null, ['class' => 'form-control']) !!}
</div>

<!-- Mulai Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mulai', 'Mulai:') !!}
    {!! Form::text('mulai', null, ['class' => 'form-control']) !!}
</div>

<!-- Selesai Field -->
<div class="form-group col-sm-6">
    {!! Form::label('selesai', 'Selesai:') !!}
    {!! Form::text('selesai', null, ['class' => 'form-control']) !!}
</div>

<!-- Cross Day Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cross_day', 'Lewati Hari:') !!}
    {!! Form::select('cross_day', ['N' => 'N', 'Y' => 'Y'], null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('shift_category', 'Kategori Shift:') !!}
    {!! Form::select('shift_category', ['1' => '1', '2' => '2', '3' => '3'], null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('listShifts.index') }}" class="btn btn-default">Cancel</a>
</div>
