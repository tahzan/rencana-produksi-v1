<div class="table-responsive">
<form id="edit-form"  method="POST" 

action="{{ route('rencana-produksi.bakcup_button_post') }}">
@csrf
@method('POST')
<!-- Input fields for ID, Mesin Name, Nama, Jumlah Produksi, and Jumlah Aktual -->
<div  class="form-group col-sm-2">
    <p>RP ID</p>
    <input type="text" name="id" id="edit-id" readonly>
</div>
<div  class="form-group col-sm-2">
    <p>Mesin</p>
    <input type="text" name="mesin_name" id="edit-mesin-name" readonly>
</div>
<div  class="form-group col-sm-2">
    <p>Nama</p>
    <input type="text" name="nama" id="edit-nama" readonly>
</div>
<div  class="form-group col-sm-2">
    <p>Jumlah Produksi</p>
    <input type="text" name="jumlah_produksi" id="edit-jumlah-produksi" readonly>
</div>
<div  class="form-group col-sm-2">
    <p>Jumlah Aktual</p>
    <input type="text" name="jumlah_aktual" id="edit-jumlah-aktual" readonly>
</div>
<!-- Submit button to update data -->
<div class="form-group col-sm-2">
    <p>.</p>
    <button type="submit">PUSH BUTTON</button>
</div>
</form>

    <table class="table data" id="tipes-table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Mesin</th>
                <th>Mesin</th>
                <th>Tipe</th>
                <th>Jumlah Rencana</th>
                <th>Aktual</th>
                <th ></th>
            </tr>
        </thead>
        <tbody>
        @foreach($rps as $rencanaProduksi)
            <tr>
            <td>{{ $rencanaProduksi->id }}</td>
            <td>{{ $rencanaProduksi->status }}</td>
            <td>{{ $rencanaProduksi->mesin ? $rencanaProduksi->mesin->nama : '-' }}</td>
            <td>{{ $rencanaProduksi->nama }}</td> 
            <td>{{ $rencanaProduksi->jumlah_rencana_produksi }}</td>
            <td>{{ $rencanaProduksi->jumlah_aktual_produksi }}</td>
            <td>
            <button class="btn select-button" 
                    data-id="{{ $rencanaProduksi->id }}"
                    data-mesin-name="{{ $rencanaProduksi->mesin ? $rencanaProduksi->mesin->nama : '-' }}"
                    data-nama="{{ $rencanaProduksi->nama }}"
                    data-jumlah-produksi="{{ $rencanaProduksi->jumlah_rencana_produksi }}"
                    data-jumlah-aktual="{{ $rencanaProduksi->jumlah_aktual_produksi }}">
                select
            </button>
            </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    

</div>

@section('scripts')
<script>
     // Function to handle button click
     function handleSelectButtonClick(button) {
        // Remove the class from all buttons to deactivate them
        $('.select-button').removeClass('active-select-button');

        // Add the class to the clicked button to activate it
        $(button).addClass('active-select-button');

        // Get data attributes from the button
        var id = $(button).data('id');
        var mesinName = $(button).data('mesin-name');
        var nama = $(button).data('nama');
        var jumlahProduksi = $(button).data('jumlah-produksi');
        var jumlahAktual = $(button).data('jumlah-aktual');

        // Populate the form fields with the data
        $('#edit-id').val(id);
        $('#edit-mesin-name').val(mesinName);
        $('#edit-nama').val(nama);
        $('#edit-jumlah-produksi').val(jumlahProduksi);
        $('#edit-jumlah-aktual').val(jumlahAktual);
    }

    // When a "select" button is clicked
    $('.select-button').click(function () {
        handleSelectButtonClick(this);
    });

    // Handle form submission via AJAX
    $('#edit-form').submit(function (event) {
        event.preventDefault(); // Prevent the default form submission

        // Serialize the form data
        var formData = $(this).serialize();
        console.log("formData", formData);

        // Send an AJAX request to the API
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: formData,
            success: function (response) {
                // Handle the response from the API
                // Update the table or perform any other actions with the response data
                console.log('Response from the API:', response);

                var newJumlahAktual = response['aktual'];
                $('#edit-jumlah-aktual').val(newJumlahAktual);
                var id = $('#edit-id').val();

                // Find the table row with the matching ID
                var $tableRow = $('#tipes-table tbody tr').filter(function () {
                    return $(this).find('td:first').text() === id;
                });

                // Update the "Jumlah Aktual" cell in the table row
                $tableRow.find('td:eq(5)').text(newJumlahAktual);
            },
            error: function (error) {
                // console.error('Error:', error);
                alert( error.responseJSON.message);
            },
        });
    });

</script>
@endsection
