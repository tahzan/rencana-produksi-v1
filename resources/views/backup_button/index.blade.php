@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Backup Button</h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')
        @include('adminlte-templates::common.errors')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"></h3>
            </div>
            
            <div class="box-body">
                @include('backup_button.table')
            </div>

        </div>
    </div>
@endsection

