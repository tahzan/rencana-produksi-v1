@if(Auth::user()->jabatan == 'supervisor')
<li class="treeview ">
  <a href="#">
  <i class="fa fa-bars"></i><span>PPIC</span> <i class="fa fa-angle-left pull-right"></i>
  </a>
  <ul class="treeview-menu" >
    <li class="{{ Request::is('mesin*') ? 'active' : '' }}">
      <a href="{{ route('mesin.index') }}"><i class="fa fa-industry"></i><span>Mesin</span></a>
    </li>
    <li class="{{ Request::is('jadwal-istirahat*') ? 'active' : '' }}">
      <a href="{{ route('jadwal-istirahat.index') }}"><i class="fa fa-calendar"></i><span>Jadwal Istirahat Mesin</span></a>
    </li>
    <li class="{{ Request::is('jadwal-mesin*') ? 'active' : '' }}">
      <a href="{{ route('jadwal-mesin.index') }}"><i class="fa fa-calendar"></i><span>Jadwal Mesin</span></a>
    </li>
    <li class="{{ Request::is('tipe*') ? 'active' : '' }}">
      <a href="{{ route('tipe.index') }}"><i class="fa fa-sticky-note"></i><span>Tipe</span></a>
    </li>
    <li class="{{ Request::is('draft-rencana-produksi*') ? 'active' : '' }}">
      <a href="{{ route('draft-rencana-produksi.index', ['status'=>'booking']) }}"><i class="fa fa-calendar-check-o"></i><span>Draft Rencana Produksi</span></a>
    </li>
    <li class="{{ Request::is('rencana-produksi*') ? 'active' : '' }}">
      <a href="{{ route('rencana-produksi.index', ['status'=>'aktif']) }}"><i class="fa fa-calendar-check-o"></i><span>Rencana Produksi</span></a>
    </li>
    <li class="{{ Request::is('perubahan-rencana-produksi*') ? 'confirm' : '' }}">
      <a href="{{ route('perubahan-rencana-produksi.index', ['status'=>'aktif']) }}">
      <i class="fa fa-calendar-check-o"></i><span>Perubahan RP</span></a>
    </li>
    <li class="{{ Request::is('laporan*') ? 'active' : '' }}">
      <a href="{{ route('laporan.index') }}"><i class="fa fa-table"></i><span>Laporan Produksi</span></a>
    </li>
    <li class="{{ Request::is('tipe*') ? 'active' : '' }}">
      <a href="{{ route('rencana-produksi.live-info') }}"><i class="fa fa-sticky-note"></i><span>Live</span></a>
    </li>
    <li class="{{ Request::is('tipe*') ? 'active' : '' }}">
      <a href="{{ route('rencana-produksi.live-export') }}" onclick="exportTasks(event.target);"><i class="fa fa-sticky-note"></i><span>Download Data Live</span></a>
    </li>
    <li class="{{ Request::is('shiftSettings*') ? 'active' : '' }}">
      <a href="{{ route('shiftSettings.index') }}"><i class="fa fa-edit"></i><span>Pengaturan Shift</span></a>
    </li>
    <li class="{{ Request::is('infos*') ? 'active' : '' }}">
      <a href="{{ route('infos.index') }}"><i class="fa fa-info"></i><span>Info</span></a>
    </li>
    <li class="{{ Request::is('listShifts*') ? 'active' : '' }}">
      <a href="{{ route('listShifts.index') }}"><i class="fa fa-list"></i><span>Daftar Shift</span></a>
    </li>
    <li class="{{ Request::is('users*') ? 'active' : '' }}">
      <a href="{!! route('users.index') !!}"><i class="fa fa-user"></i><span>Pengguna</span></a>
    </li>
    <li class="{{ Request::is('users*') ? 'active' : '' }}">
      <a href="{!! route('rencana-produksi.backup_button') !!}"><i class="fa fa-user"></i><span>Backup Button</span></a>
    </li>
  </ul>
</li>

<li class="treeview">
  <a href="#">
  <i class="fa fa-suitcase"></i><span>Sales</span> <i class="fa fa-angle-left pull-right"></i>
  </a>
  <ul class="treeview-menu">
    <li class="{{ Request::is('outstandings*') ? 'active' : '' }}">
      <a href="{{ route('outstandings.index') }}"><i class="fa fa-table"></i><span>Upload Outstanding</span></a>

    <li class="{{ Request::is('dailyStockItems*') ? 'active' : '' }}">
      <a href="{{ route('outstandingItems.index') }}"><i class="fa fa-edit"></i><span>Laporan Outstanding</span></a>
    </li>
    </li>
    <li class="{{ Request::is('daily-stocks*') ? 'active' : '' }}">
      <a href="{{ route('daily-stocks.index') }}"><i class="fa fa-file-archive-o"></i><span>Upload Stok Harian</span></a>
    </li>
    <!-- <li class="{{ Request::is('dailyStockItems*') ? 'active' : '' }}">
      <a href="{{ route('dailyStockItems.index') }}"><i class="fa fa-edit"></i><span>Daily Stock Items</span></a>
      </li> -->
    <li class="{{ Request::is('report-daily-stock*') ? 'active' : '' }}">
      <a href="{{ route('report-daily-stock.index') }}?sum=stok+<+0"><i class="fa fa-file-archive-o"></i><span>Laporan Stok Harian</span></a>
    </li>
    <!-- <li class="{{ Request::is('notifications*') ? 'active' : '' }}">
      <a href="{{ route('notifications.index') }}"><i class="fa fa-comment"></i><span>Notifications</span></a>
    </li> -->
  </ul>
</li>
@endif

@if(Auth::user()->jabatan == 'ppic')
<li class="treeview ">
  <a href="#">
  <i class="fa fa-bars"></i><span>PPIC</span> <i class="fa fa-angle-left pull-right"></i>
  </a>
  <ul class="treeview-menu" >
    <li class="{{ Request::is('mesin*') ? 'active' : '' }}">
      <a href="{{ route('mesin.index') }}"><i class="fa fa-industry"></i><span>Mesin</span></a>
    </li>
    <li class="{{ Request::is('jadwal-istirahat*') ? 'active' : '' }}">
      <a href="{{ route('jadwal-istirahat.index') }}"><i class="fa fa-calendar"></i><span>Jadwal Istirahat Mesin</span></a>
    </li>
    <li class="{{ Request::is('jadwal-mesin*') ? 'active' : '' }}">
      <a href="{{ route('jadwal-mesin.index') }}"><i class="fa fa-calendar"></i><span>Jadwal Mesin</span></a>
    </li>
    <li class="{{ Request::is('tipe*') ? 'active' : '' }}">
      <a href="{{ route('tipe.index') }}"><i class="fa fa-sticky-note"></i><span>Tipe</span></a>
    </li>
    <li class="{{ Request::is('draft-rencana-produksi*') ? 'active' : '' }}">
      <a href="{{ route('draft-rencana-produksi.index', ['status'=>'booking']) }}"><i class="fa fa-calendar-check-o"></i><span>Draft Rencana Produksi</span></a>
    </li>
    <li class="{{ Request::is('rencana-produksi*') ? 'active' : '' }}">
      <a href="{{ route('rencana-produksi.index', ['status'=>'aktif']) }}"><i class="fa fa-calendar-check-o"></i><span>Rencana Produksi</span></a>
    </li>
    <li class="{{ Request::is('perubahan-rencana-produksi*') ? 'active' : '' }}">
      <a href="{{ route('perubahan-rencana-produksi.index', ['status'=>'aktif']) }}">
      <i class="fa fa-calendar-check-o"></i><span>Perubahan RP</span></a>
    </li>
    <li class="{{ Request::is('laporan*') ? 'active' : '' }}">
      <a href="{{ route('laporan.index') }}"><i class="fa fa-table"></i><span>Laporan Produksi</span></a>
    </li>
    <li class="{{ Request::is('tipe*') ? 'active' : '' }}">
      <a href="{{ route('rencana-produksi.live-info') }}"><i class="fa fa-sticky-note"></i><span>Live</span></a>
    </li>
    <li class="{{ Request::is('tipe*') ? 'active' : '' }}">
      <a href="{{ route('rencana-produksi.live-export') }}" onclick="exportTasks(event.target);"><i class="fa fa-sticky-note"></i><span>Download Data Live</span></a>
    </li>
    <li class="{{ Request::is('shiftSettings*') ? 'active' : '' }}">
      <a href="{{ route('shiftSettings.index') }}"><i class="fa fa-edit"></i><span>Pengaturan Shift</span></a>
    </li>
    <li class="{{ Request::is('infos*') ? 'active' : '' }}">
      <a href="{{ route('infos.index') }}"><i class="fa fa-info"></i><span>Info</span></a>
    </li>
    <li class="{{ Request::is('listShifts*') ? 'active' : '' }}">
      <a href="{{ route('listShifts.index') }}"><i class="fa fa-list"></i><span>Daftar Shift</span></a>
    </li>
    <li class="{{ Request::is('users*') ? 'active' : '' }}">
      <a href="{!! route('users.index') !!}"><i class="fa fa-user"></i><span>Pengguna</span></a>
    </li>
    <li class="{{ Request::is('users*') ? 'active' : '' }}">
      <a href="{!! route('rencana-produksi.backup_button') !!}"><i class="fa fa-user"></i><span>Backup Button</span></a>
    </li>
  </ul>
</li>

<li class="treeview">
  <a href="#">
  <i class="fa fa-suitcase"></i><span>Sales</span> <i class="fa fa-angle-left pull-right"></i>
  </a>
  <ul class="treeview-menu">
    <li class="{{ Request::is('report-daily-stock*') ? 'active' : '' }}">
      <a href="{{ route('report-daily-stock.index') }}?sum=stok+<+0"><i class="fa fa-file-archive-o"></i><span>Laporan Stok Harian</span></a>
    </li>
  </ul>
</li>
@endif

@if(Auth::user()->jabatan == 'sales')
<li class="treeview ">
  <a href="#">
  <i class="fa fa-suitcase"></i><span>Sales</span> <i class="fa fa-angle-left pull-right"></i>
  </a>
  <ul class="treeview-menu" >
    <li class="{{ Request::is('outstandings*') ? 'active' : '' }}">
      <a href="{{ route('outstandings.index') }}"><i class="fa fa-table"></i><span>Upload Outstanding</span></a>
    </li>
    <li class="{{ Request::is('daily-stocks*') ? 'active' : '' }}">
      <a href="{{ route('daily-stocks.index') }}"><i class="fa fa-file-archive-o"></i><span>Upload Daily Stock</span></a>
    </li>
    <!-- <li class="{{ Request::is('dailyStockItems*') ? 'active' : '' }}">
      <a href="{{ route('dailyStockItems.index') }}"><i class="fa fa-edit"></i><span>Daily Stock Items</span></a>
      </li> -->
    <li class="{{ Request::is('dailyStockItems*') ? 'active' : '' }}">
      <a href="{{ route('outstandingItems.index') }}"><i class="fa fa-edit"></i><span>Laporan Outstanding</span></a>
    </li>
    <li class="{{ Request::is('report-daily-stock*') ? 'active' : '' }}">
      <a href="{{ route('report-daily-stock.index') }}?sum=stok+<+0"><i class="fa fa-file-archive-o"></i><span>Laporan Stok Harian</span></a>
    </li>
    <!-- <li class="{{ Request::is('notifications*') ? 'active' : '' }}">
      <a href="{{ route('notifications.index') }}"><i class="fa fa-comment"></i><span>Notifications</span></a>
    </li> -->
  </ul>
</li>
@endif

@if(Auth::user()->jabatan == 'purchasing')
<li class="treeview ">
  <a href="#">
  <i class="fa fa-suitcase"></i><span>Sales</span> <i class="fa fa-angle-left pull-right"></i>
  </a>
  <ul class="treeview-menu" >
    <!-- <li class="{{ Request::is('outstandings*') ? 'active' : '' }}">
      <a href="{{ route('outstandings.index') }}"><i class="fa fa-table"></i><span>Outstandings</span></a>
    </li> -->
    <li class="{{ Request::is('daily-stocks*') ? 'active' : '' }}">
      <a href="{{ route('daily-stocks.index') }}"><i class="fa fa-file-archive-o"></i><span>Upload Stok Harian</span></a>
    </li>
    <!-- <li class="{{ Request::is('dailyStockItems*') ? 'active' : '' }}">
      <a href="{{ route('dailyStockItems.index') }}"><i class="fa fa-edit"></i><span>Daily Stock Items</span></a>
      </li> -->
    <li class="{{ Request::is('report-daily-stock*') ? 'active' : '' }}">
      <a href="{{ route('report-daily-stock.index') }}?sum=stok+<+0"><i class="fa fa-file-archive-o"></i><span>Laporan Stok Harian</span></a>
    </li>
    <!-- <li class="{{ Request::is('notifications*') ? 'active' : '' }}">
      <a href="{{ route('notifications.index') }}"><i class="fa fa-comment"></i><span>Notifications</span></a>
    </li> -->
  </ul>
</li>
@endif
<!-- <li class="{{ Request::is('attachments*') ? 'active' : '' }}">
    <a href="{{ route('attachments.index') }}"><i class="fa fa-edit"></i><span>Attachments</span></a>
</li> -->


