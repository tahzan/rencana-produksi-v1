@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Outstanding Item
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($outstandingItem, ['route' => ['outstandingItems.update', $outstandingItem->id], 'method' => 'patch']) !!}

                        @include('outstanding_items.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection