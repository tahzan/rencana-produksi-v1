<!-- Outstanding Id Field -->
<div class="form-group">
    {!! Form::label('outstanding_id', 'Outstanding Id:') !!}
    <p>{{ $outstandingItem->outstanding_id }}</p>
</div>

<!-- Qty Field -->
<div class="form-group">
    {!! Form::label('qty', 'Qty:') !!}
    <p>{{ $outstandingItem->qty }}</p>
</div>

<!-- Customer Field -->
<div class="form-group">
    {!! Form::label('customer', 'Customer:') !!}
    <p>{{ $outstandingItem->customer }}</p>
</div>

<!-- Type Id Field -->
<div class="form-group">
    {!! Form::label('type_id', 'TypeId:') !!}
    <p>{{ $outstandingItem->type_id }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $outstandingItem->status }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $outstandingItem->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $outstandingItem->updated_at }}</p>
</div>

