@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Outstanding Item
        </h1>
    </section>
    <div class="content">
        @include('flash::message')
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('outstanding_items.show_fields')
                    <a href="{{ route('outstandingItems.index') }}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
