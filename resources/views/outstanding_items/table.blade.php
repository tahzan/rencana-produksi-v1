<div class="table-responsive">
    <table class="table" id="outstandingItems-table">
        <thead>
            <tr>
                <!-- <th>Outstanding Id</th> -->
                <th>Tanggal</th>
                <th>Qty</th>
                <th>Customer</th>
                <th>Type Name</th>
                <th>Status</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($outstandingItems as $outstandingItem)
            <tr>
                <!-- <td>{{ $outstandingItem->outstanding_id }}</td> -->
                <td>{{ $outstandingItem->created_at->format('Y-m-d H:i') }}</td>
                <td>{{ $outstandingItem->qty }}</td>
                <td>{{ $outstandingItem->customer }}</td>
                <td>{{ optional($outstandingItem->type)->nama }}</td>
                <td>{{ $outstandingItem->status }}</td>
                <td>
                    <a href="{{ route('outstandingItems.edit', [$outstandingItem->id]) }}" class='btn btn-info btn-xs table-button'><i class="fa fa-pencil" aria-hidden="true"></i></a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
