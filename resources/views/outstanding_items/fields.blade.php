<!-- Outstanding Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('outstanding_id', 'Outstanding Id:') !!}
    {!! Form::number('outstanding_id', null, ['class' => 'form-control', 'readonly']) !!}
</div>

<!-- Qty Field -->
<div class="form-group col-sm-6">
    {!! Form::label('qty', 'Qty:') !!}
    {!! Form::number('qty', null, ['class' => 'form-control', 'readonly']) !!}
</div>

<!-- Customer Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer', 'Customer:') !!}
    {!! Form::text('customer', null, ['class' => 'form-control', 'readonly']) !!}
</div>

<!-- TypeId Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type_id', 'TypeID:') !!}
    {!! Form::number('type_id', null, ['class' => 'form-control', 'readonly']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::select('status', ['open' => 'open', 'delivered' => 'delivered'], null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('outstandingItems.index') }}" class="btn btn-default">Cancel</a>
</div>
