
<div class="form-group col-sm-12">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('info', 'Info:') !!}
    {!! Form::textarea('info', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('start_date', 'Start Date:') !!}
    {!! Form::datetimelocal('start_date', (isset($info)) ? $info->start_date : null, ['class' => 'form-control','id'=>'mulai']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('end_date', 'End Date:') !!}
    {!! Form::datetimelocal('end_date', (isset($info)) ? $info->end_date : null, ['class' => 'form-control','id'=>'selesai']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('is_active', 'Is Active:') !!}
    {!! Form::select('is_active', ['Y' => 'Y', 'N' => 'N'], null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('file', 'File:') !!}
    {!! Form::file('file', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('infos.index') }}" class="btn btn-default">Cancel</a>
</div>


@section('scripts')
    <script type="text/javascript">
        $('#mulai, #selesai').datetimepicker({
            format: 'YYYY-MM-DDTHH:mm',
            useCurrent: true,
            locale: 'id'
        })
    </script>

    <script src="https://cdn.tiny.cloud/1/qfeozj07y0yh274t1hksv202qym4hfbefinvrac45c8hgsrl/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector:'textarea',
            plugins: 'media mediaembed advcode',
            width: 900,
            height: 300
        });
    </script>
@endsection
