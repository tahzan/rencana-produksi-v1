<div class="table-responsive">
    <table class="table" id="infos-table">
        <thead>
            <tr>
                <th>Title</th>
                <th>Is Active</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($infos as $info)
            <tr>
                <td>{{ $info->title }}</td>
                <td>{{ $info->is_active }}</td>
                <td>{{ $info->start_date }}</td>
                <td>{{ $info->end_date }}</td>
                <td>
                    {!! Form::open(['route' => ['infos.destroy', $info->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <!-- <a href="{{ route('infos.show', [$info->id]) }}" class='btn btn-info btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a> -->
                        <a href="{{ route('infos.edit', [$info->id]) }}" class='btn btn-info btn-xs table-button'>
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                        {!! Form::button('<i class="fa fa-times" aria-hidden="true"></i>', ['type' => 'submit', 'class' => 'btn btn-warning btn-xs table-button', 'onclick' => "return confirm('Apakah anda yakin?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
