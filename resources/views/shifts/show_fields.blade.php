<!-- Tanggal:nullable Field -->
<div class="form-group">
    {!! Form::label('tanggal:nullable', 'Tanggal:nullable:') !!}
    <p>{{ $shift->tanggal:nullable }}</p>
</div>

<!-- Shift Field -->
<div class="form-group">
    {!! Form::label('shift', 'Shift:') !!}
    <p>{{ $shift->shift }}</p>
</div>

<!-- Durasi Field -->
<div class="form-group">
    {!! Form::label('durasi', 'Durasi:') !!}
    <p>{{ $shift->durasi }}</p>
</div>

<!-- Estimasi Hasil Field -->
<div class="form-group">
    {!! Form::label('estimasi_hasil', 'Estimasi Hasil:') !!}
    <p>{{ $shift->estimasi_hasil }}</p>
</div>

<!-- Aktual Hasil Field -->
<div class="form-group">
    {!! Form::label('aktual_hasil', 'Aktual Hasil:') !!}
    <p>{{ $shift->aktual_hasil }}</p>
</div>

<!-- Rencana Produksi Id Field -->
<div class="form-group">
    {!! Form::label('rencana_produksi_id', 'Rencana Produksi Id:') !!}
    <p>{{ $shift->rencana_produksi_id }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $shift->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $shift->updated_at }}</p>
</div>

