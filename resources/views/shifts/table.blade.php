<div class="table-responsive">
    <table class="table data" id="shifts-table">
        <thead>
            <tr>
                <th>Mulai</th>
                <th>Selesai</th>
                <th>Shift</th>
                <th>Kategori Shift</th>
                <th>Durasi</th>
                <th>Estimasi Hasil</th>
                <th>Hasil Aktual</th>
                <th>Update Hasil Aktual</th>
            </tr>
        </thead>
        <tbody>
        @foreach($shifts as $shift)
            <tr>
                <td>{{ $shift->mulai->format('d M Y H:i') }}</td>
                <td>{{ $shift->selesai->format('d M Y H:i') }}</td>
                <td>{{ $shift->shift }}</td>
                <td>{{ $shift->shift_category }}</td>
                <td>{{ gmdate('H:i', $shift->durasi) }}</td>
                <td>{{ $shift->estimasi_hasil }}</td>
                <td>{{ $shift->aktual_hasil }}</td>
                <td>
                    <div class='btn-group'>
                        <a href="{{ route('shifts.edit', [$shift->id]) }}" class='btn btn-info btn-xs table-button'><i class="fa fa-pencil" aria-hidden="true"></i></a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
