
{!! Form::model($shift, ['route' => ['shifts.update', $shift->id], 'method' => 'patch']) !!}
    <div class="col-sm-12" style="margin-bottom:16px">
    <div class="form-group col-sm-12 border-box-middle"> EDIT SHIFT </div>
<!-- Aktual Hasil Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('aktual_hasil', 'Aktual Hasil:') !!}
        {!! Form::text('aktual_hasil', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-sm-6">
        {!! Form::label('gangguan__', 'Gangguan ? (menit):') !!}
        {!! Form::text('gangguan__', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-sm-6">
        {!! Form::label('gangguan_sett', 'Gangguan SETT (menit)') !!}
        {!! Form::text('gangguan_sett', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-sm-6">
        {!! Form::label('gangguan_mld', 'Gangguan MLD (menit)') !!}
        {!! Form::text('gangguan_mld', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-sm-6">
        {!! Form::label('gangguan_bhn', 'Gangguan BHN (menit)') !!}
        {!! Form::text('gangguan_bhn', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-sm-6">
        <!-- {!! Form::label('aktual_hasil', 'Rencana Produksi:') !!} -->
        {!! Form::hidden('rencana_produksi_id', (isset($shift)) ? $shift->rencana_produksi_id : null, ['class' => 'form-control','readonly'=>'true']) !!}
        {!! Form::hidden('estimasi_hasil', (isset($shift)) ? $shift->estimasi_hasil : null, ['class' => 'form-control','readonly'=>'true']) !!}
    </div>

    <div class="form-group col-sm-12">
        {!! Form::label('note', 'Catatan') !!}
        <textarea class="form-control" id="content" name="note" rows="2">@isset($shift){{$shift->note}}@else @endIf</textarea>
    </div>

    <div class="form-group col-sm-12 border-box-middle"> OPERATOR & NG SETTINGS</div>
    
    <div class="form-group col-sm-6">
        {!! Form::label('operator', 'Operator:') !!}
        {!! Form::text('operator', null, ['class' => 'form-control',  'id'=>'operator', 
            'readonly' => (isset($shift)) ? false : true ]) !!}
    </div>

    <div class="form-group col-sm-6">
        {!! Form::label('berat_ng_produksi', 'NG proses: Berat NG produksi (Gr):') !!}
        {!! Form::text('berat_ng_produksi', null, ['class' => 'form-control',  'id'=>'berat_ng_produksi',
            'readonly' => (isset($shift)) ? false : true]) !!}
    </div>

    <div class="form-group col-sm-6">
        {!! Form::label('berat_ng_produksi', 'NG setting: Berat Set Awal (Gr):') !!}
        {!! Form::text('berat_set_awal', null, ['class' => 'form-control',  'id'=>'berat_set_awal',
            'readonly' => (isset($shift)) ? false : true]) !!}
    </div>

    <div class="form-group col-sm-6">
        {!! Form::label('target_settings', 'Target Setting:') !!}
        {!! Form::text('target_settings', null, ['class' => 'form-control',  'id'=>'target_settings',
            'readonly' => (isset($shift)) ? false : true]) !!}
    </div>

    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Simpan Data', ['class' => 'btn btn-primary']) !!}
    </div>

{!! Form::close() !!}
</div>

 
<div class="col-sm-12" style="margin-bottom:30px">
    <div class="form-group col-sm-12 border-box-middle"> LAMPIRAN </div>
    <div class="form-group col-sm-12">
        @foreach($files as $file)
            {!! Form::open(['route' => ['shifts.delete-attachment', $file->id], 'method' => 'post']) !!}
                <a href="{{ asset('file/'.$file->name) }}" target="_blank" >{{ $file->name }}</a>
                {!! Form::button('<i class="fa fa-times" aria-hidden="true"></i>', 
                    ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Apakah anda yakin?')"]) !!}
            {!! Form::close() !!}
        @endforeach
    </div> 

    <div class="form-group col-sm-12">
        <form action="{{ route('shift.upload-attachment') }}" 
            method="post" 
            enctype="multipart/form-data">
        @csrf
            <input type="file" name="gambar" >
            {!! Form::hidden('shift_id', $shift->id) !!}
            {!! Form::hidden('rencana_produksi_id', $shift->rencana_produksi_id) !!}
            <br/>
            {!! Form::submit('Upload', ['class' => 'btn btn-primary']) !!}
            <a href="{{ route('rencana-produksi.show', [$shift->rencana_produksi_id]) }}" class="btn btn-info">Back</a>
            </form>
    </div>
</div>



