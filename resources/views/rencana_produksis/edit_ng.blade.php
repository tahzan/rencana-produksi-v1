@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Rencana Produksi
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($rencProd, ['route' => ['rencana-produksi.update_ng', $rencProd->id], 'method' => 'post']) !!}
                        @include('rencana_produksis.edit_ng_fields')
                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection