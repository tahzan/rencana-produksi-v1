<!DOCTYPE html>
<html>
<head>
	<title>Cetak SPK</title>
	<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">
        body{
            background: white;
        }
		table tr td,
		table tr th{
            font-size: 9pt;
        }
        table {
            margin: 5px !important;
        }
        table tr{
            border: none
        }
        table tr td{
            padding: 1px 3px !important;
            border: none
        }

        .tabledoc {
            border-collapse:separate; 
            border-spacing: 0 2px;
        }

        @page {
            margin: 25px !important;
            padding: 25px !important;
        }


	</style>
</head>
<body>
	<center>
		<p style="font-size: 14px; font-weight: bold">
            SURAT PERINTAH KERJA (SPK) <br/> PRODUKSI</p>
	</center>

    <table cellspacing="0" cellpadding="0" style="width: 100%;">
        <tr>
            <td  width=90>No Formulir</td>
            <td  width=150>: ________________________ </td>
            <td  width=110><i> </i></td>
            <td  width=150><i> </i></td>
        </tr>
        <tr>
            <td  >No Revisi</td>
            <td  >:  ________________________ </td>
            <td colspan=2 style="text-align: center">  </td>
        </tr>
        <tr>
            <td >Tanggal Terbit</td>
            <td >:  ________________________ </td>
            <td colspan=2 style="text-align: center">  </td>
        </tr>
	</table>


    <table cellspacing="0" class='table table-borderless' style="width: 100%;">
        <tr>
            <td  width=90>Lot Produksi</td>
            <td  width=150>: {{ $RP->urutan }} </td>
            <td  width=110>Mesin / Line</td>
            <td  width=150>: {{ $RP->mesin->nama }} </td>
        </tr>

        <tr>
            <td  >Item / Type</td>
            <td  >: {{ $RP->nama }} </td>
            <td  >C/T Pcs</td>
            <td  >: ________________________ </td>
        </tr>
	</table>

    <table cellspacing="0" class='table table-borderless' style="margin-top: 40px; width: 100%;">
        <tr>
            <td width=90><i> </i></td>
            <td width=150><b> PLAN</b></td>
            <td width=110><b> AKTUAL</b></td>
            <td width=150><b> KETERANGAN</b></td>
        </tr>
        <tr>
            <td>Lot Produksi</td>
            <td>: {{ number_format($RP->jumlah_rencana_produksi) }} Pcs</td>
            <td>: __________________ </td>
            <td>: __________________ </td>
        </tr>
        <tr>
            <td>Waktu Produksi</td>
            <td>: {{ convert_to_time($RP->estimasi_durasi) }} </td>
            <td>: __________________ </td>
            <td>: __________________ </td>
        </tr>
        <tr>
            <td>Dandon</td>
            <td>: {{ number_format($RP->durasi_dandon) }} Menit</td>
            <td>: __________________ </td>
            <td>: __________________ </td>
        </tr>

        <tr>
            <td>Total</td>
            <td>: __________________ Pcs </td>
            <td>: __________________ </td>
            <td>: __________________ </td>
        </tr>

        <tr>
            <td>Mulai Produksi</td>
            <td>: {{ $RP->mulai->format('d / m / Y') ?:"-" }} Jam {{ $RP->mulai->format('H:i') ?:"-" }}</td>
            <td>: __________________ </td>
            <td>: __________________ </td>
        </tr>

        <tr>
            <td>Selesai Produksi</td>
            <td>: {{ $RP->selesai->format('d / m / Y') ?:"-" }} Jam {{ $RP->selesai->format('H:i') ?:"-" }}</td>
            <td>: __________________ </td>
            <td>: __________________ </td>
        </tr>
	</table>

    <p style="font-size: 10px; margin-left: 8px; margin-bottom: 0; font-weight:bold"> Dokumen Pendukung</p>
    
    <table class='table table-borderless tabledoc' style="margin-top: 20px; width: 100%;">
        <tr>
            <td width=90><i> </i></td>
            <td width=30><i> </i></td>
            <td width=90 style="padding-left: 10px !important;"> Check Sheet QC</td>
            <td style="border: 1px solid black", width="30"><i> </i></td>
            <td colspan=2 style="text-align: center"></td> 
        </tr>

        <tr>
            <td >Validasi Proses Awal</td>
            <td style="border: 1px solid black", ></td>
            <td style="padding-left: 10px !important;"> Form Gangguan</td>
            <td style="border: 1px solid black", ></td>
            <td colspan=2 style="text-align: center"><b>LEADER</b></td>
        </tr>

        <tr>
            <td >Setting Record</td>
            <td style="border: 1px solid black", ></td>
            <td style="padding-left: 10px !important; ">Dispensi QC</td>
            <td style="border: 1px solid black", ></td>
            <td colspan=2 style="text-align: center"></td> 
        </tr>

        <tr>
            <td >PAR</td>
            <td style="border: 1px solid black", ></td>
            <td style="padding-left: 10px !important;"> KTS</td>
            <td style="border: 1px solid black", ></td>
            <td colspan=2 style="text-align: center"></td> 
        </tr>

        <tr>
            <td >Slip WareHouse</td>
            <td style="border: 1px solid black", ></td>
            <td style="padding-left: 10px !important;"> ______________ </td>
            <td style="border: 1px solid black", ></td> 
            <td colspan=2 style="text-align: center; font-size: 8px; vertical-align: bottom">
                #Saya mengetahui dan menyetuji C/T, Qty, Waktu yang tersedia di SPK ini 
            </td> 
        </tr>

    </table>
    
    <p style="font-size: 8px; margin-left: 9px;"> Produksi berhenti sesuai waktu yang tertera dalam kolom "Selesai Produksi" meskipun quantity belum terpenuhi</p>

</body>
</html>