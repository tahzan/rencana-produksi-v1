<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Rencana Produksi</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <!-- <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}"> -->
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/_all-skins.min.css') }}">

    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('css/_all.css') }}/">
    <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">

    @yield('css')
    <link rel="stylesheet" href="{{ asset('/css/live.css') }}">
    <link rel="icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon"/>
</head>

<body>
    <div class="row">
        <div class="col-md-12 info-wrapper" >
            <h1 style="font-weight: bold; margin: 24px 0">{!! $info->title !!}</h1>
            @if($info->file)
                <img style="width: 55%; margin-bottom: 24px" src="{{ url('/info_image/'.$info->file) }}">
            @endif
            {!! $info->info !!}
        </div>
    </div>

    <div class="row" style="width:100%; display: flex; position: absolute; bottom: 32px; right: 32px;">
        <div class="col-md-12 table-responsive" style="padding-right: 20px; text-align: right; ">
            {{ $infos->links() }}
        </div>
    </div>

    <a href="{{ route('rencana-produksi.live-info') }}?page={{$nextPage}}" style="position: absolute; top: 32px; right: 32px;" id="timer"> 60 detik </a> 
    
    <!-- jQuery 3.1.1 -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script>
        let timeleft = 60;
        $(document).ready(function() {
            setInterval('tryTorefreshPage()', 1000);
        });
    
        function tryTorefreshPage() { 
            timeleft--;
            document.getElementById("timer").innerHTML = `${timeleft} detik`;
            if(timeleft <= 0){
                window.location.href = '?page={{$nextPage}}';
            }
        }
    </script>
    @yield('scripts')
</body>
</html>