<div class="table-responsive">
    <table class="table data" id="jadwalMesins-table">
        <thead>
            <tr>
                <th>Mulai</th>
                <th>Selesai</th>
                <th>Durasi</th>
                <th>Mesin</th>
                <th>Tipe</th>
                <!-- <th></th> -->
            </tr>
        </thead>
        <tbody>
        @foreach($jadwalMesins as $jadwalMesin)
            <tr>
            <td>{{ $jadwalMesin->mulai->format('d M Y H:i') }}</td>
            <td>{{ $jadwalMesin->selesai->format('d M Y H:i') }}</td>
            <td>{{ $jadwalMesin->durasi > 0 ? convert_to_time($jadwalMesin->durasi) : 0 }}</td>
            <td>{{ $jadwalMesin->mesin->nama }}</td>
            <td>{{ $jadwalMesin->tipe }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
