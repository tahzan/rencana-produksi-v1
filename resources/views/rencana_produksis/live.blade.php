<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Rencana Produksi</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <!-- <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}"> -->
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/_all-skins.min.css') }}">

    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('css/_all.css') }}/">
    <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">

    @yield('css')
    <link rel="stylesheet" href="{{ asset('/css/live.css') }}">
    <link rel="icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon"/>
</head>

<body class="live2">
    <div class="row ">
        
        <div class="col-md-6 table-responsive">
            <h4>{{$tanggal}}</h4>
            <table class="table data" id="rencanaProduksis-table">
                <thead>
                    <tr>
                        <th>RP ID</th>
                        <th>Mesin</th>
                        <th>Tipe</th>
                        <th>Target PAR</th>
                        <th>Aktual PAR</th>
                        <th>Finish Shift</th>
                        <th width="160">Status</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($datas as $data)
                    @if($loop->iteration % 2 != 0)
                        <tr class="first">
                            <td>{{ $data['id'] ?:"-" }}</td>
                            <td>{{ $data['mesin_name'] }}</td>
                            <td>{{ $data['name'] }}</td>
                            <td>{{ number_format($data['target_shift_ini'], 0) }}</td>
                            <td>{{ number_format($data['aktual_shift_ini'], 0) }}</td>
                            <td>{{ $data['finish_date']}}</td>
                            <td>
                                <img src="{{ asset('/img/'.$data['status'].'.png') }}" class="info" />
                                <br/>{{ (isset($data['last_hint'])) ? $data['last_hint']->format('d/m/y H:i') : "" }}
                            </td>
                        </tr>
                        @if($data['status'] == "running" || $data['status'] == "warning")
                            <tr>
                                <td></td>
                                <td>Shift -- % PAR</td>
                                <td colspan=4> 
                                    <div class="progress">
                                        <div class="w3-blue" style="height:24px;width:{{$data['persent_shift']}}%">
                                    </div>
                                </td>
                                <td>
                                    {{$data['persent_shift']}}%
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>% SPK</td>
                                <td colspan=4> 
                                    <div class="progress">
                                        <div class="w3-blue" style="height:24px;width:{{$data['persent_plan']}}%">
                                    </div>
                                </td>
                                <td>
                                    {{$data['persent_plan']}}%
                                </td>
                            </tr>
                        @endif
                    @endif
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-6 table-responsive">
            <h4>.</h4>
            <table class="table data" id="rencanaProduksis-table">
                <thead>
                    <tr >
                        <th>RP ID</th>
                        <th>Mesin</th>
                        <th>Tipe</th>
                        <th>Target PAR</th>
                        <th>Aktual PAR</th>
                        <th>Finish Shift</th>
                        <th width="160">Status</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($datas as $data)
                    @if($loop->iteration % 2 == 0)
                        <tr class="first">
                            <td>{{ $data['id'] ?:"-" }}</td>
                            <td>{{ $data['mesin_name'] }}</td>
                            <td>{{ $data['name'] }}</td>
                            <td>{{ number_format($data['target_shift_ini'], 0) }}</td>
                            <td>{{ number_format($data['aktual_shift_ini'], 0) }}</td>
                            <td>{{ $data['finish_date']}}</td>
                            <td>
                                <img src="{{ asset('/img/'.$data['status'].'.png') }}" class="info" />
                                <br/>{{ (isset($data['last_hint'])) ? $data['last_hint']->format('d/m/y H:i') : "" }} 
                            </td>
                        </tr>
                        @if($data['status'] == "running" || $data['status'] == "warning")
                            <tr>
                                <td></td>
                                <td>Shift -- % PAR</td>
                                <td colspan=4> 
                                    <div class="progress">
                                        <div class="w3-blue" style="height:24px;width:{{$data['persent_shift']}}%">
                                    </div>
                                </td>
                                <td>
                                    {{$data['persent_shift']}}%
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>% SPK</td>
                                <td colspan=4> 
                                    <div class="progress">
                                        <div class="w3-blue" style="height:24px;width:{{$data['persent_plan']}}%">
                                    </div>
                                </td>
                                <td>
                                    {{$data['persent_plan']}}%
                                </td>
                            </tr>
                        @endif
                    @endif
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="row" style="width:100%; display: flex; position: absolute; bottom: 32px; right: 32px;">
        <div class="col-md-12 table-responsive" style="padding-right: 20px; text-align: right; ">
            {{ $mesins->links() }}
        </div>
    </div>
    
    <a href="{{ route('rencana-produksi.live') }}?page={{$nextPage}}" style="position: absolute; top: 32px; right: 32px;" id="timer"> 60 detik </a> 

    <!-- jQuery 3.1.1 -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script>
        let timeleft = 60;
        $(document).ready(function() {
            setInterval('tryTorefreshPage()', 1000);
        });
    
        function tryTorefreshPage() { 
            timeleft--;
            document.getElementById("timer").innerHTML = `${timeleft} detik`;
            if(timeleft <= 0){
                window.location.href = '?page={{$nextPage}}';
            }
        }
    </script>
    @yield('scripts')
</body>
</html>