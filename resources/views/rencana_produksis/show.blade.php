@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Rencana Produksi
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>
            @include('flash::message')
        <div class="clearfix"></div>

        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('rencana_produksis.show_fields')
                </div>
            </div>
        </div>

        <div class="box box-primary">
            <h4 class="title-box">JADWAL MESIN</h4>
            <div class="box-body">
                @include('rencana_produksis.table_jadwal')
            </div>
        </div>

        <div class="box box-primary">
            <h4 class="title-box">DATA SHIFT</h4>
            <div class="box-body">
                @include('shifts.table')
            </div>
        </div>

        <div class="box box-primary">
            <h4 class="title-box">HISTORY PERUBAHAN DATA</h4>
            <div class="box-body">
                @include('rencana_produksi_cache.table')
            </div>
        </div>
    </div>
@endsection
