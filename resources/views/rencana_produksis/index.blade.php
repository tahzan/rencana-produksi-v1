@extends('layouts.app')

@section('content')
{{--
    <section class="content-header">
        <h1 class="pull-left">Draft Rencana Produksi</h1>
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" 
            href="{{ route('draft-rencana-produksi.create') }}">Tambah Data</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>
        @include('flash::message')
        <div class="clearfix"></div>
        <div class="box box-primary" style="margin-bottom: 0px;">
            <div class="box-body">
                @include('rencana_proudksis_draft.table')
                <div style="text-align: right">
                    <a class="btn btn-info search-button" href="{{ route('draft-rencana-produksi.index') }}" 
                    style="font-weight: bold">Lihat Semua Draft</a>
                </div>
            </div>
        </div>
    </div>
    --}}
    <section class="content-header">
        <h1 class="pull-left">Rencana Produksi Aktif</h1>
        <h1 class="pull-right">
           <!-- <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{ route('rencana-produksi.create') }}">Tambah Data</a> -->
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{ route('draft-rencana-produksi.index') }}">Draft Produksi</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"></h3>
                <div class="box-tools">
                    <form action="" method="GET" class="search-apsc">
                        {!! Form::select('status', [ 'aktif'=>'Aktif', 'cancel' => 'Cancel', 'semua' => 'Semua', 'booking' => 'Booking'], $status, ['class' => 'form-control filter-input', 'id'=>'status']) !!}
                        <input type="text" name="mesin_name" class="form-control filter-input"  placeholder="Mesin" id='mesinName' value="{{$mesin_name}}")>
                        <input type="hidden" name="mesin_id" class="form-control filter-input" placeholder="Mesin" id='mesinID' value="{{$mesin_id}}">
                        <input type="date" name='tanggal' class="form-control filter-input" placeholder="Tanggal" id='tanggalfilter' value="{{$tanggal}}" >
                        <!-- <input type="text" name='selesai' class="form-control filter-input" placeholder="Selesai" id='tanggalfilter2' > -->
                        <div class="input-group-btn search-button-container">
                            <button type="submit" class="btn btn-info search-button" ><i class="fa fa-search"></i></button>
                            <button type="button" data-href="{{ route('cetak-laporan') }}"  class="btn btn-info search-button" id="printbutton">PRINT</i></button>
                            <button type="button" data-href="{{ route('export-csv') }}"  class="btn btn-info search-button" id="exportButton">EXPORT</i></button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="box-body">
                @include('rencana_produksis.table')
            </div>

            <div class="box-footer clearfix">
                {{ $rencanaProduksis->links() }}
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">

        // $('#tanggalfilter, #tanggalfilter2').datetimepicker({
        //     format: 'DD-MM-YYYY',
        //     useCurrent: true,
        //     locale: 'id'
        // })

        var path = "{{ url('/mesin/cari') }}";
        $input = $('#mesinName');
        $input.typeahead({
            minLength: 0,
            items: 10,
            source:  function (query, process) {
                return $.get(path, { q: query }, function (data) {
                    return process(data);
                });
            },
            autoSelect: true,
            displayText: function(item){ return `${item.nama} `;}
        });

        $input.change(function() {
            var current = $input.typeahead("getActive");
            $('input[id=mesinID]').val(current.id);
        });

        $("#searchButton").click(function() {
            let status = document.getElementById("status").value
            let mesin_id = document.getElementById("mesinID").value
            let date = document.getElementById("tanggalfilter").value
            let href = $(this).data("href");
            let url = href + "?tanggal=" + date + '&status=' + status + '&mesin_id=' + mesin_id 
            window.open(
                url,
                '_blank' // <- This is what makes it open in a new window.
            );
        });

        $("#printbutton").click(function() {
            let status = document.getElementById("status").value
            let mesin_id = document.getElementById("mesinID")?.value
            let date = document.getElementById("tanggalfilter").value
            // let selesai = document.getElementById("tanggalfilter2").value
            let href = $(this).data("href");
            let url = href + "?tanggal=" + date + '&status=' + status + '&mesin_id=' + mesin_id 
            window.open(
                url,
                '_blank' // <- This is what makes it open in a new window.
            );
        });

        $("#exportButton").click(function() {
            let status = document.getElementById("status").value
            let mesin_id = document.getElementById("mesinID")?.value
            let date = document.getElementById("tanggalfilter").value
            // let selesai = document.getElementById("tanggalfilter2").value
            let href = $(this).data("href");
            let url = href + "?tanggal=" + date + '&status=' + status + '&mesin_id=' + mesin_id 
            window.open(
                url,
                '_blank' // <- This is what makes it open in a new window.
            );
        });

    </script>
@endsection
