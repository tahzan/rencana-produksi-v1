<div class="table-responsive">
    <table class="table data" id="rencanaProduksis-table">
        <thead>
            <tr>
                <!-- <th>Urutan Booking</th> -->
                <th>No Transaksi</th>
                <th>Mulai</th>
                <th>Selesai</th>
                <th>Urutan</th>
                <th>Urutan Booking</th>
                <th>Mesin</th>
                <th>Tipe</th>
                <th>Status</th>
                <!-- <th>Kapasitas Produksi</th>  -->
                <th>Est. Total Durasi</th>
                <th>Jumlah Produksi</th>
                <th>Jumlah Aktual</th>
                <th>% Aktual</th>
                <!-- <th>% Aktual Kap. Mesin</th> -->
                <!-- <th></th> -->
            </tr>
        </thead>
        <tbody>
        @foreach($rencanaProduksis as $rencanaProduksi)
            <tr>
                <!-- <td>{{ $rencanaProduksi->id }}</td> -->
                <td><a href="{{ route('rencana-produksi.show', [$rencanaProduksi->id]) }}" class='table-link'>{{ $rencanaProduksi->nomor_transaksi }}</a></td>
                <td>{{ isset($rencanaProduksi->mulai)  ? $rencanaProduksi->mulai->format('d/m/y H:i') : "-"}}</td>
                <td>{{ isset($rencanaProduksi->selesai) ? $rencanaProduksi->selesai->format('d/m/y H:i') : '-' }}</td>
                <td>{{ $rencanaProduksi->urutan }}</td>
                <td>{{ $rencanaProduksi->urutan_booking }}</td>
                <td>{{ $rencanaProduksi->mesin->nama }}</td>
                <td>{{ $rencanaProduksi->nama }}</td>  <!-- TIPE -->
                <td> {{ strtoupper($rencanaProduksi->status) }} </td>
                <!-- <td> ? </td> -->
                <td>{{ $rencanaProduksi->estimasi_durasi }}</td>
                <td>{{ $rencanaProduksi->jumlah_rencana_produksi }}</td>
                <td>{{ $rencanaProduksi->jumlah_aktual_produksi }}</td>
                <td>{{ $rencanaProduksi->persentasi_aktual }} %</td>
                <!-- <td> ? </td> -->
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
