<!-- Nomor Transaksi Field -->
<div class="form-group col-sm-12 border-box-middle"> RENCANA PRODUKSI </div>
<div class="form-group col-sm-3">
    {!! Form::label('nomor_transaksi', 'Nomor Transaksi:') !!}
    <p>{{ $rencanaProduksi->nomor_transaksi ?:"-" }} </p>
</div>

<div class="form-group col-sm-3">
    {!! Form::label('urutan', 'Nama:') !!}
    <p>{{ $rencanaProduksi->nama ?:"-" }} </p>
</div>

<div class="form-group col-sm-3">
    {!! Form::label('urutan', 'Urutan:') !!}
    <p>{{ $rencanaProduksi->urutan ?:"-" }} </p>
</div>

<!-- Tanggal Field -->
<div class="form-group col-sm-3">
    {!! Form::label('mulai', 'Mulai:') !!}
    <p>{{ $rencanaProduksi->mulai->format('d/m/y H:i') ?:"-" }} </p>
</div>

<div class="form-group col-sm-3">
    {!! Form::label('selesai', 'Selesai:') !!}
    <p>{{ isset($rencanaProduksi->selesai) ? $rencanaProduksi->selesai->format('d/m/y H:i') : '-' }}</p>
</div>

<!-- Shift Field -->
<div class="form-group col-sm-3">
    {!! Form::label('shift', 'Shift Mulai:') !!}
    <p>{{ $rencanaProduksi->shift ?:"-" }} </p>
</div>

<!-- Shift Field -->
<div class="form-group col-sm-3">
    {!! Form::label('shift', 'Shift Selesai:') !!}
    <p>{{ $rencanaProduksi->shift_selesai ?:"-" }} </p>
</div>

<div class="form-group col-sm-3">
    {!! Form::label('shift_category', 'Kategori Shift:') !!}
    <p>{{ $rencanaProduksi->shift_category ?:"-" }} </p>
</div>


<!-- Mesin Id Field -->
<div class="form-group col-sm-3">
    {!! Form::label('mesin_id', 'Mesin:') !!}
    <p>{{ $rencanaProduksi->mesin->nama ?:"-" }} </p>
</div>

<!-- Estimasi Durasi Field -->
<div class="form-group col-sm-3">
    {!! Form::label('estimasi_durasi', 'Durasi Kerja:') !!}
    <!-- {{ $rencanaProduksi->estimasi_durasi ?:"-" }} / -->
    <p>{{ convert_to_time($rencanaProduksi->estimasi_durasi) }}</p>
</div>

<!-- Estimasi Total Shift Field -->
<div class="form-group col-sm-3">
    {!! Form::label('estimasi_total_shift', 'Estimasi shift kerja:') !!}
    <p>{{ $rencanaProduksi->estimasi_total_shift ?:"-" }} </p>
</div>


<!-- Jumlah Rencana Produksi Field -->
<div class="form-group col-sm-3">
    {!! Form::label('jumlah_rencana_produksi', 'Jumlah Rencana Produksi:') !!}
    <p>{{ $rencanaProduksi->jumlah_rencana_produksi ?:"-" }} </p>
</div>

<!-- Umlah Aktual Produksi Field -->
<div class="form-group col-sm-3">
    {!! Form::label('umlah_aktual_produksi', 'Jumlah Aktual Produksi:') !!}
    <p>{{ $rencanaProduksi->jumlah_aktual_produksi ?:"-" }} </p>
</div>

<!-- Persentasi Aktual Field -->
<div class="form-group col-sm-3">
    {!! Form::label('persentasi_aktual', 'Persentasi Aktual:') !!}
    <p>{{ $rencanaProduksi->persentasi_aktual.' %' ? :"-" }} </p>
</div>

<!-- Persentasi Cycle Time Field -->
<div class="form-group col-sm-3">
    {!! Form::label('persentasi_cycle_time', 'Persentasi Cycle Time:') !!}
    <p>{{ $rencanaProduksi->persentasi_cycle_time ?:"-" }} </p>
</div>

<!-- Persentasi Cycle Time Field -->
<div class="form-group col-sm-3">
    {!! Form::label('operator', 'Operator:') !!}
    <p>{{ $rencanaProduksi->operator ?:"-" }} </p>
</div>

<div class="form-group col-sm-3">
    {!! Form::label('berat_ng_produksi', 'NG Proses: Berat NG Produksi:') !!}
    <p>{{ $rencanaProduksi->berat_ng_produksi ?:"-" }} </p>
</div>

<div class="form-group col-sm-3">
    {!! Form::label('berat_set_awal', 'NG Settings: Berat Set Awal:') !!}
    <p>{{ $rencanaProduksi->berat_set_awal ?:"-" }} </p>
</div>


<div class="form-group col-sm-3">
    {!! Form::label('berat_set_awal', 'Target Setting:') !!}
    <p>{{ $rencanaProduksi->target_settings ?:"-" }} </p>
</div>

<div class="form-group col-sm-12 border-box-middle"> DETAIL TIPE </div>
<div class="form-group col-sm-3">
    {!! Form::label('nama', 'Nama Tipe:') !!}
    <p>{{ $rencanaProduksi->nama ?:"-" }} </p>
</div>

<!-- Jumlah Operator Field -->
<div class="form-group col-sm-3">
    {!! Form::label('jumlah_operator', 'Jumlah Operator:') !!}
    <p>{{ $rencanaProduksi->jumlah_operator ?:"-" }} </p>
</div>

<!-- Cycle Time Field -->
<div class="form-group col-sm-3">
    {!! Form::label('cycle_time', 'Cycle Time:') !!}
    <p>{{ $rencanaProduksi->cycle_time ?:"-" }} </p>
</div>

<!-- Durasi Dandon Field -->
<div class="form-group col-sm-3">
    {!! Form::label('durasi_dandon', 'Durasi Dandon:') !!}
    <p>{{ $rencanaProduksi->durasi_dandon ?:"-" }} </p>
</div>

<!-- Toleransi Field -->
<div class="form-group col-sm-3">
    {!! Form::label('toleransi', 'Toleransi:') !!}
    <p>{{ $rencanaProduksi->toleransi ?:"-" }} </p>
</div>

<!-- Durasi Produksi Field -->
<div class="form-group col-sm-3">
    {!! Form::label('durasi_produksi', 'Durasi Produksi:') !!}
    <p>{{ $rencanaProduksi->durasi_produksi ?:"-" }} </p>
</div>

<!-- Type Pasangan:nullable Field -->
<div class="form-group col-sm-3">
    {!! Form::label('type_pasangan', 'Tipe Pasangan:') !!}
    <p>{{ $rencanaProduksi->type_pasangan ?:"-" }} </p>
</div>


<!-- Created At Field -->
<div class="form-group col-sm-3">
    {!! Form::label('created_at', 'Dibuat:') !!}
    <p>{{ $rencanaProduksi->created_at->format('d/m/Y') ?:"-"}} </p>
</div>

<!-- Updated At Field -->
<div class="form-group col-sm-3">
    {!! Form::label('updated_at', 'Diubah:') !!}
    <p>{{ $rencanaProduksi->updated_at->format('d/m/Y') ?:"-" }} </p>
</div>

{{--
<div class="form-group col-sm-3">
    {!! Form::label('', 'List Tautan:') !!}
        @foreach($files as $file)
            {!! Form::open(['route' => ['rencana-produksi.deleteattachment', $file->id], 'method' => 'post']) !!}
                <a href="{{ asset('file/'.$file->name) }}" target="_blank">{{ $file->name }}</a>
                {!! Form::button('<i class="fa fa-times" aria-hidden="true"></i>', 
                    ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Apakah anda yakin?')"]) !!}
            {!! Form::close() !!}
        @endforeach
</div>

<div class="form-group col-sm-3">
    {!! Form::label('', 'Catatan:') !!}
    <p>{{ $rencanaProduksi->note }}</p>
</div>
--}}
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    <p><b>{{ strtoupper($rencanaProduksi->status) ?:"-" }} </b></p>
</div>

<!-- Updated At Field -->
<div class="form-group col-sm-12">
    <div class='btn-group rp-btn-group'>

        @if($rencanaProduksi->status == 'pending')
            {!! Form::open(['route' => ['update_status',  $rencanaProduksi->id]]) !!}        
                {!! Form::hidden('status', 'confirm') !!}
                {!! Form::button('CONFIRM ', ['type' => 'submit', 'class' => 'btn btn-info btn-xs table-button', 'onclick' => "return confirm('Apakah anda yakin?')"]) !!}
            {!! Form::close() !!}
        @endif

        @if($rencanaProduksi->status == 'pending' || $rencanaProduksi->status == 'confirm')
            {!! Form::open(['route' => ['rencana-produksi.destroy', $rencanaProduksi->id], 'method'=>'delete']) !!}        
                {!! Form::hidden('status', 'cancel') !!}
                {!! Form::button('<i class="fa fa-times" aria-hidden="true"></i> CANCEL', ['type' => 'submit', 'class' => 'btn btn-warning btn-xs table-button', 'onclick' => "return confirm('Rencana Produksi di yang di batalkan tidak dapat dikembalikan')"]) !!}
            {!! Form::close() !!}
        @endif

        @if($rencanaProduksi->status == 'pending' || $rencanaProduksi->status == 'confirm')
            <a href="{{ route('rencana-produksi.edit', [$rencanaProduksi->id]) }}" 
                class='btn btn-info btn-xs table-button'>EDIT</a>
        @endif


        @if($rencanaProduksi->status != 'finish')
            {!! Form::open(['route' => ['update_status',  $rencanaProduksi->id]]) !!}        
                {!! Form::hidden('status', 'aktual') !!}
                {!! Form::button('AKTUAL ', ['type' => 'submit', 'class' => 'btn btn-info btn-xs table-button', 'onclick' => "return confirm('Apakah anda yakin?')"]) !!}
            {!! Form::close() !!}

            {!! Form::open(['route' => ['update_status',  $rencanaProduksi->id]]) !!}        
                {!! Form::hidden('status', 'hold') !!}
                {!! Form::button('HOLD ', ['type' => 'submit', 'class' => 'btn btn-info btn-xs table-button', 'onclick' => "return confirm('Apakah anda yakin?')"]) !!}
            {!! Form::close() !!}
        @endif


        {!! Form::open(['route' => ['update_status',  $rencanaProduksi->id]]) !!}        
            {!! Form::hidden('status', 'finish') !!}
            {!! Form::button('FINISH ', ['type' => 'submit', 'class' => 'btn btn-info btn-xs table-button', 'onclick' => "return confirm('Apakah anda yakin?')"]) !!}
        {!! Form::close() !!}

        <a href="{{ route('rencana-produksi.cetak_spk', [$rencanaProduksi->id]) }}" 
                class='btn btn-info btn-xs table-button' target="_blank">PRINT SPK</a>
    </div>
</div>

<div class="form-group col-sm-12">
    <div style="margin-top: 16px;" id="divselesai" data-selesai="{{ $rencanaProduksi->selesai }}">
        @if($rencanaProduksi->status == 'finish')
            {!! Form::label('', 'Lanjut ke rencana produksi Berikutnya:') !!}
            <form action="{{ route('rencana-produksi.generateNext', [$rencanaProduksi->id]) }}" method="get" >
                @csrf
                <p>RP berikutnya akan dimulai sekitar: 
                    <input type="datetime-local" id="time" name="nextMulai"></input> 
                </p>
                <button type="submit" class='btn btn-danger btn-xs table-button'>NEXT</a>
            </form>
        @endif
    </div>
   
</div>


@section('scripts')
    <script type="text/javascript">
        var options = {
            day: '2-digit',
            month: '2-digit', 
            year: 'numeric', 
            hour: '2-digit', 
            minute: '2-digit', 
            second: '2-digit', 
            hour12: false,
            // second: '2-digit',
        };

        // mengubah waktu menjadi string dengan format tertentu
       
        var selesai = document.getElementById('divselesai').dataset.selesai;
        selesai = new Date(selesai);
        var diffInMs = selesai - Date.now();
        var diffInSeconds = Math.floor(diffInMs / 1000);

        var startTime = new Date();
        if (diffInSeconds > 0){
            startTime = new Date(selesai);
        }
        startTime.setMinutes(startTime.getMinutes() + 10);
        // var dateString = startTime.toLocaleString('en-US', options).replace(/,/g, '');
        var formatString = 'YYYY-MM-DDTHH:mm';
        var formattedDate = moment(startTime).format(formatString);

        var paragraf = document.getElementById('time');
        // console.log(paragraf)
		paragraf.value = formattedDate;
        // console.log("formattedDate", formattedDate)
        // $('input[id=time]').val(formattedDate);
    </script>
@endsection

