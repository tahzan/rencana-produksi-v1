@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Report Daily Stock
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($reportDailyStock, ['route' => ['report-daily-stock.update', $reportDailyStock->id], 'method' => 'patch']) !!}

                        @include('report_daily_stocks.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection