@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Report Daily Stock
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'report-daily-stock.store']) !!}

                        @include('report_daily_stocks.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
