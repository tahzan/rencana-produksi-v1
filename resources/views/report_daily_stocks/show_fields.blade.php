<!-- Type Id Field -->
<div class="form-group">
    {!! Form::label('type_id', 'Type Id:') !!}
    <p>{{ $reportDailyStock->type_id }}</p>
</div>

<!-- Daily Stok Field -->
<div class="form-group">
    {!! Form::label('daily_stok', 'Daily Stok:') !!}
    <p>{{ $reportDailyStock->daily_stok }}</p>
</div>

<!-- Out Standing Field -->
<div class="form-group">
    {!! Form::label('out_standing', 'Out Standing:') !!}
    <p>{{ $reportDailyStock->out_standing }}</p>
</div>

<!-- Sum Field -->
<div class="form-group">
    {!! Form::label('sum', 'Sum:') !!}
    <p>{{ $reportDailyStock->sum }}</p>
</div>

<!-- Ppic Id Field -->
<div class="form-group">
    {!! Form::label('ppic_id', 'Ppic Id:') !!}
    <p>{{ $reportDailyStock->ppic_id }}</p>
</div>

<!-- Tanggal Mulai Field -->
<div class="form-group">
    {!! Form::label('tanggal_mulai', 'Tanggal Mulai:') !!}
    <p>{{ $reportDailyStock->tanggal_mulai }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $reportDailyStock->status }}</p>
</div>

<!-- Tanggal Modif Field -->
<div class="form-group">
    {!! Form::label('tanggal_modif', 'Tanggal Modif:') !!}
    <p>{{ $reportDailyStock->tanggal_modif }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $reportDailyStock->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $reportDailyStock->updated_at }}</p>
</div>

