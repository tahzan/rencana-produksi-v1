@extends('layouts.app')

@section('content')
    <section class="content-header" style="display:flex; align-items: center">
        <h1 class="pull-left">Report Daily Stocks</h1>
        <h5 style="margin-left: 16px">
            Per-Tanggal: {{$tanggalstring}}
        </h5>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"></h3>
                <div class="box-tools">
                    <form action="" method="GET" class="search-apsc">
                        <!-- <input type="text" class="form-control filter-input" placeholder="Mesin" id='mesin'>
                        <input type="hidden" name="mesin_id" class="form-control filter-input" placeholder="Mesin" id='mesin_id'> --> 
                        <input type="date" name='tanggal' class="form-control filter-input" 
                            placeholder="Tanggal" id='tanggal' value="{{$tanggal}}" >
                        {!! Form::select('sum', [''=>'SUM', 'stok < 0' => 'stok < 0', 'stok = 0' => 'stok = 0', 'stok > 0' => 'stok > 0', ], 
                            $sum, ['class' => 'form-control filter-input']) !!}
                        {!! Form::select('outstanding', [''=>'Outstanding', 'outstanding > 0' => 'outstanding > 0', 'outstanding = 0' => 'outstanding = 0', ], 
                            $outstanding, ['class' => 'form-control filter-input']) !!}
                        {!! Form::select('active_type', 
                            $tipes, 
                            $active_type, 
                            ['class' => 'form-control filter-input']) 
                        !!} 
                        <div class="input-group-btn search-button-container">
                            <button type="submit" class="btn btn-info search-button"><i class="fa fa-search"></i></button>

                            <button type="button" 
                                data-url="{{ route('ppicController.close_ppic') }}"
                                class="btn btn-info search-button btn-close" style="margin-left: 8px; display: none" hide>Close PPIC</i></button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="box-body">
                @if($showMessage)
                <div class="text-center bg-pink" style="padding: 8px; margin: 16px 0;">{{$showMessage}}</div>
                @endif
                @if(Auth::user()->jabatan == 'sales')
                    @include('report_daily_stocks.table_sales')
                @else
                    @include('report_daily_stocks.table')
                @endif
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

@section('scripts')
  <script type="text/javascript">
    $(function() {
    
      const getCheckedCheckbox = () => {
        checkedValue = []
        $('.custom-control-input').each(function(i, obj) {
          if (obj.checked) {
            checkedValue.push(obj.getAttribute( "data" ))
          }
        });
        return checkedValue;
      }

      $('.custom-control-input').change(function() {
        let chechked = getCheckedCheckbox();
        if (chechked.length > 0)
            $(".btn-close").show();
        else 
            $(".btn-close").hide();
      });


      $('.btn-close').click(function (event) {
        let chechked = getCheckedCheckbox();
        if (confirm('Apakah anda yakin untuk mennyelesaikan transasksi ini?')) {
            $.ajax({
                url: $(this).data('url'),
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "ids": chechked
                },
                success: function (dd) {
                    window.location.reload();
                }
            });
        }
    });

    });
  </script>
@endsection