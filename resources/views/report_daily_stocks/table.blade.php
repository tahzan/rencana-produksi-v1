<div class="table-responsive">
    <table class="table" id="reportDailyStocks-table">
        <thead>
            <tr>
                <th></th>
                <th>Created</th>
                <th>Type</th>
                <th>Daily Stok</th>
                <th>Outstanding</th>
                <th>SUM</th>
                <th class="bg-pink">PPIC</th>
                <th class="bg-pink">Selesai Produksi</th>
                <th class="bg-pink">Status</th>
                <th class="bg-pink">Keterangan</th>
                <th class="bg-pink">Tanggal Modif</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($reportDailyStocks as $reportDailyStock)
            <tr>
                <td> 
                    <input type="checkbox" class="custom-control-input" data="{{ $reportDailyStock->id }}"  />
                </td>
                <td>{{ $reportDailyStock->created_at }}</td>
                <td>{{ $reportDailyStock->type->nama }}</td>
                <td>{{ $reportDailyStock->daily_stok ?? 0  }}</td>
                <td>{{ $reportDailyStock->out_standing }}</td>
                <td>
                    <a href="{{ route('outstandingItems.index')}}?type_id={{$reportDailyStock->type->id}}&status=open">
                        {{ $reportDailyStock->sum }}
                    </a>
                </td>

                @if($reportDailyStock->sum >= 0)

                <td class="bg-pink">-</td>
                <td class="bg-pink">-</td>
                <td class="bg-pink">-</td>
                <td class="bg-pink">-</td>
                <td class="bg-pink">-</td>
                <td>
                   
                </td>

                @else
                
                <td class="bg-pink">{{ $reportDailyStock->ppic ? $reportDailyStock->ppic->no_transaksi :"-" }} </td>
                <td class="bg-pink">{{ $reportDailyStock->ppic && $reportDailyStock->ppic->tanggal_produksi ? $reportDailyStock->ppic->tanggal_produksi->format('d/m/Y') :"-" }}</td>

                <td class="bg-pink">
                    {{ $reportDailyStock->ppic ? $reportDailyStock->ppic->status :"Waiting Response" }}
                </td>
                <td class="bg-pink">{{ $reportDailyStock->ppic ? Str::limit($reportDailyStock->ppic->keterangan, 20) :"-" }}</td>
                <td class="bg-pink">{{ $reportDailyStock->ppic ? $reportDailyStock->ppic->last_update :"-" }}</td>
                <td>
                    <div class='btn-group'>
                   
                    @if($reportDailyStock->ppic_id )
                        <a href="{{ route('ppics.show', [$reportDailyStock->ppic_id]) }}" 
                            class='btn btn-info btn-xs table-button'><i class="fa fa-eye" aria-hidden="true"></i></a>
                            @if(Auth::user()->jabatan !== 'purchasing')
                            <a href="{{ route('ppics.edit', [$reportDailyStock->ppic_id]) }}?report_id={{$reportDailyStock->id}}&type_id={{ $reportDailyStock->type->id }}&outstanding={{ $reportDailyStock->sum }}&type_name={{$reportDailyStock->type->nama}}" 
                                class='btn btn-info btn-xs table-button'><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            @endif
                    @else
                        <a href="{{ route('ppics.create') }}?report_id={{$reportDailyStock->id}}&type_id={{ $reportDailyStock->type->id }}&outstanding={{ $reportDailyStock->sum }}&type_name={{$reportDailyStock->type->nama}}" 
                            class='btn btn-info btn-xs table-button'><i class="fa fa-plus" aria-hidden="true"></i></a>
                    @endif
                
                    <!-- {!! Form::open(['route' => ['report-daily-stock.destroy', $reportDailyStock->id], 'method' => 'delete']) !!}
                         {!! Form::button('<i class="fa fa-times" aria-hidden="true"></i>', ['type' => 'submit', 'class' => 'btn btn-warning btn-xs table-button', 'onclick' => "return confirm('Apakah anda yakin?')"]) !!}
                    {!! Form::close() !!}  -->
                    </div>
                </td>
                @endif

            </tr>
        @endforeach
        </tbody>
    </table>
</div>


