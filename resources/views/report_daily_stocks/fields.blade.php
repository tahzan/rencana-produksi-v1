<!-- Type Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type_id', 'Type Id:') !!}
    {!! Form::number('type_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Daily Stok Field -->
<div class="form-group col-sm-6">
    {!! Form::label('daily_stok', 'Daily Stok:') !!}
    {!! Form::number('daily_stok', null, ['class' => 'form-control']) !!}
</div>

<!-- Out Standing Field -->
<div class="form-group col-sm-6">
    {!! Form::label('out_standing', 'Out Standing:') !!}
    {!! Form::number('out_standing', null, ['class' => 'form-control']) !!}
</div>

<!-- Sum Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sum', 'Sum:') !!}
    {!! Form::number('sum', null, ['class' => 'form-control']) !!}
</div>

<!-- Ppic Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ppic_id', 'Ppic Id:') !!}
    {!! Form::number('ppic_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Tanggal Mulai Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tanggal_mulai', 'Tanggal Mulai:') !!}
    {!! Form::date('tanggal_mulai', null, ['class' => 'form-control','id'=>'tanggal_mulai']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#tanggal_mulai').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::select('status', ['Waiting Response' => 'Waiting Response', 'On Schedule' => 'On Schedule', 'failed' => 'failed', 'closed' => 'closed'], null, ['class' => 'form-control']) !!}
</div>

<!-- Tanggal Modif Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tanggal_modif', 'Tanggal Modif:') !!}
    {!! Form::date('tanggal_modif', null, ['class' => 'form-control','id'=>'tanggal_modif']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#tanggal_modif').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('report-daily-stock.index') }}" class="btn btn-default">Cancel</a>
</div>
