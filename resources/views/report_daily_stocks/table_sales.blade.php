<div class="table-responsive">
    <table class="table" id="reportDailyStocks-table">
        <thead>
            <tr>
                <th></th>
                <th>Type</th>
                <th>Daily Stok</th>
                <th>Outstanding</th>
                <th>SUM</th>
                <th class="bg-pink">Selesai Produksi</th>
                <th class="bg-pink">Status</th>
            </tr>
        </thead>
        <tbody>
        @foreach($reportDailyStocks as $reportDailyStock)
            <tr>
                <td> 
                    <input type="checkbox" class="custom-control-input" data="{{ $reportDailyStock->id }}"  />
                </td>
                <td>{{ $reportDailyStock->type->nama }}</td>
                <td>{{ $reportDailyStock->daily_stok ?? 0  }}</td>
                <td>{{ $reportDailyStock->out_standing }}</td>
                <td>
                    <a href="{{ route('outstandingItems.index')}}?type_id={{$reportDailyStock->type->id}}&status=open">
                        {{ $reportDailyStock->sum }}
                    </a>
                </td>

                @if($reportDailyStock->sum >= 0)
                    <td class="bg-pink">-</td>
                    <td class="bg-pink">-</td>
                @else
                    <td class="bg-pink">{{ $reportDailyStock->ppic && $reportDailyStock->ppic->tanggal_produksi ? $reportDailyStock->ppic->tanggal_produksi->format('d/m/Y') :"-" }}</td>
                    <td class="bg-pink">
                        {{ $reportDailyStock->ppic ? $reportDailyStock->ppic->status :"Waiting Response" }}
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
    </table>
</div>


