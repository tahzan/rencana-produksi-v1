<div class="table-responsive">
    <table class="table table-history data" id="rencanaProduksisCache-table">
        <thead>
            <tr>
                <th>Tanggal Diubah</th>
                <th>No Transaksi</th>
                <th>Mulai</th>
                <th>Selesai</th>
                <th>Shift</th>
                <th>Kategori Shift</th>
                <th>Mesin</th>
                <th>Urutan</th>
                <th>Tipe</th>
                <th>Status</th>
                <th>Jumlah Operator</th>
                <th>Cycle Time</th>
                <th>Durasi Dandon</th>
                <th>Toleransi</th>
                <th>Durasi Produksi</th>
                <th>Tipe Pasangan</th>
                <th>Rencana Produksi</th> 
                <th>Durasi Kerja</th>
                <th>Estimasi Total Shift</th>
                <th>Aktual Produksi</th>
                <th>Persentasi Aktual</th>
                <th>Persentasi Cycle Time</th>
                <th>User</th>
            </tr>
        </thead>
        <tbody>
        @foreach($rencanaProduksisCache as $rencanaProduksi)
            <tr>
                <td>{{ $rencanaProduksi->created_at->format('d-m-Y H:i') }}</td>
                <td><a href="{{ route('rencana-produksi.show', [ $rencanaProduksi->rencana_proudksi_id ]) }}" class='table-link'>{{ $rencanaProduksi->nomor_transaksi }}</a></td>
                <td>{{ isset($rencanaProduksi->mulai)  ? $rencanaProduksi->mulai->format('d/m/y H:i') : "-"}}</td>
                <td>{{ isset($rencanaProduksi->selesai) ? $rencanaProduksi->selesai->format('d/m/y H:i') : '-' }}</td>
                <td>{{ $rencanaProduksi->shift }}</td>
                <td>{{ $rencanaProduksi->shift_category }}</td>
                <td>{{ $rencanaProduksi->mesin->nama }}</td>
                <td>{{ $rencanaProduksi->urutan }}</td>
                <td>{{ $rencanaProduksi->nama }}</td>
                <td>{{ $rencanaProduksi->status }}</td>
                <td>{{ $rencanaProduksi->jumlah_operator }}</td>
                <td>{{ $rencanaProduksi->cycle_time }}</td>
                <td>{{ $rencanaProduksi->durasi_dandon }}</td>
                <td>{{ $rencanaProduksi->toleransi }}</td>
                <td>{{ $rencanaProduksi->durasi_produksi }}</td>
                <td>{{ $rencanaProduksi->type_pasangan }}</td>
                <td>{{ $rencanaProduksi->jumlah_rencana_produksi }}</td>
                <td>{{ $rencanaProduksi->estimasi_durasi }}</td>
                <td>{{ $rencanaProduksi->estimasi_total_shift }}</td>
                <td>{{ $rencanaProduksi->jumlah_aktual_produksi }}</td>
                <td>{{ $rencanaProduksi->persentasi_aktual }}</td>
                <td>{{ $rencanaProduksi->persentasi_cycle_time }}</td>
                <td>{{ $rencanaProduksi->user ? $rencanaProduksi->user->name :"-" }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
