@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Data Perubahan Rencana Produksi</h1>
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{ route('rencana-produksi.create') }}">Tambah Data</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"></h3>
                <div class="box-tools">
                    <form action="" method="GET" class="search-apsc">
                        <!-- <input type="text" class="form-control filter-input" placeholder="Mesin" id='mesin'>
                        <input type="hidden" name="mesin_id" class="form-control filter-input" placeholder="Mesin" id='mesin_id'> --> 
                        <input type="text" name='tanggal' class="form-control filter-input" placeholder="Tanggal" id='tanggal' >
                        {!! Form::select('active_type', 
                            $tipes, 
                            $active_type, 
                            ['class' => 'form-control filter-input']) 
                        !!} 
                        <div class="input-group-btn search-button-container">
                            <button type="submit" class="btn btn-info search-button"><i class="fa fa-search"></i></button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="box-body">
                @include('rencana_produksi_cache.table')
            </div>

            <div class="box-footer clearfix">
                {{ $rencanaProduksisCache->links() }}
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">

        $('#tanggal').datetimepicker({
            format: 'DD-MM-YYYY',
            useCurrent: true,
            locale: 'id'
        })

        var path = "{{ url('/mesin/cari') }}";
        $input = $('#mesin');
        $input.typeahead({
            minLength: 0,
            items: 10,
            source:  function (query, process) {
                return $.get(path, { q: query }, function (data) {
                    return process(data);
                });
            },
            autoSelect: true,
            displayText: function(item){ return `${item.nama} `;}
        });
        $input.change(function() {
            var current = $input.typeahead("getActive");
            $('input[id=mesin_id]').val(current.id);
        });

    </script>
@endsection
