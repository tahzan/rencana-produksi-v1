<?php

Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
]);


Route::get('/', 'HomeController@index'); 

// bisa di arahakan ke front end sebenarnya
// Route::get('/', function () {
//     return view('home');
// });
    
Route::group(['middleware' => 'auth'], function(){
    Route::get('/mesin/cari', 'mesinController@search');
    Route::resource('mesin', 'mesinController');
    Route::post('/tipe/import_excel', 'tipeController@import_excel')->name('tipe.import_excel');
    Route::get('/tipe/cari', 'tipeController@search');
    Route::post('/tipe/hapus-semua', 'tipeController@hapusSemua')->name('tipe.hapusSemua'); 
    Route::resource('tipe', 'tipeController');
    Route::get('/jadwal-mesin/grup', 'jadwal_mesinController@grup');
    Route::get('/jadwal-mesin/cari-tanggal', 'jadwal_mesinController@cariTanggal');
    Route::resource('jadwal-mesin', 'jadwal_mesinController');
    Route::get('/rencana-produksi-live', 'rencana_produksiController@live')->name('rencana-produksi.live'); 
    Route::get('/rencana-produksi-live-export', 'rencana_produksiController@live_export')->name('rencana-produksi.live-export'); 
    Route::get('/rencana-produksi-live-info', 'rencana_produksiController@live_info')->name('rencana-produksi.live-info'); 
    Route::get('/rencana-produksi/cetak-laporan', 'rencana_produksiController@cetak')->name('cetak-laporan'); 
    Route::get('/rencana-produksi/export-csv', 'rencana_produksiController@exportcsv')->name('export-csv'); 
    Route::get('/rencana-produksi/cetak-spk/{id}', 'rencana_produksiController@cetak_spk')->name('rencana-produksi.cetak_spk'); 
    Route::post('/rencana-produksi/updatestatus/{id}', 'rencana_produksiController@updateStatus')->name('update_status');
    Route::get('/rencana-produksi/generate-next/{id}', 'rencana_produksiController@generateNext')->name('rencana-produksi.generateNext'); 
    Route::post('/rencana-produksi/upload', 'rencana_produksiController@upload')->name('rencana-produksi.upload');
    Route::post('/rencana-produksi/deleteattachment/{id}', 'rencana_produksiController@deleteattachment')->name('rencana-produksi.deleteattachment');
    Route::post('/rencana-produksi/addnote', 'rencana_produksiController@addnote')->name('rencana-produksi.addnote');
   
    Route::get('/rencana-produksi/{id}/edit-ng', 'rencana_produksiController@editNG')->name('rencana-produksi.edit_ng');
    Route::post('/rencana-produksi/update-ng/{id}', 'rencana_produksiController@updateNG')->name('rencana-produksi.update_ng');
    Route::get('/rencana-produksi/backup-button}', 'rencana_produksiController@backupButton')->name('rencana-produksi.backup_button');
    Route::post('/rencana-produksi/backup-button-post', 'rencana_produksiController@backupButtonPost')->name('rencana-produksi.bakcup_button_post');

    Route::post('/jadwal/hapus-semua', 'istirahatMesinController@hapusSemua')->name('jadwal-istirahat.hapusSemua'); 
    Route::post('/jadwal-istirahat/import_excel', 'istirahatMesinController@import_excel')->name('jadwal-istirahat.import_excel'); 
    Route::post('/jadwal-istirahat/generate', 'istirahatMesinController@generate')->name('jadwal-istirahat.generate'); 
    Route::resource('jadwal-istirahat', 'istirahatMesinController');
    Route::resource('rencana-produksi', 'rencana_produksiController');
    Route::resource('draft-rencana-produksi', 'rencana_produksiControllerDraft');
    Route::resource('perubahan-rencana-produksi', 'rencana_produksiControllerCache');
    Route::get('/laporan/cetak-laporan-injection', 'laporanController@cetak')->name('cetaklaporaninjection'); 
    Route::resource('laporan', 'laporanController');
    Route::resource('shifts', 'shiftController'); 
    Route::post('/shifts/upload-attachment', 'shiftController@upload')->name('shift.upload-attachment');
    Route::post('/shifts/delete-attachment/{id}', 'shiftController@deleteattachment')->name('shifts.delete-attachment');

    Route::resource('users', 'UserController')->middleware('auth');
    Route::resource('shiftSettings', 'shift_settingController');
    Route::resource('infos', 'infoController');
    Route::resource('listShifts', 'list_shiftController');

    Route::post('/outstandings/updatestatus/{id}', 'outstandingController@updateStatus')->name('outstandings.update_status');
    Route::post('/outstandings/import_excel', 'outstandingController@import_excel')->name('outstandings.import_excel');
    Route::resource('outstandings', 'outstandingController');

    Route::post('/outstandingItems/import_excel', 'outstanding_itemController@import_excel')->name('outstandingItems.import_excel');
    Route::resource('outstandingItems', 'outstanding_itemController');
    
    Route::post('/ppics/close-ppic', 'ppicController@close_ppic')->name('ppicController.close_ppic');
    Route::resource('ppics', 'ppicController');
    Route::resource('ppicLogs', 'ppic_logController');
    Route::resource('ppicComments', 'ppic_commentController');
    Route::resource('report-daily-stock', 'report_daily_stockController');

    Route::post('/daily-stocks/updatestatus/{id}', 'daily_stockController@updateStatus')->name('daily-stocks.update_status');
    Route::resource('daily-stocks', 'daily_stockController');

    Route::get('/notifications/updatestatus/{id}', 'notificationController@updateStatus')->name('notifications.update_status');
    Route::resource('notifications', 'notificationController');
    
    Route::post('/daily-stock-item/import_excel', 'daily_stock_itemController@import_excel')->name('daily-stock-item.import_excel');
    Route::resource('dailyStockItems', 'daily_stock_itemController');

    Route::get('/laporan/cetak-laporan-injection', 'laporanController@cetak')->name('cetaklaporaninjection'); 
    Route::resource('attachments', 'attachmentsController');

});

Route::group(['middleware' => 'App\Http\Middleware\SuperVisorMiddleWare'], function(){
    Route::resource('users', 'UserController')->middleware('auth');
});

?>

















