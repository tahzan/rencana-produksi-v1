<?php

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('set-count', [ApiController::class, 'set_count'])->name('set-count');
Route::post('broken', [ApiController::class, 'set_rusak'])->name('set_rusak');
Route::get('newnotif', [ApiController::class, 'newnotif'])->name('newnotif');
