-- MySQL dump 10.13  Distrib 8.0.22, for macos10.15 (x86_64)
--
-- Host: localhost    Database: rencana-produksi
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `infos`
--

DROP TABLE IF EXISTS `infos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `infos` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `info` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` datetime NOT NULL,
  `is_active` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `end_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `infos`
--

LOCK TABLES `infos` WRITE;
/*!40000 ALTER TABLE `infos` DISABLE KEYS */;
INSERT INTO `infos` VALUES (3,'<p><img src=\"https://www.fujifilm.com/products/digital_cameras/x/fujifilm_x_t1/sample_images/img/index/pic_01.jpg\" width=\"317\" height=\"211\" /></p>','2021-08-11 00:52:56','2021-08-11 01:08:55','2021-08-11 01:08:55','sdfsdfsd','2021-08-11 09:05:00','N',NULL,'0000-00-00 00:00:00'),(4,'sdfsdfsd','2021-08-11 01:09:03','2021-08-11 01:24:24','2021-08-11 01:24:24','Offline Event','2021-08-29 08:24:00','Y',NULL,'0000-00-00 00:00:00'),(5,'ssss','2021-08-11 01:24:41','2021-08-11 03:30:19','2021-08-11 03:30:19','sss','2021-08-05 08:26:00','Y',NULL,'0000-00-00 00:00:00'),(6,'<div style=\"max-width: 650px;\" data-ephox-embed-iri=\"https://images.unsplash.com/photo-1579353977828-2a4eab540b9a?ixlib=rb-1.2.1&amp;ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&amp;auto=format&amp;fit=crop&amp;w=667&amp;q=80\"><img src=\"https://images.unsplash.com/photo-1579353977828-2a4eab540b9a?ixlib=rb-1.2.1&amp;ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&amp;auto=format&amp;fit=crop&amp;w=667&amp;q=80\" /></div>','2021-08-11 03:34:41','2021-08-12 23:38:08','2021-08-12 23:38:08','sss','2021-08-11 00:00:00','Y',NULL,'0000-00-00 00:00:00'),(7,'<p>vvvv</p>\r\n<p>&nbsp;</p>\r\n<div style=\"max-width: 650px;\" data-ephox-embed-iri=\"https://www.fujifilm.com/products/digital_cameras/x/fujifilm_x_t1/sample_images/img/index/pic_01.jpg\"><img src=\"https://www.fujifilm.com/products/digital_cameras/x/fujifilm_x_t1/sample_images/img/index/pic_01.jpg\" /></div>','2021-08-11 03:40:11','2021-08-12 23:38:10','2021-08-12 23:38:10','vvvv','2021-08-11 00:40:00','Y',NULL,'0000-00-00 00:00:00'),(8,'<p>ssss</p>','2021-08-11 03:40:27','2021-08-12 23:38:12','2021-08-12 23:38:12','Offline Event','2021-08-11 09:40:00','N',NULL,'0000-00-00 00:00:00'),(9,'<h3 style=\"text-align: center;\"><strong>UJIAN NASIONAL AKAN DILAKSANAKAN PADA</strong></h3>\r\n<h3 style=\"text-align: center;\"><strong>TANGGAL 20 AGUSTUS 2020</strong></h3>','2021-08-12 23:38:41','2021-08-14 04:20:01','2021-08-14 04:20:01','Test File','1970-01-01 07:00:00','Y','1628811756_111.jpeg','2021-08-14 21:05:00'),(10,'<p>dsdfsdf</p>','2021-08-13 14:31:19','2021-08-14 04:20:03','2021-08-14 04:20:03','Info 2','2021-08-12 21:30:00','Y',NULL,'2021-08-14 21:31:00');
/*!40000 ALTER TABLE `infos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jadwal_mesins`
--

DROP TABLE IF EXISTS `jadwal_mesins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jadwal_mesins` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `mulai` datetime NOT NULL,
  `selesai` datetime NOT NULL,
  `perencanaan_id` int DEFAULT NULL,
  `mesin_id` int DEFAULT NULL,
  `tipe` enum('dandon','kerja','istirahat') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `durasi` int NOT NULL,
  `auto_generated` enum('N','Y') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jadwal_mesins`
--

LOCK TABLES `jadwal_mesins` WRITE;
/*!40000 ALTER TABLE `jadwal_mesins` DISABLE KEYS */;
INSERT INTO `jadwal_mesins` VALUES (81,'2021-09-30 07:55:00','2021-09-30 08:05:00',23,1,'dandon','2021-09-28 17:56:33','2021-09-28 17:56:33',NULL,600,'N'),(82,'2021-09-30 08:05:00','2021-10-01 20:45:00',23,1,'kerja','2021-09-28 17:56:33','2021-09-28 17:56:33',NULL,132000,'N');
/*!40000 ALTER TABLE `jadwal_mesins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `list_shifts`
--

DROP TABLE IF EXISTS `list_shifts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `list_shifts` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `shift` int NOT NULL,
  `mulai` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `selesai` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cross_day` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `list_shifts`
--

LOCK TABLES `list_shifts` WRITE;
/*!40000 ALTER TABLE `list_shifts` DISABLE KEYS */;
INSERT INTO `list_shifts` VALUES (1,1,'07','15','N','2021-09-25 02:23:03','2021-09-28 01:06:37',NULL),(2,2,'15','23','N','2021-09-25 02:23:23','2021-09-28 01:06:44',NULL),(3,3,'23','07','Y','2021-09-25 02:23:48','2021-09-27 00:38:17','2021-09-27 00:38:17'),(4,3,'23','7','Y','2021-09-28 01:06:54','2021-09-28 01:06:54',NULL);
/*!40000 ALTER TABLE `list_shifts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mesins`
--

DROP TABLE IF EXISTS `mesins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mesins` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `tipe` enum('Injeksi','Non Injeksi') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `rusak` enum('Y','N') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mesins`
--

LOCK TABLES `mesins` WRITE;
/*!40000 ALTER TABLE `mesins` DISABLE KEYS */;
INSERT INTO `mesins` VALUES (1,'Mesin1','Mesin1','2020-06-14 19:36:35','2020-10-11 02:35:17',NULL,'Injeksi','N'),(2,'Mesin2','Mesin2','2020-06-14 19:36:43','2020-10-04 04:07:40',NULL,'Injeksi','N'),(3,'Mesin-3','Mesin 3','2021-08-08 04:13:04','2021-08-08 04:13:04',NULL,'Injeksi','N');
/*!40000 ALTER TABLE `mesins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2020_01_10_231223_create_mesins_table',1),(5,'2020_01_11_070053_create_tipes_table',1),(6,'2020_01_11_093710_create_rencana_produksis_table',2),(7,'2020_01_17_150449_create_rencana_produksi_caches_table',2),(8,'2020_01_18_103750_create_jadwal_mesins_table',2),(9,'2020_01_18_141650_add_duration_jadwal_mesin',2),(10,'2020_01_23_142333_add_mulai_selesai_rencana_produksi',2),(11,'2020_01_23_143303_add_mulai_selesai_rencana_produksi_caches',2),(12,'2020_01_25_101412_create_shifts_table',2),(13,'2020_01_26_082037_add_shift_selesai_rencana_produksi',2),(14,'2020_01_26_121901_shift_nullable',2),(15,'2020_02_01_010553_add_mulai_selesai_shift',2),(16,'2020_02_02_005420_change_estimasi_durasi_rp',2),(17,'2020_02_02_165805_add__status_rencana_produksi',2),(18,'2020_02_02_205219_add_status_to_rp_cache',2),(19,'2020_02_02_234714_add_jabatan_to_users',2),(20,'2020_02_04_062136_add_to_rencana_produksi_cache',2),(21,'2020_02_14_214131_add_tipe_rencana_produksi_id',2),(22,'2020_02_15_101239_add_barang_lain_rencana_produksi',2),(23,'2020_03_16_071209_add_satuan_rencana_produksi',2),(24,'2020_03_19_065811_add_user_to__users',2),(25,'2020_03_19_082203_add_tipe_to_mesin',2),(26,'2020_03_26_074808_generate_field_to_jadwal_mesins',2),(27,'2020_06_15_014154_add_berat_to_types',2),(28,'2020_06_15_213604_add_fields_to_rencanaproduksi',3),(29,'2020_06_17_071628_add_field_to_rencanaproduksis',4),(30,'2020_07_15_071554_create_shift_settings_table',5),(31,'2020_09_19_151048_acc_count',6),(32,'2020_10_04_104318_add_rusak',7),(33,'2020_10_27_213315_add_last_hint',8),(34,'2021_08_08_120718_create_infos_table',9),(35,'2021_08_11_065513_add_last_hint',10),(36,'2021_08_13_062955_add_file',11),(37,'2021_08_13_202658_end_date',12),(38,'2021_09_23_211033_create_list_shifts_table',13),(39,'2021_09_25_084021_create_list_shifts_table',14);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rencana_produksi_caches`
--

DROP TABLE IF EXISTS `rencana_produksi_caches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rencana_produksi_caches` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `nomor_transaksi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shift` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mesin_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `urutan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jumlah_operator` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cycle_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `durasi_dandon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `toleransi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `durasi_produksi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_pasangan:nullable` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jumlah_rencana_produksi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estimasi_durasi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estimasi_total_shift` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jumlah_aktual_produksi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `persentasi_aktual` double(8,2) DEFAULT NULL,
  `persentasi_cycle_time` double(8,2) DEFAULT NULL,
  `rencana_proudksi_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `mulai` datetime DEFAULT NULL,
  `selesai` datetime DEFAULT NULL,
  `status` enum('pending','confirm','cancel','aktual') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `satuan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rencana_produksi_caches`
--

LOCK TABLES `rencana_produksi_caches` WRITE;
/*!40000 ALTER TABLE `rencana_produksi_caches` DISABLE KEYS */;
INSERT INTO `rencana_produksi_caches` VALUES (80,'2109-0001','1','1','Mesin1-2109:001','3','spakbor','3','5','10','10','66',NULL,'10000','132600','4.6',NULL,NULL,NULL,'23','2021-09-28 17:56:33','2021-09-28 17:56:33',NULL,'2021-09-30 07:55:00','2021-10-01 20:45:00','confirm',7,'pcs');
/*!40000 ALTER TABLE `rencana_produksi_caches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rencana_produksis`
--

DROP TABLE IF EXISTS `rencana_produksis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rencana_produksis` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `nomor_transaksi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shift` int DEFAULT NULL,
  `mesin_id` int NOT NULL,
  `urutan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_id` int NOT NULL,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jumlah_operator` int DEFAULT NULL,
  `cycle_time` decimal(8,2) DEFAULT NULL,
  `durasi_dandon` int DEFAULT NULL,
  `toleransi` int DEFAULT NULL,
  `durasi_produksi` int DEFAULT NULL,
  `type_pasangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jumlah_rencana_produksi` int NOT NULL,
  `estimasi_durasi` int DEFAULT NULL,
  `estimasi_total_shift` double(8,2) NOT NULL,
  `jumlah_aktual_produksi` int DEFAULT NULL,
  `persentasi_aktual` double(8,2) DEFAULT NULL,
  `persentasi_cycle_time` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `mulai` datetime DEFAULT NULL,
  `selesai` datetime DEFAULT NULL,
  `shift_selesai` int DEFAULT NULL,
  `status` enum('pending','confirm','cancel','aktual') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barang_lain` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `satuan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `operator` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `berat_ng_produksi` decimal(8,2) DEFAULT NULL,
  `berat_set_awal` decimal(8,2) DEFAULT NULL,
  `target_settings` decimal(8,2) DEFAULT NULL,
  `berat_per_pcs` decimal(6,2) DEFAULT NULL,
  `last_hint` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rencana_produksis`
--

LOCK TABLES `rencana_produksis` WRITE;
/*!40000 ALTER TABLE `rencana_produksis` DISABLE KEYS */;
INSERT INTO `rencana_produksis` VALUES (23,'2109-0001',1,1,'Mesin1-2109:001',3,'spakbor',3,5.00,10,10,66,NULL,10000,132600,4.60,NULL,NULL,NULL,'2021-09-28 17:56:33','2021-09-28 17:56:33',NULL,'2021-09-30 07:55:00','2021-10-01 20:45:00',2,'confirm',NULL,'pcs',NULL,NULL,NULL,NULL,120.00,NULL);
/*!40000 ALTER TABLE `rencana_produksis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shift_settings`
--

DROP TABLE IF EXISTS `shift_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `shift_settings` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` date NOT NULL,
  `shift_1` int DEFAULT NULL,
  `shift_2` int DEFAULT NULL,
  `shift_3` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shift_settings`
--

LOCK TABLES `shift_settings` WRITE;
/*!40000 ALTER TABLE `shift_settings` DISABLE KEYS */;
INSERT INTO `shift_settings` VALUES (1,'2020-07-15',2,3,4,'2020-07-15 00:21:15','2020-07-15 00:24:42','2020-07-15 00:24:42'),(2,'2020-07-25',10,10,10,'2020-07-15 00:25:34','2020-07-21 01:30:29',NULL),(3,'2020-07-26',10,10,10,'2020-07-20 01:59:59','2020-07-21 01:30:40',NULL),(4,'2020-07-29',3,3,3,'2020-07-25 01:28:22','2020-07-25 01:28:22',NULL),(5,'2020-07-28',5,5,5,'2020-07-26 05:29:35','2020-07-26 05:29:35',NULL),(6,'2020-07-27',5,5,5,'2020-07-26 05:35:25','2020-07-26 05:35:25',NULL);
/*!40000 ALTER TABLE `shift_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shifts`
--

DROP TABLE IF EXISTS `shifts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `shifts` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `shift` int DEFAULT NULL,
  `durasi` int DEFAULT NULL,
  `estimasi_hasil` int DEFAULT NULL,
  `aktual_hasil` int DEFAULT NULL,
  `rencana_produksi_id` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `mulai` datetime DEFAULT NULL,
  `selesai` datetime DEFAULT NULL,
  `gangguan__` int DEFAULT NULL,
  `gangguan_sett` int DEFAULT NULL,
  `gangguan_mld` int DEFAULT NULL,
  `gangguan_bhn` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=616 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shifts`
--

LOCK TABLES `shifts` WRITE;
/*!40000 ALTER TABLE `shifts` DISABLE KEYS */;
INSERT INTO `shifts` VALUES (611,NULL,1,24900,1886,0,23,'2021-09-28 17:56:33','2021-09-28 17:56:33',NULL,'2021-09-30 08:05:00','2021-09-30 15:00:00',NULL,NULL,NULL,NULL),(612,NULL,2,28800,2182,0,23,'2021-09-28 17:56:33','2021-09-28 17:56:33',NULL,'2021-09-30 15:00:00','2021-09-30 23:00:00',NULL,NULL,NULL,NULL),(613,NULL,3,28800,2182,0,23,'2021-09-28 17:56:33','2021-09-28 17:56:33',NULL,'2021-09-30 23:00:00','2021-10-01 07:00:00',NULL,NULL,NULL,NULL),(614,NULL,1,28800,2182,0,23,'2021-09-28 17:56:33','2021-09-28 17:56:33',NULL,'2021-10-01 07:00:00','2021-10-01 15:00:00',NULL,NULL,NULL,NULL),(615,NULL,2,20700,1568,0,23,'2021-09-28 17:56:33','2021-09-28 17:56:33',NULL,'2021-10-01 15:00:00','2021-10-01 20:45:00',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `shifts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipes`
--

DROP TABLE IF EXISTS `tipes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipes` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah_operator` int NOT NULL,
  `cycle_time` decimal(8,2) NOT NULL,
  `durasi_dandon` int NOT NULL,
  `toleransi` int NOT NULL,
  `durasi_produksi` int DEFAULT NULL,
  `kombinasi` enum('Ya','Tidak') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `barang_lain` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_pasangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `barang_lain_id` int DEFAULT NULL,
  `berat` decimal(6,2) NOT NULL,
  `count` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipes`
--

LOCK TABLES `tipes` WRITE;
/*!40000 ALTER TABLE `tipes` DISABLE KEYS */;
INSERT INTO `tipes` VALUES (1,'Spion',3,5.00,3,10,66,'Ya','Spion.R','Pasangan Sepion','2020-06-14 19:42:51','2020-10-27 15:39:37',NULL,NULL,4.65,2),(3,'spakbor',3,5.00,10,10,66,'Ya',NULL,NULL,'2020-06-17 23:31:43','2020-09-19 08:29:16',NULL,NULL,120.00,3);
/*!40000 ALTER TABLE `tipes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `jabatan` enum('admin','supervisor') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (7,'admin','admin@gmail.com',NULL,'$2y$10$Rw0AS2n3NplmJ39MpXZ8EOdYYuVx/lEJBNawfewVscVmNfzTZhd.G',NULL,'2020-06-14 19:35:33','2020-06-14 19:35:33','supervisor','admin'),(8,'live','live@gmail.com',NULL,'$2y$10$BHs4v0Kss1HE0kBVYcMZA.ph6K3ieBiZ9PC4vgcMc0iSjgyOcU.3u',NULL,'2021-01-11 01:51:25','2021-01-11 01:51:25','','live');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-29  7:30:08
