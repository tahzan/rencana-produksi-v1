<?php
// use DateTime;

if (! function_exists('convert_to_time')) {
    function convert_to_time($sec){
        $date1 = new DateTime("@0");
        $date2 = new DateTime("@$sec");
        $interval = date_diff($date1, $date2);
        $parts = ['tahun ' => 'y', 'bulan ' => 'm', 'hari ' => 'd', 'jam ' => 'h', 'menit ' => 'i', 'detik ' => 's'];
        $formatted = [];
        foreach($parts as $i => $part)
        {
            $value = $interval->$part;
            if ($value !== 0)
            {
                if ($value == 1){
                    $i = substr($i, 0, -1);
                }
                $formatted[] = "$value $i";
            }
        }

        if (count($formatted) == 1){
            return $formatted[0];
        } else {
            $str = implode(' ', array_slice($formatted, 0, -1));
            $str.= ' ' . $formatted[count($formatted) - 1];
            return $str;
        }
    }
}