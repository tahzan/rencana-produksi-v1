<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createshift_settingRequest;
use App\Http\Requests\Updateshift_settingRequest;
use App\Repositories\shift_settingRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class shift_settingController extends AppBaseController
{
    /** @var  shift_settingRepository */
    private $shiftSettingRepository;

    public function __construct(shift_settingRepository $shiftSettingRepo)
    {
        $this->shiftSettingRepository = $shiftSettingRepo;
    }

    /**
     * Display a listing of the shift_setting.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $shiftSettings = $this->shiftSettingRepository->all();

        return view('shift_settings.index')
            ->with('shiftSettings', $shiftSettings);
    }

    /**
     * Show the form for creating a new shift_setting.
     *
     * @return Response
     */
    public function create()
    {
        return view('shift_settings.create');
    }

    /**
     * Store a newly created shift_setting in storage.
     *
     * @param Createshift_settingRequest $request
     *
     * @return Response
     */
    public function store(Createshift_settingRequest $request)
    {
        $input = $request->all();

        $shiftSetting = $this->shiftSettingRepository->create($input);

        Flash::success('Shift Setting saved successfully.');

        return redirect(route('shiftSettings.index'));
    }

    /**
     * Display the specified shift_setting.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $shiftSetting = $this->shiftSettingRepository->find($id);

        if (empty($shiftSetting)) {
            Flash::error('Shift Setting not found');

            return redirect(route('shiftSettings.index'));
        }

        return view('shift_settings.show')->with('shiftSetting', $shiftSetting);
    }

    /**
     * Show the form for editing the specified shift_setting.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $shiftSetting = $this->shiftSettingRepository->find($id);

        if (empty($shiftSetting)) {
            Flash::error('Shift Setting not found');

            return redirect(route('shiftSettings.index'));
        }

        return view('shift_settings.edit')->with('shiftSetting', $shiftSetting);
    }

    /**
     * Update the specified shift_setting in storage.
     *
     * @param int $id
     * @param Updateshift_settingRequest $request
     *
     * @return Response
     */
    public function update($id, Updateshift_settingRequest $request)
    {
        $shiftSetting = $this->shiftSettingRepository->find($id);

        if (empty($shiftSetting)) {
            Flash::error('Shift Setting not found');

            return redirect(route('shiftSettings.index'));
        }

        $shiftSetting = $this->shiftSettingRepository->update($request->all(), $id);

        Flash::success('Shift Setting updated successfully.');

        return redirect(route('shiftSettings.index'));
    }

    /**
     * Remove the specified shift_setting from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $shiftSetting = $this->shiftSettingRepository->find($id);

        if (empty($shiftSetting)) {
            Flash::error('Shift Setting not found');

            return redirect(route('shiftSettings.index'));
        }

        $this->shiftSettingRepository->delete($id);

        Flash::success('Shift Setting deleted successfully.');

        return redirect(route('shiftSettings.index'));
    }
}
