<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DatePeriod;
use DateInterval;
use DateTime;
use PDF;

use App\Repositories\mesinRepository;
use App\Repositories\shiftRepository;
use App\Repositories\rencana_produksiRepository;
use Illuminate\Support\Facades\DB;

class laporanController extends Controller {
    private $mesinRepository;
    private $shiftRepository;
    private $rencanaProduksiRepository;

    public function __construct(
            mesinRepository $mesinRepo, 
            shiftRepository $shiftRep, 
            rencana_produksiRepository $rencanaProduksiRepo){
        $this->mesinRepository = $mesinRepo;
        $this->shiftRepository = $shiftRep;
        $this->rencanaProduksiRepository = $rencanaProduksiRepo;
        $this->middleware('auth');
    }

    public function index(Request $request){
        $mulai = $request->get('mulai');
        $selesai = $request->get('selesai');
        $shiftCategory = $request->get('shift_category');
        $date = date("Y/m/d");
        if (!$mulai && !$selesai) {
            $mulai = date('Y-m-01 00:00', strtotime($date));
            $selesai = date('Y-m-t 23:59', strtotime($date));
        } else {
            $mulai = date('Y-m-d 00:00', strtotime($mulai));
            $selesai = date('Y-m-d 23:59', strtotime($selesai));
        }

        $data = $this->getDatesFromRange($mulai, $selesai);
        $listDates = $data['listDates'];
        $listShifts = $data['listShifts'];
        $totalColumns = count($listShifts) * 3;
        $rowDatas = array();
        $mesins = $this->mesinRepository->all([], null, null, ['id','nama']);
        foreach ($mesins as $mesin) {
            array_push($rowDatas, [
                ['colspan'=>3, 'class'=>'mesin_name', 'data'=>$mesin->nama],
                ['colspan'=>3, 'class'=>'mesin_name', 'data'=>''],
                ['colspan'=>0, 'class'=>'mesin_name', 'data'=>'']
            ]);
            
            $rencanaProduksi = $this->rencanaProduksiRepository->allQuery()
                ->where('deleted_at', null)
                ->where('mesin_id', $mesin->id)
                ->whereIn('status',['confirm', 'aktual', 'finish'])
                ->where('selesai', '>=', $mulai)
                ->where('mulai', '<=', $selesai)
                // ->where('shift_category', $shiftCategory)
                ->orderBy('mulai', 'asc')
                ->get();
        
            foreach ($rencanaProduksi as $RP) { 
                $arrayContent = [];
                //NAMA
                array_push(
                    $arrayContent, 
                    ['colspan'=>3, 'class'=>'infot', 'data'=>$RP->nama, 'link'=>$RP->id]
                );
                //JUMLAH
                array_push(
                    $arrayContent, 
                    ['colspan'=>3, 'class'=>'jumlah main-data', 'data'=>$RP->jumlah_rencana_produksi]
                );
                // POPULATE SHIFTS
                $shifts = DB::table('shifts')
                    ->where('rencana_produksis.id',  $RP->id)
                    ->join('rencana_produksis', 'rencana_produksis.id', '=', 'shifts.rencana_produksi_id')
                    ->select(
                        'shifts.shift', 'shifts.mulai as mul', 'rencana_produksis.jumlah_operator', 'shifts.selesai as sel', 'rencana_produksis.nama', 'jumlah_rencana_produksi', 
                        DB::raw('SUM(estimasi_hasil) as est_hasil, SUM(aktual_hasil) as akt_hasil'),
                        // DB::Raw('DATE(shifts.selesai) day')
                    )->groupBy('shifts.id')
                    ->get();
                
                // print_r(json_encode($shifts));
                $shiftDict = array();
                foreach($shifts as $shift) {
                    $date = date('Y-m-d', strtotime($shift->mul));
                    $date2 = date('Y-m-d', strtotime($shift->sel));
                    $dateg = date('Y-m-d H:i', strtotime($shift->mul));
                    $date2g = date('Y-m-d H:i', strtotime($shift->sel));
                    // echo "<br>imam".$dateg."  ".$date2g;
                    if ($shift->shift==3 && $date == $date2) {
                        $date = date('Y-m-d', strtotime($shift->mul.'-1 day'));
                    } else {
                        $date = date('Y-m-d', strtotime($shift->mul));
                    }
                    $shiftDict[$date][$shift->shift] = $shift;
                };


                foreach($listDates as $index => $date) {
                    if ($index < 2) continue;
                    $date = date('Y-m-d', strtotime($date));
                    if (isset($shiftDict[$date])) {
                        $datas = $shiftDict[$date];
                        for ($idx = 1; $idx<=3; $idx++){
                            if (isset($datas[$idx])) {
                                $shift = $datas[$idx];
                                $dataColumn = $shift->est_hasil . " / <b style='color:green'>".$shift->akt_hasil.'</b>';
                                array_push($arrayContent, ['colspan'=>0, 'class'=>'main-data', 'data'=>$dataColumn]);
                                $listShifts[count($arrayContent) / 3 + 1][$idx - 1]['operator'] += $shift->jumlah_operator;
                            } else {
                                array_push($arrayContent, ['colspan'=>0, 'class'=>'main-data', 'data'=>'']); 
                            }
                        }
                    } else {
                        for ($idx = 1; $idx<=3; $idx++){
                            array_push($arrayContent, ['colspan'=>0, 'class'=>'', 'data'=>'']);
                        }
                    }
                }
                array_push($rowDatas, $arrayContent);
                if($RP->type_pasangan) {
                    $arrayContent[0]['data'] = $RP->type_pasangan;
                    array_push($rowDatas, $arrayContent);
                }
            }
            // $newRow = [['colspan'=>3, 'class'=>'blank_row', 'data'=>" "], ['colspan'=>3, 'class'=>'mesin_name', 'data'=>'']];
            // array_push($rowDatas, $newRow);
        }
        
        return view('laporan.index')
            ->with('listDates', $data['listDates'])
            ->with('listShifts', $listShifts)
            ->with('rowDatas', $rowDatas);
    }

    function getDatesFromRange($start, $end, $format = 'd-m-Y') {
        $arrayDate = ["Info", "Jumlah"];
        $arrayShift = [
            [
                ['shift'=>'', 'operator'=>''], ['shift'=>'', 'operator'=>''], ['shift'=>'', 'operator'=>'']
            ], 
            [   
                ['shift'=>'', 'operator'=>''], ['shift'=>'', 'operator'=>''], ['shift'=>'', 'operator'=>'']
            ]
        ];
        $interval = new DateInterval('P1D');
        $realEnd = new DateTime($end);
        $realEnd->add($interval);
        $period = new DatePeriod(new DateTime($start), $interval, $realEnd);
    
        foreach($period as $date) { 
            $arrayDate[] = $date->format($format); 
            $arrayShift[] = [
                ['shift'=>'1', 'operator'=>0], ['shift'=>'2', 'operator'=>0], ['shift'=>'3', 'operator'=>0]
            ];
        }

        array_pop($arrayDate);
        array_pop($arrayShift);
        
        return [
            "listDates" => $arrayDate,
            "listShifts" => $arrayShift,
        ];
    }

    public function cetak(Request $request){
        $tanggal = $request->get('tanggal');
        $selesaiOrg = $request->get('selesai');
        $selesai = $request->get('selesai');
        $mulai = date('Y-m-d 06:59:59', strtotime($tanggal));
        if ($selesai && $selesai != $tanggal) {
            $selesai = date('Y-m-d 07:00:00', strtotime($selesai.'+1 day'));
        } else {
            $selesai = date('Y-m-d 07:00:00', strtotime($tanggal.'+1 day'));
        }
        $data_table_row = array();
        $rps = DB::table('shifts')
            ->join('rencana_produksis', 'rencana_produksis.id', '=', 'shifts.rencana_produksi_id')
            ->join('mesins', 'rencana_produksis.mesin_id', '=', 'mesins.id')
            ->where('shifts.mulai', '>=', $mulai)
            ->where('shifts.selesai', '<=', $selesai)
            ->select(
                'shifts.*', 'shifts.operator as shift_operator', 'shifts.shift as shift_number',  'rencana_produksis.*', 'mesins.nama as nama_mesin',
                DB::raw('SUM(estimasi_hasil) as estimasi_hasil'),
                DB::raw('SUM(aktual_hasil) as aktual_hasil'),
                DB::raw('SUM(gangguan__) as gangguan__'),
                DB::raw('SUM(gangguan_sett) as gangguan_sett'),
                DB::raw('SUM(gangguan_mld) as gangguan_mld'),
                DB::raw('SUM(gangguan_bhn) as gangguan_bhn'),
                DB::Raw('DATE(shifts.mulai) day')
            )
            ->groupBy('shifts.rencana_produksi_id', 'shifts.shift', 'day')
            ->orderBy('rencana_produksis.id', 'asc')
            ->orderBy('shifts.mulai', 'asc')
            ->get();
        
        // $pdf = PDF::loadview('laporan.cetak_lap_injection', [
        //     'rps'=>$rps,  
        //     'tanggal'=>$tanggal, 
        //     'selesai'=>$selesaiOrg
        // ])->setPaper('F4', 'landscape');
        
        // $pdf = PDF::loadview('laporan.cetak_lap_injectionp1', [
        //     'rps'=>$rps,  
        //     'tanggal'=>$tanggal, 
        //     'selesai'=>$selesaiOrg, 
        // ])->setPaper('F4', 'landscape');
        
        // return $pdf->stream();

        return view('laporan.cetak_lap_injectionp1')
            ->with('rps',  $rps)
            ->with('tanggal',  $tanggal)
            ->with('selesai',  $selesaiOrg);

    }            

}
