<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createppic_logRequest;
use App\Http\Requests\Updateppic_logRequest;
use App\Repositories\ppic_logRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ppic_logController extends AppBaseController
{
    /** @var  ppic_logRepository */
    private $ppicLogRepository;

    public function __construct(ppic_logRepository $ppicLogRepo)
    {
        $this->ppicLogRepository = $ppicLogRepo;
    }

    /**
     * Display a listing of the ppic_log.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $ppicLogs = $this->ppicLogRepository->all();

        return view('ppic_logs.index')
            ->with('ppicLogs', $ppicLogs);
    }

    /**
     * Show the form for creating a new ppic_log.
     *
     * @return Response
     */
    public function create()
    {
        return view('ppic_logs.create');
    }

    /**
     * Store a newly created ppic_log in storage.
     *
     * @param Createppic_logRequest $request
     *
     * @return Response
     */
    public function store(Createppic_logRequest $request)
    {
        $input = $request->all();

        $ppicLog = $this->ppicLogRepository->create($input);

        Flash::success('Ppic Log saved successfully.');

        return redirect(route('ppicLogs.index'));
    }

    /**
     * Display the specified ppic_log.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ppicLog = $this->ppicLogRepository->find($id);

        if (empty($ppicLog)) {
            Flash::error('Ppic Log not found');

            return redirect(route('ppicLogs.index'));
        }

        return view('ppic_logs.show')->with('ppicLog', $ppicLog);
    }

    /**
     * Show the form for editing the specified ppic_log.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ppicLog = $this->ppicLogRepository->find($id);

        if (empty($ppicLog)) {
            Flash::error('Ppic Log not found');

            return redirect(route('ppicLogs.index'));
        }

        return view('ppic_logs.edit')->with('ppicLog', $ppicLog);
    }

    /**
     * Update the specified ppic_log in storage.
     *
     * @param int $id
     * @param Updateppic_logRequest $request
     *
     * @return Response
     */
    public function update($id, Updateppic_logRequest $request)
    {
        $ppicLog = $this->ppicLogRepository->find($id);

        if (empty($ppicLog)) {
            Flash::error('Ppic Log not found');

            return redirect(route('ppicLogs.index'));
        }

        $ppicLog = $this->ppicLogRepository->update($request->all(), $id);

        Flash::success('Ppic Log updated successfully.');

        return redirect(route('ppicLogs.index'));
    }

    /**
     * Remove the specified ppic_log from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ppicLog = $this->ppicLogRepository->find($id);

        if (empty($ppicLog)) {
            Flash::error('Ppic Log not found');

            return redirect(route('ppicLogs.index'));
        }

        $this->ppicLogRepository->delete($id);

        Flash::success('Ppic Log deleted successfully.');

        return redirect(route('ppicLogs.index'));
    }
}
