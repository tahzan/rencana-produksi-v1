<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createlist_shiftRequest;
use App\Http\Requests\Updatelist_shiftRequest;
use App\Repositories\list_shiftRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class list_shiftController extends AppBaseController
{
    /** @var  list_shiftRepository */
    private $listShiftRepository;

    public function __construct(list_shiftRepository $listShiftRepo)
    {
        $this->listShiftRepository = $listShiftRepo;
    }

    /**
     * Display a listing of the list_shift.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $listShifts = $this->listShiftRepository->all();

        return view('list_shifts.index')
            ->with('listShifts', $listShifts);
    }

    /**
     * Show the form for creating a new list_shift.
     *
     * @return Response
     */
    public function create()
    {
        return view('list_shifts.create');
    }

    /**
     * Store a newly created list_shift in storage.
     *
     * @param Createlist_shiftRequest $request
     *
     * @return Response
     */
    public function store(Createlist_shiftRequest $request)
    {
        $input = $request->all();

        $listShift = $this->listShiftRepository->create($input);

        Flash::success('List Shift saved successfully.');

        return redirect(route('listShifts.index'));
    }

    /**
     * Display the specified list_shift.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $listShift = $this->listShiftRepository->find($id);

        if (empty($listShift)) {
            Flash::error('List Shift not found');

            return redirect(route('listShifts.index'));
        }

        return view('list_shifts.show')->with('listShift', $listShift);
    }

    /**
     * Show the form for editing the specified list_shift.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $listShift = $this->listShiftRepository->find($id);

        if (empty($listShift)) {
            Flash::error('List Shift not found');

            return redirect(route('listShifts.index'));
        }

        return view('list_shifts.edit')->with('listShift', $listShift);
    }

    /**
     * Update the specified list_shift in storage.
     *
     * @param int $id
     * @param Updatelist_shiftRequest $request
     *
     * @return Response
     */
    public function update($id, Updatelist_shiftRequest $request)
    {
        $listShift = $this->listShiftRepository->find($id);

        if (empty($listShift)) {
            Flash::error('List Shift not found');

            return redirect(route('listShifts.index'));
        }

        $listShift = $this->listShiftRepository->update($request->all(), $id);

        Flash::success('List Shift updated successfully.');

        return redirect(route('listShifts.index'));
    }

    /**
     * Remove the specified list_shift from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $listShift = $this->listShiftRepository->find($id);

        if (empty($listShift)) {
            Flash::error('List Shift not found');

            return redirect(route('listShifts.index'));
        }

        $this->listShiftRepository->delete($id);

        Flash::success('List Shift deleted successfully.');

        return redirect(route('listShifts.index'));
    }
}
