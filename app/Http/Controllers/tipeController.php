<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatetipeRequest;
use App\Http\Requests\UpdatetipeRequest;
use App\Repositories\tipeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Imports\TipeImport;
use Maatwebsite\Excel\Facades\Excel;
use Flash;
use Response;

class tipeController extends AppBaseController
{
    private $tipeRepository;
    public function __construct(tipeRepository $tipeRepo){
        $this->tipeRepository = $tipeRepo;
    }

    public function index(Request $request){
        $params = $request->get('query');
        $query = $this->tipeRepository->allQuery();

        if ($query) {
            $query = $query->where('nama', 'like', '%'.$params.'%');
        };

        $tipes = $query->paginate(35)->appends(request()->query());
        return view('tipes.index')->with('tipes', $tipes);
    }

    public function search(Request $request) {
        $mesins = DB::table('tipes')
            ->where('deleted_at', null)
            ->where('nama', 'LIKE', '%'.$request->q.'%')->get();
        return response()->json($mesins);
    }

    public function create(){
        return view('tipes.create');
    }

    public function store(CreatetipeRequest $request){
        $this->validate($request, [
            'nama' => 'required',
            'jumlah_operator' => 'required',
            'cycle_time' => 'required',
            'berat' => 'required',
            'durasi_dandon' => 'required',
            'toleransi' => 'required',
            'jumlah_operator' => 'required',
            'durasi_produksi' => 'required',
            'count' => 'required',
        ]);
        
        $input = $request->all();
        // if ($input['barang_lain'] == '') {
        //     $input['barang_lain_id'] = '';
        // }

        // $tipe = $this->tipeRepository->find($input['barang_lain_id']);
        // if (!$tipe || $tipe->nama != $input['barang_lain']) {
        //     $input['barang_lain_id'] = '';
        //     $input['barang_lain'] = '';
        // }

        $tipe = $this->tipeRepository->create($input);
        Flash::success('Tipe berhasil di simpan.');
        return redirect(route('tipe.index'));
    }

    public function show($id){
        $tipe = $this->tipeRepository->find($id);
        if (empty($tipe)) {
            Flash::error('Tipe tidak ditemukan');
            return redirect(route('tipe.index'));
        }
        return view('tipes.show')->with('tipe', $tipe);
    }

    public function edit($id){
        $tipe = $this->tipeRepository->find($id);
        if (empty($tipe)) {
            Flash::error('Tipe tidak ditemukan');
            return redirect(route('tipe.index'));
        }
        return view('tipes.edit')->with('tipe', $tipe);
    }

    public function update($id, UpdatetipeRequest $request){
        $this->validate($request, [
            'nama' => 'required',
            'jumlah_operator' => 'required',
            'cycle_time' => 'required',
            'berat' => 'required',
            'durasi_dandon' => 'required',
            'toleransi' => 'required',
            'jumlah_operator' => 'required',
            'durasi_produksi' => 'required',
            'count' => 'required',
        ]);

        $tipe = $this->tipeRepository->find($id);   
        if (empty($tipe)) {
            Flash::error('Tipe tidak ditemukan');
            return redirect(route('tipes.index'));
        }
        
        $input = $request->all();
        // if ($input['barang_lain'] == '') {
        //     $input['barang_lain_id'] = '';
        // }

        // $tipe = $this->tipeRepository->find($input['barang_lain_id']);
        // if (!$tipe || $tipe->nama != $input['barang_lain']) {
        //     $input['barang_lain_id'] = '';
        //     $input['barang_lain'] = '';
        // }

        $tipe = $this->tipeRepository->update($input, $id);
        Flash::success('Tipe berhasil di update.');
        return redirect(route('tipe.index'));
    }

    public function destroy($id){
        $tipe = $this->tipeRepository->find($id);
        if (empty($tipe)) {
            Flash::error('Tipe tidak ditemukan');
            return redirect(route('tipe.index'));
        }
        $this->tipeRepository->delete($id);
        Flash::success('Tipe berhasil di delete.');
        return redirect(route('tipe.index'));
    }

    public function import_excel(Request $request) {
		// validasi
		$this->validate($request, [
			'file' => 'required|mimes:csv,xls,xlsx'
        ]);
        
 
		// menangkap file excel
		$file = $request->file('file');
 
		// membuat nama file unik
		$nama_file = rand().$file->getClientOriginalName();
 
        // upload ke folder file_siswa di dalam folder public
        $destination = public_path().'/file_tipe/';
		$file->move($destination, $nama_file);
 
		// import data
		Excel::import(new TipeImport, public_path('/file_tipe/'.$nama_file));
 
		// notifikasi dengan session
        Flash::success('Data tipe berhasil diimport!');
 
		// alihkan halaman kembali
		return redirect('/tipe');
    }
    
    function hapusSemua() {
        $tipes = DB::table('tipes')
        ->whereNotIn('id', function ($query) {
            $query->select('type_id')->from('rencana_produksis');
        })->delete();
        Flash::success('Tipe Berhasil Dihapus');
        return redirect('/tipe');
    }   

}
