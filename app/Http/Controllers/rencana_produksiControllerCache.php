<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createrencana_produksiRequest;
use App\Http\Requests\Updaterencana_produksiRequest;
use App\Repositories\rencana_produksiRepository;
use App\Repositories\rencana_produksi_cacheRepository;
use App\Repositories\jadwal_mesinRepository;
use App\Rules\more_than_now;
use App\Rules\validate_mulai_exclude_rencana_id;
use App\Rules\validate_mulai_cek_istirahat;
use App\Repositories\shiftRepository;
use App\Rules\validate_jadwal_mulai;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Repositories\tipeRepository;
use Flash;
use Response;
use Auth;
use PDF;

// TODO: bug yang cysle time 0.5 di penghitungan shift
class rencana_produksiControllerCache extends AppBaseController {
    private $rencanaProduksiCacheRepository;
    private $tipeRepository;

    public function __construct(
        rencana_produksi_cacheRepository $rencanaProduksiCacheRepository,
        tipeRepository $tipeRepo) {
        $this->rencanaProduksiCacheRepository = $rencanaProduksiCacheRepository;
        $this->tipeRepository = $tipeRepo;
    }

    public function index(Request $request){
        $tanggal = $request->get('tanggal');
        $mesin_id = $request->get('mesin_id');
        $status = $request->get('status');
        $active_type = $request->get('active_type');
        $query = $this->rencanaProduksiCacheRepository->allQuery();
        if ($tanggal) {
            $query = $query->whereYear('mulai', '=', substr($tanggal, 6, 4));
            $query = $query->whereMonth('mulai', '=', substr($tanggal, 3, 2));
            $query = $query->whereDay('mulai', '=', substr($tanggal, 0, 2));
        };

        $tipes = $this->tipeRepository->all([], null, null, ['id', 'nama']);
        $tipesArray = [];
        foreach ($tipes as $tipe) {
            $tipesArray[$tipe->id] = $tipe->nama;
        }

        if (!$active_type && count($tipes) > 0) {
            $active_type = $tipes[0]->id;
        }
        
        $query = $query->where('type_id', '=', $active_type);
        $query->orderBy('mulai', 'desc');
        $rencanaProduksisCache= $query->paginate(25)->appends(request()->query());
        return view('rencana_produksi_cache.index')
            ->with('rencanaProduksisCache', $rencanaProduksisCache)
            ->with('active_type', $active_type)
            ->with('tipes', $tipesArray);
    }
}