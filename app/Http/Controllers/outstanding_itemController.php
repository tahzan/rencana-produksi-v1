<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoutstanding_itemRequest;
use App\Http\Requests\Updateoutstanding_itemRequest;
use App\Repositories\outstanding_itemRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Imports\OutstandingItemImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use App\Repositories\report_daily_stockRepository;


class outstanding_itemController extends AppBaseController
{
    /** @var  outstanding_itemRepository */
    private $outstandingItemRepository;
    private $reportDailyStockRepository;

    public function __construct(
        report_daily_stockRepository $reportDailyStockRepo,
        outstanding_itemRepository $outstandingItemRepo)
    {
        $this->outstandingItemRepository = $outstandingItemRepo;
        $this->reportDailyStockRepository = $reportDailyStockRepo;
    }

    /**
     * Display a listing of the outstanding_item.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $type_id = $request->get('type_id');
        
        $start = $request->get('start') ?? date('Y-m-d');
        $end = $request->get('end') ?? date('Y-m-d');
        $customer = $request->get('customer') ?? "";
        $typename = $request->get('typename') ?? "";
        $status = $request->get('status');


        $outstandingItems = $this->outstandingItemRepository->allQuery();
        $outstandingItems = $outstandingItems->where('error', '=', "");

        if ($start  && $end) {
            $outstandingItems = $outstandingItems->whereDate('created_at', '>=', $start);
            $outstandingItems = $outstandingItems->whereDate('created_at', '<=',  $end);
        }

        else if ($start) {
            $outstandingItems = $outstandingItems->whereDate('created_at', '>=',  $start);
        }

        else if ($end) {
            $outstandingItems = $outstandingItems->whereDate('created_at', '<=',  $end);
        };

        if ($status)
            $outstandingItems = $outstandingItems->where('outstanding_items.status', '=', $status);


        if ($customer) {
            $outstandingItems = $outstandingItems->where('customer', 'like', $customer);
        }

        if ($typename) {
            $outstandingItems = $outstandingItems->whereHas('type', function ($query) use ($typename) {
                $query->where('nama', $typename);
            });
        }

        if ($type_id) {
            $outstandingItems = $outstandingItems->whereHas('type', function ($query) use ($type_id) {
                $query->where('id', $type_id);
            });
        }

        return view('outstanding_items.index')
             ->with('start', $start)
            ->with('end', $end)
            ->with('status', $status)
            ->with('customer', $customer)
            ->with('typename', $typename)
            ->with('outstandingItems', $outstandingItems->get());
    }

    /**
     * Show the form for creating a new outstanding_item.
     *
     * @return Response
     */
    public function create()
    {
        return view('outstanding_items.create');
    }

    /**
     * Store a newly created outstanding_item in storage.
     *
     * @param Createoutstanding_itemRequest $request
     *
     * @return Response
     */
    public function store(Createoutstanding_itemRequest $request)
    {
        $input = $request->all();

        $outstandingItem = $this->outstandingItemRepository->create($input);

        Flash::success('Outstanding Item saved successfully.');

        return redirect(route('outstandingItems.index'));
    }

    /**
     * Display the specified outstanding_item.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $outstandingItem = $this->outstandingItemRepository->find($id);

        if (empty($outstandingItem)) {
            Flash::error('Outstanding Item not found');

            return redirect(route('outstandingItems.index'));
        }

        return view('outstanding_items.show')->with('outstandingItem', $outstandingItem);
    }

    /**
     * Show the form for editing the specified outstanding_item.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $outstandingItem = $this->outstandingItemRepository->find($id);

        if (empty($outstandingItem)) {
            Flash::error('Outstanding Item not found');

            return redirect(route('outstandingItems.index'));
        }

        return view('outstanding_items.edit')->with('outstandingItem', $outstandingItem);
    }

    /**
     * Update the specified outstanding_item in storage.
     *
     * @param int $id
     * @param Updateoutstanding_itemRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoutstanding_itemRequest $request)
    {
        $outstandingItem = $this->outstandingItemRepository->find($id);

        if (empty($outstandingItem)) {
            Flash::error('Outstanding Item not found');

            return redirect(route('outstandingItems.index'));
        }

        $outstandingItem = $this->outstandingItemRepository->update($request->all(), $id);
        $outstandingItem = $this->outstandingItemRepository->find($id);
        $item = $outstandingItem;
        $query = DB::table('outstanding_items')
            ->join('outstandings', 'outstandings.id', '=', 'outstanding_items.outstanding_id')
            ->where('outstanding_items.type_id', '=', $item->type_id)
            ->where('outstandings.status', '=', 'processed')
            ->where('outstanding_items.status', '=', 'open')
            ->where('outstandings.deleted_at',  null)
            ->where('outstanding_items.deleted_at',  null)
            ->where('outstanding_items.error',  "")
            ->get();

        // search last stock from report
        $lastStock = 0;
        $query2 = DB::table('report_daily_stocks')
            ->where('type_id', '=', $item->type_id)
            ->where('deleted_at', '=', null)
            ->orderBy('id', 'DESC')
            ->first();

        if ($query2) {
            $lastStock = $query2->daily_stok;
        }
            
        // 1. hitung semua outstanding & create report
        $outstanding_total = $query->sum("qty");
        $sum = $lastStock - $outstanding_total;
        $input2 = array(
            // "sum" => $sum,
            "out_standing" => $outstanding_total,
        );
        $reportDailyStock = $this->reportDailyStockRepository->update($input2, $query2->id);

        // createPPIC
        // $ppicId = app('App\Http\Controllers\ppicController')->createPPIC($item->type_id, $sum, $request->user());
        // $udpateData = array(
        //     "ppic_id" => $ppicId
        // );
        // $this->reportDailyStockRepository->update($udpateData, $reportDailyStock->id);

        $dailystock = DB::table('daily_stocks')->first();
        if($dailystock) {
            DB::table('daily_stocks')->where('id', $dailystock->id)->update(['need_to_update'=>'delivered']);
        }

        Flash::success('Outstanding Item updated successfully.');

        return redirect()->route('report-daily-stock.index');
    }

    /**
     * Remove the specified outstanding_item from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $outstandingItem = $this->outstandingItemRepository->find($id);

        if (empty($outstandingItem)) {
            Flash::error('Outstanding Item not found');

            return redirect(route('outstandingItems.index'));
        }

        $this->outstandingItemRepository->delete($id);

        Flash::success('Outstanding Item deleted successfully.');
        return redirect()->route('outstandings.show', [$outstandingItem->outstanding_id]);
    }

    public function import_excel(Request $request) {
		// validasi
		$this->validate($request, [
			'file' => 'required|mimes:csv,xls,xlsx',
            'outstanding_id' => 'required',
        ]);
 
		// menangkap file excel
		$file = $request->file('file');
        $input = $request->all();
 
		// membuat nama file unik
		$nama_file = rand().$file->getClientOriginalName();
 
        // upload ke folder file_siswa di dalam folder public
        $destination = public_path().'/file_tipe/';
		$file->move($destination, $nama_file);
        
		// import data
		Excel::import(new OutstandingItemImport($input['outstanding_id']), public_path('/file_tipe/'.$nama_file));
 
		// notifikasi dengan session
        Flash::success('Stok Berhasil Di Import berhasil diimport!');
 
		// alihkan halaman kembali
        return redirect()->route('outstandings.show', [$input['outstanding_id']]);
	}



}
