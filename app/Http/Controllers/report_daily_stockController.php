<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createreport_daily_stockRequest;
use App\Http\Requests\Updatereport_daily_stockRequest;
use App\Repositories\report_daily_stockRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Repositories\tipeRepository;
use Flash;
use Response;
use Illuminate\Support\Facades\DB;

class report_daily_stockController extends AppBaseController
{
    /** @var  report_daily_stockRepository */
    private $reportDailyStockRepository;
    private $tipeRepository;

    public function __construct(
        report_daily_stockRepository $reportDailyStockRepo,
        tipeRepository $tipeRepo
        )
    {
        $this->reportDailyStockRepository = $reportDailyStockRepo;
        $this->tipeRepository = $tipeRepo;
    }

    /**
     * Display a listing of the report_daily_stock.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request){  
        // $tanggal = $request->get('tanggal');
        $tanggal = $request->get('tanggal') ?? date('Y-m-d');

        $sum = $request->get('sum');
        $outstanding = $request->get('outstanding');
        $active_type = $request->get('active_type');

        $tipes = $this->tipeRepository->all([], null, null, ['id', 'nama']);
        $tipesArray = array(""=>" Type ");
        foreach ($tipes as $tipe) {
            $tipesArray[$tipe->id] = $tipe->nama;
        }

        $query = $this->reportDailyStockRepository->allQuery()
            ->orderBy('id', 'desc');
        
        if ($active_type) {
            $query = $query->where('type_id', $active_type);
        }

        if ($tanggal) {
            $query = $query->whereYear('created_at', '=', substr($tanggal, 0, 4));
            $query = $query->whereMonth('created_at', '=', substr($tanggal, 5, 2));
            $query = $query->whereDay('created_at', '=', substr($tanggal, 8, 2));
        };

        if ($sum == "stok < 0") {
            $query = $query->where('sum', '<', 0);
        } else if ($sum == "stok = 0") {
            $query = $query->where('sum', '=', 0);
        } else if ($sum == "stok > 0") { 
            $query = $query->where('sum', '>', 0);
        }

        if ($outstanding == "outstanding > 0") {
            $query = $query->where('out_standing', '>', 0);
        } else if ($outstanding == "outstanding = 0") {
            $query = $query->where('out_standing', '=', 0);
        } 

        
        $report_ids = array();
        $report_results = array();
        foreach($query->get() as $ss) {
            if (!in_array($ss->type_id, $report_ids)) {
                array_push($report_ids,$ss->type_id);
                array_push($report_results,$ss);
            }
        }
        $firstData = $query->first();
        $tanggalstring = $tanggal;
        if ($firstData) {
            $tanggalstring = $firstData->created_at;
        }
        $showMessage = DB::table('daily_stocks')->where('need_to_update', "cancelled")->first();
        if ($showMessage){
            $showMessage = "Report ini dimungkinkan tidak valid karena pembatalan sales. Silahkan upload ulang stock hari ini!";
        }

        $showMessage = DB::table('daily_stocks')->where('need_to_update', "delivered")->first();
        if ($showMessage){
            $showMessage = "Report ini dimungkinkan tidak valid karena item outstanding telah di deliver. Silahkan upload ulang stock hari ini!";
        }

        // update all ppice where sum < 0 and selesai produksi is in the past
        $dailystocks = DB::table('report_daily_stocks')->where('sum', '<=', 0)->get();
        foreach($dailystocks as $ds){
            if ($ds->sum <= 0) {
                $ppic = DB::table('ppics')
                    ->where('id', '=', $ds->ppic_id)
                    ->where('status', '=', 'On Schedule')
                    ->where('tanggal_produksi', '<', date('Y-m-d').' 00:00:00')
                    ->get();
                if (count($ppic)>0) {
                    $ppic = DB::table('ppics')
                        ->where('id', '=', $ds->ppic_id)
                        ->where('status', '=', 'On Schedule')
                        ->where('tanggal_produksi', '<', date('Y-m-d').' 00:00:00')
                        ->update(['status' => 'Waiting Response']);
                }
            }
        }
        


        return view('report_daily_stocks.index')
            ->with('reportDailyStocks', $report_results)
            ->with('active_type', $active_type)
            ->with('tanggal', $tanggal)
            ->with('tanggalstring', $tanggalstring)
            ->with('sum', $sum)
            ->with('showMessage', $showMessage)
            ->with('outstanding', $outstanding)
            ->with('active_type', $active_type)
            ->with('tipes', $tipesArray);
    }

    /**
     * Show the form for creating a new report_daily_stock.
     *
     * @return Response
     */
    public function create()
    {
        return view('report_daily_stocks.create');
    }

    /**
     * Store a newly created report_daily_stock in storage.
     *
     * @param Createreport_daily_stockRequest $request
     *
     * @return Response
     */
    public function store(Createreport_daily_stockRequest $request)
    {
        $input = $request->all();

        $reportDailyStock = $this->reportDailyStockRepository->create($input);

        Flash::success('Report Daily Stock saved successfully.');

        return redirect(route('report-daily-stock.index'));
    }

    /**
     * Display the specified report_daily_stock.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $reportDailyStock = $this->reportDailyStockRepository->find($id);

        if (empty($reportDailyStock)) {
            Flash::error('Report Daily Stock not found');

            return redirect(route('report-daily-stock.index'));
        }

        return view('report_daily_stocks.show')->with('reportDailyStock', $reportDailyStock);
    }

    /**
     * Show the form for editing the specified report_daily_stock.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $reportDailyStock = $this->reportDailyStockRepository->find($id);

        if (empty($reportDailyStock)) {
            Flash::error('Report Daily Stock not found');

            return redirect(route('report-daily-stock.index'));
        }

        return view('report_daily_stocks.edit')->with('reportDailyStock', $reportDailyStock);
    }

    /**
     * Update the specified report_daily_stock in storage.
     *
     * @param int $id
     * @param Updatereport_daily_stockRequest $request
     *
     * @return Response
     */
    public function update($id, Updatereport_daily_stockRequest $request)
    {
        $reportDailyStock = $this->reportDailyStockRepository->find($id);

        if (empty($reportDailyStock)) {
            Flash::error('Report Daily Stock not found');

            return redirect(route('report-daily-stock.index'));
        }

        $reportDailyStock = $this->reportDailyStockRepository->update($request->all(), $id);

        Flash::success('Report Daily Stock updated successfully.');

        return redirect(route('report-daily-stock.index'));
    }

    /**
     * Remove the specified report_daily_stock from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $reportDailyStock = $this->reportDailyStockRepository->find($id);

        if (empty($reportDailyStock)) {
            Flash::error('Report Daily Stock not found');

            return redirect(route('report-daily-stock.index'));
        }

        $this->reportDailyStockRepository->delete($id);

        Flash::success('Report Daily Stock deleted successfully.');

        return redirect(route('report-daily-stock.index'));
    }

    public function import_excell()
    {
        return view('report_daily_stocks.create');
    }
}
