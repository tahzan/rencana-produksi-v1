<?php

namespace App\Http\Controllers;
use App\Http\Requests\Createrencana_produksiRequest;
use App\Http\Requests\Updaterencana_produksiRequest;
use App\Repositories\rencana_produksiRepository;
use App\Repositories\rencana_produksi_cacheRepository;
use App\Repositories\jadwal_mesinRepository;
use App\Repositories\mesinRepository;
use App\Repositories\infoRepository;
use App\Repositories\list_shiftRepository;
use App\Rules\more_than_now;
use App\Rules\validate_mulai_in_shift;
use App\Rules\validate_mulai_exclude_rencana_id;
use App\Rules\validate_shift_ready;
use App\Rules\validate_mulai_cek_istirahat;
use App\Rules\validate_mulai_rencana_prod_setelah;
use App\Repositories\shiftRepository;
use App\Rules\validate_jadwal_mulai;
use App\Http\Controllers\AppBaseController;
use App\Repositories\attachmentsRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Repositories\tipeRepository;
use Flash;
use Response;
use Auth;
use PDF;

// TODO: bug yang cysle time 0.5 di penghitungan shift

class rencana_produksiController extends AppBaseController {
    private $rencanaProduksiRepository;
    private $rencanaProduksiCacheRepository;
    private $jadwalMesinRepository;
    private $shiftRepository;
    private $mesinRepository;
    private $tipeRepository;
    private $infoRepository;
    private $listShiftRepository;
    private $attachmentsRepository;

    public function __construct(
        tipeRepository $tipeRepository,
        rencana_produksiRepository $rencanaProduksiRepo,
        rencana_produksi_cacheRepository $rencanaProduksiCacheRepository,
        jadwal_mesinRepository $jadwalMesinRepo,
        shiftRepository $shiftRepo,
        mesinRepository $mesinRepository,
        list_shiftRepository $listShiftRepository,
        infoRepository $infoRepository,
        attachmentsRepository $attachmentsRepo
    ) {
        $this->rencanaProduksiRepository = $rencanaProduksiRepo;
        $this->rencanaProduksiCacheRepository = $rencanaProduksiCacheRepository;
        $this->jadwalMesinRepository = $jadwalMesinRepo;
        $this->shiftRepository = $shiftRepo;
        $this->mesinRepository = $mesinRepository;
        $this->infoRepository = $infoRepository;
        $this->listShiftRepository = $listShiftRepository;
        $this->attachmentsRepository = $attachmentsRepo;
        $this->tipeRepository = $tipeRepository;
    }

    public function index(Request $request){
        $tanggal = $request->get('tanggal');
        $selesai = $request->get('selesai');
        
        $mesin_id = $request->get('mesin_id');
        $mesin_name = $request->get('mesin_name');
        $status = $request->get('status');
        $query = $this->rencanaProduksiRepository->allQuery();
        

        // if (!$tanggal) {
        //     $tanggal = date('Y-m-d');
        // } 

        // $tanggal = date('Y-m-d');
        if ($tanggal) {
            $tanggal = date('Y-m-d 23:59:00', strtotime($tanggal));
            $tanggalselesai = date('Y-m-d 00:00:01', strtotime($tanggal));
            $query = $query->where('mulai', '<=', $tanggal);
            $query = $query->where('selesai', '>=', $tanggalselesai);
        }
        // $query = $query->where('selesai', '>=', $tanggal);
        // $query = $query->where('selesai', '>=', $tanggal);
        // if($selesai) {
        //     $selesai = date('Y-m-d 23:59:00', strtotime($selesai));
        //     $query = $query->where('selesai', '<=', $selesai);
        // }
        if ($mesin_id) {
            $query = $query->where('mesin_id', $mesin_id);
        };
        if ($status) {
            if ($status == 'semua') {
                #do nothing
            }
                // $query = $query->where('status', '!=', 'booking');
            else if ($status == 'aktif') {
                $query = $query->where('status', '!=', 'booking');
                $query = $query->where('status', '!=', 'cancel');
            } else
                $query = $query->where('status', $status);
        };
        $query->orderBy('mulai', 'desc');
        $rencanaProduksis= $query->paginate(25)->appends(request()->query());
        // $rencanaProduksisDraft = $this->rencanaProduksiRepository->allQuery()
        //     ->where('status','booking')
        //     ->limit(2)
        //     ->get();
        return view('rencana_produksis.index')
            ->with('tanggal',  $request->get('tanggal'))
            ->with('mesin_id', $mesin_id)
            ->with('mesin_name', $mesin_name)
            ->with('status', $status)
            ->with('rencanaProduksis', $rencanaProduksis);
            // ->with('rencanaProduksisDraft', $rencanaProduksisDraft);
    }

    public function change_history(Request $request){
        $tanggal = $request->get('tanggal');
        $mesin_id = $request->get('mesin_id');
        $status = $request->get('status');
        $query = $this->rencanaProduksiCacheRepository->allQuery();
        if ($tanggal) {
            $query = $query->whereYear('mulai', '=', substr($tanggal, 6, 4));
            $query = $query->whereMonth('mulai', '=', substr($tanggal, 3, 2));
            $query = $query->whereDay('mulai', '=', substr($tanggal, 0, 2));
        };

        if ($mesin_id) {
            $query = $query->where('mesin_id', $mesin_id);
        };

        if ($status) {
            if ($status == 'semua') {
                
            } else if ($status == 'aktif') 
                $query = $query->where('status', '!=', 'cancel');
            else
                $query = $query->where('status', $status);
        };
        $query->orderBy('mulai', 'desc');
        $rencanaProduksisCache= $query->paginate(25)->appends(request()->query());
        return view('rencana_produksis.index_history')
            ->with('rencanaProduksisCache', $rencanaProduksisCache);
    }

    public function create(){
        return view('rencana_produksis.create');
    }
  
    public function generateNext(Request $request, $id){
        $mulai = $request->get('nextMulai');
        $selftActivation = $request->get('selftActivation');
        $mulai = date('Y-m-d H:i:s', strtotime($mulai));

        
        if (!$mulai) {
            $mulai = date("Y-m-d  H:i:s", strtotime("+5 minutes"));
        }
        
        $rencanaProduksi = $this->rencanaProduksiRepository->find($id);
        // if(strtotime($rencanaProduksi->selesai) > strtotime("today")) {
        //     $mulai = date("Y-m-d  H:i:s", strtotime($rencanaProduksi->selesai."+5 minutes"));
        // } 
            
        if (empty($rencanaProduksi)) {
            Flash::error('Rencana Produksi tidak ditemukan');
            return redirect(route('rencana-produksi.index'));
        }
        // cari rp
        if ($rencanaProduksi->status !== "finish" && $selftActivation !== "true") {
            Flash::error('Hanya rencana produksi yang finish yang bisa generate next');
            return redirect(route('rencana-produksi.show', [$id]));
        }
        
        $mesinId = $rencanaProduksi->mesin_id;
        if ($selftActivation === "true") {
            $nextRP = $this->rencanaProduksiRepository->allQuery();
            $nextRP = $nextRP->where('id', $id)->get();
        } else {
            $nextRP = $this->rencanaProduksiRepository->allQuery();
            $nextRP = $nextRP->where('status', 'booking')
                ->where('mesin_id',$mesinId)
                ->orderBy('urutan_booking', 'asc')->get();
        }
        
        if ($nextRP->count() == 0) {
            Flash::error('Tidak ditemukan booking dengan mesin ini');
            return redirect(route('rencana-produksi.show', [$id]));
        }
        $nextRP = $nextRP[0];

        // $mulai = date("Y-m-d  H:i:s", strtotime("+5 minutes"));
        // if(strtotime($rencanaProduksi->selesai) > strtotime("today")) {
        //     $mulai = date("Y-m-d  H:i:s", strtotime($rencanaProduksi->selesai."+5 minutes"));
        // } 
        $inputdata = [
            "id" =>  $nextRP->id,
            "mulai" => $mulai,
            'jumlah_rencana_produksi' => $nextRP->jumlah_rencana_produksi,
            'type_id'  => $nextRP->type_id,
            'nama'  => $nextRP->nama,
            'mesin_id'  => $nextRP->mesin_id,
            'estimasi_durasi'  => $nextRP->estimasi_durasi,
            'jumlah_operator' => $nextRP->jumlah_operator,
            'satuan'  => $nextRP->satuan,
        ];
        $rencanaProduksi = $this->rencanaProduksiRepository->update($inputdata, $nextRP->id);
        $rencanaProduksi = $this->rencanaProduksiRepository->find($inputdata['id']);

        $data = $this->hitung_jadwal_dan_shift($rencanaProduksi);

        if ($data['shift'] === null) {
            Flash::error('Shift tidak available, mohon ganti jadwal mulai');
            return redirect(route('draft-rencana-produksi.show', [$id]));
        }

        $inputdata2 = [
            'status' => 'confirm'
        ];
        $rencanaProduksi2 = $this->rencanaProduksiRepository->update($inputdata2, $nextRP->id);

        $data['urutan'] = $this->getNomorUrut($rencanaProduksi, $inputdata['mulai'], $inputdata['mesin_id']);
        $data['nomor_transaksi'] = $this->generateNoTransaksi($rencanaProduksi, $rencanaProduksi['mulai']);
        $rencanaProduksi = $this->rencanaProduksiRepository->update($data, $rencanaProduksi->id);
        $this->simpanHistoryPerubahan($rencanaProduksi);
        Flash::success('Rencana produksi berhasil disimpan.');
        if ($rencanaProduksi->barang_lain) {
            Flash::warning('Jangan lupa untuk membuat Rencana Produksi '.$rencanaProduksi->barang_lain );
        }
        return redirect(route('rencana-produksi.show', [$rencanaProduksi->id]));
    }
    
    public function store(Createrencana_produksiRequest $request){
        $input = $request->all();
        $input['mulai'] = date('Y-m-d H:i:s', strtotime($input['mulai']));
        $shiftAvailable = $this->convertTimeToShift($input['mulai'], false, 1);
        $penyisipanProses = false;
        $this->validate($request, [
            'jumlah_rencana_produksi' => ['required'],
            'type_id'  => 'required',
            'mesin_id'  => 'required',
            'estimasi_durasi'  => 'required',
            'mulai' => [
                new more_than_now(),
                new validate_mulai_cek_istirahat($input['mesin_id']),
                new validate_mulai_rencana_prod_setelah($input['mesin_id']),
                new validate_mulai_in_shift($shiftAvailable),
            ], 
        ]);
        
        if ($input['penyisipan'] != 'tidak') {
            $mulai = date('Y-m-d H:i:59', strtotime($input['mulai']));
            $jadwalMesins = DB::table('jadwal_mesins')
                ->join('rencana_produksis', 'rencana_produksis.id', '=', 'jadwal_mesins.perencanaan_id')
                ->where('jadwal_mesins.id', '<>', null)
                ->where('jadwal_mesins.mesin_id', '=', $input['mesin_id'])
                ->where('jadwal_mesins.mulai', '<=', $mulai)
                ->where('jadwal_mesins.selesai', '>',  $mulai)
                ->get();
            
            // validasi kalau penyisipan diisi tapi tidak nabrak
            if (count($jadwalMesins) > 0) {
                $jadwal= $jadwalMesins[0];
                if ($input['penyisipan'] == 'sebelum') {
                    $input['mulai'] = $jadwal->mulai;
                } else if ($input['penyisipan'] == 'setelah') {
                    $input['mulai'] = $jadwal->selesai;
                }
                $penyisipanProses = true;
            }
        } else {
            $mulai = date('Y-m-d H:i:59', strtotime($input['mulai']));
            $isShiftReady = $this->shift_validation(
                $mulai, $input['estimasi_durasi'], $input['mesin_id'], $input['jumlah_operator'], 1);
            
            if ($isShiftReady) {
                echo "<br/>shift valid";
            } else {
                echo "<br/>shift tidak valid";
            }

            $this->validate($request, [
                'mulai' => [
                    new validate_mulai_exclude_rencana_id(null, $input['mesin_id']),
                    new validate_shift_ready($isShiftReady)
                ], 
            ]);
        }
        
        $input['status'] = 'confirm';
        $rencanaProduksi = $this->rencanaProduksiRepository->create($input); // simpan dulu
        $data = $this->hitung_jadwal_dan_shift($rencanaProduksi);
        $data['urutan'] = $this->getNomorUrut($rencanaProduksi, $data['mulai'], $input['mesin_id']);
        $data['nomor_transaksi'] = $this->generateNoTransaksi($rencanaProduksi, $data['mulai']);
        // UPDATE DATA RENCANA PRODUKSI KARENA ADA PERUBAHAN DARI NO URUT, MULAI DAN SELESAI
        $rencanaProduksi = $this->rencanaProduksiRepository->update($data, $rencanaProduksi->id);
        $this->simpanHistoryPerubahan($rencanaProduksi);
        if($penyisipanProses) {
            $this->updateSetelahRencanaProduksi($rencanaProduksi);
            $this->susunKembaliNomorUrut($rencanaProduksi);
        } 
        Flash::success('Rencana produksi berhasil disimpan.');
        if ($rencanaProduksi->barang_lain) {
            Flash::warning('Jangan lupa untuk membuat Rencana Produksi '.$rencanaProduksi->barang_lain );
        }
        return redirect(route('rencana-produksi.index'));
    } 

    function shift_validation($mulai, $estimasi_durasi, $mesin_id, $jumlah_operator, $shiftCategory) {
        $durasiKerja = $estimasi_durasi / 60; // in minutes
        $hasilcek = True;
        while ($durasiKerja > 0) {
            $hasil = $this->hitungJadwal($mulai, $mesin_id, $durasiKerja, 'kerja');
            // echo "jadwal kerja yang mau di cek: <br/>";
            // print_r($hasil);
            // echo "<br/><br/>";
            $jadwalMesin = $hasil['jadwal'];
            $mulai = $jadwalMesin['selesai']; // assign value untuk loop berikutnya
            $durasiKerja = $hasil['sisaWaktu']; // assign value untuk loop berikutnya
            $hasilcek = $this->hitungShitValidation(
                $jadwalMesin['mulai'], $jadwalMesin['selesai'], $jumlah_operator, $shiftCategory);
        }
        return $hasilcek;
    }

    function hitungShitValidation($mulai, $selesai, $jumlah_operator, $shiftCategory) { 
        $run = true;
        while($run) {
            // cari shift berikutnya
            $nextShift = $this->getNextShift($mulai, 2);
            // jika waktu selesai masih sebelum next shift
            if ($selesai <= $nextShift[0]) {
                // echo "<br/>LOOP1<br/>";
                $shift = $this->convertTimeToShift(date('Y-m-d H:i:1', strtotime($mulai)), false, $shiftCategory);
                $durasi = round(abs(strtotime($selesai) - strtotime($mulai)) / 60,2);
                $data = [
                    'mulai' => $mulai,
                    'selesai' => $selesai,
                    'shift'  => $shift,
                    'durasi'  => $durasi * 60,
                ];
                // print_r($data);
                // print_r("<br/>rangeshift ");
                $rangeshift = $this->convertTimeToShift(date('Y-m-d H:i:1', strtotime($mulai)), True, $shiftCategory);
                // print_r($rangeshift);
                $isShiftReady = $this->isShiftReady($shift, $rangeshift, $jumlah_operator);
                // echo "<br/>";
                if (!$isShiftReady) {
                    return False;
                } else {
                    return True;
                }
                $run = false;
            } else {
                // echo "<br/>LOOP2<br/>";
                $shift = $this->convertTimeToShift(date('Y-m-d H:i:1', strtotime($mulai)), false, $shiftCategory);
                $durasi = round(abs(strtotime($nextShift[0]) - strtotime($mulai)) / 60,2);
                $data = [
                    'mulai' => $mulai,
                    'selesai' => $nextShift[0],
                    'shift'  => $shift,
                    'durasi'  => $durasi * 60,
                ];
                // print_r($data);
                // print_r("<br/>rangeshift");
                $rangeshift = $this->convertTimeToShift(date('Y-m-d H:i:1', strtotime($mulai)), True, $shiftCategory);
                // print_r($rangeshift);
                $isShiftReady = $this->isShiftReady($shift, $rangeshift, $jumlah_operator);
                if (!$isShiftReady) {
                    return False;
                } 
                $mulai = $nextShift[1]; // for next looping
            }
        }
    }

    public function isShiftReady($shiftNumber, $rangeshift, $jumlah_operator){
        // dd($rangeshift);
        // echo "<br/> shift yg mau di cek : ".$shiftNumber;
        // echo "<br/> jumlah operator mau di tambahkan: ".$jumlah_operator;
        $mulai = $rangeshift[0];
        $selesai = $rangeshift[1];
        $shifts = DB::table('shifts')
            ->where('mulai', '>=', $mulai)
            ->where('selesai', '<=', $selesai)
            ->groupBy('rencana_produksi_id')->get()->toArray();
        // echo "<br/> shift lain di jam ini <br/>";
        // print_r($shifts); 
        // jumlah operator dari shift yang ada + yang mau di tambah
        foreach($shifts as $shift){
            $rencanaProduksi = $this->rencanaProduksiRepository->find($shift->rencana_produksi_id);
            $jumlah_operator += $rencanaProduksi['jumlah_operator'];
        }

        // check di tabel settings operator yang tersedia
        $mulai_date = substr($mulai, 0, 10);
        $shift_settings = DB::table('shift_settings')
            ->where('tanggal', '=', $mulai_date)->get()->toArray();
        // echo "<br/> Total Operator Terpakai & mau dipakai ".$jumlah_operator."<br/>";
        // echo "shift settings yang ada  <br/>";
        // print_r($shift_settings);
        if (count($shift_settings)>0) {
            if ($shiftNumber == 1) {
                // print_r("<br/>sini1, jumlah op tersedia ".$shift_settings[0]->shift_1);
                return $jumlah_operator <= $shift_settings[0]->shift_1;
            } else if ($shiftNumber == 2) {
                // print_r("<br/>sini2, jumlah op tersedia ".$shift_settings[0]->shift_2);
                return $jumlah_operator <= $shift_settings[0]->shift_2;
            } else if ($shiftNumber == 3) {
                // print_r("<br/>sini3, jumlah op tersedia ".$shift_settings[0]->shift_3);
                return $jumlah_operator <= $shift_settings[0]->shift_3;
            }
        } else {
            return True;
        }
        // bandingkan jumlah operator + kebutuhan dengan yang di tabel baru berdasrkan tanggal dan shift:
        // return true false
    }

    public function update($id, Updaterencana_produksiRequest $request){
        $rencanaProduksi = $this->rencanaProduksiRepository->find($id);
        $prevMesinId = $rencanaProduksi->mesin_id;
        if (empty($rencanaProduksi)) {
            Flash::error('Rencana Produksi tidak ditemukan');
            return redirect(route('rencana-produksi.index'));
        }
        $input = $request->all();
        $input['mulai'] = date('Y-m-d H:i:s', strtotime($input['mulai']));
        $this->validate($request, [
            'jumlah_rencana_produksi' => ['required'],
            'type_id'  => 'required',
            'mesin_id'  => 'required',
            'estimasi_durasi'  => 'required',
            'mulai' => ['required', 
                new more_than_now(),
                new validate_mulai_cek_istirahat($input['mesin_id']),
            ], 
        ]);

        if ($input['penyisipan'] != 'tidak') {
            $mulai = date('Y-m-d H:i:59', strtotime($input['mulai']));
            $jadwalMesins = DB::table('jadwal_mesins')
                ->join('rencana_produksis', 'rencana_produksis.id', '=', 'jadwal_mesins.perencanaan_id')
                ->where('jadwal_mesins.id', '<>', null)
                ->where('jadwal_mesins.mesin_id', '=', $input['mesin_id'])
                ->where('jadwal_mesins.mulai', '<=', $mulai)
                ->where('jadwal_mesins.selesai', '>',  $mulai)
                ->get();

            // validasi kalau penyisipan diisi tapi tidak nabrak
            if (count($jadwalMesins) > 0) {
                $jadwal= $jadwalMesins[0];
                if ($input['penyisipan'] == 'sebelum') {
                    $input['mulai'] = $jadwal->mulai;
                } else if ($input['penyisipan'] == 'setelah') {
                    $input['mulai'] = $jadwal->selesai;
                }
            }
        } else {
            $this->validate($request, [
                'mulai' => [
                    new validate_mulai_exclude_rencana_id($id, $input['mesin_id'])
                ], 
            ]);
        }

        $input['selesai'] = date('Y-m-d H:i:s', strtotime($input['selesai'])); // sesuai sebelumnya
        $rencanaProduksi = $this->rencanaProduksiRepository->update($input, $id);
        $this->jadwalMesinRepository->deleteByRP($rencanaProduksi->id);
        $this->shiftRepository->deleteByRP($rencanaProduksi->id);
        $rencanaProduksi = $this->updateRencanaProduksi($rencanaProduksi, $prevMesinId);
        $this->updateSetelahRencanaProduksi($rencanaProduksi);
        $this->susunKembaliNomorUrut($rencanaProduksi);
        Flash::success('Rencana Produksi berhasil di update.');
        if ($rencanaProduksi->barang_lain) {
            Flash::warning('Jangan lupa untuk membuat Rencana Produksi '.$rencanaProduksi->barang_lain );
        }
        return redirect(route('rencana-produksi.show', [$rencanaProduksi->id]));
    }

    // dipanggil oleh method update untuk menghitung kembali jadwal mesin 
    function updateRencanaProduksi($rencanaProduksi, $prevMesinId=null) {
        $data = $this->hitung_jadwal_dan_shift($rencanaProduksi);
        // jika ganti mesin maka nomor urut dirubah
        if ($prevMesinId != null && $prevMesinId != $rencanaProduksi->mesin_id){
            $data['urutan'] = $this->getNomorUrut($rencanaProduksi, $data['mulai'], $rencanaProduksi->mesin_id);
        }
        $rencanaProduksi = $this->rencanaProduksiRepository->update($data, $rencanaProduksi->id);
        // $this->simpanHistoryPerubahan($rencanaProduksi);
        return $rencanaProduksi;
    }

    function hitung_jadwal_dan_shift($rencanaProduksi) {
        $estimasiDurasi = $rencanaProduksi->estimasi_durasi / 60; // in minutes
        $durasiDandon = $rencanaProduksi->durasi_dandon;
        $durasiKerja = $estimasiDurasi - $durasiDandon;
        $mulaiBaru = $rencanaProduksi->mulai; // to handle if dandon = 0
        $selsaiBaru = $rencanaProduksi->mulai;

        // LOOP SIMPAN DANDON KE JADWAL MESIN
        $mulai = $mulaiBaru; 
        while ($durasiDandon > 0) {
            $hasil = $this->hitungJadwal($mulai, $rencanaProduksi->mesin_id, $durasiDandon, 'dandon');
            $jadwalMesin = $hasil['jadwal'];
            $jadwalMesin['perencanaan_id'] = $rencanaProduksi->id;
            if (!$mulaiBaru) { 
                $mulaiBaru = $jadwalMesin['mulai']; // assign rencana-produksi mulai menggunakan jadwal mesin pertama
            }
            $selsaiBaru = date('Y-m-d H:i:s', strtotime($jadwalMesin['selesai']));  // assign rencana-produksi selsai sampe looping terakhir
            $mulai = $jadwalMesin['selesai']; // assign value untuk loop berikutnya
            $durasiDandon = $hasil['sisaWaktu']; // assign value untuk loop berikutnya
            // echo "<br/><br/> JADWAL DANDON "; print_r($jadwalMesin);
            $jadwalMesinObj = $this->jadwalMesinRepository->create($jadwalMesin); // save jadwal ke database
        }

        // LOOP DURASI KERJA
        $shiftMulai = 0; // disimpan ke rencana produksi sebagi shift awal
        $shiftSelesai = 0; // disimpan ke rencana produksi sebagi shift slesai
        while ($durasiKerja > 0) {
            $hasil = $this->hitungJadwal($mulai, $rencanaProduksi->mesin_id, $durasiKerja, 'kerja');
            $jadwalMesin = $hasil['jadwal'];
            $jadwalMesin['perencanaan_id'] =  $rencanaProduksi->id;
            if (!$mulaiBaru) { 
                $mulaiBaru = $jadwalMesin['mulai']; // assign rencana-produksi mulai menggunakan jadwal mesin pertama
            }
            $selsaiBaru = date('Y-m-d H:i:s', strtotime($hasil['jadwal']['selesai']));  // assign rencana-produksi selsai sampe looping terakhir
            $mulai = $hasil['jadwal']['selesai']; // assign value untuk loop berikutnya
            $durasiKerja = $hasil['sisaWaktu']; // assign value untuk loop berikutnya
            // echo "<br/><br/> JADWAL MESIN "; print_r($jadwalMesin);
            $jadwalMesinObj= $this->jadwalMesinRepository->create($jadwalMesin); // save jadwal ke database
            $shift = $this->hitungShitDanSimpan($jadwalMesinObj, $rencanaProduksi);
            // dd($shift);
            if ($shift['shift_mulai'] === null && $shift['shift_selesai'] === null) {
                $durasiKerja = 0;
                return [
                    'mulai' => null,
                    'selesai' => null,
                    'shift' => null,
                    'shift_selesai' => null,
                ];
            }
            if ($shiftMulai == 0) {
                $shiftMulai = $shift['shift_mulai'];
            }
            $shiftSelesai = $shift['shift_selesai'];
        }

        return [
            'mulai' => $mulaiBaru,
            'selesai' => $selsaiBaru,
            'shift' => $shiftMulai,
            'shift_selesai' => $shiftSelesai,
        ];
    }

    function hitungJadwal($mulai, $mesin_id, $durasiYangDibutuhkan, $tipe) {
        // INFO: JADWAL MESIN DI SAVE DI LOOPINGAN
        $jadwalIstirahats = DB::table('jadwal_mesins')
            ->where('mesin_id', $mesin_id)
            ->where('tipe', '=', 'istirahat')
            ->where('mulai', '>=', $mulai)
            ->where('deleted_at', '=', null)
            ->orderBy('mulai', 'asc')
            ->limit(10)
            ->get();
        
        // jika tidak ada istirahat sama sekali setelah 
        if (count($jadwalIstirahats) == 0) {
            $akhir = strtotime($mulai) + $durasiYangDibutuhkan * 60;
            $hasil = [
                'mulai'=>$mulai,  
                'selesai'=> date('Y-m-d H:i:s', $akhir),
                'tipe'=>$tipe,
                'mesin_id'=>$mesin_id,
                'durasi' => $durasiYangDibutuhkan * 60
            ];
            return ['jadwal'=>$hasil, 'sisaWaktu'=>0];
        
        // jika ada istirahat yang ketabrak
        } else {
            $index = 0;
            $nextIndex = 1;
            $jadwalIstirahat = $jadwalIstirahats[$index];
            $jadwalIstirahatberikutnya = null;
            if (count($jadwalIstirahats) > 1) {
                $jadwalIstirahatberikutnya = $jadwalIstirahats[$nextIndex];
            }
           
            $waktuYangTersedia = round(abs(
                strtotime($jadwalIstirahat->mulai) - strtotime($mulai)) / 60,2);
            
            // jika tidak ada jarak / waktu mulai sama dengan waktu start istirahat, 
            // maka diambil waktu akhir istirhat sbg start
            if ($waktuYangTersedia <= 0) {
                // echo "waktu mulai sama dengan jam istirahat mulai <br/>";
                $mulai = $jadwalIstirahat->selesai;
                // cek apakah ada jam istirahat setelahnya?
                while($jadwalIstirahatberikutnya && $jadwalIstirahat->selesai == $jadwalIstirahatberikutnya->mulai) { 
                    $jadwalIstirahat = $jadwalIstirahatberikutnya;
                    $mulai = $jadwalIstirahatberikutnya->selesai;

                    $nextIndex++;
                    if( isset( $jadwalIstirahats[$nextIndex] )) {  
                        $jadwalIstirahatberikutnya = $jadwalIstirahats[$nextIndex];
                    } else {
                        $jadwalIstirahatberikutnya = null;
                    }
                }

                // cek apakah ada jam istirahat setelahnya?
                if ($jadwalIstirahatberikutnya) {
                    $waktuYangTersedia = round(abs(
                        strtotime($jadwalIstirahatberikutnya->mulai) - strtotime($mulai)) / 60,2);
                    $sisaWaktu = $waktuYangTersedia - $durasiYangDibutuhkan;
                    // jika watu sampe istirhat berikutnya kurang
                    if ($sisaWaktu < 0) {
                        // echo "waktu yang tersedia sebelum istirahat berikutnya tidak cukup<br/>";
                        $hasil = [
                            'mulai'=>$mulai,  
                            'selesai'=>$jadwalIstirahatberikutnya->mulai,
                            'tipe'=>$tipe,
                            'mesin_id'=>$mesin_id,
                            'durasi' => $waktuYangTersedia * 60
                        ];
                        return ['jadwal'=>$hasil, 'sisaWaktu'=>abs($sisaWaktu)];
                    // jika waktu sebelum istirahat cukup
                    } else {
                        // echo "waktu yang tersedia sebelum istirahat berikutnya cukup<br/>";
                        $akhir = strtotime($mulai) + $durasiYangDibutuhkan * 60;
                        $hasil = [
                            'mulai'=>$mulai,  
                            'selesai'=> date('Y-m-d H:i:s', $akhir),
                            'tipe'=>$tipe,
                            'mesin_id'=>$mesin_id,
                            'durasi' => $durasiYangDibutuhkan * 60
                        ];
                        return ['jadwal'=>$hasil, 'sisaWaktu'=>0];
                    }
                } else {
                    // "tidak ada lagi istirahat setelahnya <br/>";
                    $akhir = strtotime($mulai) + $durasiYangDibutuhkan * 60;
                    $hasil = [
                        'mulai'=>$mulai,  
                        'selesai'=> date('Y-m-d H:i:s', $akhir),
                        'tipe'=>$tipe,
                        'mesin_id'=>$mesin_id,
                        'durasi' => $durasiYangDibutuhkan * 60
                    ];
                    return ['jadwal'=>$hasil, 'sisaWaktu'=>0];
                }
                
            // jika waktu mulai tidak sama dengan waktu start istirahat, 
            // ada waktu tersedia sebelum istirahat
            } else {
                $sisaWaktu = $waktuYangTersedia - $durasiYangDibutuhkan;
                // waktu yang dibutuhkan sebelum istirahat tidak cukup
                if ($sisaWaktu < 0) {
                    $hasil = [
                        'mulai'=>$mulai,  
                        'selesai'=>$jadwalIstirahat->mulai,
                        'tipe'=>$tipe,
                        'mesin_id'=>$mesin_id,
                        'durasi' => $waktuYangTersedia * 60
                    ];
                    return ['jadwal'=>$hasil, 'sisaWaktu'=>abs($sisaWaktu)];
                // waktu yang dibutuhkan sebelum istirahat SUDAH cukup
                } else {
                    $akhir = strtotime($mulai) + $durasiYangDibutuhkan * 60;
                    $hasil = [
                        'mulai'=>$mulai,  
                        'selesai'=> date('Y-m-d H:i:s', $akhir),
                        'tipe'=>$tipe,
                        'mesin_id'=>$mesin_id,
                        'durasi' => $durasiYangDibutuhkan * 60
                    ];
                    return ['jadwal'=>$hasil, 'sisaWaktu'=>0];
                }
            } 
        }
    }

    function hitungShitDanSimpan($jadwalMesin, $RP) { 
        $mulai = date('Y-m-d H:i:s', strtotime($jadwalMesin['mulai']));
        $selesai = date('Y-m-d H:i:s', strtotime($jadwalMesin['selesai']));
        $shiftMulai = $this->convertTimeToShift($mulai, false, $RP->shift_category);
        $shiftSelesai = $this->convertTimeToShift($selesai, false, $RP->shift_category);
        // print_r(json_encode($mulai));
        // print_r(json_encode($selesai));
        // print_r(json_encode($shiftMulai));
        // print_r(json_encode($shiftSelesai));

        // print_r( $shiftMulai);
        $run = true;
        $hasError = false;
        $i = 0;
        while($run) {
            $i++;
            $nextShift = $this->getNextShift($mulai, $RP->shift_category);
            // if ($i===5) {
            //     print_r(json_encode($mulai));
            //     dd($nextShift);
            // }
            if (count($nextShift) === 0) {
                $run = false;
                $hasError = true;
            }
            else if ($selesai <= $nextShift[0]) {
                $shift = $this->convertTimeToShift(
                    date('Y-m-d H:i:1', strtotime($mulai)), false, $RP->shift_category);
                $durasi = round(abs(strtotime($selesai) - strtotime($mulai)) / 60,2);
                $estimasiHasil = $durasi * 60 / $RP->durasi_produksi * $RP->cycle_time;
                $data = [
                    'mulai' => $mulai,
                    'selesai' => $selesai,
                    'shift'  => $shift,
                    'durasi'  => $durasi * 60,
                    'estimasi_hasil' => round($estimasiHasil),
                    'aktual_hasil' => 0,
                    'rencana_produksi_id' => $RP->id,
                    'shift_category' => $RP->shift_category,
                ];
                $shift = $this->shiftRepository->create($data);
                $run = false;
            } else {
                $shift = $this->convertTimeToShift(date('Y-m-d H:i:1', strtotime($mulai)), false, $RP->shift_category);
                $durasi = round(abs(strtotime($nextShift[0]) - strtotime($mulai)) / 60,2);
                $estimasiHasil = $durasi * 60 / $RP->durasi_produksi * $RP->cycle_time;
                $data = [
                    'mulai' => $mulai,
                    'selesai' => $nextShift[0],
                    'shift'  => $shift,
                    'durasi'  => $durasi * 60,
                    'estimasi_hasil' => $estimasiHasil,
                    'aktual_hasil' => 0,
                    'rencana_produksi_id' => $RP->id,
                    'shift_category' => $RP->shift_category,
                ];
                $shift = $this->shiftRepository->create($data);
                $mulai = $nextShift[1];
                $run = true;
            }
        }
        if ($hasError) {
            $inputdata = [
                'status' => 'booking'
            ];
            $rencanaProduksi = $this->rencanaProduksiRepository->update($inputdata, $RP->id);
            $this->jadwalMesinRepository->deleteByRP($RP->id);
            $this->shiftRepository->deleteByRP($RP->id);
            return ['shift_mulai'=>null, 'shift_selesai' => null];
        } 
        // dd("sinisss");
        return ['shift_mulai'=>$shiftMulai, 'shift_selesai' => $shiftSelesai];
    }

    function getNextShift($dateTime, $shiftCategory) {
        $haritanggal = date('Y-m-d H:i:01', strtotime($dateTime));
        $listRange = $this->getListShiftRange($haritanggal, $shiftCategory);
        $maxShift = $this->listShiftRepository->allQuery()
            ->where('shift_category', $shiftCategory)
            ->orderBy('shift', 'desc')->get()->toArray(); 
        $maxShift = $maxShift[0]['shift'];
        $nextShift=null;
        $result = array();
        print_r("<br/><br/>");
        print_r("Next Shift<br/><br/>");
        print_r("<br/><br/>");
        print_r("<br/><br/>haritanggal");
        print_r(json_encode($haritanggal));

        // dd(json_encode($listRange));
        foreach($listRange as $index=>$range){ 
            print_r("<br/><br/>");
            if ($haritanggal >= $range['range'][0] && $haritanggal <= $range['range'][1]) {
                print_r("masuk");
                $shift = $range['shift'];
                $need_add_day = $range['need_add_day'];
                if ($shift < $maxShift) 
                    $nextShift = $shift + 1;
                else 
                    $nextShift = 1;

                print_r("Next shift nya yaitu: ");
                print_r($nextShift);

                $nextShiftObj = $this->listShiftRepository->allQuery()
                    ->where('shift_category', $shiftCategory)
                    ->where('shift', $nextShift)->get()->toArray(); 
                
                // dd(json_encode($nextShiftObj));
                // jika next shift === 1 kemungkinan tambah 1 hari, 
                if (($nextShift == 1 && $need_add_day) || $maxShift === 1) {
                    echo("<br/>next shift 1 masuk sini tambah 1 hari");
                    $nextShiftObj  = date('Y-m-d '.$nextShiftObj[0]['mulai'].':00:00', 
                        strtotime($haritanggal.'+ 1day'));
                    $result = array($range['range'][1], $nextShiftObj);
                } else {
                    echo("<br/>if ke 1");
                    $nextShiftObj  = date('Y-m-d '.$nextShiftObj[0]['mulai'].':00:00', strtotime($haritanggal));
                    $result = array($range['range'][1], $nextShiftObj);
                }
            };
        }

        print_r("<br/><br/> result");
        print_r(json_encode($result));
        return $result;
    }

    function convertTimeToShift($haritanggal, $rangedate=False, $shiftCategory) {
        $haritanggal = date('Y-m-d H:i:s', strtotime($haritanggal));
        $listRange = $this->getListShiftRange($haritanggal, $shiftCategory);
        // echo "<br/> haritanggal :".$haritanggal;
        foreach($listRange as $range){ 
            if ($haritanggal >= $range['range'][0] && $haritanggal <= $range['range'][1]) {
                if ($rangedate) return $range['range']; else return $range['shift'];
            }
        }
    }

    function getListShiftRange($haritanggal, $shiftCategory) {
        $listShift = $this->listShiftRepository->allQuery();
        $listShift = $listShift->where('shift_category', $shiftCategory)->get()->toArray();
        $result = array();
        foreach($listShift as $shift){
            if ($shift['cross_day'] == 'Y') {
                $data1 = [
                    'shift' => $shift['shift'],
                    'cross_day' => $shift['cross_day'],
                    'need_add_day' => false,
                    'range' => [
                        date('Y-m-d '.$shift['mulai'].':00:00', strtotime($haritanggal.'- 1day')),
                        date('Y-m-d '.$shift['selesai'].':00:00', strtotime($haritanggal)),
                    ]
                ];
                $data2 = [
                    'shift' => $shift['shift'],
                    'cross_day' => $shift['cross_day'],
                    'need_add_day' => true, // karena akan selesai di hari berikutnya maka harus ditambah di next shift
                    'range' => [
                      date('Y-m-d '.$shift['mulai'].':00:00', strtotime($haritanggal)),
                      date('Y-m-d '.$shift['selesai'].':00:00', strtotime($haritanggal.'+ 1day')),
                    ]
                ];
                array_push($result, $data1, $data2);
            } else {
                $data = [
                    'shift' => $shift['shift'],
                    'cross_day' => $shift['cross_day'],
                    'need_add_day' => false,
                    'range' => [
                      date('Y-m-d '.$shift['mulai'].':00:00', strtotime($haritanggal)),
                      date('Y-m-d '.$shift['selesai'].':00:00', strtotime($haritanggal)),
                    ]
                ];
                array_push($result, $data);
            }
        }
        return $result;
    }

    function hitungJarakkeAkhirShift($dateBefore, $shift){
        $akhirShift = null;
        if ($shift == 1) {
            $akhirShift = date('Y-m-d 15:00', strtotime($dateBefore));
        } else if ($shift == 2) {
            $akhirShift = date('Y-m-d 23:00', strtotime($dateBefore));
        } else if ($shift == 3) {
            $akhirShift = date('Y-m-d 07:00', strtotime($dateBefore));
        }
        
        return [
            'durasi'=> round(abs(strtotime($akhirShift) - strtotime($dateBefore)) / 60,2),
            'akhir_shift' => $akhirShift
        ];
    }

    function hitungJarakdariAwalShift($dateAfter, $shift){
        $awalShift = null;
        if ($shift == 1) {
            $awalShift = date('Y-m-d 07:00', strtotime($dateAfter));
        } else if ($shift == 2) {
            $awalShift = date('Y-m-d 15:00', strtotime($dateAfter));
        } else if ($shift == 3) {
            $awalShift = date('Y-m-d 23:00', strtotime($dateAfter));
        }
        
        return [
            'durasi'=> round(abs(strtotime($dateAfter) - strtotime($awalShift)) / 60,2),
            'awal_shift' => $awalShift
        ];

    }

    function getNomorUrut($rencanaProduksi, $tanggal, $mesin_id) {
        $year = substr($tanggal, 0, 4);
        $month = substr($tanggal, 5, 2);
        $otherRencana = DB::table('rencana_produksis')
            ->where('id', '<>', $rencanaProduksi->id)
            ->where('mesin_id', $mesin_id)
            ->whereYear('mulai', $year)
            ->whereMonth('mulai', $month)
            ->orderBy('urutan', 'desc')
            ->first();

        $urutanTanggal = str_replace("-", "", $tanggal);
        $key1 = substr($urutanTanggal, 2, 2);
        $key2 = substr($urutanTanggal, 4, 2);
        
        $kode_mesin = $rencanaProduksi->mesin->kode;
        if (!$otherRencana) {
            $urutan = sprintf("%03d", 1);
            return $kode_mesin.'-'.$key1.$key2.":".$urutan;
        } else {
            $prevUrutan = substr($otherRencana->urutan, -3);
            $prevUrutan = (int) $prevUrutan;
            $urutan = sprintf("%03d", $prevUrutan + 1);
            return $kode_mesin.'-'.$key1.$key2.":".$urutan;
        }
    }

    function generateNoTransaksi($rencanaProduksi, $tanggal) {
        $year = substr($tanggal, 0, 4);
        $month = substr($tanggal, 5, 2);
        $otherRencana = DB::table('rencana_produksis')
            ->where('id', '<>', $rencanaProduksi->id)
            ->whereYear('mulai', $year)
            ->whereMonth('mulai', $month)
            ->orderBy('nomor_transaksi', 'desc')
            ->first();

        $urutanTanggal = str_replace("-", "", $tanggal);
        $key1 = substr($urutanTanggal, 2, 2);
        $key2 = substr($urutanTanggal, 4, 2);
        if (!$otherRencana) {
            $nomor_transaksi = sprintf("%04d", 1);
            $nomor_transaksi  = $key1.$key2."-".$nomor_transaksi;
        } else {
            $prevUrutan = substr($otherRencana->nomor_transaksi, -3);
            $prevUrutan = (int) $prevUrutan;
            $nomor_transaksi = sprintf("%04d", $prevUrutan + 1);
            $nomor_transaksi  =  $key1.$key2."-".$nomor_transaksi;
        }
        return $nomor_transaksi;
    }

    public function show($id){
        $rencanaProduksi = $this->rencanaProduksiRepository->find($id);
        $rencanaProduksisCache = $this->rencanaProduksiCacheRepository
            ->allQuery()->where(['rencana_proudksi_id' => $id])->paginate(25)->appends(request()->query());
        $jadwalMesins = $this->jadwalMesinRepository->allQuery()
            ->where(['perencanaan_id' => $id])->get();
        $shifts = $this->shiftRepository->allQuery()
            ->where(['rencana_produksi_id' => $id])->get();

        if (empty($rencanaProduksi)) {
            Flash::error('Rencana Produksi tidak ditemukan');
            return redirect(route('rencana-produksi.index'));
        }

        $files = $this->attachmentsRepository->allQuery()->get();
        return view('rencana_produksis.show')
            ->with('rencanaProduksi', $rencanaProduksi)
            ->with('jadwalMesins', $jadwalMesins)
            ->with('shifts', $shifts)
            ->with('files', $files)
            ->with('rencanaProduksisCache', $rencanaProduksisCache);
    }

    public function edit($id){
        $rencanaProduksi = $this->rencanaProduksiRepository->find($id);
        if (empty($rencanaProduksi) || $rencanaProduksi->status == 'aktual' || $rencanaProduksi->status == 'cancel' || $rencanaProduksi->status == 'finish') {
            Flash::error('Rencana Produksi tidak ditemukan');
            return redirect(route('rencana-produksi.index'));
        }

        return view('rencana_produksis.edit')->with('rencProd', $rencanaProduksi);
    }

    public function editNG($id){
        $rencanaProduksi = $this->rencanaProduksiRepository->find($id);
        if (empty($rencanaProduksi) || $rencanaProduksi->status == 'cancel') {
            Flash::error('Rencana Produksi tidak ditemukan');
            return redirect(route('rencana-produksi.index'));
        }
        return view('rencana_produksis.edit_ng')->with('rencProd', $rencanaProduksi);
    }

    public function updateNG($id, Updaterencana_produksiRequest $request){
        $rencanaProduksi = $this->rencanaProduksiRepository->find($id);
        $input = $request->all();
        if (empty($rencanaProduksi)) {
            Flash::error('Rencana Produksi tidak ditemukan');
            return redirect(route('rencana-produksi.show', [$id]));
        } else {
            $rencanaProduksi = $this->rencanaProduksiRepository->update($input, $id);
            Flash::success('Data berhasil diubah');
            return redirect(route('rencana-produksi.show', [$id]));
        }
    }

    public function destroy($id, Updaterencana_produksiRequest $request){
        $rencanaProduksi = $this->rencanaProduksiRepository->find($id);

        if (empty($rencanaProduksi)) {
            Flash::error('Rencana Produksi tidak ditemukan');
            return redirect(route('rencana-produksi.show', [$id]));
        } else if($rencanaProduksi->status == 'aktual' || $rencanaProduksi->status == 'finish') {
            Flash::error('Tidak dapat membatalkan transaksi yang aktual');
            return redirect(route('rencana-produksi.show', [$id]));
        }

        $anotherRencanaProduksis = $this->rencanaProduksiRepository->allQuery()
            ->where('mesin_id', '=',  $rencanaProduksi->mesin_id)
            ->where('deleted_at', '=', null)
            ->where('status', '!=', 'cancel')
            ->where('urutan', '>',  $rencanaProduksi->urutan)->get(); // salah,,, harusnya nomor urut

        if (count($anotherRencanaProduksis) > 0) {
            Flash::error('Anda harus membatalkan Rencana Produksi setelah '.$rencanaProduksi->urutan.' terlebih dahulu.!');
            return redirect(route('rencana-produksi.index'));
        }

        // $this->rencanaProduksiCacheRepository->deleteByRP($rencanaProduksi->id);
        $this->jadwalMesinRepository->deleteByRP($rencanaProduksi->id);
        $this->shiftRepository->deleteByRP($rencanaProduksi->id);
        $input = $request->all();
        $input['urutan'] = "";
        $this->rencanaProduksiRepository->update($input, $id);
        $this->simpanHistoryPerubahan($rencanaProduksi, 'cancel');
        // $this->rencanaProduksiRepository->delete($id);

        Flash::success('Rencana Produksi berhasil di cancel.');
        return redirect(route('rencana-produksi.index'));
    }

    // update status dg button click dari halaman detail
    public function updateStatus($id, Updaterencana_produksiRequest $request){
        $rencanaProduksi = $this->rencanaProduksiRepository->find($id);
        $this->validate($request, [
            'status'  => 'required',
        ]);
        
        $input = $request->all();
        $status = $input['status'];
        if (empty($rencanaProduksi)) {
            Flash::error('Rencana Produksi tidak ditemukan');
            return redirect(route('rencana-produksi.show', [$id]));
        } 
        // else if ($status == 'confirm' && $rencanaProduksi->status != 'pending') {
        //     Flash::error('Hanya status pending yang bisa di confirm');
        //     return redirect(route('rencana-produksi.show', [$id]));
        // } 
        else if ($status == 'aktual' && $rencanaProduksi->status == 'finish') {
            Flash::error('Aktual hanya boleh dilakukan sebelum finish');
            return redirect(route('rencana-produksi.show', [$id]));
        } 
        else if ($status == 'hold' && $rencanaProduksi->status == 'finish') {
            Flash::error('Hold hanya boleh dilakukan sebelum finish');
            return redirect(route('rencana-produksi.show', [$id]));
        } 
        else {
            if ($status === 'finish') {
                $input['selesai'] = date('Y-m-d H:i:s');
            }
            $rencanaProduksi = $this->rencanaProduksiRepository->update($input, $id);
            $this->simpanHistoryPerubahan($rencanaProduksi, $status);
            Flash::success('Status berhasil dirubah jadi '.$status);
            return redirect(route('rencana-produksi.show', [$id]));
        }
    }

    // KALKULASI Rencana Produksi BERIKUTNYA YANG TERKENA IMBAS
    public function updateSetelahRencanaProduksi($rencanaProduksi) {
        // cari jadwal mesin setelahnya
        $anotherRencanaProduksis = $this->rencanaProduksiRepository->allQuery()
            ->where('id', '<>',  $rencanaProduksi->id)
            ->where('mesin_id', '=',  $rencanaProduksi->mesin_id)
            ->where('status', '!=', 'cancel')
            ->where('mulai', '>=',  $rencanaProduksi->mulai)->get(); 
        
        //cancel semua jadwal mesin setelahnya dulu
        foreach($anotherRencanaProduksis as $RP){
            $this->jadwalMesinRepository->deleteByRP($RP->id);
            $this->shiftRepository->deleteByRP($RP->id);
        }
        
        // hitung ulang dulu baru set lagi updat produksi
        foreach($anotherRencanaProduksis as $RP){
            $mulaiBaru = $this->cariTanggal($RP->mesin_id);
            $data['mulai'] = $mulaiBaru;
            $rencanaProduksi = $this->rencanaProduksiRepository->update($data, $RP->id);
            
            $this->updateRencanaProduksi($rencanaProduksi, null);
        }
    }

    // UNTUK MENCARI TANGGAL Rencana Produksi BERIKUTNYA YANG TERKENA IMBAS
    public function cariTanggal($mesin_id) {
        $query = $this->jadwalMesinRepository->allQuery();
        $lastMesin = $query->where('tipe', '=', 'kerja')
            ->where('mesin_id', $mesin_id)
            ->orderBy('selesai', 'desc')
            ->first();
        return date('Y-m-d H:i:s', strtotime($lastMesin->selesai));
    }

    // Untuk menyimpan rencana produksi history saat edit
    function simpanHistoryPerubahan($rencanaProduksi, $status=null) {
        $dataCache = @json_decode(json_encode($rencanaProduksi), true);
        $dataCache['rencana_proudksi_id'] = $rencanaProduksi->id;
        $dataCache['tanggal'] = null;
        $dataCache['user_id'] = Auth::user()->id;
        if ($status) {
            $dataCache['status'] = $status;
        }
        $this->rencanaProduksiCacheRepository->create($dataCache);
    }

    // NORMALISASI NOMOR URUT KARENA ADANYA PENYISISIPN
    public function susunKembaliNomorUrut($rencanaProduksi) {
        // cari jadwal mesin sebelumnya yang masih di bulan yang sama 
        // untuk mendapatkan nomor urutunya
        $year = substr($rencanaProduksi->mulai, 0, 4); // 4 digit year
        $month = substr($rencanaProduksi->mulai, 5, 2); // 2 digit month   
        $urutan = 0; // urutan mulai
        $initializeKey1 = substr($year, -2); // year in 2 digit
        $initializeKey2 = $month; // month 
        // normalisasi semua urutan mulai bulan itu sampai selesai
        $rencanaProduksis = $this->rencanaProduksiRepository->allQuery()
            ->whereYear('mulai', '>=', $year)
            ->whereMonth('mulai', '>=', $month)
            ->where('mesin_id', '=',  $rencanaProduksi->mesin_id)
            ->orderBy('mulai', 'asc')->get(); 
        
        // print_r(json_encode($rencanaProduksis));
        
        // hitung ulang dulu baru set lagi updat produksi
        foreach($rencanaProduksis as $RP){
            $urutanTanggal = str_replace("-", "", $RP->mulai);
            $key1 = substr($urutanTanggal, 2, 2); // year in 2 digit
            $key2 = substr($urutanTanggal, 4, 2); // month

            if ($key1 == $initializeKey1 && $key2 == $initializeKey2) {
                $urutan += 1;
            } else {
                $urutan = 1;
                $initializeKey1 = $key1;
                $initializeKey2 = $key2;
            }
            
            $urutanFormat = sprintf("%03d", $urutan);
            $data['urutan'] = $RP->mesin->kode.'-'.$key1.$key2.":".$urutanFormat;
            $rencanaProduksi = $this->rencanaProduksiRepository->update($data, $RP->id);
            $this->simpanHistoryPerubahan($rencanaProduksi);
        }
    }

    public function cetak_spk($id){
        $rencanaProduksi = $this->rencanaProduksiRepository->find($id);
        $pdf = PDF::loadview('rencana_produksis.spk', ['RP'=>$rencanaProduksi]);
        return $pdf->stream();
        // return view('rencana_produksis.spk')
        //     ->with('RP',  $rencanaProduksi);
    }

    public function cetak(Request $request){
        $tanggal = $request->get('tanggal');
        $selesai = $request->get('selesai');
        
        $mesin_id = $request->get('mesin_id');
        $mesin_name = $request->get('mesin_name');
        $status = $request->get('status');
        $query = $this->rencanaProduksiRepository->allQuery();
        
        if (!$tanggal) {
            $tanggal = date('Y-m-d');
        } 

        $tanggal = date('Y-m-d 23:59:00', strtotime($tanggal));
        $tanggalselesai = date('Y-m-d 00:00:01', strtotime($tanggal));
        $query = $query->where('mulai', '<=', $tanggal);
        $query = $query->where('selesai', '>=', $tanggalselesai);

        // $query = $query->where('selesai', '>=', $tanggal);
        // $query = $query->where('selesai', '>=', $tanggal);
        // if($selesai) {
        //     $selesai = date('Y-m-d 23:59:00', strtotime($selesai));
        //     $query = $query->where('selesai', '<=', $selesai);
        // }
        
        if ($mesin_id) {
            $query = $query->where('mesin_id', $mesin_id);
        };
        if ($status) {
            if ($status == 'semua') {
                #do nothing
                
            }
            else if ($status == 'aktif') {
                $query = $query->where('status', '!=', 'booking');
                $query = $query->where('status', '!=', 'cancel');
            } else
                $query = $query->where('status', $status);
        };
        // $query->orderBy('id', 'desc');
        $query->orderBy('mesin_id', 'asc');
        $rencanaProduksis = $query->paginate(25)->appends(request()->query());


        $aktuals = array();
        $estimates = array();
        foreach($rencanaProduksis as $field=>$value){
            // $shifts = $this->shiftRepository->allQuery()
            //     ->where('mulai', '<=', $tanggal)
            //     ->where('selesai', '>=', $tanggalselesai)
            //     ->where(['rencana_produksi_id' => $value->id])->get();
            $query = DB::table("shifts")
                ->where('mulai', '<=', $tanggal)
                ->where('selesai', '>=', $tanggalselesai)
                ->where(['rencana_produksi_id' => $value->id])->get();
            $aktual = $query->sum("aktual_hasil");
            $estimate = $query->sum("estimasi_hasil");
            array_push($aktuals,  $aktual);
            array_push($estimates,  $estimate);
        }


        $tanggal = date('d/m/Y', strtotime($tanggal));
        $selesai = date('d/m/Y', strtotime($selesai));
        $pdf = PDF::loadview('rencana_produksis.harian', [
            'RPS'=>$rencanaProduksis,  
            'date'=>$tanggal,
            'selesai'=>$selesai,
            'aktuals'=>$aktuals,
            'estimates'=>$estimates
        ], [], 'utf-8');
        // return view('rencana_produksis.harian')
            // ->with('RPS', $rencanaProduksis)
            // ->with('date', $tanggal)
            // ->with('selesai', $selesai)
            // ->with('aktuals', $aktuals)
            // ->with('estimates', $estimates);
        return $pdf->stream();
    }

    public function exportcsv(Request $request){
        $tanggal = $request->get('tanggal');
        $selesai = $request->get('selesai');
        $mesin_id = $request->get('mesin_id');
        $mesin_name = $request->get('mesin_name');
        $status = $request->get('status');
        $query = $this->rencanaProduksiRepository->allQuery();
       
        if (!$tanggal) {
            $tanggal = date('Y-m-d');
        } 

        $tanggal = date('Y-m-d 23:59:00', strtotime($tanggal));
        $tanggalselesai = date('Y-m-d 00:00:01', strtotime($tanggal));
        $query = $query->where('mulai', '<=', $tanggal);
        $query = $query->where('selesai', '>=', $tanggalselesai);

        if ($mesin_id) {
            $query = $query->where('mesin_id', $mesin_id);
        };
        if ($status) {
            if ($status == 'semua') {
                #do nothing
            }
            else if ($status == 'aktif') {
                $query = $query->where('status', '!=', 'booking');
                $query = $query->where('status', '!=', 'cancel');
            } else
                $query = $query->where('status', $status);
        };
        $query->orderBy('id', 'desc');
        $rencanaProduksis = $query->paginate(25)->appends(request()->query());
        $datas = [];

        foreach($rencanaProduksis as $field=>$value){
            $query = DB::table("shifts")
                ->where('mulai', '<=', $tanggal)
                ->where('selesai', '>=', $tanggalselesai)
                ->where(['rencana_produksi_id' => $value->id])->get();
            $aktual = $query->sum("aktual_hasil");
            $estimate = $query->sum("estimasi_hasil");
            $data = array(
                'id'=>$value->mesin->id,
                "mesin"=>$value->mesin->nama,
                "tipe"=>$value->nama,
                "jumlah"=>$aktual." / ". $estimate,
                "mulai"=>$value->mulai,
                "selesai"=>$value->selesai,
                "aktual"=>$value->persentasi_aktual,
            );
            array_push($datas, $data);
        }

        $fileName = 'export2.csv';
        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );
        $columns = array('RP ID', 'Mesin', 'Tipe', 'Jumlah', 'Mulai', 'Selesai', 'Persentasi Aktual');
        $callback = function() use($datas, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);
            foreach ($datas as $key => $data) {
                $new_row = array(
                    $data["id"], 
                    $data["mesin"], 
                    $data["tipe"], 
                    $data["jumlah"], 
                    $data["mulai"], 
                    $data["selesai"], 
                    $data["aktual"], 
                );
                fputcsv($file, $new_row);
            }
            fclose($file);
        };
        return response()->stream($callback, 200, $headers);
    }

    public function live(Request $request){
        $tanggal = date('Y-m-d H:i');
        $mesins = $this->mesinRepository->allQuery();
        $mesins = $mesins->paginate(8);
        $lastPage = $mesins->lastPage();
        $page = $request->get('page', 1);
        if ($page > $lastPage) {
            return redirect(route('rencana-produksi.live-info'));
        } 
        $datas = array();
        foreach($mesins as $field=>$value){
            $data = array(
                'id'=>"",
                "name"=>"",
                "finish_date"=>"",
                "status"=>"break",
                "mesin_name"=> $value->nama,
                "aktual_shift_ini"=>0,
                "target_shift_ini"=>0,
                'last_hint'=>null,
            );
            $rp = $this->rencanaProduksiRepository->allQuery();
            $rp = $rp->where('mulai', '<=', $tanggal);
            $rp = $rp->where('selesai', '>=', $tanggal);
            $rp = $rp->where('status', '!=', 'cancel');
            $rp = $rp->where('status', '!=', 'finish');
            $rp = $rp->where('mesin_id', $value->id)->get();

            if (count($rp) > 0) {
                // semua shift untuk rencana produksi ini
                $shifts = $this->shiftRepository->allQuery();
                $shifts = $shifts->where('rencana_produksi_id', $rp[0]->id);
                
                // shift sekarang
                $currentShift = $shifts->where('mulai', '<=', $tanggal);
                $currentShift = $currentShift->where('selesai', '>=', $tanggal)->get();
                if (count($currentShift) > 0) {
                    $data['id'] = $rp[0]->id;
                    $data['name'] = $rp[0]->nama;
                
                    $data['finish_date'] = date('d/m/y H:i', strtotime($currentShift[0]->selesai));
                    $data['aktual_shift_ini'] = $currentShift[0]->aktual_hasil;
                    $data['target_shift_ini'] = $currentShift[0]->estimasi_hasil;

                    // persentase dari shift seakrang untuk shift progress data show
                    $data['persent_shift'] = round($data['aktual_shift_ini'] / $data['target_shift_ini'] * 100, 2);

                    // hitung aktual dari keseluruhan rencana produksi untuk plan progress
                    $planAktual = $rp[0]->jumlah_aktual_produksi;
                    $planTarget = $rp[0]->jumlah_rencana_produksi;
                    $data['persent_plan'] = round($planAktual / $planTarget * 100, 2);

                    // HITUNG WARNING ATAU ENGGAK
                    // hitung waktu terakhir kali di tekan tombol ke skarang
                    $tanggal = date('Y-m-d H:i:s');
                    $jarak_last_hint = strtotime($tanggal) - strtotime($rp[0]->last_hint);
                    $data['last_hint'] = $rp[0]->last_hint;
                    if ($jarak_last_hint > 300) {
                        $data['status'] = "warning";
                    } else {
                        $data['status'] = "running";
                    }
                }
                if ($rp[0]->status == 'hold'){
                    $data['status'] = "hold";
                }
            } else {
                $data['status'] = "break";
            }
            if ($value->rusak == 'Y'){
                $data['status'] = "broken";
            } 
            
            array_push($datas, $data);
        }
        $mesins =  $this->mesinRepository->allQuery();
        $mesins =  $mesins->paginate(8)->appends(request()->query());
        return view('rencana_produksis.live')
            ->with('tanggal', $tanggal = date('d F Y, H:i'))
            ->with('nextPage', $page + 1)
            ->with('datas', $datas)
            ->with('mesins', $mesins);
    }

    public function live_export(Request $request){
        $tanggal = date('Y-m-d H:i');
        $mesins = $this->mesinRepository->allQuery()->get();
        $datas = array();
        foreach($mesins as $field=>$value){
            $data = array(
                'id'=>"",
                "name"=>"",
                "finish_date"=>"",
                "status"=>"break",
                "mesin_name"=> $value->nama,
                "aktual_shift_ini"=>0,
                "target_shift_ini"=>0,
                'last_hint'=>null,
            );
            $rp = $this->rencanaProduksiRepository->allQuery();
            $rp = $rp->where('mulai', '<=', $tanggal);
            $rp = $rp->where('selesai', '>=', $tanggal);
            $rp = $rp->where('status', '!=', 'cancel');
            $rp = $rp->where('status', '!=', 'finish');
            $rp = $rp->where('mesin_id', $value->id)->get();
            if (count($rp) > 0) {
                // semua shift untuk rencana produksi ini
                $shifts = $this->shiftRepository->allQuery();
                $shifts = $shifts->where('rencana_produksi_id', $rp[0]->id);
                
                // shift sekarang
                $currentShift = $shifts->where('mulai', '<=', $tanggal);
                $currentShift = $currentShift->where('selesai', '>=', $tanggal)->get();
                if (count($currentShift) > 0) {
                    $data['id'] = $rp[0]->id;
                    $data['name'] = $rp[0]->nama;
                
                    $data['finish_date'] = date('d/m/y H:i', strtotime($currentShift[0]->selesai));
                    $data['aktual_shift_ini'] = $currentShift[0]->aktual_hasil;
                    $data['target_shift_ini'] = $currentShift[0]->estimasi_hasil;

                    // persentase dari shift seakrang untuk shift progress data show
                    $data['persent_shift'] = round($data['aktual_shift_ini'] / $data['target_shift_ini'] * 100, 2);

                    // hitung aktual dari keseluruhan rencana produksi untuk plan progress
                    $planAktual = $rp[0]->jumlah_aktual_produksi;
                    $planTarget = $rp[0]->jumlah_rencana_produksi;
                    $data['persent_plan'] = round($planAktual / $planTarget * 100, 2);

                    // HITUNG WARNING ATAU ENGGAK
                    // hitung waktu terakhir kali di tekan tombol ke skarang
                    $tanggal = date('Y-m-d H:i:s');
                    $jarak_last_hint = strtotime($tanggal) - strtotime($rp[0]->last_hint);
                    $data['last_hint'] = $rp[0]->last_hint;
                    if ($jarak_last_hint > 300) {
                        $data['status'] = "warning";
                    } else {
                        $data['status'] = "running";
                    }
                    
                }
            } else {
                $data['status'] = "break";
            }
            if ($value->rusak == 'Y'){
                $data['status'] = "broken";
            }
            array_push($datas, $data);
        }
        
        $fileName = 'live_report.csv';
        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );
        $columns = array('RP ID', 'Mesin', 'Tipe', 'Target PAR', 'Aktual PAR', 'Finish Shift', 'Status',
                         'Shift -- % PAR', '% SPK');
        $callback = function() use($datas, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);
            foreach ($datas as $key => $data) {
                $new_row = array(
                    $data["id"], $data["mesin_name"], $data["name"], $data["target_shift_ini"], 
                    $data["aktual_shift_ini"], $data["finish_date"], $data["status"],
                    isset($data['persent_shift']) ? $data['persent_shift']:'-',
                    isset($data['persent_plan']) ? $data['persent_plan']:'-'
                );
                fputcsv($file, $new_row);
                // if ($data['status'] == "running" || $data['status'] == "warning") {
                //     $new_row = array(
                //         '-', 'Shift -- % PAR', '-', '-', '-', '-', $data['persent_shift']."%"
                //     );
                //     fputcsv($file, $new_row);
                //     $new_row = array(
                //         '-', '% SPK', '-', '-', '-', '-', $data['persent_plan'].'%'
                //     );
                //     fputcsv($file, $new_row);
                // }
            }
            fclose($file);
        };
        return response()->stream($callback, 200, $headers);
    }

    public function live_info(Request $request){
        $tanggal = date('Y-m-d H:i');
        $infos = $this->infoRepository->allQuery();
        $infos = $infos->where('is_active', '=', 'Y');
        $infos = $infos->where('start_date', '<=', $tanggal);
        $infos = $infos->where('end_date', '>=', $tanggal);
        $infos = $infos->orderBy('start_date', 'desc');
        $infos = $infos->paginate(1);
        $lastPage = $infos->lastPage();
        $page = $request->get('page', 1);
        if ($page > $lastPage || $infos->isEmpty()) {
            return redirect(route('rencana-produksi.live'));
        } 
        $infos = $infos->appends(request()->query());
        return view('rencana_produksis.live2')
            ->with('nextPage', $page + 1)
            ->with('info', $infos->first())
            ->with('infos', $infos);
    }

    public function upload(Request $request)
    {
        
        $request->validate([
            'gambar' => 'required|max:2048',
            'id' => 'required',
        ]);
        $id = $request->id;

        $rencanaProduksi = $this->rencanaProduksiRepository->find($id);
        $input = $request->all();
        if (empty($rencanaProduksi)) {
            Flash::error('Rencana Produksi tidak ditemukan');
            return redirect(route('rencana-produksi.show', [$id]));
        } else {
            $imageName = time() . '.' . $request->gambar->extension();  
            $request->gambar->move(public_path('file'), $imageName);

            $input = array("rencana_produksi_id"=>$id, "name"=> $imageName);
            $attachments = $this->attachmentsRepository->create($input);

            Flash::success('File berhasil di upload.');
            return back();
        }
    }

    public function addnote(Request $request)
    {
        $request->validate([
            'note' => 'required',
            'id' => 'required',
        ]);
        $id = $request->id;
        $rencanaProduksi = $this->rencanaProduksiRepository->find($id);
        if (empty($rencanaProduksi)) {
            Flash::error('Rencana Produksi tidak ditemukan');
            return redirect(route('rencana-produksi.show', [$id]));
        } else {
            $input = array("note"=>$request->note);
            $attachments = $this->rencanaProduksiRepository->update($input, $id);
            Flash::success('Catatan berhasil ditambahkan.');
            return back();
        }
    }

    public function deleteattachment($id)
    {
        $attachments = $this->attachmentsRepository->find($id);
        if (empty($attachments)) {
            Flash::error('Attachments not found');
            return redirect(route('attachments.index'));
        }
        $this->attachmentsRepository->delete($id);
        return back();
    }

    public function backupButton(){
        $query = $this->rencanaProduksiRepository->allQuery();
        $query =  $query->limit(15);
        $query = $query->where('status', '=', 'aktual');
        $query = $query->orderBy('id', 'desc');
        $rencanaProduksis = $query->get();
        return view('backup_button.index')
            ->with('rps', $rencanaProduksis);
    }

    public function backupButtonPost(Updaterencana_produksiRequest $request){
        // PERIKSA KEMBALI CODE INI MASIH BUGGI
        // kadang salah ambil shift, pastikan shift sesuai rencana produksi
        $input = $request->all();
        $RP_id = $input['id'];
        $RencanaProduksi = $this->rencanaProduksiRepository->find($RP_id);
        $mesin_id = $RencanaProduksi->mesin_id;

        // search Current Shipt
        $datetime = date("Y-m-d H:i:s");
        $query = $this->shiftRepository->allQuery();
        $query = $query->where('rencana_produksi_id', $RP_id);
        $query = $query->orderBy('id', 'desc')->get();
        // $query = $query->whereHas('rencana_produksi', function($query) use($mesin_id) {
        //     $query->where('mesin_id', $mesin_id);
        // });
        // $query = $query->where('mulai', '<=', $datetime);
        // $query = $query->where('selesai', '>=', $datetime)->get();
        if ($query->count() == 0) {
            return response()->json(["message" => "Can't found shift with ". $RencanaProduksi->mesin->nama], 400);
        }
        $shiftId = $query[0]->id;
        $aktual = $query[0]->aktual_hasil;
        // Searh Tipe Untuk mendapatkan count setiap kali push button di klik
        $tipe = $this->tipeRepository->find($query[0]->rencana_produksi->type_id);
        $aktual += $tipe->count;
        $data = [
            'aktual_hasil' => $aktual
        ];
        // update Currnet Shift
        $shift = $this->shiftRepository->update($data, $shiftId);

        // Update Rencana Produksi
        $aktual_hasil = $this->shiftRepository->allQuery()
            ->where('rencana_produksi_id', $RP_id)->sum('aktual_hasil');
        $persentasi_aktual = $aktual_hasil / $RencanaProduksi->jumlah_rencana_produksi * 100;
        $data = [
            'jumlah_aktual_produksi' => $aktual_hasil,
            'persentasi_aktual' => $persentasi_aktual,
            'last_hint' => $datetime
        ];
        $this->rencanaProduksiRepository->update($data, $RP_id);
        
        return response()->json(['message'=>'Berhasil tambah data', 'aktual'=>$aktual_hasil]);
    }

}

