<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateshiftRequest;
use App\Http\Requests\UpdateshiftRequest;
use App\Repositories\shiftRepository;
use App\Repositories\tipeRepository;
use App\Repositories\rencana_produksiRepository;
use App\Repositories\mesinRepository;
use Illuminate\Http\Request;
use App\Repositories\notificationRepository;

use Flash;
use Response;


class ApiController extends Controller
{
    private $shiftRepository;
    private $tipeRepository;
    private $rencana_produksiRepository;
    private $mesinRepository;
    private $notificationRepository;

    public function __construct(
        tipeRepository $tipeRepository,
        rencana_produksiRepository $rencana_produksiRepository,
        shiftRepository $shiftRepo,
        mesinRepository $mesinRepository,
        notificationRepository $notificationRepo
    )
    {
        $this->shiftRepository = $shiftRepo;
        $this->tipeRepository = $tipeRepository;
        $this->rencana_produksiRepository = $rencana_produksiRepository;
        $this->mesinRepository = $mesinRepository;
        $this->notificationRepository = $notificationRepo;
    }


    public function set_count(Request $request){
        $mesin_id = $request->get('mesin_id');
        $button_id = $request->get('button_id');
        $datetime = date("Y-m-d H:i:s");
        
        $mesin = array();
        if($button_id) {
            $mesin = $this->mesinRepository->allQuery()->where('button_id', '=', $button_id)->get();
        }
        if(count($mesin)>1) {
            return response()->json(["message" => "multiple machine with button id ".$button_id], 406);
        }
        if(count($mesin) == 1) {
            $mesin_id = $mesin[0]->id;
        }

        // search Current Shipt
        $query = $this->shiftRepository->allQuery();
        $query = $query->whereHas('rencana_produksi', function($query) use($mesin_id) {
            $query->where('mesin_id', $mesin_id);
        });
        $query = $query->where('mulai', '<=', $datetime);
        $query = $query->where('selesai', '>=', $datetime)->get();
        if ($query->count() == 0) {
            return response()->json(["message" => "can't found shift with mesin id ". $mesin_id . " - ".  $mesin[0]->nama], 406);
        }

        $id = $query[0]->id;
        $aktual = $query[0]->aktual_hasil;

        // Searh Tipe Untuk mendapatkan count
        $tipe = $this->tipeRepository->find($query[0]->rencana_produksi->type_id);
        $aktual += $tipe->count;
        $data = [
            'aktual_hasil' => $aktual
        ];

        // update Currnet Shift
        $shift = $this->shiftRepository->update($data, $id);

        // Update Rencana Produksi
        $RP_id = $query[0]->rencana_produksi->id;
        $RencanaProduksi = $this->rencana_produksiRepository->find($RP_id);
        $aktual_hasil = $this->shiftRepository->allQuery()
            ->where('rencana_produksi_id', $RP_id)->sum('aktual_hasil');
        $persentasi_aktual = $aktual_hasil / $RencanaProduksi->jumlah_rencana_produksi * 100;
        $data = [
            'jumlah_aktual_produksi' => $aktual_hasil,
            'persentasi_aktual' => $persentasi_aktual,
            'last_hint' => $datetime
        ];
        $this->rencana_produksiRepository->update($data, $RP_id);
        
        return response()->json(['message'=>'Berhasil tambah data', 'status'=>'success']);
    }

    public function set_rusak(Request $request){
        $mesin_id = $request->get('mesin_id', '');
        $button_id = $request->get('button_id', '');
        $status = $request->get('status');
        if (!$status) {
            $status = 'Y';
        }
        
        $mesin = array();
        if($button_id) {
            $mesin = $this->mesinRepository->allQuery()->where('button_id', '=', $button_id)->get();
        }
        if(count($mesin)>1) {
            return response()->json(["message" => "multiple machine with button id ".$button_id], 406);
        }
        if(count($mesin) == 1) {
            $mesin_id = $mesin[0]->id;
        }

        $data['rusak'] = $status;
        $updatedMesin = $this->mesinRepository->update($data, $mesin_id);

        if ($updatedMesin === false) {
            return response()->json(["message" => "Gagal update mesin, mesin tidak ditemukan", 'status'=>'failed'], 406);
        }

        return response()->json(['message'=>'Berhasil set mesin', 'status'=>'success']);
    }

    public function newnotif(Request $request){
        $jabatan = $request->get('jabatan');
        $notifications = $this->notificationRepository->allQuery()
            ->where('departement', $jabatan)
            ->where('is_read', 'F')
            ->get();
        $total = $notifications->count();
        if ($total>0)
            return response()->json(['status'=>'true']);
        return response()->json(['status'=>'false']);
    }

}
