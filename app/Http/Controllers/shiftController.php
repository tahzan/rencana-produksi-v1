<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateshiftRequest;
use App\Http\Requests\UpdateshiftRequest;
use App\Repositories\shiftRepository;
use App\Repositories\rencana_produksiRepository;
use App\Http\Controllers\AppBaseController;
use App\Repositories\attachmentsRepository;
use Illuminate\Http\Request;
use Flash;
use Response;
use Illuminate\Support\Facades\File;

class shiftController extends AppBaseController
{
    /** @var  shiftRepository */
    private $shiftRepository;
    private $rencanaProduksiRepository;
    private $attachmentsRepository;


    public function __construct(
        rencana_produksiRepository $rencanaProduksiRepo,
        shiftRepository $shiftRepo,
        attachmentsRepository $attachmentsRepo
    )
    {
        $this->shiftRepository = $shiftRepo;
        $this->rencanaProduksiRepository = $rencanaProduksiRepo;
        $this->attachmentsRepository = $attachmentsRepo;
    }

    /**
     * Display a listing of the shift.
     *
     * @param Request $request
     *
     * @return Response
     */
    // public function index(Request $request)
    // {
    //     $shifts = $this->shiftRepository->all();

    //     return view('shifts.index')
    //         ->with('shifts', $shifts);
    // }

    /**
     * Show the form for creating a new shift.
     *
     * @return Response
     */
    // 
    
    /**
     * Store a newly created shift in storage.
     *
     * @param CreateshiftRequest $request
     *
     * @return Response
     */
    // public function store(CreateshiftRequest $request)
    // {
    //     $input = $request->all();

    //     $shift = $this->shiftRepository->create($input);

    //     Flash::success('Shift berhasil di simpan.');

    //     return redirect(route('shifts.index'));
    // }

    /**
     * Display the specified shift.
     *
     * @param int $id
     *
     * @return Response
     */
    // public function show($id){
    //     $shift = $this->shiftRepository->find($id);
    //     if (empty($shift)) {
    //         Flash::error('Shift tidak ditemukan');
    //         return redirect(route('shifts.index'));
    //     }
    //     return view('shifts.show')->with('shift', $shift);
    // }

    /**
     * Show the form for editing the specified shift.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $shift = $this->shiftRepository->find($id);
        if (empty($shift)) {
            Flash::error('Shift tidak ditemukan');
            return redirect(route('shifts.index'));
        }
        $files = $this->attachmentsRepository->allQuery()->where('shift_id',$id)->get();
        return view('shifts.edit')
            ->with('files', $files)
            ->with('shift', $shift);
    }

    /**
     * Update the specified shift in storage.
     *
     * @param int $id
     * @param UpdateshiftRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateshiftRequest $request){
        $this->validate($request, [
            'rencana_produksi_id' => 'required',
            'aktual_hasil'  => 'required',
            'estimasi_hasil' => 'required',
        ]);

        $shift = $this->shiftRepository->find($id);
        if (empty($shift)) {
            Flash::error('Shift tidak ditemukan');
            return redirect(route('rencana-produksi.index'));
        }
        
        $input = $request->all();
        $shift = $this->shiftRepository->update($input, $id);
        $RP_id = $input['rencana_produksi_id'];
        $RencanaProduksi = $this->rencanaProduksiRepository->find($RP_id);

        $aktual_hasil = $this->shiftRepository->allQuery()
            ->where('rencana_produksi_id', $RP_id)->sum('aktual_hasil');

        $persentasi_aktual = $aktual_hasil / $RencanaProduksi->jumlah_rencana_produksi * 100;

        $data = [
            'jumlah_aktual_produksi' => $aktual_hasil,
            'persentasi_aktual' => $persentasi_aktual
        ];

        $this->rencanaProduksiRepository->update($data, $RP_id);

        Flash::success('Shift berhasil di update.');
        return back();
    }

    public function upload(Request $request)
    {
        $request->validate([
            'gambar' => 'required|max:2048',
            'rencana_produksi_id' => 'required',
            'shift_id' => 'required',
        ]);
        $shift_id = $request->shift_id;
        $rencana_produksi_id = $request->rencana_produksi_id;
        $input = $request->all();

        $shift = $this->shiftRepository->find($shift_id);
        if (empty($shift)) {
            Flash::error('Shift tidak ditemukan');
            return back();
        } else {
            $imageName = time().$request->gambar->getClientOriginalName();  
            $dataa = $request->gambar->move(public_path('file'), $imageName);
            $input = array(
                "shift_id"=>$shift_id,
                "rencana_produksi_id"=>$rencana_produksi_id,   
                "name"=> $imageName
            );
            $attachments = $this->attachmentsRepository->create($input);
            Flash::success('File berhasil di upload.');
            return back();
        }
    }

    public function deleteattachment($id)
    {
        $attachments = $this->attachmentsRepository->find($id);
        if (empty($attachments)) {
            Flash::error('Lampiran tidak ditemukan');
            return redirect(route('attachments.index'));
        }
        $filename = $attachments;
        $filePath = public_path('file/'.$attachments->name);
        if (File::exists($filePath)) {
            File::delete($filePath);
        } else {
            // dd("file not exist");
        }
        $this->attachmentsRepository->delete($id);
        return back();
    }

    /**
     * Remove the specified shift from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    // public function destroy($id)
    // {
    //     $shift = $this->shiftRepository->find($id);

    //     if (empty($shift)) {
    //         Flash::error('Shift tidak ditemukan');

    //         return redirect(route('shifts.index'));
    //     }

    //     $this->shiftRepository->delete($id);

    //     Flash::success('Shift berhasil di delete.');

    //     return redirect(route('shifts.index'));
    // }
}
