<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createdaily_stockRequest;
use App\Http\Requests\Updatedaily_stockRequest;
use App\Repositories\daily_stockRepository;
use App\Repositories\daily_stock_itemRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Flash;
use Response;
use App\Repositories\outstanding_itemRepository;
use App\Repositories\outstandingRepository;
use App\Repositories\report_daily_stockRepository;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\StockImport;
use App\Imports\StockImportValidation;
use App\Repositories\tipeRepository;
use App\Repositories\ppicRepository;
use App\Repositories\ppic_logRepository;
use App\Http\Controllers\ppicController\createPPIC;
use App\Repositories\notificationRepository;


class daily_stockController extends AppBaseController
{
    /** @var  daily_stockRepository */
    private $dailyStockRepository;
    private $dailyStockItemRepository;
    private $outstandingRepository;
    private $outstandingItemRepository;
    private $reportDailyStockRepository;
    private $tipeRepository;
    private $ppicRepository;
    private $ppicLogRepository;

    public function __construct(
        daily_stockRepository $dailyStockRepo,
        daily_stock_itemRepository $dailyStockItemRepo,
        outstandingRepository $outstandingRepo, 
        outstanding_itemRepository $outstandingItemRepo,
        report_daily_stockRepository $reportDailyStockRepo,
        tipeRepository $tipeRepo,
        ppicRepository $ppicRepo, 
        ppic_logRepository $ppicLogRepo,
        notificationRepository $notificationRepo
    )
    {
        $this->dailyStockRepository = $dailyStockRepo;
        $this->dailyStockItemRepository = $dailyStockItemRepo;
        $this->outstandingRepository = $outstandingRepo;
        $this->outstandingItemRepository = $outstandingItemRepo;
        $this->reportDailyStockRepository = $reportDailyStockRepo;
        $this->tipeRepository = $tipeRepo;
        $this->ppicRepository = $ppicRepo;
        $this->ppicLogRepository = $ppicLogRepo;
        $this->notificationRepository = $notificationRepo;
    }

    /**
     * Display a listing of the daily_stock.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $dailyStocks = $this->dailyStockRepository->allQuery();

        $start = $request->get('start') ?? "";
        $end = $request->get('end')?? "";

        $status = $request->get('status');
        if ($status) {
            $dailyStocks->where('status', $status);
        } 
        // else {
        //     $dailyStocks->where('status', '<>', 'cancle');
        // }

        if ($start  && $end) {
            $dailyStocks = $dailyStocks->whereDate('created_at', '>=', $start);
            $dailyStocks = $dailyStocks->whereDate('created_at', '<=',  $end);
        }

        else if ($start) {
            $dailyStocks = $dailyStocks->whereDate('created_at', '>=',  $start);
        }

        else if ($end) {
            $dailyStocks = $dailyStocks->whereDate('created_at', '<=',  $end);
        };

        $dailyStocks = $dailyStocks->orderBy('id', 'DESC');
        $dailyStocks = $dailyStocks->paginate(50)->appends(request()->query());

        return view('daily_stocks.index')
            ->with('start', $start)
            ->with('end', $end)
            ->with('status', $status)
            ->with('dailyStocks', $dailyStocks);
    }

    /**
     * Show the form for creating a new daily_stock.
     *
     * @return Response
     */
    public function create()
    {
        return view('daily_stocks.create');
    }

    /**
     * Store a newly created daily_stock in storage.
     *
     * @param Createdaily_stockRequest $request
     *
     * @return Response
     */
    public function store(Createdaily_stockRequest $request)
    {
        $input = $request->all();
        $input['user_id'] = $request->user()->id;
        $input['status'] = 'draft';
        $input['total_type'] = 0;
        $input['total_qty'] = 0;
        $input['no_transaksi'] = $this->generateNoTransaksi($input['tanggal']);

        $this->validate($request, [
			'file' => 'required|mimes:csv,xls,xlsx',
        ]);
 
		// menangkap file excel
		$file = $request->file('file');
 
		// membuat nama file unik
		$nama_file = rand().$file->getClientOriginalName();
 
        // upload ke folder file_siswa di dalam folder public
        $destination = public_path().'/file_tipe/';
		$file->move($destination, $nama_file);
        
        $tipeIds = $this->tipeRepository->allQuery();
        $tipeIds = $tipeIds->pluck('nama')->toArray();
        Excel::import(new StockImportValidation($tipeIds), public_path('/file_tipe/'.$nama_file));

		// import data
        $dailyStock = $this->dailyStockRepository->create($input);
        Excel::import(new StockImport($dailyStock->id), public_path('/file_tipe/'.$nama_file));
 
		// notifikasi dengan session
        Flash::success('Stok Berhasil Di Import berhasil diimport!');
 
		// alihkan halaman kembali
        return redirect()->route('daily-stocks.show', [$dailyStock->id]);

    }

    /**
     * Display the specified daily_stock.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {   
        $dailyStock = $this->dailyStockRepository->find($id);
        $dailyStockItems = $this->dailyStockItemRepository->all();
        $dailyStockItems = $dailyStockItems->where('daily_stock_id', $id);

        $dailyStock->total_type = count($dailyStockItems);
        $dailyStock->total_qty = $dailyStockItems->sum('stock');
        $dailyStock->save();

        if (empty($dailyStock)) {
            Flash::error('Daily Stock not found');

            return redirect(route('daily-stocks.index'));
        }

        return view('daily_stocks.show')->with(
            'dailyStock', $dailyStock)->with('dailyStockItems', $dailyStockItems);
    }

    /**
     * Show the form for editing the specified daily_stock.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $dailyStock = $this->dailyStockRepository->find($id);
        $dailyStockItems = $this->dailyStockItemRepository->all();
        $dailyStockItems = $dailyStockItems->where('daily_stock_id', $id);


        if (empty($dailyStock)) {
            Flash::error('Tidak ada stock ditumen');

            return redirect(route('daily-stocks.index'));
        }

        return view('daily_stocks.edit')->with('dailyStock', $dailyStock)
            ->with('dailyStockItems', $dailyStockItems);
    }

    /**
     * Update the specified daily_stock in storage.
     *
     * @param int $id
     * @param Updatedaily_stockRequest $request
     *
     * @return Response
     */
    public function update($id, Updatedaily_stockRequest $request)
    {
        $dailyStock = $this->dailyStockRepository->find($id);

        if (empty($dailyStock)) {
            Flash::error('Daily Stock not found');

            return redirect(route('daily-stocks.index'));
        }

        $dailyStock = $this->dailyStockRepository->update($request->all(), $id);

        $this->validate($request, [
			'file' => 'required|mimes:csv,xls,xlsx',
        ]);
 
		// menangkap file excel
		$file = $request->file('file');
        $input = $request->all();

        // membuat nama file unik
		$nama_file = rand().$file->getClientOriginalName();
 
        // upload ke folder file_siswa di dalam folder public
        $destination = public_path().'/file_tipe/';
		$file->move($destination, $nama_file);

        $tipeIds = $this->tipeRepository->allQuery();
        $tipeIds = $tipeIds->pluck('id')->toArray();
        // Excel::import(new StockImportValidation($tipeIds), public_path('/file_tipe/'.$nama_file));
        
		// import data
		Excel::import(new StockImport($dailyStock->id), public_path('/file_tipe/'.$nama_file));
 
		// notifikasi dengan session
        Flash::success('Stok Berhasil Di Update!');
 
		// alihkan halaman kembali
        return redirect()->route('daily-stocks.show', [$dailyStock->id]);
    }

    /**
     * Remove the specified daily_stock from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy(Request $request, $id)
    {   
        $dailyStock = $this->dailyStockRepository->find($id);

        $input = $request->all();
        $start = $input['start'];
        $end = $input['end'];

        if (empty($dailyStock)) {
            Flash::error('Daily Stock not found');
            return redirect(route('daily-stocks.index'));
        }

        // $this->dailyStockRepository->delete($id);
        $udpateData = array(
            'status' => 'cancel'
        );
        // cancle daily stock
        $this->dailyStockRepository->update($udpateData, $id);
        // cancle daily stock items
        // DB::table('daily_stock_items')->where('daily_stock_id', '=', $id)
        //     ->update(['status' => 'cancle']);

        // delete ppic
        $report = DB::table('report_daily_stocks')->where('daily_stock_id', '=', $id)->get();
        foreach ($report as $rr) {
            DB::table('ppics')->where('id', '=', $rr->ppic_id)->delete();
        }
        // delete report
        DB::table('report_daily_stocks')->where('daily_stock_id', '=', $id)->delete();

        Flash::success('Daily Stock canceled successfully.');

        return redirect(route('daily-stocks.index', ['status'=>"",'start'=>$start, 'end'=>$end]));
    }

    public function updateStatus($id, Updatedaily_stockRequest $request){
        $dailyStock = $this->dailyStockRepository->find($id);
        $dailyStockItems = $this->dailyStockItemRepository->all()->where('daily_stock_id', $id);
        foreach($dailyStockItems as $item){
            $query = DB::table('outstanding_items')
                ->join('outstandings', 'outstandings.id', '=', 'outstanding_items.outstanding_id')
                ->where('outstanding_items.type_id', '=', $item->type_id)
                ->where('outstandings.status', '=', 'processed')
                ->where('outstandings.deleted_at',  null)
                ->where('outstanding_items.deleted_at',  null)
                ->where('outstanding_items.error',  "")
                ->get();
            
            // 1. hitung semua outstanding & create report
            $outstanding_total = $query->sum("qty");
            $sum = $item->stock - $outstanding_total;
            $input = array(
                "type_id" => $item->type_id,
                "daily_stok" => $item->stock,
                "out_standing" => $outstanding_total,
                "sum" => $sum,
                // "no_transaksi" => $this->generateNoTransaksiLaporan($dailyStock->tanggal),
                "tanggal" => $dailyStock->tanggal,
                "daily_stock_id" => $dailyStock->id
            );
            if ($item->type_id !== 0) {
                $reportDailyStock = $this->reportDailyStockRepository->create($input);
            }

            
            $ppicId = app('App\Http\Controllers\ppicController')->createPPIC($item->type_id, $sum, $request->user());
            $udpateData = array(
                "ppic_id" => $ppicId
            );

            // notif PPIC jika sum minus
            if($sum < 0) {
                $input3 = array (
                    'departement'=>'ppic',
                    'tanggal' => $dailyStock->tanggal,
                    'status' => 'Waiting Response',
                    'ppic_id' => $ppicId,
                    'message' => $item->type->nama . ' | OUTSTANDING '.$outstanding_total,
                    'is_read' => 'F',
                );
                $notifications = $this->notificationRepository->allQuery();
                $notifications = $notifications->where('ppic_id', $ppicId)->delete();
                $this->notificationRepository->create($input3);
            }
            if ($item->type_id !== 0) {
                $this->reportDailyStockRepository->update($udpateData, $reportDailyStock->id);
            }
            DB::table('daily_stocks')->where('need_to_update', '<>', '')->update(['need_to_update'=>'']);
        }

        // dd(@json_encode($dailyStock));
        $input = $request->all();
        $this->validate($request, [
            'status'  => 'required',
        ]);
        $this->dailyStockRepository->update($input, $id);
        Flash::success('Status berhasil dirubah jadi '.$input['status']);
        return redirect()->route('daily-stocks.show', [$id]);
    }


    function generateNoTransaksi($tanggal) {
        $year = substr($tanggal, 0, 4);
        $month = substr($tanggal, 5, 2);
        $otherRencana = DB::table('daily_stocks')
            ->whereYear('tanggal', $year)
            ->whereMonth('tanggal', $month)
            ->orderBy('no_transaksi', 'desc')
            ->first();

        $urutanTanggal = str_replace("-", "", $tanggal);
        $key0 = "DS-";
        $key1 = substr($urutanTanggal, 2, 2);
        $key2 = substr($urutanTanggal, 4, 2);
        if (!$otherRencana) {
            $no_transaksi = sprintf("%04d", 1);
            $no_transaksi  = $key0.$key1.$key2."-".$no_transaksi;
        } else {
            $prevUrutan = substr($otherRencana->no_transaksi, -3);
            $prevUrutan = (int) $prevUrutan;
            $no_transaksi = sprintf("%04d", $prevUrutan + 1);
            $no_transaksi  =  $key0.$key1.$key2."-".$no_transaksi;
        }
        return $no_transaksi;
    }

    // function generateNoTransaksiLaporan($tanggal) {
    //     $year = substr($tanggal, 0, 4);
    //     $month = substr($tanggal, 5, 2);
    //     $otherRencana = DB::table('report_daily_stocks')
    //         ->whereYear('tanggal', $year)
    //         ->whereMonth('tanggal', $month)
    //         ->orderBy('no_transaksi', 'desc')
    //         ->first();

    //     $urutanTanggal = str_replace("-", "", $tanggal);
    //     $key0 = "PP-";
    //     $key1 = substr($urutanTanggal, 6, 2);
    //     $key2 = substr($urutanTanggal, 4, 2);
    //     if (!$otherRencana) {
    //         $no_transaksi = sprintf("%04d", 1);
    //         $no_transaksi  = $key0.$key1.$key2."-".$no_transaksi;
    //     } else {
    //         $prevUrutan = substr($otherRencana->no_transaksi, -3);
    //         $prevUrutan = (int) $prevUrutan;
    //         $no_transaksi = sprintf("%04d", $prevUrutan + 1);
    //         $no_transaksi  =  $key0.$key1.$key2."-".$no_transaksi;
    //     }
    //     return $no_transaksi;
    // }

}
