<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateattachmentsRequest;
use App\Http\Requests\UpdateattachmentsRequest;
use App\Repositories\attachmentsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class attachmentsController extends AppBaseController
{
    /** @var  attachmentsRepository */
    private $attachmentsRepository;

    public function __construct(attachmentsRepository $attachmentsRepo)
    {
        $this->attachmentsRepository = $attachmentsRepo;
    }

    /**
     * Display a listing of the attachments.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $attachments = $this->attachmentsRepository->all();

        return view('attachments.index')
            ->with('attachments', $attachments);
    }

    /**
     * Show the form for creating a new attachments.
     *
     * @return Response
     */
    public function create()
    {
        return view('attachments.create');
    }

    /**
     * Store a newly created attachments in storage.
     *
     * @param CreateattachmentsRequest $request
     *
     * @return Response
     */
    public function store(CreateattachmentsRequest $request)
    {
        $input = $request->all();

        $attachments = $this->attachmentsRepository->create($input);

        Flash::success('Attachments saved successfully.');

        return redirect(route('attachments.index'));
    }

    /**
     * Display the specified attachments.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $attachments = $this->attachmentsRepository->find($id);

        if (empty($attachments)) {
            Flash::error('Attachments not found');

            return redirect(route('attachments.index'));
        }

        return view('attachments.show')->with('attachments', $attachments);
    }

    /**
     * Show the form for editing the specified attachments.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $attachments = $this->attachmentsRepository->find($id);

        if (empty($attachments)) {
            Flash::error('Attachments not found');

            return redirect(route('attachments.index'));
        }

        return view('attachments.edit')->with('attachments', $attachments);
    }

    /**
     * Update the specified attachments in storage.
     *
     * @param int $id
     * @param UpdateattachmentsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateattachmentsRequest $request)
    {
        $attachments = $this->attachmentsRepository->find($id);

        if (empty($attachments)) {
            Flash::error('Attachments not found');

            return redirect(route('attachments.index'));
        }

        $attachments = $this->attachmentsRepository->update($request->all(), $id);

        Flash::success('Attachments updated successfully.');

        return redirect(route('attachments.index'));
    }

    /**
     * Remove the specified attachments from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $attachments = $this->attachmentsRepository->find($id);

        if (empty($attachments)) {
            Flash::error('Attachments not found');
            return redirect(route('attachments.index'));
        }
        $this->attachmentsRepository->delete($id);
        Flash::success('File berhasil di hapus.');
        return redirect(route('attachments.index'));
    }
}
