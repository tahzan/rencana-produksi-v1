<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\UserRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use Hash;

class UserController extends AppBaseController{
    private $userRepository;

    public function __construct(UserRepository $userRepo){
        $this->userRepository = $userRepo;
    }

    public function index(Request $request){
        $users = $this->userRepository->all();
        return view('users.index')->with('users', $users);
    }

    public function create(){
        return view('users.create');
    }

    public function store(CreateUserRequest $request){
        $this->validate($request, [
            'username' => 'unique:users|required',
        ]);

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $user = $this->userRepository->create($input);
        Flash::success('Data pengguna berhasil disimpan.');
        return redirect(route('users.index'));
    }

    // public function show($id){
    //     $user = $this->userRepository->find($id);
    //     if (empty($user)) {
    //         Flash::error('Data Pengguna tidak ditemukan');
    //         return redirect(route('users.index'));
    //     }
    //     return view('users.show')->with('user', $user);
    // }

    public function edit($id){
        $user = $this->userRepository->find($id);
        if (empty($user)) {
            Flash::error('Data pengguna tidak ditemukan');
            return redirect(route('users.index'));
        }
        return view('users.edit')->with('user', $user);
    }


    public function update($id, UpdateUserRequest $request){
        $user = $this->userRepository->find($id);
        if (empty($user)) {
            ash::error('Data pengguna tidak ditemukan');
            return redirect(route('users.index'));
        }
        $input =  $request->all();
        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
        } else {
            unset($input['password']);
        }
        $user = $this->userRepository->update($input, $id);
        Flash::success('Data pengguna berhasil diubah.');
        return redirect(route('users.index'));
    }

    public function destroy($id){
        $user = $this->userRepository->find($id);
        if (empty($user)) {
            Flash::error('Data pengguna tidak ditemukan');
            return redirect(route('users.index'));
        }
        $this->userRepository->delete($id);
        Flash::success('Data pengguna berhasil dihapus.');
        return redirect(route('users.index'));
    }

}
