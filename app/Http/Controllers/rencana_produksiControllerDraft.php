<?php

namespace App\Http\Controllers;
use App\Http\Requests\Createrencana_produksiRequest;
use App\Http\Requests\Updaterencana_produksiRequest;
use App\Repositories\rencana_produksiRepository;
use App\Repositories\rencana_produksi_cacheRepository;
use App\Repositories\jadwal_mesinRepository;
use App\Repositories\mesinRepository;
use App\Repositories\infoRepository;
use App\Repositories\list_shiftRepository;
use App\Rules\more_than_now;
use App\Rules\validate_mulai_in_shift;
use App\Rules\validate_mulai_exclude_rencana_id;
use App\Rules\validate_shift_ready;
use App\Rules\validate_mulai_cek_istirahat;
use App\Rules\validate_mulai_rencana_prod_setelah;
use App\Repositories\shiftRepository;
use App\Rules\validate_jadwal_mulai;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Flash;
use Response;
use Auth;
use PDF;

// TODO: bug yang cysle time 0.5 di penghitungan shift

class rencana_produksiControllerDraft extends AppBaseController {
    private $rencanaProduksiRepository;
    private $rencanaProduksiCacheRepository;
    private $jadwalMesinRepository;
    private $shiftRepository;
    private $mesinRepository;
    private $infoRepository;
    private $listShiftRepository;

    public function __construct(
        rencana_produksiRepository $rencanaProduksiRepo,
        rencana_produksi_cacheRepository $rencanaProduksiCacheRepository,
        jadwal_mesinRepository $jadwalMesinRepo,
        shiftRepository $shiftRepo,
        mesinRepository $mesinRepository,
        list_shiftRepository $listShiftRepository,
        infoRepository $infoRepository) {
        $this->rencanaProduksiRepository = $rencanaProduksiRepo;
        $this->rencanaProduksiCacheRepository = $rencanaProduksiCacheRepository;
        $this->jadwalMesinRepository = $jadwalMesinRepo;
        $this->shiftRepository = $shiftRepo;
        $this->mesinRepository = $mesinRepository;
        $this->infoRepository = $infoRepository;
        $this->listShiftRepository = $listShiftRepository;
    }

    public function index(Request $request){
        $status = $request->get('status');
        $mesin_id = $request->get('mesin_id');
        $query = $this->rencanaProduksiRepository->allQuery();
        // if ($status !== 'booking') {
        //     $tanggal = $request->get('tanggal');
        //     $selesai = $request->get('selesai');
        //     $mesin_id = $request->get('mesin_id');
        //     if (!$tanggal) {
        //         $tanggal = date('Y-m-d');
        //     } 
        //     $tanggal = date('Y-m-d 23:59:00', strtotime($tanggal));
        //     $tanggalselesai = date('Y-m-d 00:00:01', strtotime($tanggal));
        //     $query = $query->where('mulai', '<=', $tanggal);
        //     $query = $query->where('selesai', '>=', $tanggalselesai);
        // } else if ($status == 'aktif') {
        //     $query = $query->where('status', '!=', 'cancel');
        // } else {
        //     $query = $query->where('status', $status);
        // }
        $query = $query->where('status', '=', 'booking');
        if ($mesin_id) {
            $query = $query->where('mesin_id', $mesin_id);
        };
        $query->orderBy('id', 'asc');
        $rencanaProduksis= $query->paginate(25)->appends(request()->query());
        return view('rencana_proudksis_draft.index')
            ->with('rencanaProduksisDraft', $rencanaProduksis);
    }

    public function change_history(Request $request){
        $tanggal = $request->get('tanggal');
        $mesin_id = $request->get('mesin_id');
        $status = $request->get('status');
        $query = $this->rencanaProduksiCacheRepository->allQuery();
        if ($tanggal) {
            $query = $query->whereYear('mulai', '=', substr($tanggal, 6, 4));
            $query = $query->whereMonth('mulai', '=', substr($tanggal, 3, 2));
            $query = $query->whereDay('mulai', '=', substr($tanggal, 0, 2));
        };

        if ($mesin_id) {
            $query = $query->where('mesin_id', $mesin_id);
        };

        if ($status) {
            if ($status == 'semua') {
                
            } else if ($status == 'aktif') 
                $query = $query->where('status', '!=', 'cancel');
            else
                $query = $query->where('status', $status);
        };
        $query->orderBy('mulai', 'desc');
        $rencanaProduksisCache= $query->paginate(25)->appends(request()->query());
        return view('rencana_proudksis_draft.index_history')
            ->with('rencanaProduksisCache', $rencanaProduksisCache);
    }

    public function create(){
        return view('rencana_proudksis_draft.create');
    }

    public function store(Createrencana_produksiRequest $request){
        $input = $request->all();
        // $input['mulai'] = date('Y-m-d H:i:s', strtotime($input['mulai']));
        // $shiftAvailable = $this->convertTimeToShift($input['mulai']);
        // $penyisipanProses = false;
        $this->validate($request, [
            'jumlah_rencana_produksi' => ['required'],
            'type_id'  => 'required',
            'mesin_id'  => 'required',
            'estimasi_durasi'  => 'required',
        ]);
        
        $input['status'] = 'booking';
        $input['urutan_booking'] =  $this->getNomorUrutBooking($input['mesin_id']);
        $rencanaProduksi = $this->rencanaProduksiRepository->create($input); // simpan dulu
        $this->simpanHistoryPerubahan($rencanaProduksi);
        Flash::success('Rencana produksi berhasil disimpan.');
        if ($rencanaProduksi->barang_lain) {
            Flash::warning('Jangan lupa untuk membuat Rencana Produksi '.$rencanaProduksi->barang_lain );
        }
        return redirect(route('draft-rencana-produksi.index'));
    } 


    function shift_validation($mulai, $estimasi_durasi, $mesin_id, $jumlah_operator) {
        $durasiKerja = $estimasi_durasi / 60; // in minutes
        $hasilcek = True;
        while ($durasiKerja > 0) {
            $hasil = $this->hitungJadwal($mulai, $mesin_id, $durasiKerja, 'kerja');
            // echo "jadwal kerja yang mau di cek: <br/>";
            // print_r($hasil);
            // echo "<br/><br/>";
            $jadwalMesin = $hasil['jadwal'];
            $mulai = $jadwalMesin['selesai']; // assign value untuk loop berikutnya
            $durasiKerja = $hasil['sisaWaktu']; // assign value untuk loop berikutnya
            $hasilcek = $this->hitungShitValidation(
                $jadwalMesin['mulai'], $jadwalMesin['selesai'], $jumlah_operator);
        }
        return $hasilcek;
    }

    function hitungShitValidation($mulai, $selesai, $jumlah_operator) { 
        $run = true;
        while($run) {
            // cari shift berikutnya
            $nextShift = $this->getNextShift($mulai);
            // jika waktu selesai masih sebelum next shift
            if ($selesai <= $nextShift[0]) {
                // echo "<br/>LOOP1<br/>";
                $shift = $this->convertTimeToShift(date('Y-m-d H:i:1', strtotime($mulai)));
                $durasi = round(abs(strtotime($selesai) - strtotime($mulai)) / 60,2);
                $data = [
                    'mulai' => $mulai,
                    'selesai' => $selesai,
                    'shift'  => $shift,
                    'durasi'  => $durasi * 60,
                ];
                // print_r($data);
                // print_r("<br/>rangeshift ");
                $rangeshift = $this->convertTimeToShift(date('Y-m-d H:i:1', strtotime($mulai)), True);
                // print_r($rangeshift);
                $isShiftReady = $this->isShiftReady($shift, $rangeshift, $jumlah_operator);
                // echo "<br/>";
                if (!$isShiftReady) {
                    return False;
                } else {
                    return True;
                }
                $run = false;
            } else {
                // echo "<br/>LOOP2<br/>";
                $shift = $this->convertTimeToShift(date('Y-m-d H:i:1', strtotime($mulai)));
                $durasi = round(abs(strtotime($nextShift[0]) - strtotime($mulai)) / 60,2);
                $data = [
                    'mulai' => $mulai,
                    'selesai' => $nextShift[0],
                    'shift'  => $shift,
                    'durasi'  => $durasi * 60,
                ];
                // print_r($data);
                // print_r("<br/>rangeshift");
                $rangeshift = $this->convertTimeToShift(date('Y-m-d H:i:1', strtotime($mulai)), True);
                // print_r($rangeshift);
                $isShiftReady = $this->isShiftReady($shift, $rangeshift, $jumlah_operator);
                if (!$isShiftReady) {
                    return False;
                } 
                $mulai = $nextShift[1]; // for next looping
            }
        }
    }

    public function isShiftReady($shiftNumber, $rangeshift, $jumlah_operator){
        // dd($rangeshift);
        // echo "<br/> shift yg mau di cek : ".$shiftNumber;
        // echo "<br/> jumlah operator mau di tambahkan: ".$jumlah_operator;
        $mulai = $rangeshift[0];
        $selesai = $rangeshift[1];
        $shifts = DB::table('shifts')
            ->where('mulai', '>=', $mulai)
            ->where('selesai', '<=', $selesai)
            ->groupBy('rencana_produksi_id')->get()->toArray();
        // echo "<br/> shift lain di jam ini <br/>";
        // print_r($shifts); 
        // jumlah operator dari shift yang ada + yang mau di tambah
        foreach($shifts as $shift){
            $rencanaProduksi = $this->rencanaProduksiRepository->find($shift->rencana_produksi_id);
            $jumlah_operator += $rencanaProduksi['jumlah_operator'];
        }

        // check di tabel settings operator yang tersedia
        $mulai_date = substr($mulai, 0, 10);
        $shift_settings = DB::table('shift_settings')
            ->where('tanggal', '=', $mulai_date)->get()->toArray();
        // echo "<br/> Total Operator Terpakai & mau dipakai ".$jumlah_operator."<br/>";
        // echo "shift settings yang ada  <br/>";
        // print_r($shift_settings);
        if (count($shift_settings)>0) {
            if ($shiftNumber == 1) {
                // print_r("<br/>sini1, jumlah op tersedia ".$shift_settings[0]->shift_1);
                return $jumlah_operator <= $shift_settings[0]->shift_1;
            } else if ($shiftNumber == 2) {
                // print_r("<br/>sini2, jumlah op tersedia ".$shift_settings[0]->shift_2);
                return $jumlah_operator <= $shift_settings[0]->shift_2;
            } else if ($shiftNumber == 3) {
                // print_r("<br/>sini3, jumlah op tersedia ".$shift_settings[0]->shift_3);
                return $jumlah_operator <= $shift_settings[0]->shift_3;
            }
        } else {
            return True;
        }
        // bandingkan jumlah operator + kebutuhan dengan yang di tabel baru berdasrkan tanggal dan shift:
        // return true false
    }

    public function update($id, Updaterencana_produksiRequest $request){
        // filter booking only
        $rencanaProduksi = $this->rencanaProduksiRepository->find($id);
        $prevMesinId = $rencanaProduksi->mesin_id;
        if (empty($rencanaProduksi)) {
            Flash::error('Rencana Produksi tidak ditemukan');
            return redirect(route('rencana-produksi.index'));
        }
        $input = $request->all();
        // $input['mulai'] = date('Y-m-d H:i:s', strtotime($input['mulai']));
        $this->validate($request, [
            'jumlah_rencana_produksi' => ['required'],
            'type_id'  => 'required',
            'mesin_id'  => 'required',
            'estimasi_durasi'  => 'required',
            // 'mulai' => ['required', 
            //     new more_than_now(),
            //     new validate_mulai_cek_istirahat($input['mesin_id']),
            // ], 
        ]);
        $rencanaProduksi = $this->rencanaProduksiRepository->update($input, $id);
        Flash::success('Rencana Produksi berhasil di update.');
        if ($rencanaProduksi->barang_lain) {
            Flash::warning('Jangan lupa untuk membuat Rencana Produksi '.$rencanaProduksi->barang_lain );
        }
        return redirect(route('draft-rencana-produksi.index', [$rencanaProduksi->id]));
    }

    // dipanggil oleh method update untuk menghitung kembali jadwal mesin 
    function updateRencanaProduksi($rencanaProduksi, $prevMesinId=null) {
        $data = $this->hitung_jadwal_dan_shift($rencanaProduksi);
        // jika ganti mesin maka nomor urut dirubah
        if ($prevMesinId != null && $prevMesinId != $rencanaProduksi->mesin_id){
            $data['urutan'] = $this->getNomorUrut($rencanaProduksi, $data['mulai'], $rencanaProduksi->mesin_id);
        }
        $rencanaProduksi = $this->rencanaProduksiRepository->update($data, $rencanaProduksi->id);
        // $this->simpanHistoryPerubahan($rencanaProduksi);
        return $rencanaProduksi;
    }

    function hitung_jadwal_dan_shift($rencanaProduksi) {
        $estimasiDurasi = $rencanaProduksi->estimasi_durasi / 60; // in minutes
        $durasiDandon = $rencanaProduksi->durasi_dandon;
        $durasiKerja = $estimasiDurasi - $durasiDandon;
        $mulaiBaru = $rencanaProduksi->mulai; // to handle if dandon = 0
        $selsaiBaru = $rencanaProduksi->mulai;
        
        // LOOP SIMPAN DANDON KE JADWAL MESIN
        $mulai = $mulaiBaru; 
        while ($durasiDandon > 0) {
            $hasil = $this->hitungJadwal($mulai, $rencanaProduksi->mesin_id, $durasiDandon, 'dandon');
            $jadwalMesin = $hasil['jadwal'];
            $jadwalMesin['perencanaan_id'] = $rencanaProduksi->id;
            if (!$mulaiBaru) { 
                $mulaiBaru = $jadwalMesin['mulai']; // assign rencana-produksi mulai menggunakan jadwal mesin pertama
            }
            $selsaiBaru = date('Y-m-d H:i:s', strtotime($jadwalMesin['selesai']));  // assign rencana-produksi selsai sampe looping terakhir
            $mulai = $jadwalMesin['selesai']; // assign value untuk loop berikutnya
            $durasiDandon = $hasil['sisaWaktu']; // assign value untuk loop berikutnya
            // echo "<br/><br/> JADWAL DANDON "; print_r($jadwalMesin);
            $jadwalMesinObj = $this->jadwalMesinRepository->create($jadwalMesin); // save jadwal ke database
        }

        // LOOP DURASI KERJA
        $shiftMulai = 0; // disimpan ke rencana produksi sebagi shift awal
        $shiftSelesai = 0; // disimpan ke rencana produksi sebagi shift slesai
        while ($durasiKerja > 0) {
            $hasil = $this->hitungJadwal($mulai, $rencanaProduksi->mesin_id, $durasiKerja, 'kerja');
            $jadwalMesin = $hasil['jadwal'];
            $jadwalMesin['perencanaan_id'] =  $rencanaProduksi->id;
            if (!$mulaiBaru) { 
                $mulaiBaru = $jadwalMesin['mulai']; // assign rencana-produksi mulai menggunakan jadwal mesin pertama
            }
            $selsaiBaru = date('Y-m-d H:i:s', strtotime($hasil['jadwal']['selesai']));  // assign rencana-produksi selsai sampe looping terakhir
            $mulai = $hasil['jadwal']['selesai']; // assign value untuk loop berikutnya
            $durasiKerja = $hasil['sisaWaktu']; // assign value untuk loop berikutnya
            // echo "<br/><br/> JADWAL MESIN "; print_r($jadwalMesin);
            $jadwalMesinObj= $this->jadwalMesinRepository->create($jadwalMesin); // save jadwal ke database
            $shift = $this->hitungShitDanSimpan($jadwalMesinObj, $rencanaProduksi);
            if ($shiftMulai == 0) {
                $shiftMulai = $shift['shift_mulai'];
            }
            $shiftSelesai = $shift['shift_selesai'];
        }

        return [
            'mulai' => $mulaiBaru,
            'selesai' => $selsaiBaru,
            'shift' => $shiftMulai,
            'shift_selesai' => $shiftSelesai,
        ];
    }

    function hitungJadwal($mulai, $mesin_id, $durasiYangDibutuhkan, $tipe) {
        // INFO: JADWAL MESIN DI SAVE DI LOOPINGAN
        $jadwalIstirahats = DB::table('jadwal_mesins')
            ->where('mesin_id', $mesin_id)
            ->where('tipe', '=', 'istirahat')
            ->where('mulai', '>=', $mulai)
            ->where('deleted_at', '=', null)
            ->orderBy('mulai', 'asc')
            ->limit(10)
            ->get();
        
        // jika tidak ada istirahat sama sekali setelah 
        if (count($jadwalIstirahats) == 0) {
            $akhir = strtotime($mulai) + $durasiYangDibutuhkan * 60;
            $hasil = [
                'mulai'=>$mulai,  
                'selesai'=> date('Y-m-d H:i:s', $akhir),
                'tipe'=>$tipe,
                'mesin_id'=>$mesin_id,
                'durasi' => $durasiYangDibutuhkan * 60
            ];
            return ['jadwal'=>$hasil, 'sisaWaktu'=>0];
        
        // jika ada istirahat yang ketabrak
        } else {
            $index = 0;
            $nextIndex = 1;
            $jadwalIstirahat = $jadwalIstirahats[$index];
            $jadwalIstirahatberikutnya = null;
            if (count($jadwalIstirahats) > 1) {
                $jadwalIstirahatberikutnya = $jadwalIstirahats[$nextIndex];
            }
           
            $waktuYangTersedia = round(abs(
                strtotime($jadwalIstirahat->mulai) - strtotime($mulai)) / 60,2);
            
            // jika tidak ada jarak / waktu mulai sama dengan waktu start istirahat, 
            // maka diambil waktu akhir istirhat sbg start
            if ($waktuYangTersedia <= 0) {
                // echo "waktu mulai sama dengan jam istirahat mulai <br/>";
                $mulai = $jadwalIstirahat->selesai;
                // cek apakah ada jam istirahat setelahnya?
                while($jadwalIstirahatberikutnya && $jadwalIstirahat->selesai == $jadwalIstirahatberikutnya->mulai) { 
                    $jadwalIstirahat = $jadwalIstirahatberikutnya;
                    $mulai = $jadwalIstirahatberikutnya->selesai;

                    $nextIndex++;
                    if( isset( $jadwalIstirahats[$nextIndex] )) {  
                        $jadwalIstirahatberikutnya = $jadwalIstirahats[$nextIndex];
                    } else {
                        $jadwalIstirahatberikutnya = null;
                    }
                }

                // cek apakah ada jam istirahat setelahnya?
                if ($jadwalIstirahatberikutnya) {
                    $waktuYangTersedia = round(abs(
                        strtotime($jadwalIstirahatberikutnya->mulai) - strtotime($mulai)) / 60,2);
                    $sisaWaktu = $waktuYangTersedia - $durasiYangDibutuhkan;
                    // jika watu sampe istirhat berikutnya kurang
                    if ($sisaWaktu < 0) {
                        // echo "waktu yang tersedia sebelum istirahat berikutnya tidak cukup<br/>";
                        $hasil = [
                            'mulai'=>$mulai,  
                            'selesai'=>$jadwalIstirahatberikutnya->mulai,
                            'tipe'=>$tipe,
                            'mesin_id'=>$mesin_id,
                            'durasi' => $waktuYangTersedia * 60
                        ];
                        return ['jadwal'=>$hasil, 'sisaWaktu'=>abs($sisaWaktu)];
                    // jika waktu sebelum istirahat cukup
                    } else {
                        // echo "waktu yang tersedia sebelum istirahat berikutnya cukup<br/>";
                        $akhir = strtotime($mulai) + $durasiYangDibutuhkan * 60;
                        $hasil = [
                            'mulai'=>$mulai,  
                            'selesai'=> date('Y-m-d H:i:s', $akhir),
                            'tipe'=>$tipe,
                            'mesin_id'=>$mesin_id,
                            'durasi' => $durasiYangDibutuhkan * 60
                        ];
                        return ['jadwal'=>$hasil, 'sisaWaktu'=>0];
                    }
                } else {
                    // "tidak ada lagi istirahat setelahnya <br/>";
                    $akhir = strtotime($mulai) + $durasiYangDibutuhkan * 60;
                    $hasil = [
                        'mulai'=>$mulai,  
                        'selesai'=> date('Y-m-d H:i:s', $akhir),
                        'tipe'=>$tipe,
                        'mesin_id'=>$mesin_id,
                        'durasi' => $durasiYangDibutuhkan * 60
                    ];
                    return ['jadwal'=>$hasil, 'sisaWaktu'=>0];
                }
                
            // jika waktu mulai tidak sama dengan waktu start istirahat, 
            // ada waktu tersedia sebelum istirahat
            } else {
                $sisaWaktu = $waktuYangTersedia - $durasiYangDibutuhkan;
                // waktu yang dibutuhkan sebelum istirahat tidak cukup
                if ($sisaWaktu < 0) {
                    $hasil = [
                        'mulai'=>$mulai,  
                        'selesai'=>$jadwalIstirahat->mulai,
                        'tipe'=>$tipe,
                        'mesin_id'=>$mesin_id,
                        'durasi' => $waktuYangTersedia * 60
                    ];
                    return ['jadwal'=>$hasil, 'sisaWaktu'=>abs($sisaWaktu)];
                // waktu yang dibutuhkan sebelum istirahat SUDAH cukup
                } else {
                    $akhir = strtotime($mulai) + $durasiYangDibutuhkan * 60;
                    $hasil = [
                        'mulai'=>$mulai,  
                        'selesai'=> date('Y-m-d H:i:s', $akhir),
                        'tipe'=>$tipe,
                        'mesin_id'=>$mesin_id,
                        'durasi' => $durasiYangDibutuhkan * 60
                    ];
                    return ['jadwal'=>$hasil, 'sisaWaktu'=>0];
                }
            } 
        }
    }

    function hitungShitDanSimpan($jadwalMesin, $RP) { 
        $mulai = date('Y-m-d H:i:s', strtotime($jadwalMesin['mulai']));
        $selesai = date('Y-m-d H:i:s', strtotime($jadwalMesin['selesai']));
        $shiftMulai = $this->convertTimeToShift($mulai);
        $shiftSelesai = $this->convertTimeToShift($selesai);

        $run = true;
        while($run) {
            $nextShift = $this->getNextShift($mulai);
            if ($selesai <= $nextShift[0]) {
                $shift = $this->convertTimeToShift(date('Y-m-d H:i:1', strtotime($mulai)));
                $durasi = round(abs(strtotime($selesai) - strtotime($mulai)) / 60,2);
                $estimasiHasil = $durasi * 60 / $RP->durasi_produksi * $RP->cycle_time;
                $data = [
                    'mulai' => $mulai,
                    'selesai' => $selesai,
                    'shift'  => $shift,
                    'durasi'  => $durasi * 60,
                    'estimasi_hasil' => round($estimasiHasil),
                    'aktual_hasil' => 0,
                    'rencana_produksi_id' => $RP->id,
                ];
                $shift = $this->shiftRepository->create($data);
                $run = false;
            } else {
                $shift = $this->convertTimeToShift(date('Y-m-d H:i:1', strtotime($mulai)));
                $durasi = round(abs(strtotime($nextShift[0]) - strtotime($mulai)) / 60,2);
                $estimasiHasil = $durasi * 60 / $RP->durasi_produksi * $RP->cycle_time;
                $data = [
                    'mulai' => $mulai,
                    'selesai' => $nextShift[0],
                    'shift'  => $shift,
                    'durasi'  => $durasi * 60,
                    'estimasi_hasil' => $estimasiHasil,
                    'aktual_hasil' => 0,
                    'rencana_produksi_id' => $RP->id,
                ];
                $shift = $this->shiftRepository->create($data);
                $mulai = $nextShift[1];
            }
        }
        return ['shift_mulai'=>$shiftMulai, 'shift_selesai' => $shiftSelesai];
    }

    function getNextShift($dateTime) {
        $haritanggal = date('Y-m-d H:i:01', strtotime($dateTime));
        // print_r("haritanggal dipangil");
        // print_r($haritanggal);
        // $range0 = [
        //     date('Y-m-d 23:00:00', strtotime($haritanggal.'-1 day')),
        //     date('Y-m-d 07:00:00', strtotime($haritanggal)),
        // ];

        // $range1 = [
        //     date('Y-m-d 07:00:00', strtotime($haritanggal)),
        //     date('Y-m-d 15:00:00', strtotime($haritanggal)),
        // ];

        // $range2 = [
        //     date('Y-m-d 15:00:00', strtotime($haritanggal)),
        //     date('Y-m-d 23:00:00', strtotime($haritanggal)),
        // ];

        // $range3 = [
        //     date('Y-m-d 23:00:00', strtotime($haritanggal)),
        //     date('Y-m-d 07:00:00', strtotime($haritanggal.'1 day')),
        // ];
        
        // if ($haritanggal >= $range0[0] && $haritanggal <= $range0[1]) {
        //     return $range0[1];
        // }
        // else if ($haritanggal >= $range1[0] && $haritanggal <= $range1[1]) {
        //     return $range1[1];
        // } else if ($haritanggal >= $range2[0] && $haritanggal <= $range2[1]) {
        //     return $range2[1];
        // } else if ($haritanggal <= $range3[1]) {
        //     return $range3[1];
        // }
        $listRange = $this->getListShiftRange($haritanggal);
        // print_r("<br/>list range");
        // print_r($listRange);
        $maxShift = $this->listShiftRepository->allQuery()
            ->orderBy('shift', 'desc')->get()->toArray(); 
        // print_r("<br/>max shift");
        // print_r($maxShift[0]['shift']);
        $maxShift = $maxShift[0]['shift'];
        $nextShift=null;
        $result = array();
        foreach($listRange as $index=>$range){ 
            if ($haritanggal >= $range['range'][0] && $haritanggal <= $range['range'][1]) {
                $shift = $range['shift'];
                if ($shift < $maxShift) $shift = $shift + 1;
                else $shift = 1;
                $nextShift = $this->listShiftRepository->allQuery()->where('shift', $shift)->get()->toArray(); 
                // print_r("<br/>Range Terpilih");
                // print_r($range);
                // print_r("<br/>Next Shift");
                // print_r($shift);
                // print_r("<br/>Next ArrAY");
                // print_r($nextShift);
                if ($shift == 1) {
                    // echo("<br/>if ke 1");
                    $nextShift  = date('Y-m-d '.$nextShift[0]['mulai'].':00:00', strtotime($haritanggal.'+ 1day'));
                    $result = array($range['range'][1], $nextShift);
                } else {
                    $nextShift  = date('Y-m-d '.$nextShift[0]['mulai'].':00:00', strtotime($haritanggal));
                    // echo("<br/>if ke2");
                    // echo($nextShift);
                    $result = array($range['range'][1], $nextShift);
                }
            };
        }
        return $result;
    }

    function convertTimeToShift($haritanggal, $rangedate=False) {
        $haritanggal = date('Y-m-d H:i:s', strtotime($haritanggal));
        $listRange = $this->getListShiftRange($haritanggal);
        // echo "<br/> haritanggal :".$haritanggal;
        foreach($listRange as $range){ 
            // echo($range['range'][0]);
            // echo($range['range'][1]);
            // echo($range['shift']);
            // echo "<br/> <br/>";
            if ($haritanggal >= $range['range'][0] && $haritanggal <= $range['range'][1]) {
                if ($rangedate) return $range['range']; else return $range['shift'];
            }
        }
        // $range1 = [
        //     date('Y-m-d 07:00:00', strtotime($haritanggal)),
        //     date('Y-m-d 15:00:00', strtotime($haritanggal)),
        // ];

        // $range2 = [
        //     date('Y-m-d 15:00:00', strtotime($haritanggal)),
        //     date('Y-m-d 23:00:00', strtotime($haritanggal)),
        // ];

        // $range3 = [
        //     date('Y-m-d 23:00:00', strtotime($haritanggal.'- 1day')),
        //     date('Y-m-d 07:00:00', strtotime($haritanggal)),
        // ];

        // $range4 = [
        //     date('Y-m-d 23:00:00', strtotime($haritanggal)),
        //     date('Y-m-d 07:00:00', strtotime($haritanggal.'+ 1day')),
        // ];

        // if ($haritanggal >= $range1[0] && $haritanggal <= $range1[1]) {
        //     if ($rangedate) return $range1; else return 1;
        // } else if ($haritanggal >= $range2[0] && $haritanggal <= $range2[1]) {
        //     if ($rangedate) return $range2; else return 2;
        // } else if ($haritanggal >= $range3[0] && $haritanggal <= $range3[1]) {
        //     if ($rangedate) return $range3; else return 3;
        // } else if ($haritanggal >= $range4[0] && $haritanggal <= $range4[1]) {
        //     if ($rangedate) return $range4; else return 3;
        // }
    }

    function getListShiftRange($haritanggal) {
        $listShift = $this->listShiftRepository->allQuery()->get()->toArray();
        $result = array();
        foreach($listShift as $shift){
            if ($shift['cross_day'] == 'Y') {
                $data1 = [
                    'shift' => $shift['shift'],
                    'cross_day' => $shift['cross_day'],
                    'range' => [
                      date('Y-m-d '.$shift['mulai'].':00:00', strtotime($haritanggal.'- 1day')),
                      date('Y-m-d '.$shift['selesai'].':00:00', strtotime($haritanggal)),
                    ]
                ];
                $data2 = [
                    'shift' => $shift['shift'],
                    'cross_day' => $shift['cross_day'],
                    'range' => [
                      date('Y-m-d '.$shift['mulai'].':00:00', strtotime($haritanggal)),
                      date('Y-m-d '.$shift['selesai'].':00:00', strtotime($haritanggal.'+ 1day')),
                    ]
                ];
                array_push($result, $data1, $data2);
            } else {
                $data = [
                    'shift' => $shift['shift'],
                    'cross_day' => $shift['cross_day'],
                    'range' => [
                      date('Y-m-d '.$shift['mulai'].':00:00', strtotime($haritanggal)),
                      date('Y-m-d '.$shift['selesai'].':00:00', strtotime($haritanggal)),
                    ]
                ];
                array_push($result, $data);
            }
        }
        return $result;
    }

    function hitungJarakkeAkhirShift($dateBefore, $shift){
        $akhirShift = null;
        if ($shift == 1) {
            $akhirShift = date('Y-m-d 15:00', strtotime($dateBefore));
        } else if ($shift == 2) {
            $akhirShift = date('Y-m-d 23:00', strtotime($dateBefore));
        } else if ($shift == 3) {
            $akhirShift = date('Y-m-d 07:00', strtotime($dateBefore));
        }
        
        return [
            'durasi'=> round(abs(strtotime($akhirShift) - strtotime($dateBefore)) / 60,2),
            'akhir_shift' => $akhirShift
        ];
    }

    function hitungJarakdariAwalShift($dateAfter, $shift){
        $awalShift = null;
        if ($shift == 1) {
            $awalShift = date('Y-m-d 07:00', strtotime($dateAfter));
        } else if ($shift == 2) {
            $awalShift = date('Y-m-d 15:00', strtotime($dateAfter));
        } else if ($shift == 3) {
            $awalShift = date('Y-m-d 23:00', strtotime($dateAfter));
        }
        
        return [
            'durasi'=> round(abs(strtotime($dateAfter) - strtotime($awalShift)) / 60,2),
            'awal_shift' => $awalShift
        ];

    }

    function getNomorUrut($rencanaProduksi, $tanggal, $mesin_id) {
        $year = substr($tanggal, 0, 4);
        $month = substr($tanggal, 5, 2);
        $otherRencana = DB::table('rencana_produksis')
            ->where('id', '<>', $rencanaProduksi->id)
            ->where('mesin_id', $mesin_id)
            ->whereYear('mulai', $year)
            ->whereMonth('mulai', $month)
            ->orderBy('urutan', 'desc')
            ->first();

        $urutanTanggal = str_replace("-", "", $tanggal);
        $key1 = substr($urutanTanggal, 2, 2);
        $key2 = substr($urutanTanggal, 4, 2);
        
        $kode_mesin = $rencanaProduksi->mesin->kode;
        if (!$otherRencana) {
            $urutan = sprintf("%03d", 1);
            return $kode_mesin.'-'.$key1.$key2.":".$urutan;
        } else {
            $prevUrutan = substr($otherRencana->urutan, -3);
            $prevUrutan = (int) $prevUrutan;
            $urutan = sprintf("%03d", $prevUrutan + 1);
            return $kode_mesin.'-'.$key1.$key2.":".$urutan;
        }
    }

    function generateNoTransaksi($rencanaProduksi, $tanggal) {
        $year = substr($tanggal, 0, 4);
        $month = substr($tanggal, 5, 2);
        $otherRencana = DB::table('rencana_produksis')
            ->where('id', '<>', $rencanaProduksi->id)
            ->whereYear('mulai', $year)
            ->whereMonth('mulai', $month)
            ->orderBy('nomor_transaksi', 'desc')
            ->first();

        $urutanTanggal = str_replace("-", "", $tanggal);
        $key1 = substr($urutanTanggal, 2, 2);
        $key2 = substr($urutanTanggal, 4, 2);
        if (!$otherRencana) {
            $nomor_transaksi = sprintf("%04d", 1);
            $nomor_transaksi  = $key1.$key2."-".$nomor_transaksi;
        } else {
            $prevUrutan = substr($otherRencana->nomor_transaksi, -3);
            $prevUrutan = (int) $prevUrutan;
            $nomor_transaksi = sprintf("%04d", $prevUrutan + 1);
            $nomor_transaksi  =  $key1.$key2."-".$nomor_transaksi;
        }
        return $nomor_transaksi;
    }

    public function show($id){
        $rencanaProduksi = $this->rencanaProduksiRepository->find($id);
        $rencanaProduksisCache = $this->rencanaProduksiCacheRepository
            ->allQuery()->where(['rencana_proudksi_id' => $id])->paginate(25)->appends(request()->query());
        $jadwalMesins = $this->jadwalMesinRepository->allQuery()
            ->where(['perencanaan_id' => $id])->get();
        $shifts = $this->shiftRepository->allQuery()
            ->where(['rencana_produksi_id' => $id])->get();

        if (empty($rencanaProduksi)) {
            Flash::error('Rencana Produksi tidak ditemukan');
            return redirect(route('rencana-produksi.index'));
        }

        // cari rencana produksi lain dengan mesin yg sama dan status bukan booking dan deleted nya null
        // jika tidak ada, maka RP ini dapat mengaktifkan diri sendiri sebagai next 
        $selfActivation = false;
        $otherRP = $this->rencanaProduksiRepository->allQuery()
            ->where('mesin_id', '=', $rencanaProduksi->mesin_id)
            ->where('deleted_at', '=', null)
            ->where('status', '<>', 'booking')->get();
    
        // if (count($otherRP) == 0) 
        $selfActivation = true;

        return view('rencana_proudksis_draft.show')
            ->with('rencanaProduksi', $rencanaProduksi)
            ->with('jadwalMesins', $jadwalMesins)
            ->with('shifts', $shifts)
            ->with('selfActivation', $selfActivation)
            ->with('rencanaProduksisCache', $rencanaProduksisCache);
    }

    public function edit($id){
        $rencanaProduksi = $this->rencanaProduksiRepository->find($id);
        if (empty($rencanaProduksi) || $rencanaProduksi->status == 'aktual' || $rencanaProduksi->status == 'cancel' || $rencanaProduksi->status == 'finish') {
            Flash::error('Rencana Produksi tidak ditemukan');
            return redirect(route('rencana-produksi.index'));
        }

        return view('rencana_proudksis_draft.edit')->with('rencProd', $rencanaProduksi);
    }

    public function editNG($id){
        $rencanaProduksi = $this->rencanaProduksiRepository->find($id);
        if (empty($rencanaProduksi) || $rencanaProduksi->status == 'cancel') {
            Flash::error('Rencana Produksi tidak ditemukan');
            return redirect(route('rencana-produksi.index'));
        }
        return view('rencana_proudksis_draft.edit_ng')->with('rencProd', $rencanaProduksi);
    }

    public function updateNG($id, Updaterencana_produksiRequest $request){
        $rencanaProduksi = $this->rencanaProduksiRepository->find($id);
        $input = $request->all();
        if (empty($rencanaProduksi)) {
            Flash::error('Rencana Produksi tidak ditemukan');
            return redirect(route('rencana-produksi.show', [$id]));
        } else {
            $rencanaProduksi = $this->rencanaProduksiRepository->update($input, $id);
            Flash::success('Data berhasil diubah');
            return redirect(route('rencana-produksi.show', [$id]));
        }
    }

    

    public function destroy($id, Updaterencana_produksiRequest $request){
        $rencanaProduksi = $this->rencanaProduksiRepository->find($id);
        if (empty($rencanaProduksi)) {
            Flash::error('Rencana Produksi tidak ditemukan');
            return redirect(route('rencana-produksi.show', [$id]));
        } else if($rencanaProduksi->status != 'booking') {
            Flash::error('Tidak dapat meghapus transaksi ini');
            return redirect(route('rencana-produksi.show', [$id]));
        }
        $this->rencanaProduksiRepository->delete($id);
        Flash::success('Rencana Produksi berhasil di hapus.');
        return redirect(route('draft-rencana-produksi.index'));
    }

    // update status dg button click dari halaman detail
    public function updateStatus($id, Updaterencana_produksiRequest $request){
        $rencanaProduksi = $this->rencanaProduksiRepository->find($id);
        $this->validate($request, [
            'status'  => 'required',
        ]);
        
        $input = $request->all();
        $status = $input['status'];
        if (empty($rencanaProduksi)) {
            Flash::error('Rencana Produksi tidak ditemukan');
            return redirect(route('rencana-produksi.show', [$id]));
        } else if ($status == 'confirm' && $rencanaProduksi->status != 'pending') {
            Flash::error('Hanya status pending yang bisa di confirm');
            return redirect(route('rencana-produksi.show', [$id]));
        } else if ($status == 'aktual' && $rencanaProduksi->status != 'confirm') {
            Flash::error('Hanya status confirm yang bisa dijadikan aktual');
            return redirect(route('rencana-produksi.show', [$id]));
        } else {
            $rencanaProduksi = $this->rencanaProduksiRepository->update($input, $id);
            $this->simpanHistoryPerubahan($rencanaProduksi, $status);
            Flash::success('Status berhasil dirubah jadi '.$status);
            return redirect(route('rencana-produksi.show', [$id]));
        }
    }

    // KALKULASI Rencana Produksi BERIKUTNYA YANG TERKENA IMBAS
    public function updateSetelahRencanaProduksi($rencanaProduksi) {
        // cari jadwal mesin setelahnya
        $anotherRencanaProduksis = $this->rencanaProduksiRepository->allQuery()
            ->where('id', '<>',  $rencanaProduksi->id)
            ->where('mesin_id', '=',  $rencanaProduksi->mesin_id)
            ->where('status', '!=', 'cancel')
            ->where('mulai', '>=',  $rencanaProduksi->mulai)->get(); 
        
        //cancel semua jadwal mesin setelahnya dulu
        foreach($anotherRencanaProduksis as $RP){
            $this->jadwalMesinRepository->deleteByRP($RP->id);
            $this->shiftRepository->deleteByRP($RP->id);
        }
        
        // hitung ulang dulu baru set lagi updat produksi
        foreach($anotherRencanaProduksis as $RP){
            $mulaiBaru = $this->cariTanggal($RP->mesin_id);
            $data['mulai'] = $mulaiBaru;
            $rencanaProduksi = $this->rencanaProduksiRepository->update($data, $RP->id);
            
            $this->updateRencanaProduksi($rencanaProduksi, null);
        }
    }

    // UNTUK MENCARI TANGGAL Rencana Produksi BERIKUTNYA YANG TERKENA IMBAS
    public function cariTanggal($mesin_id) {
        $query = $this->jadwalMesinRepository->allQuery();
        $lastMesin = $query->where('tipe', '=', 'kerja')
            ->where('mesin_id', $mesin_id)
            ->orderBy('selesai', 'desc')
            ->first();
        return date('Y-m-d H:i:s', strtotime($lastMesin->selesai));
    }

    // Untuk menyimpan rencana produksi history saat edit
    function simpanHistoryPerubahan($rencanaProduksi, $status=null) {
        $dataCache = @json_decode(json_encode($rencanaProduksi), true);
        $dataCache['rencana_proudksi_id'] = $rencanaProduksi->id;
        $dataCache['tanggal'] = null;
        $dataCache['user_id'] = Auth::user()->id;
        if ($status) {
            $dataCache['status'] = $status;
        }
        $this->rencanaProduksiCacheRepository->create($dataCache);
    }

  

    function getNomorUrutBooking($mesin_id) {
        $otherRencana = DB::table('rencana_produksis')
            ->where('mesin_id', $mesin_id)
            ->orderBy('urutan_booking', 'desc')
            ->first();
        if (!$otherRencana) {
            $urutan_booking = sprintf("%05d", 1);
            return $mesin_id."-".$urutan_booking;
        } else {
            $prevUrutan = substr($otherRencana->urutan_booking, -5);
            $prevUrutan = (int) $prevUrutan;
            $urutan_booking = sprintf("%05d", $prevUrutan + 1);
            return $mesin_id."-".$urutan_booking;
        }
    }


}

