<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatemesinRequest;
use App\Http\Requests\UpdatemesinRequest;
use App\Repositories\mesinRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Flash;
use Response;

class mesinController extends AppBaseController
{
    private $mesinRepository;

    public function __construct(mesinRepository $mesinRepo)
    {
        $this->middleware('auth');
        $this->mesinRepository = $mesinRepo;
    }

    public function index(Request $request)
    {
        $mesins = $this->mesinRepository->paginate(25)->appends(request()->query());
        return view('mesins.index')
            ->with('mesins', $mesins);
    }

    public function create()
    {
        return view('mesins.create');
    }


    public function store(CreatemesinRequest $request)
    {
        $this->validate($request, [
            'kode' => 'unique:mesins|required',
            'nama' => 'required',
            'button_id' => 'unique:mesins,button_id',
        ]);

        $input = $request->all();
        $mesin = $this->mesinRepository->create($input);
        Flash::success('Mesin berhasil di simpan.');
        return redirect(route('mesin.index'));
    }

    public function search(Request $request) {
        $mesins = DB::table('mesins')
            ->where('nama', 'LIKE', '%'.$request->q.'%')
            ->where('deleted_at', null)->get();
        return response()->json($mesins);
    }

    public function show($id)
    {
        $mesin = $this->mesinRepository->find($id);

        if (empty($mesin)) {
            Flash::error('Mesin tidak ditemukan');

            return redirect(route('mesin.index'));
        }

        return view('mesins.show')->with('mesin', $mesin);
    }


    public function edit($id)
    {
        $mesin = $this->mesinRepository->find($id);

        if (empty($mesin)) {
            Flash::error('Mesin tidak ditemukan');

            return redirect(route('mesin.index'));
        }

        return view('mesins.edit')->with('mesin', $mesin);
    }


    public function update($id, UpdatemesinRequest $request)
    {
        $this->validate($request, [
            'kode' => 'required|unique:mesins,kode,'.$id,
            'nama' => 'required',
            'button_id' => 'unique:mesins,button_id,'.$id,
        ]);

        $mesin = $this->mesinRepository->find($id);
        if (empty($mesin)) {
            Flash::error('Mesin tidak ditemukan');
            return redirect(route('mesin.index'));
        }

        $mesin = $this->mesinRepository->update($request->all(), $id);
        Flash::success('Mesin berhasil di update.');
        return redirect(route('mesin.index'));
    }


    public function destroy($id)
    {
        $mesin = $this->mesinRepository->find($id);

        if (empty($mesin)) {
            Flash::error('Mesin tidak ditemukan');

            return redirect(route('mesin.index'));
        }

        $this->mesinRepository->delete($id);

        Flash::success('Mesin berhasil di delete.');

        return redirect(route('mesin.index'));
    }

}
