<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateoutstandingRequest;
use App\Http\Requests\UpdateoutstandingRequest;
use App\Repositories\outstandingRepository;
use App\Http\Controllers\AppBaseController;
use App\Repositories\outstanding_itemRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Flash;
use Response;
use App\Imports\OutstandingItemImport;
use App\Imports\OutstandingItemImportValidation;
use Maatwebsite\Excel\Facades\Excel;
use App\Repositories\tipeRepository;
use App\Repositories\report_daily_stockRepository;
use App\Repositories\ppicRepository;
use App\Repositories\ppic_logRepository;
use App\Repositories\notificationRepository;


class outstandingController extends AppBaseController
{
    /** @var  outstandingRepository */
    private $outstandingRepository;
    private $outstandingItemRepository;
    private $tipeRepository;
    private $reportDailyStockRepository;
    private $ppicRepository;
    private $ppicLogRepository;

    public function __construct(
        outstandingRepository $outstandingRepo, 
        outstanding_itemRepository $outstandingItemRepo,
        report_daily_stockRepository $reportDailyStockRepo,
        tipeRepository $tipeRepo,
        ppicRepository $ppicRepo, 
        ppic_logRepository $ppicLogRepo,
        notificationRepository $notificationRepo
    )
    {
        $this->outstandingRepository = $outstandingRepo;
        $this->outstandingItemRepository = $outstandingItemRepo;
        $this->reportDailyStockRepository = $reportDailyStockRepo;
        $this->tipeRepository = $tipeRepo;
        $this->ppicRepository = $ppicRepo;
        $this->ppicLogRepository = $ppicLogRepo;
        $this->notificationRepository = $notificationRepo;
    }

    /**
     * Display a listing of the outstanding.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $outstandings = $this->outstandingRepository->allQuery();
        $start = $request->get('start') ?? date('Y-m-d');
        $end = $request->get('end') ?? date('Y-m-d');
        $status = $request->get('status');

        if ($start  && $end) {
            $outstandings = $outstandings->whereDate('created_at', '>=', $start);
            $outstandings = $outstandings->whereDate('created_at', '<=',  $end);
        }

        else if ($start) {
            $outstandings = $outstandings->whereDate('created_at', '>=',  $start);
        }

        else if ($end) {
            $outstandings = $outstandings->whereDate('created_at', '<=',  $end);
        };

        $outstandings = $outstandings->orderBy('id', 'DESC');
        $outstandings = $outstandings->paginate(50)->appends(request()->query());


        return view('outstandings.index')
            ->with('start', $start)
            ->with('end', $end)
            ->with('status', $status)
            ->with('outstandings', $outstandings);
    }

    /**
     * Show the form for creating a new outstanding.
     *
     * @return Response
     */
    public function create()
    {
        return view('outstandings.create');
    }

    /**
     * Store a newly created outstanding in storage.
     *
     * @param CreateoutstandingRequest $request
     *
     * @return Response
     */
    public function store(CreateoutstandingRequest $request)
    {
        $this->validate($request, [
			'file' => 'required|mimes:csv,xls,xlsx',
        ]);
        
        $input = $request->all();
        $input['total_type'] = 0;
        $input['total_qty'] = 0;
        $input['status'] = 'draft';
        $input['user_id'] = $request->user()->id;
        $input['no_transaksi'] = $this->generateNoTransaksi($input['tanggal']);

        
    	// menangkap file excel
		$file = $request->file('file');
 
		// membuat nama file unik
		$nama_file = rand().$file->getClientOriginalName();
 
        // upload ke folder file_siswa di dalam folder public
        $destination = public_path().'/file_tipe/';
		$file->move($destination, $nama_file);

        // untuk validasi tipe
        $tipeIds = $this->tipeRepository->allQuery();
        $tipeIds = $tipeIds->pluck('nama')->toArray();
        // Excel::import(new OutstandingItemImportValidation($tipeIds), public_path('/file_tipe/'.$nama_file));

        // dd(array_values($tipeIds));
		// import data
        $outstanding = $this->outstandingRepository->create($input);
		Excel::import(new OutstandingItemImport($outstanding->id), public_path('/file_tipe/'.$nama_file));
 
		// notifikasi dengan session
        Flash::success('Stok Berhasil Di Import berhasil diimport!');
 
		// alihkan halaman kembali
        return redirect()->route('outstandings.show', [$outstanding->id]);
    }

    /**
     * Display the specified outstanding.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $outstanding = $this->outstandingRepository->find($id);
        $outstandingItems = $this->outstandingItemRepository->all();
        $outstandingItems = $outstandingItems->where('outstanding_id', $id);


        if (empty($outstanding)) {
            Flash::error('Outstanding not found');

            return redirect(route('outstandings.index'));
        }

        $outstanding->total_type = count($outstandingItems);
        $outstanding->total_qty = $outstandingItems->sum('qty');
        $outstanding->save();

        return view('outstandings.show')->with('outstanding', $outstanding)
            ->with('outstandingItems', $outstandingItems);
    }

    /**
     * Show the form for editing the specified outstanding.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $outstanding = $this->outstandingRepository->find($id);
        $outstandingItems = $this->outstandingItemRepository->all();
        $outstandingItems = $outstandingItems->where('outstanding_id', $id);

        if (empty($outstanding)) {
            Flash::error('Outstanding not found');

            return redirect(route('outstandings.index'));
        }

        return view('outstandings.edit')->with('outstanding', $outstanding)
            ->with('outstandingItems', $outstandingItems);
    }

    /**
     * Update the specified outstanding in storage.
     *
     * @param int $id
     * @param UpdateoutstandingRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateoutstandingRequest $request)
    {
        $outstanding = $this->outstandingRepository->find($id);

        if (empty($outstanding)) {
            Flash::error('Outstanding not found');
            return redirect(route('outstandings.index'));
        }

        $this->validate($request, [
			'file' => 'required|mimes:csv,xls,xlsx',
        ]);
 
		// menangkap file excel
		$file = $request->file('file');
        $input = $request->all();

        $outstanding = $this->outstandingRepository->update($request->all(), $id);

		// membuat nama file unik
		$nama_file = rand().$file->getClientOriginalName();
 
        // upload ke folder file_siswa di dalam folder public
        $destination = public_path().'/file_tipe/';
		$file->move($destination, $nama_file);
        
		// import data
        $tipeIds = $this->tipeRepository->allQuery();
        $tipeIds = $tipeIds->pluck('id')->toArray();
        // Excel::import(new OutstandingItemImportValidation($tipeIds), public_path('/file_tipe/'.$nama_file));

		Excel::import(new OutstandingItemImport($outstanding->id), public_path('/file_tipe/'.$nama_file));
 
		// notifikasi dengan session
        Flash::success('Stok Berhasil Di Update!');
 
		// alihkan halaman kembali
        return redirect()->route('outstandings.show', [$outstanding->id]);
       
    }

    /**
     * Remove the specified outstanding from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy(Request $request, $id)
    {
        $outstanding = $this->outstandingRepository->find($id);
        $input = $request->all();
        $start = $input['start'];
        $end = $input['end'];

        if (empty($outstanding)) {
            Flash::error('Outstanding not found');
            return redirect(route('outstandings.index'));
        }

        DB::table('outstanding_items')->where('outstanding_id', '=', $id)->delete();

        // $this->outstandingRepository->delete($id);
        $udpateData = array(
            'status' => 'cancel'
        );
        // cancle outstanding
        $this->outstandingRepository->update($udpateData, $id);
        // cancle outstanding items
        // DB::table('outstanding_items')->where('outstanding_id', '=', $id)
        //     ->update(['status' => 'cancle']);
        // delete ppic
        $report = DB::table('report_daily_stocks')->where('outstanding_id', '=', $id)->get();
        foreach ($report as $rr) {
            DB::table('ppics')->where('id', '=', $rr->ppic_id)->delete();
        }
        // delete report
        DB::table('report_daily_stocks')->where('outstanding_id', '=', $id)
            ->delete();
        // delete report
        // update daily stock to reupload if there is cancellation
        // last stock update need to update
        $dailystock = DB::table('daily_stocks')->first();
        if($dailystock) {
            DB::table('daily_stocks')->where('id', $dailystock->id)->update(['need_to_update'=>'cancelled']);
        }
        Flash::success('Outstanding canceled successfully.');
        return redirect(route('outstandings.index', ['status'=>"",'start'=>$start, 'end'=>$end]));
    }

    public function updateStatus($id, UpdateoutstandingRequest $request){
        $oustanding = $this->outstandingRepository->find($id);
        $input = $request->all();
        $this->validate($request, [
            'status'  => 'required',
        ]);
        $this->outstandingRepository->update($input, $id);
        $outstandingItems = $this->outstandingItemRepository->all()->where('outstanding_id', $id);
        foreach($outstandingItems as $item){
            $query = DB::table('outstanding_items')
                ->join('outstandings', 'outstandings.id', '=', 'outstanding_items.outstanding_id')
                ->where('outstanding_items.type_id', '=', $item->type_id)
                ->where('outstandings.status', '=', 'processed')
                ->where('outstandings.deleted_at',  null)
                ->where('outstanding_items.deleted_at',  null)
                ->where('outstanding_items.error',  "")
                ->get();

            // search last stock from report
            $lastStock = 0;
            $query2 = DB::table('report_daily_stocks')
                ->where('type_id', '=', $item->type_id)
                ->orderBy('id', 'DESC')
                ->first();
            if ($query2) {
                $lastStock = $query2->daily_stok;
            }
                
            // 1. hitung semua outstanding & create report
            $outstanding_total = $query->sum("qty");
            $sum = $lastStock - $outstanding_total;
            $input2 = array(
                "type_id" => $item->type_id,
                "daily_stok" => $lastStock,
                "out_standing" => $outstanding_total,
                "sum" => $sum,
                // "no_transaksi" => $this->generateNoTransaksiLaporan($dailyStock->tanggal),
                "tanggal" => $oustanding->tanggal,
                "outstanding_id" => $oustanding->id
            );
            if ($item->type_id !== 0) {
                $reportDailyStock = $this->reportDailyStockRepository->create($input2);
            }

            // createPPIC
            $ppicId = app('App\Http\Controllers\ppicController')->createPPIC($item->type_id, $sum, $request->user());
            $udpateData = array(
                "ppic_id" => $ppicId
            );

            // notif PPIC jika sum minus
            if($sum < 0) {
                $input3 = array (
                    'departement'=>'ppic',
                    'tanggal' => $oustanding->tanggal,
                    'status' => 'Waiting Response',
                    'ppic_id' => $ppicId,
                    'message' => $item->type->nama . ' | OUTSTANDING ' . $outstanding_total,
                    'is_read' => 'F',
                );
                $notifications = $this->notificationRepository->allQuery();
                $notifications = $notifications->where('ppic_id', $ppicId)->delete();
                $this->notificationRepository->create($input3);
            }

            if ($item->type_id !== 0) {
                $this->reportDailyStockRepository->update($udpateData, $reportDailyStock->id);
            }
        }

        Flash::success('Status berhasil dirubah jadi '.$input['status']);
        return redirect()->route('outstandings.show', [$id]);
    }

    function generateNoTransaksi($tanggal) {
        $year = substr($tanggal, 0, 4);
        $month = substr($tanggal, 5, 2);
        $otherRencana = DB::table('outstandings')
            ->whereYear('tanggal', $year)
            ->whereMonth('tanggal', $month)
            ->orderBy('no_transaksi', 'desc')
            ->first();

        $urutanTanggal = str_replace("-", "", $tanggal);
        $key0 = "O-";
        $key1 = substr($urutanTanggal, 2, 2);
        $key2 = substr($urutanTanggal, 4, 2);
        if (!$otherRencana) {
            $no_transaksi = sprintf("%04d", 1);
            $no_transaksi  = $key0.$key1.$key2."-".$no_transaksi;
        } else {
            $prevUrutan = substr($otherRencana->no_transaksi, -3);
            $prevUrutan = (int) $prevUrutan;
            $no_transaksi = sprintf("%04d", $prevUrutan + 1);
            $no_transaksi  =  $key0.$key1.$key2."-".$no_transaksi;
        }
        return $no_transaksi;
    }

}
