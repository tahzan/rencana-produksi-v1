<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateinfoRequest;
use App\Http\Requests\UpdateinfoRequest;
use App\Repositories\infoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Flash;
use Response;



class infoController extends AppBaseController
{
    /** @var  infoRepository */
    private $infoRepository;

    public function __construct(infoRepository $infoRepo)
    {
        $this->infoRepository = $infoRepo;
    }

    
    public function index(Request $request)
    {
        $infos = $this->infoRepository->allQuery();
        $infos = $infos->orderBy('start_date', 'desc')->get();
        return view('infos.index')
            ->with('infos', $infos);
    }

    
    public function create()
    {
        return view('infos.create');
    }


    public function store(CreateinfoRequest $request)
    {
        $input = $request->all();
        $input["start_date"] = date("Y-m-d G:i:s", strtotime($input['start_date']));
        $input["end_date"] = date("Y-m-d G:i:s", strtotime($input['end_date']));
        $file = $request->file('file');
        if ($file) {
            $nama_file = time()."_".$file->getClientOriginalName();
		    $file->move('info_image', $nama_file);
            $input['file'] = $nama_file;
        }
        $info = $this->infoRepository->create($input);
        Flash::success('Announcement saved successfully.');
        return redirect(route('infos.index'));
    }


    public function show($id)
    {
        $info = $this->infoRepository->find($id);
        if (empty($info)) {
            Flash::error('Announcement not found');
            return redirect(route('infos.index'));
        }
        return view('infos.show')->with('info', $info);
    }


    public function edit($id)
    {
        $info = $this->infoRepository->find($id);
        if (empty($info)) {
            Flash::error('Announcement not found');
            return redirect(route('infos.index'));
        }

        return view('infos.edit')->with('info', $info);
    }


    public function update($id, UpdateinfoRequest $request)
    {
        $info = $this->infoRepository->find($id);
        if (empty($info)) {
            Flash::error('Announcement not found');
            return redirect(route('infos.index'));
        }
        $input = $request->all();
        $input["start_date"] = date("Y-m-d G:i:s", strtotime($input['start_date']));
        $input["end_date"] = date("Y-m-d G:i:s", strtotime($input['end_date']));
        $file = $request->file('file');
        if ($file) {
            $nama_file = time()."_".$file->getClientOriginalName();
		    $file->move('info_image', $nama_file);
            $input['file'] = $nama_file;
        }
        $info = $this->infoRepository->update($input, $id);
        Flash::success('Announcement updated successfully.');
        return redirect(route('infos.index'));
    }


    public function destroy($id) {
        $info = $this->infoRepository->find($id);
        if (empty($info)) {
            Flash::error('Announcement not found');
            return redirect(route('infos.index'));
        }
        $this->infoRepository->delete($id);
        Flash::success('Announcement deleted successfully.');
        return redirect(route('infos.index'));
    }
}
