<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createdaily_stock_itemRequest;
use App\Http\Requests\Updatedaily_stock_itemRequest;
use App\Repositories\daily_stock_itemRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Imports\StockImport;
use Maatwebsite\Excel\Facades\Excel;

class daily_stock_itemController extends AppBaseController
{
    /** @var  daily_stock_itemRepository */
    private $dailyStockItemRepository;

    public function __construct(daily_stock_itemRepository $dailyStockItemRepo)
    {
        $this->dailyStockItemRepository = $dailyStockItemRepo;
    }

    /**
     * Display a listing of the daily_stock_item.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $dailyStockItems = $this->dailyStockItemRepository->all();

        return view('daily_stock_items.index')
            ->with('dailyStockItems', $dailyStockItems);
    }

    /**
     * Show the form for creating a new daily_stock_item.
     *
     * @return Response
     */
    public function create()
    {
        return view('daily_stock_items.create');
    }

    /**
     * Store a newly created daily_stock_item in storage.
     *
     * @param Createdaily_stock_itemRequest $request
     *
     * @return Response
     */
    public function store(Createdaily_stock_itemRequest $request)
    {
        $input = $request->all();

        $dailyStockItem = $this->dailyStockItemRepository->create($input);

        Flash::success('Daily Stock Item saved successfully.');

        return redirect(route('dailyStockItems.index'));
    }

    /**
     * Display the specified daily_stock_item.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $dailyStockItem = $this->dailyStockItemRepository->find($id);

        if (empty($dailyStockItem)) {
            Flash::error('Daily Stock Item not found');

            return redirect(route('dailyStockItems.index'));
        }

        return view('daily_stock_items.show')->with('dailyStockItem', $dailyStockItem);
    }

    /**
     * Show the form for editing the specified daily_stock_item.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $dailyStockItem = $this->dailyStockItemRepository->find($id);

        if (empty($dailyStockItem)) {
            Flash::error('Daily Stock Item not found');

            return redirect(route('dailyStockItems.index'));
        }

        return view('daily_stock_items.edit')->with('dailyStockItem', $dailyStockItem);
    }

    /**
     * Update the specified daily_stock_item in storage.
     *
     * @param int $id
     * @param Updatedaily_stock_itemRequest $request
     *
     * @return Response
     */
    public function update($id, Updatedaily_stock_itemRequest $request)
    {
        $dailyStockItem = $this->dailyStockItemRepository->find($id);
        $dailyStockId = $dailyStockItem->daily_stock_id;

        if (empty($dailyStockItem)) {
            Flash::error('Daily Stock Item not found');

            return redirect(route('dailyStockItems.index'));
        }

        $dailyStockItem = $this->dailyStockItemRepository->update($request->all(), $id);

        Flash::success('Daily Stock Item updated successfully.');

        return redirect()->route('daily-stocks.show', [$dailyStockId]);
    }

    /**
     * Remove the specified daily_stock_item from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $dailyStockItem = $this->dailyStockItemRepository->find($id);
        $dailyStockId = $dailyStockItem->daily_stock_id;

        if (empty($dailyStockItem)) {
            Flash::error('Daily Stock Item not found');

            return redirect(route('dailyStockItems.index'));
        }

        $this->dailyStockItemRepository->delete($id);

        Flash::success('Daily Stock Item deleted successfully.');

        return redirect()->route('daily-stocks.show', [$dailyStockId]);
    }

    public function import_excel(Request $request) {
		// validasi
		$this->validate($request, [
			'file' => 'required|mimes:csv,xls,xlsx',
            'daily_stock_id' => 'required',
        ]);
 
		// menangkap file excel
		$file = $request->file('file');
        $input = $request->all();
 
		// membuat nama file unik
		$nama_file = rand().$file->getClientOriginalName();
 
        // upload ke folder file_siswa di dalam folder public
        $destination = public_path().'/file_tipe/';
		$file->move($destination, $nama_file);
        
		// import data
		Excel::import(new StockImport($input['daily_stock_id']), public_path('/file_tipe/'.$nama_file));
 
		// notifikasi dengan session
        Flash::success('Stok Berhasil Di Import berhasil diimport!');
 
		// alihkan halaman kembali
        return redirect()->route('daily-stocks.show', [$input['daily_stock_id']]);
	}


}
