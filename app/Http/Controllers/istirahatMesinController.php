<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createjadwal_mesinRequest;
use App\Http\Requests\Updatejadwal_mesinRequest;
use App\Repositories\jadwal_mesinRepository;
use App\Repositories\mesinRepository;
use App\Http\Controllers\AppBaseController;
use App\Rules\validate_jadwal_mulai;
use App\Rules\validate_jadwal_selesai;
use App\Rules\mulai_lowerthan_selesai;
use App\Rules\validate_mulai_exclude_id;
use App\Rules\validate_selesai_exclude_id;
use App\Rules\more_than_now;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Imports\JadwalMesinImport;
use Maatwebsite\Excel\Facades\Excel;
use Flash;
use Response;

class istirahatMesinController extends AppBaseController{
    private $jadwalMesinRepository;
    private $mesinRepository;

    public function __construct(
        jadwal_mesinRepository $jadwalMesinRepo,
        mesinRepository $mesinRepo){
        $this->jadwalMesinRepository = $jadwalMesinRepo;
        $this->mesinRepository = $mesinRepo;
    }

    public function index(Request $request){
        $tanggal = $request->get('tanggal');
        $mesin_id = $request->get('mesin_id');
        $mesin_id_bottom = $request->get('mesin_id_bottom');

        $mesins = $this->mesinRepository->all([], null, null, ['id', 'nama']);
        if (!$mesin_id_bottom && count($mesins) > 0) {
            $mesin_id_bottom = $mesins[0]->id;
        }
        $mesinsArray = [];
        foreach ($mesins as $mesin) {
            $mesinsArray[$mesin->id] = $mesin->nama;
        }
        $query = $this->jadwalMesinRepository->allQuery();
        $query->where('mesin_id', $mesin_id_bottom);
        $query->where('tipe', 'istirahat');
        $query->orderBy('mulai', 'desc');
        $jadwalMesins = $query->paginate(31)->appends(request()->query());

        return view('jadwal_istirahat.index')
            ->with('jadwalMesins', $jadwalMesins)
            ->with('mesins', $mesinsArray)
            ->with('aktif_mesin', $mesin_id)
            ->with('aktif_mesin_bottom', $mesin_id_bottom)
            ->with('aktif_tanggal', $tanggal);
    }

    public function cariTanggal(Request $request) {
        $mesin_id = $request->get('mesin_id');
        $query = $this->jadwalMesinRepository->allQuery();
        $lastMesin = $query->where('jadwal_mesins.tipe', '=', 'kerja')
            ->where('mesin_id', $mesin_id)
            ->orderBy('selesai', 'desc')
            ->first();

        if (!$lastMesin) {
            return response()->json($lastMesin);
        } else {
            $response = [
                "mulai"=>date('Y-m-d\TH:i', strtotime($lastMesin->selesai)),
                "shift"=>1
            ];
            return response()->json($response);
        }
        print_r($lastMesin);
    }

    public function create(){
        return view('jadwal_istirahat.create');
    }

    public function store(Createjadwal_mesinRequest $request){
        $input = $request->all();
        $this->validate($request, [
            'mesin_id' => ['required'],
            'mulai' => [
                'required', 
                new more_than_now(), 
                new validate_mulai_exclude_id(null, $input['mesin_id']), 
                new mulai_lowerthan_selesai($input['selesai'])],
            'selesai' => [
                'required', 
                new validate_selesai_exclude_id(null, $input['mesin_id']), 
                new more_than_now()
            ],
            'tipe' => ['required'],
        ]);

        $input["mulai"] = date("Y-m-d G:i:s", strtotime($input['mulai']));
        $input["selesai"] = date("Y-m-d G:i:s", strtotime($input['selesai']));

        $start = Carbon::parse($input["mulai"]);
        $end = Carbon::parse($input["selesai"]);
        $input["durasi"] = $end->diffInSeconds($start);
        $jadwalMesin = $this->jadwalMesinRepository->create($input);
        Flash::success('Jadwal Mesin berhasil di simpan.');
        return redirect(route('jadwal-istirahat.index'));
    }

    public function show($id){
        $jadwalMesin = $this->jadwalMesinRepository->find($id);
        if (empty($jadwalMesin)) {
            Flash::error('Jadwal Mesin tidak ditemukan');
            return redirect(route('jadwal-istirahat.index'));
        }
        return view('jadwal_istirahat.show')->with('jadwalMesin', $jadwalMesin);
    }

    public function edit($id){
        $jadwalMesin = $this->jadwalMesinRepository->find($id);
        if (empty($jadwalMesin)) {
            Flash::error('Jadwal Mesin tidak ditemukan');
            return redirect(route('jadwal-istirahat.index'));
        }
        return view('jadwal_istirahat.edit')->with('jadwalMesin', $jadwalMesin);
    }

    public function update($id, Updatejadwal_mesinRequest $request){
        $jadwalMesin = $this->jadwalMesinRepository->find($id);
        if (empty($jadwalMesin)) {
            Flash::error('Jadwal Mesin tidak ditemukan');
            return redirect(route('jadwal-istirahat.index'));
        }

        if ($jadwalMesin->tipe == 'kerja' || $jadwalMesin->tipe == 'dandon') {
            Flash::error('Tidak dapat mengubah jadwal mesin yang bertipe KERJA / DANDON');
            return redirect(route('jadwal-istirahat.index'));
        }

        $input = $request->all();

        $this->validate($request, [
            'mulai' => [
                'required', 
                new validate_mulai_exclude_id($id, $input['mesin_id']), 
                new mulai_lowerthan_selesai($input['selesai'])],
            'selesai' => [
                'required', 
                new validate_selesai_exclude_id($id, $input['mesin_id'])
            ],
            'tipe' => ['required'],
            'mesin_id' => ['required'],
        ]);

        
        $input["mulai"] = date("Y-m-d G:i:s", strtotime($input['mulai']));
        $input["selesai"] = date("Y-m-d G:i:s", strtotime($input['selesai']));
        
        $start = Carbon::parse($input["mulai"]);
        $end = Carbon::parse($input["selesai"]);
        $input["durasi"] = $end->diffInSeconds($start);
        
        $jadwalMesin = $this->jadwalMesinRepository->update($input, $id);
        Flash::success('Jadwal Mesin berhasil di update.');
        return redirect(route('jadwal-istirahat.index'));
    }


    public function destroy($id){
        $jadwalMesin = $this->jadwalMesinRepository->find($id);
        if (empty($jadwalMesin)) {
            Flash::error('Jadwal Mesin tidak ditemukan');
            return redirect(route('jadwal-istirahat.index'));
        }
        $this->jadwalMesinRepository->delete($id);
        Flash::success('Jadwal Mesin berhasil di delete.');
        return redirect(route('jadwal-istirahat.index'));
    }

    public function import_excel(Request $request) {
		// validasi
		$this->validate($request, [
			'file' => 'required|mimes:csv,xls,xlsx'
        ]);
        
 
		// menangkap file excel
		$file = $request->file('file');
 
		// membuat nama file unik
		$nama_file = rand().$file->getClientOriginalName();
 
        // upload ke folder file_siswa di dalam folder public
        $destination = public_path().'/file_tipe/';
		$file->move($destination, $nama_file);
 
		// import data
		Excel::import(new JadwalMesinImport, public_path('/file_tipe/'.$nama_file));
 
		// notifikasi dengan session
        Flash::success('Jadwal mesin berhasil diimport!');
 
		// alihkan halaman kembali
		return redirect('/jadwal-istirahat');
	}

    public function generate(Request $request) 
	{
		$input = $request->all();
        $this->validate($request, [
            'tanggal' => ['required'],
        ]);

        $tanggal = $input['tanggal'];
        $month = substr($tanggal, 0, 2);
        $year = substr($tanggal, 3, 4);

        $query = $this->jadwalMesinRepository->allQuery();
        $query = $query->whereYear('mulai', '=', $year);
        $query = $query->whereMonth('mulai', '=', $month);
        $jadwals = $query->where('auto_generated', 'N')->get();

        if (count($jadwals) > 0) {
            Flash::error('Auto generate hanya berfungsi untuk bulan yang belum terisi jadwal');
            return redirect(route('jadwal-istirahat.index'));
        }

        $queryGroup = DB::table('jadwal_mesins')
            ->whereYear('mulai', '=', $year)
            ->whereMonth('mulai', '=', $month)
            ->where('auto_generated', 'Y')->delete();
    
        $allMesin = $this->mesinRepository->allQuery()->get();

        $start = date('Y-m-01', strtotime($year.'-'.$month.'-1')); // hard-coded '01' for first day
        $end  = date('Y-m-t', strtotime($year.'-'.$month.'-1'));
        
        while($start <= $end) {
            if (date('N', strtotime($start)) == 5){ 
                foreach ($allMesin as $mesin) {
                    $ninput = [
                        'mesin_id' => (string)$mesin->id,
                        'auto_generated' => 'Y',
                        'mulai' => date('Y-m-d 11:30:00', strtotime($start)),
                        'selesai' => date('Y-m-d 13:00:00', strtotime($start)),
                        'tipe' => 'istirahat',
                    ];
                    $this->saveJadwal($ninput);
                }
            } else {
                foreach ($allMesin as $mesin) {
                    if ($mesin->tipe == 'Non Injeksi') {
                        $ninput = [
                            'mesin_id' => (string)$mesin->id,
                            'auto_generated' => 'Y',
                            'mulai' => date('Y-m-d 12:00:00', strtotime($start)),
                            'selesai' => date('Y-m-d 13:00:00', strtotime($start)),
                            'tipe' => 'istirahat',
                        ];
                        $this->saveJadwal($ninput);
                    }
                }
            }
            // // everyday
            // echo date('d m Y', strtotime($start)) . '<br>';
            $start = date('Y-m-d', strtotime($start.'+1 day'));
        }    
        
        // notifikasi dengan session
        Flash::success('Jadwal Mesin Berhasil Digenerate!');
		// alihkan halaman kembali
		return redirect('/jadwal-istirahat');
    }

    function saveJadwal($input) {
        $start = Carbon::parse($input["mulai"]);
        $end = Carbon::parse($input["selesai"]);
        $input["durasi"] = $end->diffInSeconds($start);
        $jadwalMesin = $this->jadwalMesinRepository->create($input);
    }


    function hapusSemua() {
        $lastJadwal = DB::table('jadwal_mesins')
            ->where('tipe', '=', 'kerja')->orderBy('mulai', 'desc')->first();
        
        if($lastJadwal){
            $selesai =  date('Y-m-d H:i', strtotime($lastJadwal->selesai));
            $jadwalMesins = DB::table('jadwal_mesins')
                ->where('mulai', '>', $selesai)->delete();
        } else {
            $jadwalMesins = DB::table('jadwal_mesins')->delete();
        }
        Flash::success('Jadwal Mesin Berhasil Dihapus!');
        return redirect('/jadwal-istirahat');
    }   
    
}
