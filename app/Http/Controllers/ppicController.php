<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateppicRequest;
use App\Http\Requests\UpdateppicRequest;
use App\Http\Controllers\AppBaseController;
use App\Repositories\ppicRepository;
use App\Repositories\ppic_logRepository;
use App\Repositories\ppic_commentRepository;
use App\Repositories\report_daily_stockRepository;
use App\Repositories\notificationRepository;
use Illuminate\Http\Request;
use Flash;
use Response;
use Illuminate\Support\Facades\DB;

class ppicController extends AppBaseController
{
    /** @var  ppicRepository */
    private $ppicRepository;
    private $ppicLogRepository;
    private $ppicCommentRepository;
    private $reportDailyStockRepository;
    private $notificationRepository;

    public function __construct(
        ppicRepository $ppicRepo, 
        ppic_logRepository $ppicLogRepo,
        ppic_commentRepository $ppicCommentRepo,
        report_daily_stockRepository $reportDailyStockRepo,
        notificationRepository $notificationRepo
        )
    {
        $this->ppicRepository = $ppicRepo;
        $this->ppicLogRepository = $ppicLogRepo;
        $this->ppicCommentRepository = $ppicCommentRepo;
        $this->reportDailyStockRepository = $reportDailyStockRepo;
        $this->notificationRepository = $notificationRepo;
    }

    public function index(Request $request)
    {
        $ppics = $this->ppicRepository->all();

        return view('ppics.index')
            ->with('ppics', $ppics);
    }

    /**
     * Show the form for creating a new ppic.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $report_id = $request->get('report_id');
        $type_id = $request->get('type_id');
        $type_name = $request->get('type_name');
        $outstanding = $request->get('outstanding');
        return view('ppics.create')
            ->with('report_id',$report_id)
            ->with('type_id', $type_id)
            ->with('type_name', $type_name)
            ->with('outstanding', $outstanding);
    }

    /**
     * Store a newly created ppic in storage.
     *
     * @param CreateppicRequest $request
     *
     * @return Response
     */
    public function store(CreateppicRequest $request)
    {
        $input = $request->all();
        $input['no_transaksi'] = $this->generateNoTransaksi($input['tanggal']);
        $ppic = $this->ppicRepository->create($input);

        // update report daily stock
        $report_id = $input['report_id'];
        $this->reportDailyStockRepository->update(array('ppic_id'  => $ppic->id), $report_id);

        // notif purchasing jika PPIC failed
        dd($ppic->notify_purchasing);
        if($ppic->status == 'failed' && $ppic->notify_purchasing == 'y') {
            $input2 = array (
                'departement'=>'purchasing',
                'tanggal' => $ppic->tanggal,
                'status' => $ppic->keterangan,
                'ppic_id' => $ppic->id,
                'is_read' => 'F',
            );
            $this->notificationRepository->create($input2);
        }

        $input['ppic_id'] = $ppic->id;
        $input['user_id'] = $request->user()->id;
        $ppicLog = $this->ppicLogRepository->create($input);
        
        Flash::success('PPIC saved successfully.');
        return redirect(route('report-daily-stock.index'));
    }

    /**
     * Display the specified ppic.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ppic = $this->ppicRepository->find($id);
        $ppicLogs = $this->ppicLogRepository->all()->where('ppic_id', $id);
        $ppicComments = $this->ppicCommentRepository->all()->where('ppic_id', $id);;

        if (empty($ppic)) {
            Flash::error('Ppic not found');
            return redirect(route('report-daily-stock.index'));
        }

        return view('ppics.show')->with('ppic', $ppic)
            ->with('ppicLogs', $ppicLogs)
            ->with('ppicComments', $ppicComments);
    }

    /**
     * Show the form for editing the specified ppic.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit(Request $request, $id)
    {
        $ppic = $this->ppicRepository->find($id);
        $report_id = $request->get('report_id');
        $type_id = $request->get('type_id');
        $outstanding = $request->get('outstanding');
        $type_name = $request->get('type_name');
  
        if (empty($ppic)) {
            Flash::error('Ppic not found');
            return redirect(route('report-daily-stock.index'));
        }

        return view('ppics.edit')->with('ppic', $ppic)
            ->with('report_id',$report_id)
            ->with('type_id', $type_id)
            ->with('type_name', $type_name)
            ->with('outstanding', $outstanding);
    }

    /**
     * Update the specified ppic in storage.
     *
     * @param int $id
     * @param UpdateppicRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateppicRequest $request)
    {
        $ppic = $this->ppicRepository->find($id);
        $input = $request->all();

        if (empty($ppic)) {
            Flash::error('Ppic not found');

            return redirect(route('report-daily-stock.index'));
        }
        $timestamp = time(); // Timestamp saat ini, Anda juga bisa menggunakan timestamp lain
        $input['last_update'] = date('Y-m-d H:i:s', $timestamp);

        $this->ppicRepository->update($input, $id);


        // $input3 = array (
        //     'departement'=>'ppic',
        //     'tanggal' => date('Y-m-d'),
        //     'status' => $ppic_status,
        //     'message' => $ppic_typename . ' | PURCHASING feedback',
        //     'ppic_id' => $ppicid,
        //     'is_read' => 'F',
        // );
        // $this->notificationRepository->create($input3);

        // notif purchasing jika ppic failed
        $ppic = $this->ppicRepository->find($id);
        if($ppic->status == 'failed' && $ppic->notify_purchasing == 'y') {
            $input2 = array (
                'departement'=>'purchasing',
                'tanggal' => $ppic->tanggal,
                'message' => $ppic->type->nama . ' | '.$ppic->keterangan,
                'status' => $ppic->keterangan,
                'ppic_id' => $ppic->id,
                'is_read' => 'F',
            );
            // $notifications = $this->notificationRepository->all();
            // $notifications = $notifications->where('ppic_id', $ppic->id);
            // if ($notifications->count() == 0)
            $this->notificationRepository->create($input2);
        }   


        $input['ppic_id'] = $id;
        $input['user_id'] = $request->user()->id;
        $ppicLog = $this->ppicLogRepository->create($input);

        Flash::success('Ppic updated successfully.');

        return redirect(route('report-daily-stock.index'));
    }

    /**
     * Remove the specified ppic from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ppic = $this->ppicRepository->find($id);

        if (empty($ppic)) {
            Flash::error('Ppic not found');

            return redirect(route('report-daily-stock.index'));
        }

        $this->ppicRepository->delete($id);

        Flash::success('Ppic deleted successfully.');

        return redirect(route('report-daily-stock.index'));
    }

    function generateNoTransaksi($tanggal) {
        $year = substr($tanggal, 0, 4);
        $month = substr($tanggal, 5, 2);
        $otherRencana = DB::table('ppics')
            ->whereYear('tanggal', $year)
            ->whereMonth('tanggal', $month)
            ->orderBy('no_transaksi', 'desc')
            ->first();

        $urutanTanggal = str_replace("-", "", $tanggal);
        $key0 = "PP-";
        $key1 = substr($urutanTanggal, 2, 2);
        $key2 = substr($urutanTanggal, 4, 2);
        if (!$otherRencana) {
            $no_transaksi = sprintf("%04d", 1);
            $no_transaksi  = $key0.$key1.$key2."-".$no_transaksi;
        } else {
            $prevUrutan = substr($otherRencana->no_transaksi, -3);
            $prevUrutan = (int) $prevUrutan;
            $no_transaksi = sprintf("%04d", $prevUrutan + 1);
            $no_transaksi  =  $key0.$key1.$key2."-".$no_transaksi;
        }
        return $no_transaksi;
    }

    public function close_ppic(Request $request) {
		// validasi
		// menangkap file excel
        $input = $request->all();
        $ids = $input['ids'];

        foreach ($ids as $id) {
            $report = $this->reportDailyStockRepository->find($id);
            if ($report->ppic_id) {
                $inputdata = array(
                    "status" => 'closed'
                );
                $this->ppicRepository->update($inputdata, $report->ppic_id);
            }
        }
        

        $response = [
            "message"=> "success"
        ];
        return response()->json($response);
	}

    function createPPIC($type_id, $outstanding, $user) {
        // cari ppic terakhir dan cek statusnya
        // jika statusnya belum close pake untuk ppice berikutnya
        $tanggal = date('Y-m-d');
        $timestamp = time(); // Timestamp saat ini, Anda juga bisa menggunakan timestamp lain
        $data = array(
            'tanggal' => $tanggal,
            'type_id' => $type_id,
            'outstanding_stok' => $outstanding,
            'status' => 'Waiting Response',
            'last_update' => date('Y-m-d H:i:s', $timestamp),
            'no_transaksi' => $this->generateNoTransaksi($tanggal)
        );

        // cari ppic terakhir dan cek statusnya
        // jika statusnya belum close pake untuk ppice berikutnya
        $ppics = $this->ppicRepository->allQuery();
        $prevPPIC = $ppics->where('type_id', $type_id)->orderBy('id', 'DESC')->first();
        if ($prevPPIC && ($prevPPIC->status === 'On Schedule' || $prevPPIC->status === 'failed')) {
            $data['last_update'] = $prevPPIC->last_update;
            $data['status'] = $prevPPIC->status;
            $data['tanggal_produksi'] = $prevPPIC->tanggal_produksi;
            $data['keterangan'] = $prevPPIC->keterangan;
        }
        $newPPIC = $this->ppicRepository->create($data);

        // move ppic comments
        if ($prevPPIC) {
            $ppicComments = $this->ppicCommentRepository->allQuery()->where('ppic_id', $prevPPIC->id);
            $ppicComments->update(['ppic_id' => $newPPIC->id]);
        }

        // create PPIC LOG
        $data['ppic_id'] = $newPPIC->id;
        $data['user_id'] = $user->id;
        $ppicLog = $this->ppicLogRepository->create($data);

        return $newPPIC->id;
    }
    

}

