<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createppic_commentRequest;
use App\Http\Requests\Updateppic_commentRequest;
use App\Repositories\ppic_commentRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Repositories\notificationRepository;

class ppic_commentController extends AppBaseController
{
    /** @var  ppic_commentRepository */
    private $ppicCommentRepository;

    public function __construct(
        ppic_commentRepository $ppicCommentRepo,
        notificationRepository $notificationRepo
    )
    {
        $this->ppicCommentRepository = $ppicCommentRepo;
        $this->notificationRepository = $notificationRepo;
    }

    /**
     * Display a listing of the ppic_comment.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $ppicComments = $this->ppicCommentRepository->all();

        return view('ppic_comments.index')
            ->with('ppicComments', $ppicComments);
    }

    /**
     * Show the form for creating a new ppic_comment.
     *
     * @return Response
     */
    public function create(Request $request)
    {   
        $ppic_id = $request->get('ppic_id');
        $ppic_sum = $request->get('ppic_sum');
        $ppic_status = $request->get('ppic_status');
        $ppic_typename = $request->get('ppic_typename');

        return view('ppic_comments.create')
            ->with('ppic_id', $ppic_id)
            ->with('ppic_sum', $ppic_sum)
            ->with('ppic_typename', $ppic_typename)
            ->with('ppic_status', $ppic_status);
    }

    /**
     * Store a newly created ppic_comment in storage.
     *
     * @param Createppic_commentRequest $request
     *
     * @return Response
     */
    public function store(Createppic_commentRequest $request)
    {
        $user = $request->user();
        $input = $request->all();
        $ppicid = $input['ppic_id'];
        $ppic_sum = $input['ppic_sum'];
        $ppic_typename = $input['ppic_typename'];
        $ppic_status = $input['ppic_status'];
        $input['user_id'] = $request->user()->id;

        // notif PPIC jika purchasing reply
        if($user->jabatan === 'purchasing') {
            $input3 = array (
                'departement'=>'ppic',
                'tanggal' => date('Y-m-d'),
                'status' => $ppic_status,
                'message' => $ppic_typename . ' | PURCHASING feedback',
                'ppic_id' => $ppicid,
                'is_read' => 'F',
            );
            $this->notificationRepository->create($input3);
        }

        // notif sales jika PPIC berikan response atas sum minus
        if($ppic_sum < 0 && $user->jabatan === 'ppic') {
            $input3 = array (
                'departement'=>'sales',
                'tanggal' => date('Y-m-d'),
                'status' => $ppic_status,
                'message' => $ppic_typename . ' | PPIC feedback',
                'ppic_id' => $ppicid,
                'is_read' => 'F',
            );
            $this->notificationRepository->create($input3);
        }


        $ppicComment = $this->ppicCommentRepository->create($input);
        Flash::success('Transaction Comment saved successfully.');
        return redirect()->route('ppics.show', [$ppicid]);
    }

    /**
     * Display the specified ppic_comment.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ppicComment = $this->ppicCommentRepository->find($id);

        if (empty($ppicComment)) {
            Flash::error('Ppic Comment not found');

            return redirect(route('ppicComments.index'));
        }

        return view('ppic_comments.show')->with('ppicComment', $ppicComment);
    }

    /**
     * Show the form for editing the specified ppic_comment.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ppicComment = $this->ppicCommentRepository->find($id);

        if (empty($ppicComment)) {
            Flash::error('Ppic Comment not found');

            return redirect(route('ppicComments.index'));
        }

        return view('ppic_comments.edit')->with('ppicComment', $ppicComment);
    }

    /**
     * Update the specified ppic_comment in storage.
     *
     * @param int $id
     * @param Updateppic_commentRequest $request
     *
     * @return Response
     */
    public function update($id, Updateppic_commentRequest $request)
    {
        $ppicComment = $this->ppicCommentRepository->find($id);

        if (empty($ppicComment)) {
            Flash::error('Ppic Comment not found');

            return redirect(route('ppicComments.index'));
        }

        $ppicComment = $this->ppicCommentRepository->update($request->all(), $id);

        Flash::success('Ppic Comment updated successfully.');

        return redirect(route('ppicComments.index'));
    }

    /**
     * Remove the specified ppic_comment from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ppicComment = $this->ppicCommentRepository->find($id);

        if (empty($ppicComment)) {
            Flash::error('Ppic Comment not found');

            return redirect(route('ppicComments.index'));
        }

        $this->ppicCommentRepository->delete($id);

        Flash::success('Ppic Comment deleted successfully.');

        return redirect(route('ppicComments.index'));
    }
}
