<?php

namespace App\Http\Middleware;

use Closure;
use Response;


class SuperVisorMiddleWare
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {

        if ($request->user() && $request->user()->jabatan != 'supervisor'){
            return Response::view('errors.illustrated-layout', [ 'message' => 'Halaman ini hanya boleh diakses oleh supervisor' ]);
        }

        return $next($request);
    }
}
