<?php

namespace App\Repositories;

use App\Models\outstanding;
use App\Repositories\BaseRepository;

/**
 * Class outstandingRepository
 * @package App\Repositories
 * @version September 7, 2022, 10:18 pm WIB
*/

class outstandingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'no_transaksi',
        'tanggal',
        'type_id',
        'keterangan',
        'stok',
        'deliv_date',
        'status',
        'user_id',
        'total_type',
        'total_qty'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return outstanding::class;
    }
}
