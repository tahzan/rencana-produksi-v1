<?php

namespace App\Repositories;

use App\Models\ppic_log;
use App\Repositories\BaseRepository;

/**
 * Class ppic_logRepository
 * @package App\Repositories
 * @version September 8, 2022, 11:23 pm WIB
*/

class ppic_logRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'ppic_id',
        'tanggal',
        'type_id',
        'outstanding_stok',
        'status',
        'tanggal_produksi',
        'keterangan',
        'user_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ppic_log::class;
    }
}
