<?php

namespace App\Repositories;

use App\Models\report_daily_stock;
use App\Repositories\BaseRepository;

/**
 * Class report_daily_stockRepository
 * @package App\Repositories
 * @version September 8, 2022, 11:27 pm WIB
*/

class report_daily_stockRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'type_id',
        'daily_stok',
        'out_standing',
        'sum',
        'ppic_id',
        'tanggal_mulai',
        'status',
        'tanggal_modif',
        'tanggal',
        'no_transaksi'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return report_daily_stock::class;
    }
}
