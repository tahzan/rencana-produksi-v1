<?php

namespace App\Repositories;

use App\Models\ppic_comment;
use App\Repositories\BaseRepository;

/**
 * Class ppic_commentRepository
 * @package App\Repositories
 * @version September 8, 2022, 11:26 pm WIB
*/

class ppic_commentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'ppic_id',
        'user_id',
        'keterangan'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ppic_comment::class;
    }
}
