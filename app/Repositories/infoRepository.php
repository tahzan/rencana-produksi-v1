<?php

namespace App\Repositories;

use App\Models\info;
use App\Repositories\BaseRepository;

/**
 * Class infoRepository
 * @package App\Repositories
 * @version August 8, 2021, 12:07 pm WIB
*/

class infoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'info',
        'title',
        'start_date',
        'end_date',
        'is_active',
        'info'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return info::class;
    }
}
