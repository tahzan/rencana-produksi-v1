<?php

namespace App\Repositories;

use App\Models\jadwal_mesin;
use App\Repositories\BaseRepository;

/**
 * Class jadwal_mesinRepository
 * @package App\Repositories
 * @version January 18, 2020, 10:37 am UTC
*/

class jadwal_mesinRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'mulai',
        'selesai',
        'perencanaan_id',
        'mesin_id',
        'tipe',
        'durasi',
        'auto_generated'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return jadwal_mesin::class;
    }

    public function deleteByRP($id)
    {
        $query = $this->allQuery();
        return $query->where('perencanaan_id', $id)->forceDelete();
    }

}
