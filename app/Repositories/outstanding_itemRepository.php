<?php

namespace App\Repositories;

use App\Models\outstanding_item;
use App\Repositories\BaseRepository;

/**
 * Class outstanding_itemRepository
 * @package App\Repositories
 * @version September 11, 2022, 10:14 pm WIB
*/

class outstanding_itemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'outstanding_id',
        'qty',
        'customer',
        'type_id',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return outstanding_item::class;
    }
}
