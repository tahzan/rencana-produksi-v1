<?php

namespace App\Repositories;

use App\Models\rencana_produksi_cache;
use App\Repositories\BaseRepository;

/**
 * Class rencana_produksi_cacheRepository
 * @package App\Repositories
 * @version January 17, 2020, 3:04 pm UTC
*/

class rencana_produksi_cacheRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nomor_transaksi',
        'shift',
        'mesin_id',
        'urutan',
        'type_id',
        'nama',
        'jumlah_operator',
        'cycle_time',
        'durasi_dandon',
        'toleransi',
        'durasi_produksi',
        'type_pasangan:nullable',
        'jumlah_rencana_produksi',
        'estimasi_durasi',
        'estimasi_total_shift',
        'jumlah_aktual_produksi',
        'persentasi_aktual',
        'persentasi_cycle_time',
        'rencana_proudksi_id',
        'mulai',
        'selesai',
        'status',
        'user_id',
        'satuan',
        'shift_category',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return rencana_produksi_cache::class;
    }

    public function all1($search = [], $columns = ['*'])
    {
        $query = $this->allQuery($search);
        $query->orderBy('created_at', 'desc');
        return $query->get($columns);
    }

    public function deleteByRP($id)
    {
        $query = $this->allQuery();
        return $query->where('rencana_proudksi_id', $id)->forceDelete();
    }

}
