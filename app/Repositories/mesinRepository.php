<?php

namespace App\Repositories;

use App\Models\mesin;
use App\Repositories\BaseRepository;

/**
 * Class mesinRepository
 * @package App\Repositories
 * @version January 10, 2020, 11:12 pm UTC
*/

class mesinRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'kode',
        'nama',
        'tipe',
        'rusak',
        'button_id',
        'shift_category',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return mesin::class;
    }
}
