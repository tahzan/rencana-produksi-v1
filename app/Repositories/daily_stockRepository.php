<?php

namespace App\Repositories;

use App\Models\daily_stock;
use App\Repositories\BaseRepository;

/**
 * Class daily_stockRepository
 * @package App\Repositories
 * @version September 10, 2022, 9:56 pm WIB
*/

class daily_stockRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'no_transaksi',
        'tanggal',
        'user_id',
        'status',
        'total_type',
        'total_qty'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return daily_stock::class;
    }
}
