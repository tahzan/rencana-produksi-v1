<?php

namespace App\Repositories;

use App\Models\daily_stock_item;
use App\Repositories\BaseRepository;

/**
 * Class daily_stock_itemRepository
 * @package App\Repositories
 * @version September 11, 2022, 12:55 pm WIB
*/

class daily_stock_itemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'daily_stock_id',
        'type_id',
        'stock'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return daily_stock_item::class;
    }
}
