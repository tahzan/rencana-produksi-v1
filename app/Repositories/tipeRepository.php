<?php

namespace App\Repositories;

use App\Models\tipe;
use App\Repositories\BaseRepository;

/**
 * Class tipeRepository
 * @package App\Repositories
 * @version January 11, 2020, 7:00 am UTC
*/

class tipeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama',
        'jumlah_operator',
        'cycle_time',
        'durasi_dandon',
        'toleransi',
        'durasi_produksi',
        'kombinasi',
        'barang_lain',
        'barang_lain_id',
        'type_pasangan',
        'berat',
        'count',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return tipe::class;
    }
}
