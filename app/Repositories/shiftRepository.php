<?php

namespace App\Repositories;

use App\Models\shift;
use App\Repositories\BaseRepository;

/**
 * Class shiftRepository
 * @package App\Repositories
 * @version January 25, 2020, 10:14 am UTC
*/

class shiftRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tanggal',
        'shift',
        'mulai',
        'selesai',
        'durasi',
        'estimasi_hasil',
        'aktual_hasil',
        'rencana_produksi_id',
        'gangguan__',
        'gangguan_sett',
        'gangguan_mld',
        'gangguan_bhn',
        'note',
        'operator',
        'berat_ng_produksi',
        'berat_set_awal',
        'target_settings',
        'shift_category',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return shift::class;
    }

    public function deleteByRP($id)
    {
        $query = $this->allQuery();
        return $query->where('rencana_produksi_id', $id)->forceDelete();
    }
    
}
