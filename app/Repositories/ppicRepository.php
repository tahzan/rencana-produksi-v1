<?php

namespace App\Repositories;

use App\Models\ppic;
use App\Repositories\BaseRepository;

/**
 * Class ppicRepository
 * @package App\Repositories
 * @version September 8, 2022, 11:21 pm WIB
*/

class ppicRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tanggal',
        'type_id',
        'outstanding_stok',
        'status',
        'tanggal_produksi',
        'keterangan',
        'no_transaksi',
        'last_update',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ppic::class;
    }
}
