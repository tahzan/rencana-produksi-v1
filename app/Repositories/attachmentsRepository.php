<?php

namespace App\Repositories;

use App\Models\attachments;
use App\Repositories\BaseRepository;

/**
 * Class attachmentsRepository
 * @package App\Repositories
 * @version February 26, 2023, 9:55 am WIB
*/

class attachmentsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'rencana_produksi_id',
        'shift_id',
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return attachments::class;
    }
}
