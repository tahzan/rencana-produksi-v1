<?php

namespace App\Repositories;

use App\Models\notification;
use App\Repositories\BaseRepository;

/**
 * Class notificationRepository
 * @package App\Repositories
 * @version September 11, 2022, 8:32 pm WIB
*/

class notificationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'departement',
        'tanggal',
        'no_transc',
        'status',
        'ppic_id',
        'message',
        'is_read'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return notification::class;
    }
}
