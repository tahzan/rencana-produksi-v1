<?php

namespace App\Repositories;

use App\Models\rencana_produksi;
use App\Repositories\BaseRepository;

/**
 * Class rencana_produksiRepository
 * @package App\Repositories
 * @version January 11, 2020, 9:37 am UTC
*/

class rencana_produksiRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nomor_transaksi',
        'shift',
        'shift_selesai',
        'mesin_id',
        'urutan',
        'type_id',
        'nama',
        'jumlah_operator',
        'cycle_time',
        'durasi_dandon',
        'toleransi',
        'durasi_produksi',
        'type_pasangan',
        'jumlah_rencana_produksi',
        'estimasi_durasi',
        'estimasi_total_shift',
        'jumlah_aktual_produksi',
        'persentasi_aktual',
        'persentasi_cycle_time',
        'mulai',
        'selesai',
        'status',
        'barang_lain',
        'satuan',
        'operator',
        'berat_ng_produksi',
        'berat_set_awal',
        'target_settings',
        'berat_per_pcs',
        'last_hint',
        'note',
        'shift_category',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return rencana_produksi::class;
    }

}
