<?php

namespace App\Repositories;

use App\Models\shift_setting;
use App\Repositories\BaseRepository;

/**
 * Class shift_settingRepository
 * @package App\Repositories
 * @version July 15, 2020, 7:15 am WIB
*/

class shift_settingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tanggal',
        'shift_1',
        'shift_2',
        'shift_3',
        'shift_category',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return shift_setting::class;
    }
}
