<?php

namespace App\Repositories;

use App\Models\list_shift;
use App\Repositories\BaseRepository;

/**
 * Class list_shiftRepository
 * @package App\Repositories
 * @version September 25, 2021, 8:40 am WIB
*/

class list_shiftRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'shift',
        'mulai',
        'selesai',
        'cross_day',
        'shift_category',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return list_shift::class;
    }
}
