<?php

namespace App\Imports;

use App\Models\jadwal_mesin;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Illuminate\Validation\Rule;
use App\Rules\more_than_now;
use Carbon\Carbon;
use Datetime;

class JadwalMesinImport implements ToModel, WithStartRow, WithValidation, WithMultipleSheets
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    public function startRow(): int
    {
        return 2;
    }

    public function model(array $row)
    {
        return new jadwal_mesin([
            'mulai' => $this->transformDate($row[0]),
            'selesai' => $this->transformDate($row[1]),
            'mesin_id' => $row[2],
            'tipe' => 'istirahat',
            'durasi' => $this->calculateDuration(
                $this->transformDate($row[0]), 
                $this->transformDate($row[1])
            )
        ]);
    }

    public function rules(): array
    {
        return [
            '0' => 'required',
            '0' => function($attribute, $value, $onFailure) {
                if(!preg_match('/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}/',$value)){
                    $onFailure('Format Mulai tidak sesuai (Y-m-dTH:i:s)');
                }
            },
            '1' => 'required',
            '1' => function($attribute, $value, $onFailure) {
                if(!preg_match('/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}/',$value)){
                    $onFailure('Format Selesai tidak sesuai (Y-m-dTH:i:s)');
                }
            },
            '2' => 'required|numeric',
        ];
    }

    public function customValidationMessages() {
        return [
            '0.required' => 'Mulai wajib diisi',
            '1.required' => 'Selesai wajib diisi',
            '2.numeric' => 'Mesin Id harus angka',
            '2.required' => 'Mesin Id wajib diisi',
        ];
    }

    public function transformDate($value) {
        return date("Y-m-d G:i:s", strtotime($value));
    }

    public function calculateDuration($mulai, $selesai) {
        $start = Carbon::parse($mulai);
        $end = Carbon::parse($selesai);
        return $end->diffInSeconds($start);
    }

    public function sheets(): array
    {
        return [
            0 => $this,
        ];
    }
}
