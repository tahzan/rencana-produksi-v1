<?php

namespace App\Imports;

use App\Models\outstanding_item;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Illuminate\Validation\Rule;
use App\Rules\more_than_now;
use Carbon\Carbon;
use Datetime;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class OutstandingItemImportValidation implements ToCollection, WithStartRow, WithValidation, WithMultipleSheets
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    private $typeIds;

    public function __construct(array $typeIds) 
    {
        $this->typeIds = $typeIds;
    }

    public function startRow(): int
    {
        return 2;
    }

    public function collection(Collection $rows)
    {
        return $rows;   
    }

    public function rules(): array
    {
        return [
            '0' => Rule::in($this->typeIds),
            '1' => 'required|numeric',
        ];
    }


    public function customValidationMessages() {
        return [
            // '1.required' => 'Tipe wajib diisi',
            '1.in' => 'Type ID not found',
            // '1.numeric' => 'Tipe Id harus angka',
            // '2.required' => 'QTY wajib diisi',
            // '2.numeric' => 'QTY harus angka',
        ];
    }

    public function sheets(): array
    {
        return [
            0 => $this,
        ];
    }
}
