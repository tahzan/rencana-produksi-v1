<?php

namespace App\Imports;

use App\Models\outstanding_item;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Illuminate\Validation\Rule;
use App\Rules\more_than_now;
use Carbon\Carbon;
use Datetime;
use Illuminate\Support\Facades\DB;

class OutstandingItemImport implements ToModel, WithStartRow, WithMultipleSheets
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    private $outstanding_id;
    public $continueOnError = true;

    public function __construct(int $outstanding_id) 
    {
        $this->outstanding_id = $outstanding_id;
    }

    public function startRow(): int
    {
        return 2;
    }

    public function model(array $row)
    {   
        $errorMessage = "";
        $type = DB::table('tipes')
            ->where('nama', '=', $row[0])
            ->where('deleted_at', '=', null)->first();
        
        $typeID = 0;
        if ($type)  {
            $typeID = $type->id;
        }

        if(!$row[0]) {
            $errorMessage = "Type Name required";
        } else if ($typeID == 0) {
            $errorMessage = "Type Name: ".$row[0]." not found in database";
        } else if (!$row[1]) {
            $errorMessage = "Qty is required";
        } else if (!$row[2]) {
            $errorMessage = "Customer required is required";
        }

        return new outstanding_item([
            'outstanding_id' => $this->outstanding_id,
            'type_id' => $typeID,
            'qty' => $row[1] ?? 0,
            'customer' => $row[2],
            'status' => 'open',
            'error' => $errorMessage,
        ]);
        
    }

    // public function rules(): array
    // {
    //     // return [
    //     //     '1' => 'required|numeric',
    //     //     '2' => Rule::in($this->typeIds),
    //     // ];
    // }

    // public function customValidationMessages() {
    //     // return [
    //     //     '1.required' => 'Tipe wajib diisi',
    //     //     '1.numeric' => 'Tipe Id harus angka',
    //     //     '2.in' => 'Type name not found in the database.',
    //     // ];
    // }

    public function sheets(): array
    {
        return [
            0 => $this,
        ];
    }
}
