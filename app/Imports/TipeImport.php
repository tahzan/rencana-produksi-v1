<?php

namespace App\Imports;

use App\Models\tipe;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Illuminate\Validation\Rule;

class TipeImport implements ToModel, WithStartRow, WithValidation
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    public function startRow(): int
    {
        return 2;
    }

    public function model(array $row)
    {
        return new tipe([
            'nama' => $row[0],
            'jumlah_operator' => $row[1],
            'cycle_time' => $row[2],
            'durasi_dandon' => $row[3],
            'toleransi' => $row[4],
            'durasi_produksi' => $row[5],
            'kombinasi' => $row[6],
            'barang_lain' => $row[7],
            'type_pasangan' => $row[8],
        ]);
    }

    public function rules(): array
    {
        return [
            '0' => 'required|unique:tipes,nama',
            '1' => 'numeric',
            '2' => 'numeric',
            '3' => 'numeric',
            '4' => 'numeric',
            '5' => 'numeric',
            '6' => Rule::in(['Ya', 'Tidak']),
            // 'kombinasi' => $row[6],
            // 'barang_lain' => $row[7],
            // 'type_pasangan' => $row[8],
        ];
    }

    public function customValidationMessages() {
        return [
            '0.required' => 'Nama tipe wajib diisi.',
            '0.unique' => 'Nama tipe sudah ada.',
            '1.numeric' => 'Jumlah operator harus angka',
            '2.numeric' => 'Cycle time harus angka',
            '3.numeric' => 'Durasi Dandon harus angka',
            '4.numeric' => 'Toleransi harus angka',
            '5.numeric' => 'Durasi Produksi harus angka',
            '6.in' => 'Input harus antara Ya dan Tidak',
        ];
    }
}
