<?php

namespace App\Imports;

use App\Models\daily_stock_item;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Illuminate\Validation\Rule;
use App\Rules\more_than_now;
use Carbon\Carbon;
use Datetime;
use Illuminate\Support\Facades\DB;

class StockImport implements ToModel, WithStartRow,  WithMultipleSheets
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    private $daily_stock_id;

    public function __construct(int $daily_stock_id) 
    {
        $this->daily_stock_id = $daily_stock_id;
    }

    public function startRow(): int
    {
        return 2;
    }

    public function model(array $row)
    {   
        if ($row[1] <= 0) return null;

        $errorMessage = "";
        $type = DB::table('tipes')
            ->where('nama', '=', $row[0])
            ->where('deleted_at', '=', null)->first();
        
        $typeID = 0;
        if ($type)  {
            $typeID = $type->id;
        }

        if(!$row[0]) {
            $errorMessage = "Type Name required";
        } else if ($typeID == 0) {
            $errorMessage = "Type Name: ".$row[0]." not found in database";
        } else if (!$row[1]) {
            $errorMessage = "Stock is required";
        } 

        return new daily_stock_item([
            'daily_stock_id' => $this->daily_stock_id,
            'type_id' => $typeID,
            'stock' => $row[1] ?? 0,
            'error' => $errorMessage,
        ]);
    }

    // public function rules(): array
    // {
    //     return [
    //         '1' => 'required|numeric',
    //     ];
    // }

    // public function customValidationMessages() {
    //     return [
    //         '1.required' => 'Stok wajib diisi',
    //         '1.numeric' => 'Stok harus angka',
    //     ];
    // }

    public function sheets(): array
    {
        return [
            0 => $this,
        ];
    }
}
