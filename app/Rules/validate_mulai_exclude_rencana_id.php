<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class validate_mulai_exclude_rencana_id implements Rule
{
    private $perencanaan_id = null;
    private $mesin_id = null;
    private $jadwalmesin = null;
    public function __construct($perencanaan_id, $mesin_id){
        $this->perencanaan_id = $perencanaan_id;
        $this->mesin_id = $mesin_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $value = date('Y-m-d H:i:59', strtotime($value));
        $jadwalMesin = DB::table('jadwal_mesins')
            ->join('rencana_produksis', 'rencana_produksis.id', '=', 'jadwal_mesins.perencanaan_id')
            ->where('perencanaan_id', '<>', $this->perencanaan_id)
            ->where('jadwal_mesins.mesin_id', '=', $this->mesin_id)
            ->where('jadwal_mesins.mulai', '<=', $value)
            ->where('jadwal_mesins.selesai', '>',  $value)
            ->get();

        if (count($jadwalMesin) > 0) {
            $this->jadwalMesin = $jadwalMesin[0];
        }
        return count($jadwalMesin) == 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(){
        return ':attribute menimpa '.$this->jadwalMesin->urutan.', silahkan tentukan penyisipan!';
    }
}
