<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class mulai_lowerthan_selesai implements Rule
{
    private $otherDate = null;
    public function __construct($otherDate)
    {
        $this->otherDate = $otherDate;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $value = date('Y-m-d H:i:s', strtotime($value));
        $date = date('Y-m-d H:i:s', strtotime($this->otherDate));
        return $value < $date;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Tanggal mulai harus kurang dari tanggal selesai';
    }
}
