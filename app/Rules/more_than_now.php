<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class more_than_now implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $value = date('Y-m-d H:i:s', strtotime($value));
        $now = date("Y-m-d H:i:s");
        return $value > $now;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return ':attribute harus lebih dari sekarang';
    }
}
