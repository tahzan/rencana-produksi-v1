<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class validate_mulai_rencana_prod_setelah implements Rule {
    private $mesin_id = null;
    public function __construct($mesin_id){
        $this->mesin_id = $mesin_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value) {
        $value = date('Y-m-d H:i:59', strtotime($value));
        $rencanaprod = DB::table('rencana_produksis')
            ->where('mesin_id', '=', $this->mesin_id)
            ->where('mulai', '>', $value)
            ->where('status', '<>', 'cancel')
            ->get();
        return count($rencanaprod) == 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Tanggal mulai harus di set setelah rencana produksi yang terakhir';
    }
}
