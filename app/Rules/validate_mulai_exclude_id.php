<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class validate_mulai_exclude_id implements Rule
{
    private $id = null;
    private $mesin_id = null;
    public function __construct($id, $mesin_id)
    {
        $this->id = $id;
        $this->mesin_id = $mesin_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $value = date('Y-m-d H:i:59', strtotime($value));
        $jadwalMesin = DB::table('jadwal_mesins')
            ->where('id', '<>', $this->id)
            ->where('mesin_id', '=', $this->mesin_id)
            ->where('mulai', '<=', $value)
            ->where('selesai', '>',  $value)
            ->get();
        return count($jadwalMesin) == 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return ':attribute menimpa jadwal lain, silahkan di cek!';
    }
}
