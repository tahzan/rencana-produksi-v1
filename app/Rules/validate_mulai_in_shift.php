<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class validate_mulai_in_shift implements Rule
{
    private $shiftAvailable = null;
    public function __construct($shiftAvailable)
    {
        $this->shiftAvailable = $shiftAvailable;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->shiftAvailable;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Tidak ada shift untuk jam ini';
    }
}
