<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class validate_jadwal_selesai implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $value = date('Y-m-d H:i:00', strtotime($value));
        $jadwalMesin = DB::table('jadwal_mesins')
            ->where('mulai', '<', $value)
            ->where('selesai', '>=',  $value)
            ->get();
        return count($jadwalMesin) == 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return ':attribute menimpa jadwal lain, silahkan di cek!';
    }
}
