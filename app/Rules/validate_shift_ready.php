<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class validate_shift_ready implements Rule
{
    private $otherDate = null;
    public function __construct($value){
        $this->value = $value;
    }
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->value;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Operator tidak tersedia';
    }
}
