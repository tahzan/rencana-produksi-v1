<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class daily_stock
 * @package App\Models
 * @version September 10, 2022, 9:56 pm WIB
 *
 * @property string $no_transaksi
 * @property string $tanggal
 * @property integer $user_id
 * @property string $status
 * @property integer $total_type
 */
class daily_stock extends Model
{
    // use SoftDeletes;

    public $table = 'daily_stocks';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'no_transaksi',
        'tanggal',
        'user_id',
        'status',
        'total_type',
        'total_qty',
        'need_to_update'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'no_transaksi' => 'string',
        'tanggal' => 'date',
        'user_id' => 'integer',
        'status' => 'string',
        'total_type' => 'integer',
        'total_qty' => 'integer',
        'need_to_update' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function user(){
        return $this->belongsTo(\App\User::class, 'user_id');
    }
    
}
