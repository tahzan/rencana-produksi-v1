<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class jadwal_mesin
 * @package App\Models
 * @version January 18, 2020, 10:37 am UTC
 *
 * @property string mulai
 * @property string selesai
 * @property int perencanaan_id
 * @property int mesin_id
 * @property string tipe
 */
class jadwal_mesin extends Model
{
    use SoftDeletes;

    public $table = 'jadwal_mesins';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'mulai',
        'selesai',
        'perencanaan_id',
        'mesin_id',
        'tipe',
        'durasi',
        'auto_generated'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'mulai' => 'datetime',
        'selesai' => 'datetime',
        'tipe' => 'string',
        'mesin_id' => 'integer',
        'perencanaan_id'=>'integer',
        'durasi'=>'integer',
        'auto_generated' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function mesin()
    {
        return $this->belongsTo(\App\Models\mesin::class, 'mesin_id');
    }

    public function rencana_produksi()
    {
        return $this->belongsTo(\App\Models\rencana_produksi::class, 'perencanaan_id');
    }
    
}
