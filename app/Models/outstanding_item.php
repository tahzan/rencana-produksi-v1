<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class outstanding_item
 * @package App\Models
 * @version September 11, 2022, 10:14 pm WIB
 *
 * @property integer $outstanding_id
 * @property integer $qty
 * @property string $customer
 * @property integer $type_id
 * @property string $status
 */
class outstanding_item extends Model
{
    // use SoftDeletes;

    public $table = 'outstanding_items';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'outstanding_id',
        'qty',
        'customer',
        'type_id',
        'status',
        'error'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'outstanding_id' => 'integer',
        'qty' => 'integer',
        'customer' => 'string',
        'type_id' => 'integer',
        'status' => 'string',
        'error' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function type(){
        return $this->belongsTo(\App\Models\tipe::class, 'type_id');
    }

    public function outstanding(){
        return $this->belongsTo(\App\Models\outstanding::class, 'outstanding_id');
    }

    
}
