<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class list_shift
 * @package App\Models
 * @version September 25, 2021, 8:40 am WIB
 *
 * @property integer shift
 * @property string mulai
 * @property string selesai
 * @property string cross_day
 */
class list_shift extends Model
{
    use SoftDeletes;

    public $table = 'list_shifts';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'shift',
        'mulai',
        'selesai',
        'cross_day',
        'shift_category'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'shift' => 'integer',
        'mulai' => 'string',
        'selesai' => 'string',
        'cross_day' => 'string',
        'shift_category' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
