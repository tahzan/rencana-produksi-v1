<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class daily_stock_item
 * @package App\Models
 * @version September 11, 2022, 12:55 pm WIB
 *
 * @property integer $daily_stock_id
 * @property integer $type_id
 * @property integer $stock
 */
class daily_stock_item extends Model
{
    // use SoftDeletes;

    public $table = 'daily_stock_items';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'daily_stock_id',
        'type_id',
        'stock',
        'error'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'daily_stock_id' => 'integer',
        'type_id' => 'integer',
        'stock' => 'integer',
        'error' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function type(){
        return $this->belongsTo(\App\Models\tipe::class, 'type_id');
    }
    
}
