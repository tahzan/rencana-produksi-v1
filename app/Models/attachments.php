<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class attachments
 * @package App\Models
 * @version February 26, 2023, 9:55 am WIB
 *
 * @property integer $rencana_produksi_id
 * @property string $name
 */
class attachments extends Model
{
    use SoftDeletes;

    public $table = 'attachments';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'rencana_produksi_id',
        'shift_id',
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'rencana_produksi_id' => 'integer',
        'shift_id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
