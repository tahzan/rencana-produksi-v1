<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class report_daily_stock
 * @package App\Models
 * @version September 8, 2022, 11:27 pm WIB
 *
 * @property integer $type_id
 * @property integer $daily_stok
 * @property integer $out_standing
 * @property integer $sum
 * @property integer $ppic_id
 * @property string $tanggal_mulai
 * @property string $status
 * @property string $tanggal_modif
 */
class report_daily_stock extends Model
{
    // use SoftDeletes;

    public $table = 'report_daily_stocks';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'type_id',
        'daily_stok',
        'out_standing',
        'sum',
        'ppic_id',
        'tanggal_mulai',
        'status',
        'tanggal_modif',
        'tanggal',
        'no_transaksi',
        'outstanding_id',
        'daily_stock_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'type_id' => 'integer',
        'daily_stok' => 'integer',
        'out_standing' => 'integer',
        'sum' => 'integer',
        'ppic_id' => 'integer',
        'tanggal_mulai' => 'date',
        'status' => 'string',
        'tanggal_modif' => 'date',
        'tanggal' => 'date',
        'no_transaksi' => 'string',
        'outstanding_id' => 'integer',
        'daily_stock_id' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function type(){
        return $this->belongsTo(\App\Models\tipe::class, 'type_id');
    }

    public function ppic(){
        return $this->belongsTo(\App\Models\ppic::class, 'ppic_id');
    }

    public function outstandingobj(){
        return $this->belongsTo(\App\Models\daily_stock::class, 'outstanding_id');
    }

    public function dailystockobj(){
        return $this->belongsTo(\App\Models\outstanding::class, 'daily_stock_id');
    }

    
}
