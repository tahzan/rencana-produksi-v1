<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class rencana_produksi
 * @package App\Models
 * @version January 11, 2020, 9:37 am UTC
 *
 * @property string nomor_transaksi OK
 * @property integer shift ok
 * @property integer mesin_id ok
 * @property string urutan ok
 * @property integer type_id ok /name
 * @property string nama
 * @property integer jumlah_operator ok
 * @property integer cycle_time ok
 * @property integer durasi_dandon ok
 * @property integer toleransi ok
 * @property integer durasi_produksi ok
 * @property string type_pasangan ok
 * @property integer jumlah_rencana_produksi ok
 * @property integer estimasi_durasi ok
 * @property number estimasi_total_shift ok
 * @property integer jumlah_aktual_produksi ok
 * @property number persentasi_aktual
 * @property number persentasi_cycle_time
 * @property datetime mulai ok
 * @property datetime selesai ok
 */
class rencana_produksi extends Model {
    use SoftDeletes;

    public $table = 'rencana_produksis';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'nomor_transaksi',
        'shift',
        'shift_selesai',
        'mesin_id',
        'urutan',
        'type_id',
        'nama',
        'jumlah_operator',
        'cycle_time',
        'durasi_dandon',
        'toleransi',
        'durasi_produksi',
        'type_pasangan',
        'jumlah_rencana_produksi',
        'estimasi_durasi',
        'estimasi_total_shift',
        'jumlah_aktual_produksi',
        'persentasi_aktual',
        'persentasi_cycle_time',
        'mulai',
        'selesai',
        'status',
        'barang_lain',
        'satuan',
        'operator',
        'berat_ng_produksi',
        'berat_set_awal',
        'target_settings',
        'berat_per_pcs',
        'last_hint',
        'note',
        'urutan_booking',
        'shift_category'
    ];

    protected $casts = [
        'id' => 'integer',
        'nomor_transaksi' => 'string',
        'shift' => 'integer',
        'shift_selesai' => 'integer',
        'mesin_id' => 'integer',
        'urutan' => 'string',
        'type_id' => 'integer',
        'nama' => 'string',
        'jumlah_operator' => 'integer',
        'cycle_time' => 'float',
        'durasi_dandon' => 'integer',
        'toleransi' => 'integer',
        'durasi_produksi' => 'integer',
        'type_pasangan' => 'string',
        'jumlah_rencana_produksi' => 'integer',
        'estimasi_durasi' => 'string',
        'estimasi_total_shift' => 'float',
        'jumlah_aktual_produksi' => 'integer',
        'persentasi_aktual' => 'float',
        'persentasi_cycle_time' => 'float',
        'mulai' => 'datetime',
        'selesai' => 'datetime',
        'status' => 'string',
        'barang_lain' => 'string',
        'satuan' => 'string',
        'operator' => 'string',
        'berat_ng_produksi' => 'float',
        'berat_set_awal' => 'float',
        'target_settings' => 'float',
        'berat_per_pcs' => 'string',
        'last_hint' => 'datetime',
        'note' => 'string',
        'urutan_booking' => 'string',
        'shift_category' => 'string'
    ];


    public static $rules = [
        
    ];

    public function mesin()
    {
        return $this->belongsTo(\App\Models\mesin::class, 'mesin_id');
    }

    
}
