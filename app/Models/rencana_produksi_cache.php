<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class rencana_produksi_cache
 * @package App\Models
 * @version January 17, 2020, 3:04 pm UTC
 *
 * @property string nomor_transaksi
 * @property string shift
 * @property string mesin_id
 * @property  urutan
 * @property string type_id
 * @property string nama
 * @property string jumlah_operator
 * @property string cycle_time
 * @property string durasi_dandon
 * @property string toleransi
 * @property string durasi_produksi
 * @property string type_pasangan:nullable
 * @property string jumlah_rencana_produksi
 * @property string estimasi_durasi
 * @property string estimasi_total_shift
 * @property string jumlah_aktual_produksi
 * @property number persentasi_aktual
 * @property number persentasi_cycle_time
 * @property string rencana_proudksi_id
 */
class rencana_produksi_cache extends Model {
    use SoftDeletes;

    public $table = 'rencana_produksi_caches';
    
    protected $dates = ['deleted_at'];

    public $fillable = [
        'nomor_transaksi',
        'shift',
        'mesin_id',
        'urutan',
        'type_id',
        'nama',
        'jumlah_operator',
        'cycle_time',
        'durasi_dandon',
        'toleransi',
        'durasi_produksi',
        'type_pasangan:nullable',
        'jumlah_rencana_produksi',
        'estimasi_durasi',
        'estimasi_total_shift',
        'jumlah_aktual_produksi',
        'persentasi_aktual',
        'persentasi_cycle_time',
        'rencana_proudksi_id',
        'mulai',
        'selesai',
        'status',
        'user_id',
        'satuan',
        'shift_category'
    ];

    protected $casts = [
        'id' => 'integer',
        'nomor_transaksi' => 'string',
        'shift' => 'string',
        'mesin_id' => 'string',
        'type_id' => 'string',
        'nama' => 'string',
        'jumlah_operator' => 'string',
        'cycle_time' => 'string',
        'durasi_dandon' => 'string',
        'toleransi' => 'string',
        'durasi_produksi' => 'string',
        'type_pasangan:nullable' => 'string',
        'jumlah_rencana_produksi' => 'string',
        'estimasi_durasi' => 'string',
        'estimasi_total_shift' => 'string',
        'jumlah_aktual_produksi' => 'string',
        'persentasi_aktual' => 'float',
        'persentasi_cycle_time' => 'float',
        'rencana_proudksi_id' => 'string',
        'mulai' => 'datetime',
        'selesai' => 'datetime',
        'status' => 'string',
        'user_id' => 'integer',
        'satuan' => 'string',
        'shift_category' => 'string'
    ];

    public static $rules = [
        
    ];

    public function mesin(){
        return $this->belongsTo(\App\Models\mesin::class, 'mesin_id');
    }

    public function user(){
        return $this->belongsTo(\App\User::class, 'user_id');
    }

    
}
