<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ppic
 * @package App\Models
 * @version September 8, 2022, 11:21 pm WIB
 *
 * @property string $tanggal
 * @property integer $type_id
 * @property integer $outstanding_stok
 * @property string $status
 * @property string $tanggal_produksi
 * @property string $keterangan
 */
class ppic extends Model
{
    use SoftDeletes;

    public $table = 'ppics';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'tanggal',
        'type_id',
        'outstanding_stok',
        'status',
        'tanggal_produksi',
        'keterangan',
        'no_transaksi',
        'notify_purchasing',
        'last_update'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'tanggal' => 'date',
        'type_id' => 'integer',
        'outstanding_stok' => 'integer',
        'status' => 'string',
        'tanggal_produksi' => 'date',
        'keterangan' => 'string',
        'no_transaksi' => 'string',
        'notify_purchasing' => 'string',
        'ppic' => 'datetime',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function type(){
        return $this->belongsTo(\App\Models\tipe::class, 'type_id');
    }

    // public function ppic(){
    //     return $this->belongsTo(\App\Models\tipe::class, 'ppic_id');
    // }

    
}
