<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class info
 * @package App\Models
 * @version August 8, 2021, 12:07 pm WIB
 *
 * @property string info
 */
class info extends Model
{
    use SoftDeletes;

    public $table = 'infos';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'info',
        'title',
        'start_date',
        'end_date',
        'is_active',
        'file'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'info' => 'string',
        'file' => 'string',
        'start_date' => 'datetime',
        'end_date' => 'datetime',
        'is_active' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
