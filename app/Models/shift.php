<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class shift
 * @package App\Models
 * @version January 25, 2020, 10:14 am UTC
 *
 * @property  tanggal:nullable
 * @property integer shift
 * @property integer durasi
 * @property integer estimasi_hasil
 * @property integer aktual_hasil
 * @property integer rencana_produksi_id
 */
class shift extends Model
{
    use SoftDeletes;

    public $table = 'shifts';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'tanggal',
        'shift',
        'mulai',
        'selesai',
        'durasi',
        'estimasi_hasil',
        'aktual_hasil',
        'rencana_produksi_id',
        'gangguan__',
        'gangguan_sett',
        'gangguan_mld',
        'gangguan_bhn',
        'note',
        'operator',
        'berat_ng_produksi',
        'berat_set_awal',
        'target_settings',
        'shift_category'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'tanggal' => 'date',
        'mulai' => 'datetime',
        'selesai' => 'datetime',
        'shift' => 'integer',
        'durasi' => 'integer',
        'estimasi_hasil' => 'integer',
        'aktual_hasil' => 'integer',
        'rencana_produksi_id' => 'integer',
        'gangguan__'=> 'integer',
        'gangguan_sett'=> 'integer',
        'gangguan_mld'=> 'integer',
        'gangguan_bhn'=> 'integer',
        'note'=> 'string',
        'operator' => 'string',
        'berat_ng_produksi' => 'float',
        'berat_set_awal' => 'float',
        'target_settings' => 'float',
        'shift_category' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function rencana_produksi()
    {
        return $this->belongsTo(\App\Models\rencana_produksi::class, 'rencana_produksi_id');
    }

    
}
