<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class mesin
 * @package App\Models
 * @version January 10, 2020, 11:12 pm UTC
 *
 * @property string kode
 * @property string nama
 */
class mesin extends Model
{
    use SoftDeletes;

    public $table = 'mesins';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'kode',
        'nama',
        'tipe',
        'rusak',
        'button_id',
        'shift_category',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'kode' => 'string',
        'nama' => 'string',
        'tipe' => 'string',
        'rusak' => 'string',
        'button_id' => 'string',
        'shift_category' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
