<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class tipe
 * @package App\Models
 * @version January 11, 2020, 7:00 am UTC
 *
 * @property string nama
 * @property integer jumlah_operator
 * @property integer cycle_time
 * @property integer durasi_dandon
 * @property integer toleransi
 * @property integer durasi_produksi
 * @property string kombinasi
 * @property string barang_lain
 * @property string type_pasangan
 */
class tipe extends Model
{
    use SoftDeletes;

    public $table = 'tipes';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'nama',
        'jumlah_operator',
        'cycle_time',
        'durasi_dandon',
        'toleransi',
        'durasi_produksi',
        'kombinasi',
        'type_pasangan',
        'barang_lain',
        'barang_lain_id',
        'berat',
        'count'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nama' => 'string',
        'jumlah_operator' => 'integer',
        'cycle_time' => 'float',
        'durasi_dandon' => 'integer',
        'toleransi' => 'integer',
        'durasi_produksi' => 'integer',
        'kombinasi' => 'string',
        'barang_lain' => 'string',
        'type_pasangan' => 'string',
        'barang_lain_id' => 'string',
        'berat' => 'float',
        'count' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
