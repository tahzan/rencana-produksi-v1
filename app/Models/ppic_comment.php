<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ppic_comment
 * @package App\Models
 * @version September 8, 2022, 11:26 pm WIB
 *
 * @property integer $ppic_id
 * @property integer $user_id
 * @property string $keterangan
 */
class ppic_comment extends Model
{
    use SoftDeletes;

    public $table = 'ppic_comments';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'ppic_id',
        'user_id',
        'keterangan'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'ppic_id' => 'integer',
        'user_id' => 'integer',
        'keterangan' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];


    public function user(){
        return $this->belongsTo(\App\User::class, 'user_id');
    }

    
}
