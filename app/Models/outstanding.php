<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class outstanding
 * @package App\Models
 * @version September 7, 2022, 10:18 pm WIB
 *
 * @property string $no_transaksi
 * @property string $tanggal
 * @property integer $type_id
 * @property string $keterangan
 * @property integer $stok
 * @property string $deliv_date
 * @property string $status
 */
class outstanding extends Model
{
    // use SoftDeletes;

    public $table = 'outstandings';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'no_transaksi',
        'tanggal',
        'type_id', #gk dipake
        'keterangan',
        'stok', #gk dipake
        'deliv_date', #gk dipake
        'status',
        'user_id',
        'total_type',
        'total_qty'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'no_transaksi' => 'string',
        'tanggal' => 'date',
        'type_id' => 'integer', #gk dipake
        'keterangan' => 'string',
        'stok' => 'integer', #gk dipake
        'deliv_date' => 'date', #gk dipake
        'status' => 'string', 
        'user_id' => 'integer',
        'total_type' => 'integer',
        'total_qty' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function type(){
        return $this->belongsTo(\App\Models\tipe::class, 'type_id');
    }

    public function user(){
        return $this->belongsTo(\App\User::class, 'user_id');
    }

    
}
