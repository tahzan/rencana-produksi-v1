<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ppic_log
 * @package App\Models
 * @version September 8, 2022, 11:23 pm WIB
 *
 * @property integer $ppic_id
 * @property string $tanggal
 * @property integer $type_id
 * @property integer $outstanding_stok
 * @property string $status
 * @property string $tanggal_produksi
 * @property string $keterangan
 */
class ppic_log extends Model
{
    use SoftDeletes;

    public $table = 'ppic_logs';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'ppic_id',
        'tanggal',
        'type_id',
        'outstanding_stok',
        'status',
        'tanggal_produksi',
        'keterangan',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'ppic_id' => 'integer',
        'tanggal' => 'date',
        'type_id' => 'integer',
        'outstanding_stok' => 'integer',
        'status' => 'string',
        'tanggal_produksi' => 'date',
        'keterangan' => 'string',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];


    public function type(){
        return $this->belongsTo(\App\Models\tipe::class, 'type_id');
    }

    public function ppic(){
        return $this->belongsTo(\App\Models\tipe::class, 'ppic_id');
    }

    public function user(){
        return $this->belongsTo(\App\User::class, 'user_id');
    }

    
}
