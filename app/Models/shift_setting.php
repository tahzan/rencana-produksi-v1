<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class shift_setting
 * @package App\Models
 * @version July 15, 2020, 7:15 am WIB
 *
 * @property string tanggal
 * @property integer shift_1
 * @property integer shift_2
 * @property integer shift_3
 */
class shift_setting extends Model
{
    use SoftDeletes;

    public $table = 'shift_settings';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'tanggal',
        'shift_1',
        'shift_2',
        'shift_3',
        'shift_category',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'tanggal' => 'date',
        'shift_1' => 'integer',
        'shift_2' => 'integer',
        'shift_3' => 'integer',
        'shift_category' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
