<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class notification
 * @package App\Models
 * @version September 11, 2022, 8:32 pm WIB
 *
 * @property string $departement
 * @property string $tanggal
 * @property string $no_transc
 * @property string $status
 */
class notification extends Model
{
    use SoftDeletes;

    public $table = 'notifications';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'departement',
        'tanggal',
        'no_transc',
        'status',
        'ppic_id',
        'is_read',
        'message',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'departement' => 'string',
        'tanggal' => 'date',
        'no_transc' => 'string',
        'status' => 'string',
        'ppic_id' => 'string',
        'is_read' => 'string',
        'message' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
