<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('users')->insert([
            [
                'name' => 'admin',
                'email' => 'admin@gmail.com',
                'password' => Hash::make('admin1234'),
                'username' =>'admin',
                'jabatan' => 'supervisor'
            ],
            [
                'name' => 'PPIC',
                'email' => 'ppic@gmail.com',
                'password' => Hash::make('ppic1234'),
                'username' =>'ppic',
                'jabatan' => 'ppic'
            ],
            [
                'name' => 'Sales Team',
                'email' => 'sales@gmail.com',
                'password' => Hash::make('sales1234'),
                'username' =>'sales',
                'jabatan' => 'sales'
            ],
            [
                'name' => 'Purchasing Team',
                'email' => 'purchasing@gmail.com',
                'password' => Hash::make('purchasing1234'),
                'username' =>'purchasing',
                'jabatan' => 'purchasing'
            ]
        ]);
    }
}
