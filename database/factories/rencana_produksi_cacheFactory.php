<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\rencana_produksi_cache;
use Faker\Generator as Faker;

$factory->define(rencana_produksi_cache::class, function (Faker $faker) {

    return [
        'nomor_transaksi' => $faker->word,
        'tanggal' => $faker->word,
        'shift' => $faker->word,
        'mesin_id' => $faker->word,
        'urutan' => $faker->word,
        'type_id' => $faker->word,
        'nama' => $faker->word,
        'jumlah_operator' => $faker->word,
        'cycle_time' => $faker->word,
        'durasi_dandon' => $faker->word,
        'toleransi' => $faker->word,
        'durasi_produksi' => $faker->word,
        'type_pasangan:nullable' => $faker->word,
        'jumlah_rencana_produksi' => $faker->word,
        'estimasi_durasi' => $faker->word,
        'estimasi_total_shift' => $faker->word,
        'jumlah_aktual_produksi' => $faker->word,
        'persentasi_aktual' => $faker->randomDigitNotNull,
        'persentasi_cycle_time' => $faker->randomDigitNotNull,
        'rencana_proudksi_id' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
