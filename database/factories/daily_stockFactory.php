<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\daily_stock;
use Faker\Generator as Faker;

$factory->define(daily_stock::class, function (Faker $faker) {

    return [
        'no_transaksi' => $faker->word,
        'tanggal' => $faker->word,
        'user_id' => $faker->randomDigitNotNull,
        'status' => $faker->randomElement(['draft', 'processed']),
        'total_type' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
