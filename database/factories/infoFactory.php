<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\info;
use Faker\Generator as Faker;

$factory->define(info::class, function (Faker $faker) {

    return [
        'info' => $faker->word,
        'title' => $faker->word,
        'file' => $faker->word,
        'start_date' => $faker->word,
        'end_date' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
