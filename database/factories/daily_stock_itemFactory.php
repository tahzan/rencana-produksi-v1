<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\daily_stock_item;
use Faker\Generator as Faker;

$factory->define(daily_stock_item::class, function (Faker $faker) {

    return [
        'daily_stock_id' => $faker->randomDigitNotNull,
        'type_id' => $faker->randomDigitNotNull,
        'stock' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
