<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\outstanding;
use Faker\Generator as Faker;

$factory->define(outstanding::class, function (Faker $faker) {

    return [
        'no_transaksi' => $faker->word,
        'tanggal' => $faker->word,
        'type_id' => $faker->randomDigitNotNull,
        'keterangan' => $faker->word,
        'stok' => $faker->randomDigitNotNull,
        'deliv_date' => $faker->word,
        'status' => $faker->randomElement(['open', 'delivered']),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
