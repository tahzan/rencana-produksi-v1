<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\shift;
use Faker\Generator as Faker;

$factory->define(shift::class, function (Faker $faker) {

    return [
        'tanggal:nullable' => $faker->word,
        'shift' => $faker->randomDigitNotNull,
        'durasi' => $faker->randomDigitNotNull,
        'estimasi_hasil' => $faker->randomDigitNotNull,
        'aktual_hasil' => $faker->randomDigitNotNull,
        'rencana_produksi_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
