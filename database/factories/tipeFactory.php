<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\tipe;
use Faker\Generator as Faker;

$factory->define(tipe::class, function (Faker $faker) {

    return [
        'nama' => $faker->word,
        'jumlah_operator' => $faker->randomDigitNotNull,
        'cycle_time' => $faker->randomDigitNotNull,
        'durasi_dandon' => $faker->randomDigitNotNull,
        'toleransi' => $faker->randomDigitNotNull,
        'durasi_produksi' => $faker->randomDigitNotNull,
        'kombinasi' => $faker->randomElement(['Ya', 'Tidak']),
        'barang_lain' => $faker->word,
        'type_pasangan' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
