<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ppic_log;
use Faker\Generator as Faker;

$factory->define(ppic_log::class, function (Faker $faker) {

    return [
        'ppic_id' => $faker->randomDigitNotNull,
        'tanggal' => $faker->word,
        'type_id' => $faker->randomDigitNotNull,
        'outstanding_stok' => $faker->randomDigitNotNull,
        'status' => $faker->randomElement(['Waiting Response', 'On Schedule', 'failed', 'closed']),
        'tanggal_produksi' => $faker->word,
        'keterangan' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
