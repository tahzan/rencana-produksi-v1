<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ppic_comment;
use Faker\Generator as Faker;

$factory->define(ppic_comment::class, function (Faker $faker) {

    return [
        'ppic_id' => $faker->randomDigitNotNull,
        'user_id' => $faker->randomDigitNotNull,
        'keterangan' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
