<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\attachments;
use Faker\Generator as Faker;

$factory->define(attachments::class, function (Faker $faker) {

    return [
        'rencana_produksi_id' => $faker->randomDigitNotNull,
        'name' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
