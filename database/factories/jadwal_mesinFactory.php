<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\jadwal_mesin;
use Faker\Generator as Faker;

$factory->define(jadwal_mesin::class, function (Faker $faker) {

    return [
        'mulai' => $faker->word,
        'selesai' => $faker->word,
        'perencanaan_id' => $faker->word,
        'mesin_id' => $faker->word,
        'tipe' => $faker->randomElement(['dandon', 'istirahat', 'kerja']),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
