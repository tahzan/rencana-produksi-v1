<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\rencana_produksi;
use Faker\Generator as Faker;

$factory->define(rencana_produksi::class, function (Faker $faker) {

    return [
        'nomor_transaksi' => $faker->word,
        'tanggal' => $faker->word,
        'shift' => $faker->randomDigitNotNull,
        'mesin_id' => $faker->randomDigitNotNull,
        'urutan' => $faker->word,
        'type_id' => $faker->randomDigitNotNull,
        'nama' => $faker->word,
        'jumlah_operator' => $faker->randomDigitNotNull,
        'cycle_time' => $faker->randomDigitNotNull,
        'durasi_dandon' => $faker->randomDigitNotNull,
        'toleransi' => $faker->randomDigitNotNull,
        'durasi_produksi' => $faker->randomDigitNotNull,
        'type_pasangan:nullable' => $faker->word,
        'jumlah_rencana_produksi' => $faker->randomDigitNotNull,
        'estimasi_durasi' => $faker->randomDigitNotNull,
        'estimasi_total_shift' => $faker->randomDigitNotNull,
        'umlah_aktual_produksi' => $faker->randomDigitNotNull,
        'persentasi_aktual' => $faker->randomDigitNotNull,
        'persentasi_cycle_time' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
