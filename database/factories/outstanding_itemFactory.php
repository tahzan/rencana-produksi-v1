<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\outstanding_item;
use Faker\Generator as Faker;

$factory->define(outstanding_item::class, function (Faker $faker) {

    return [
        'outstanding_id' => $faker->randomDigitNotNull,
        'qty' => $faker->randomDigitNotNull,
        'customer' => $faker->word,
        'type_id' => $faker->randomDigitNotNull,
        'status' => $faker->randomElement(['open', 'delivered']),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
