<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\list_shift;
use Faker\Generator as Faker;

$factory->define(list_shift::class, function (Faker $faker) {

    return [
        'shift' => $faker->randomDigitNotNull,
        'mulai' => $faker->word,
        'selesai' => $faker->word,
        'cross_day' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
