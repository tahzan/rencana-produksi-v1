<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\report_daily_stock;
use Faker\Generator as Faker;

$factory->define(report_daily_stock::class, function (Faker $faker) {

    return [
        'type_id' => $faker->randomDigitNotNull,
        'daily_stok' => $faker->randomDigitNotNull,
        'out_standing' => $faker->randomDigitNotNull,
        'sum' => $faker->randomDigitNotNull,
        'ppic_id' => $faker->randomDigitNotNull,
        'tanggal_mulai' => $faker->word,
        'status' => $faker->randomElement(['Waiting Response', 'On Schedule', 'failed', 'closed']),
        'tanggal_modif' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
