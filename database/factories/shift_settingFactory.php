<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\shift_setting;
use Faker\Generator as Faker;

$factory->define(shift_setting::class, function (Faker $faker) {

    return [
        'tanggal' => $faker->word,
        'shift_1' => $faker->randomDigitNotNull,
        'shift_2' => $faker->randomDigitNotNull,
        'shift_3' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
