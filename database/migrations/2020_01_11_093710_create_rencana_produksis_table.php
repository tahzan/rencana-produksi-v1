<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRencanaProduksisTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rencana_produksis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nomor_transaksi')->nullable();
            $table->integer('shift');
            $table->integer('mesin_id');
            $table->string('urutan');
            $table->integer('type_id');
            $table->string('nama')->nullable();
            $table->integer('jumlah_operator')->nullable();
            $table->decimal('cycle_time', 8,2)->nullable();
            $table->integer('durasi_dandon')->nullable();
            $table->integer('toleransi')->nullable();
            $table->integer('durasi_produksi')->nullable();
            $table->string('type_pasangan')->nullable();
            $table->integer('jumlah_rencana_produksi');
            $table->string('estimasi_durasi');
            $table->float('estimasi_total_shift');
            $table->integer('jumlah_aktual_produksi')->nullable();
            $table->float('persentasi_aktual')->nullable();
            $table->float('persentasi_cycle_time')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rencana_produksis');
    }
}
