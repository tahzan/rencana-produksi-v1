<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRencanaProduksiCachesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rencana_produksi_caches', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nomor_transaksi')->nullable();
            $table->string('shift')->nullable();
            $table->string('mesin_id')->nullable();
            $table->string('urutan')->nullable();
            $table->string('type_id')->nullable();
            $table->string('nama')->nullable();
            $table->string('jumlah_operator')->nullable();
            $table->string('cycle_time')->nullable();
            $table->string('durasi_dandon')->nullable();
            $table->string('toleransi')->nullable();
            $table->string('durasi_produksi')->nullable();
            $table->string('type_pasangan:nullable')->nullable();
            $table->string('jumlah_rencana_produksi')->nullable();
            $table->string('estimasi_durasi')->nullable();
            $table->string('estimasi_total_shift')->nullable();
            $table->string('jumlah_aktual_produksi')->nullable();
            $table->float('persentasi_aktual')->nullable();
            $table->float('persentasi_cycle_time')->nullable();
            $table->string('rencana_proudksi_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rencana_produksi_caches');
    }
}
