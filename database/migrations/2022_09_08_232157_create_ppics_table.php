<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePpicsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ppics', function (Blueprint $table) {
            $table->increments('id');
            $table->date('tanggal');
            $table->integer('type_id');
            $table->integer('outstanding_stok');
            $table->string('status'); //Waiting Response, On Scheule, Failed, Closed
            $table->date('tanggal_produksi');
            $table->string('keterangan');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ppics');
    }
}
