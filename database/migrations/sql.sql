ALTER TABLE list_shifts
ADD COLUMN shift_category VARCHAR(255) DEFAULT '1' NULL;

ALTER TABLE rencana_produksis
ADD COLUMN shift_category VARCHAR(255) DEFAULT '1' NULL;

ALTER TABLE shift_settings
ADD COLUMN shift_category VARCHAR(255) DEFAULT '1' NULL;

ALTER TABLE shifts
ADD COLUMN shift_category VARCHAR(255) DEFAULT '1' NULL;

ALTER TABLE mesins
ADD COLUMN shift_category VARCHAR(255) DEFAULT '1' NULL;

ALTER TABLE rencana_produksi_caches
ADD COLUMN shift_category VARCHAR(255) DEFAULT '1' NULL;
