<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateShiftSettingsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shift_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->date('tanggal');
            $table->integer('shift_1')->nullable();
            $table->integer('shift_2')->nullable();
            $table->integer('shift_3')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shift_settings');
    }
}
