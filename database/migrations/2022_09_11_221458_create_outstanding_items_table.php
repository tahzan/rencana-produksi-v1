<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOutstandingItemsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outstanding_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('outstanding_id');
            $table->integer('qty');
            $table->string('customer');
            $table->integer('type_id');
            $table->enum('status', ['open', 'delivered']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('outstanding_items');
    }
}
