<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTipesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->integer('jumlah_operator');
            $table->decimal('cycle_time', 8,2);
            $table->integer('durasi_dandon');
            $table->integer('toleransi');
            $table->integer('durasi_produksi')->nullable();
            $table->enum('kombinasi', ['Ya', 'Tidak']);
            $table->string('barang_lain')->nullable();
            $table->string('type_pasangan')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tipes');
    }
}
