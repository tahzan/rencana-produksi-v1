<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportDailyStocksTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_daily_stocks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type_id');
            $table->integer('daily_stok');
            $table->integer('out_standing');
            $table->integer('sum');
            $table->integer('ppic_id');
            $table->date('tanggal_mulai');
            $table->string('status');  //Waiting Response, On Scheule, Failed, Closed
            $table->date('tanggal_modif');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('report_daily_stocks');
    }
}
