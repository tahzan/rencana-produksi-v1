<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateShiftsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shifts', function (Blueprint $table) {
            $table->increments('id');
            $table->date('tanggal')->nullable();;
            $table->integer('shift')->nullable();
            $table->integer('durasi')->nullable();
            $table->integer('estimasi_hasil')->nullable();
            $table->integer('aktual_hasil')->nullable();
            $table->integer('rencana_produksi_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shifts');
    }
}
