<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToRencanaproduksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rencana_produksis', function (Blueprint $table) {
            $table->string('operator')->nullable();
            $table->decimal('berat_ng_produksi')->nullable();
            $table->decimal('berat_set_awal')->nullable();
            $table->decimal('target_settings')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rencana_produksis', function (Blueprint $table) {
            //
        });
    }
}
