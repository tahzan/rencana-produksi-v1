<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOutstandingsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outstandings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_transaksi');
            $table->date('tanggal');
            $table->integer('type_id');
            $table->string('keterangan');
            $table->integer('stok');
            $table->date('deliv_date')->nullable();
            $table->string('status'); //draft processed
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('outstandings');
    }
}
