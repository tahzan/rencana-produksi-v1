<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJadwalMesinsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwal_mesins', function (Blueprint $table) {
            $table->increments('id');
            $table->datetime('mulai');
            $table->datetime('selesai');
            $table->integer('perencanaan_id')->nullable();
            $table->integer('mesin_id')->nullable();
            $table->enum('tipe', ['dandon', 'kerja', 'istirahat']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('jadwal_mesins');
    }
}
