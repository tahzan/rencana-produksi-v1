<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToRencanaproduksis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shifts', function (Blueprint $table) {
            $table->integer('gangguan__')->nullable();
            $table->integer('gangguan_sett')->nullable();
            $table->integer('gangguan_mld')->nullable();
            $table->integer('gangguan_bhn')->nullable();
        });

        Schema::table('rencana_produksis', function (Blueprint $table) {
            $table->decimal('berat_per_pcs', 6,2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rencanaproduksis', function (Blueprint $table) {
            //
        });
    }
}
